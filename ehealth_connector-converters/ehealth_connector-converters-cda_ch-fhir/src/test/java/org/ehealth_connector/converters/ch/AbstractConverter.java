/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.converters.ch;

import java.net.URL;

/**
 *
 * @author roeland
 * @version 1.0
 * @since Jan 2, 2018 1:25:16 PM
 *
 */
public abstract class AbstractConverter {
	protected String styleSheet = "xsl/cda-ch.xsl";
	protected String css = "xsl/cda-ch.css";

	protected String testFile;

	public void setUpTestFile(String file) {
		final URL url = this.getClass().getResource(file);
		testFile = url.getFile();
	}
}
