/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.cda.ch.vacd.v210;

/**
 * Original ART-DECOR template id: 2.16.756.5.30.1.1.10.9.20 Template
 * description: Reusable template wherever a playing entity with a required name
 * is used in a CDA-CH V2 document. CDA-CH V2 derivatives, i.e. Swiss exchange
 * formats MAY use this template by either reference or specialisation.
 *
 * Element description: The playing entity's name.
 */
public class CdachOtherPlayingEntityCompilationWithName
		extends org.ehealth_connector.common.hl7cdar2.PN {
}
