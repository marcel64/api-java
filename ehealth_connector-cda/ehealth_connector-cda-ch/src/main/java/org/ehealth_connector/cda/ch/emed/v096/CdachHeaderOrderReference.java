/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.cda.ch.emed.v096;

import org.ehealth_connector.common.hl7cdar2.ObjectFactory;

/**
 * Original ART-DECOR template id: 2.16.756.5.30.1.1.10.2.16 Template
 * description: Reference to one or more orders which led to the creation of
 * this CDA document. It SHALL be declared, when the order reference is relevant
 * for some reason. All CDA-CH V2 derivatives, i.e. Swiss exchange formats MUST
 * reference this template.
 *
 * Element description: Reference to one or more orders which led to the
 * creation of this CDA document. It SHALL be declared, when the order reference
 * is relevant for some reason.
 */
public class CdachHeaderOrderReference
		extends org.ehealth_connector.common.hl7cdar2.POCDMT000040InFulfillmentOf {

	/**
	 * Creates fixed contents for CDA Element hl7TemplateId
	 *
	 * @param root
	 *            the desired fixed value for this argument.
	 */
	private static org.ehealth_connector.common.hl7cdar2.II createHl7TemplateIdFixedValue(
			String root) {
		ObjectFactory factory = new ObjectFactory();
		org.ehealth_connector.common.hl7cdar2.II retVal = factory.createII();
		retVal.setRoot(root);
		return retVal;
	}

	public CdachHeaderOrderReference() {
		super.getTemplateId().add(createHl7TemplateIdFixedValue("2.16.756.5.30.1.1.10.2.16"));
	}

	/**
	 * Gets the hl7Order
	 */
	public org.ehealth_connector.common.hl7cdar2.POCDMT000040Order getHl7Order() {
		return order;
	}

	/**
	 * Gets the hl7TemplateId
	 */
	public java.util.List<org.ehealth_connector.common.hl7cdar2.II> getHl7TemplateId() {
		return templateId;
	}

	/**
	 * Sets the hl7Order
	 */
	public void setHl7Order(org.ehealth_connector.common.hl7cdar2.POCDMT000040Order value) {
		this.order = value;
	}

	/**
	 * Sets the hl7TemplateId
	 */
	public void setHl7TemplateId(org.ehealth_connector.common.hl7cdar2.II value) {
		getTemplateId().clear();
		getTemplateId().add(value);
	}
}
