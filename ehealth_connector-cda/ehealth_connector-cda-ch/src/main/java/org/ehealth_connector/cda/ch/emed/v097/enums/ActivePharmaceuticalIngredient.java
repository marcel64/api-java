/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.cda.ch.emed.v097.enums;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.ehealth_connector.common.enums.CodeSystems;
import org.ehealth_connector.common.enums.LanguageCode;
import org.ehealth_connector.common.mdht.enums.ValueSetEnumInterface;

/**
 * <!-- @formatter:off -->
 * <div class="en">no designation found for language ENGLISH</div>
 * <div class="de">no designation found for language GERMAN</div>
 * <div class="fr">no designation found for language FRENCH</div>
 * <div class="it">no designation found for language ITALIAN</div>
 * <!-- @formatter:on -->
 */
@Generated(value = "org.ehealth_connector.codegenerator.ch.valuesets.UpdateValueSets", date = "2021-03-02")
public enum ActivePharmaceuticalIngredient implements ValueSetEnumInterface {

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Abacavir</div>
	 * <div class="de">Abacavir</div>
	 * <div class="fr">abacavir</div>
	 * <div class="it">Abacavir</div>
	 * <!-- @formatter:on -->
	 */
	ABACAVIR("387005008", "2.16.840.1.113883.6.96", "Abacavir (substance)", "Abacavir", "Abacavir",
			"abacavir", "Abacavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Abatacept</div>
	 * <div class="de">Abatacept</div>
	 * <div class="fr">abatacept</div>
	 * <div class="it">Abatacept</div>
	 * <!-- @formatter:on -->
	 */
	ABATACEPT("421777009", "2.16.840.1.113883.6.96", "Abatacept (substance)", "Abatacept",
			"Abatacept", "abatacept", "Abatacept"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Abciximab</div>
	 * <div class="de">Abciximab</div>
	 * <div class="fr">abciximab</div>
	 * <div class="it">Abciximab</div>
	 * <!-- @formatter:on -->
	 */
	ABCIXIMAB("386951001", "2.16.840.1.113883.6.96", "Abciximab (substance)", "Abciximab",
			"Abciximab", "abciximab", "Abciximab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Abemaciclib</div>
	 * <div class="de">Abemaciclib</div>
	 * <div class="fr">abémaciclib</div>
	 * <div class="it">Abemaciclib</div>
	 * <!-- @formatter:on -->
	 */
	ABEMACICLIB("761851004", "2.16.840.1.113883.6.96", "Abemaciclib (substance)", "Abemaciclib",
			"Abemaciclib", "abémaciclib", "Abemaciclib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Abiraterone</div>
	 * <div class="de">Abirateron</div>
	 * <div class="fr">abiratérone</div>
	 * <div class="it">Abiraterone</div>
	 * <!-- @formatter:on -->
	 */
	ABIRATERONE("699678007", "2.16.840.1.113883.6.96", "Abiraterone (substance)", "Abiraterone",
			"Abirateron", "abiratérone", "Abiraterone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acamprosate</div>
	 * <div class="de">Acamprosat</div>
	 * <div class="fr">acamprosate</div>
	 * <div class="it">Acamprosato</div>
	 * <!-- @formatter:on -->
	 */
	ACAMPROSATE("391698009", "2.16.840.1.113883.6.96", "Acamprosate (substance)", "Acamprosate",
			"Acamprosat", "acamprosate", "Acamprosato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acarbose</div>
	 * <div class="de">Acarbose</div>
	 * <div class="fr">acarbose</div>
	 * <div class="it">Acarbosio</div>
	 * <!-- @formatter:on -->
	 */
	ACARBOSE("386965004", "2.16.840.1.113883.6.96", "Acarbose (substance)", "Acarbose", "Acarbose",
			"acarbose", "Acarbosio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acemetacin</div>
	 * <div class="de">Acemetacin</div>
	 * <div class="fr">acémétacine</div>
	 * <div class="it">Acemetacina</div>
	 * <!-- @formatter:on -->
	 */
	ACEMETACIN("391704009", "2.16.840.1.113883.6.96", "Acemetacin (substance)", "Acemetacin",
			"Acemetacin", "acémétacine", "Acemetacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acenocoumarol</div>
	 * <div class="de">Acenocoumarol</div>
	 * <div class="fr">acénocoumarol</div>
	 * <div class="it">Acenocumarolo</div>
	 * <!-- @formatter:on -->
	 */
	ACENOCOUMAROL("387457003", "2.16.840.1.113883.6.96", "Acenocoumarol (substance)",
			"Acenocoumarol", "Acenocoumarol", "acénocoumarol", "Acenocumarolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acetazolamide</div>
	 * <div class="de">Acetazolamid</div>
	 * <div class="fr">acétazolamide</div>
	 * <div class="it">Acetazolamide</div>
	 * <!-- @formatter:on -->
	 */
	ACETAZOLAMIDE("372709008", "2.16.840.1.113883.6.96", "Acetazolamide (substance)",
			"Acetazolamide", "Acetazolamid", "acétazolamide", "Acetazolamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acetylcysteine</div>
	 * <div class="de">Acetylcystein</div>
	 * <div class="fr">acétylcystéine</div>
	 * <div class="it">Acetilcisteina</div>
	 * <!-- @formatter:on -->
	 */
	ACETYLCYSTEINE("387440002", "2.16.840.1.113883.6.96", "Acetylcysteine (substance)",
			"Acetylcysteine", "Acetylcystein", "acétylcystéine", "Acetilcisteina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aciclovir</div>
	 * <div class="de">Aciclovir</div>
	 * <div class="fr">aciclovir</div>
	 * <div class="it">Aciclovir</div>
	 * <!-- @formatter:on -->
	 */
	ACICLOVIR("372729009", "2.16.840.1.113883.6.96", "Aciclovir (substance)", "Aciclovir",
			"Aciclovir", "aciclovir", "Aciclovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acipimox</div>
	 * <div class="de">Acipimox</div>
	 * <div class="fr">acipimox</div>
	 * <div class="it">Acipimox</div>
	 * <!-- @formatter:on -->
	 */
	ACIPIMOX("391711008", "2.16.840.1.113883.6.96", "Acipimox (substance)", "Acipimox", "Acipimox",
			"acipimox", "Acipimox"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Acitretin</div>
	 * <div class="de">Acitretin</div>
	 * <div class="fr">acitrétine</div>
	 * <div class="it">Acitretina</div>
	 * <!-- @formatter:on -->
	 */
	ACITRETIN("386938006", "2.16.840.1.113883.6.96", "Acitretin (substance)", "Acitretin",
			"Acitretin", "acitrétine", "Acitretina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aclidinium</div>
	 * <div class="de">Aclidinium-Kation</div>
	 * <div class="fr">aclidinium</div>
	 * <div class="it">Aclidinio</div>
	 * <!-- @formatter:on -->
	 */
	ACLIDINIUM("703921008", "2.16.840.1.113883.6.96", "Aclidinium (substance)", "Aclidinium",
			"Aclidinium-Kation", "aclidinium", "Aclidinio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Activated charcoal</div>
	 * <div class="de">Kohle, medizinische</div>
	 * <div class="fr">charbon activé</div>
	 * <div class="it">Carbone attivo</div>
	 * <!-- @formatter:on -->
	 */
	ACTIVATED_CHARCOAL("32519007", "2.16.840.1.113883.6.96", "Activated charcoal (substance)",
			"Activated charcoal", "Kohle, medizinische", "charbon activé", "Carbone attivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Adalimumab</div>
	 * <div class="de">Adalimumab</div>
	 * <div class="fr">adalimumab</div>
	 * <div class="it">Adalimumab</div>
	 * <!-- @formatter:on -->
	 */
	ADALIMUMAB("407317001", "2.16.840.1.113883.6.96", "Adalimumab (substance)", "Adalimumab",
			"Adalimumab", "adalimumab", "Adalimumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Adapalene</div>
	 * <div class="de">Adapalen</div>
	 * <div class="fr">adapalène</div>
	 * <div class="it">Adapalene</div>
	 * <!-- @formatter:on -->
	 */
	ADAPALENE("386934008", "2.16.840.1.113883.6.96", "Adapalene (substance)", "Adapalene",
			"Adapalen", "adapalène", "Adapalene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Adefovir</div>
	 * <div class="de">Adefovir</div>
	 * <div class="fr">adéfovir</div>
	 * <div class="it">Adefovir</div>
	 * <!-- @formatter:on -->
	 */
	ADEFOVIR("412072006", "2.16.840.1.113883.6.96", "Adefovir (substance)", "Adefovir", "Adefovir",
			"adéfovir", "Adefovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Adenosine</div>
	 * <div class="de">Adenosin</div>
	 * <div class="fr">adénosine</div>
	 * <div class="it">Adenosina</div>
	 * <!-- @formatter:on -->
	 */
	ADENOSINE("35431001", "2.16.840.1.113883.6.96", "Adenosine (substance)", "Adenosine",
			"Adenosin", "adénosine", "Adenosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Afatinib</div>
	 * <div class="de">Afatinib</div>
	 * <div class="fr">afatinib</div>
	 * <div class="it">Afatinib</div>
	 * <!-- @formatter:on -->
	 */
	AFATINIB("703579002", "2.16.840.1.113883.6.96", "Afatinib (substance)", "Afatinib", "Afatinib",
			"afatinib", "Afatinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aflibercept</div>
	 * <div class="de">Aflibercept</div>
	 * <div class="fr">aflibercept</div>
	 * <div class="it">Aflibercept</div>
	 * <!-- @formatter:on -->
	 */
	AFLIBERCEPT("703840003", "2.16.840.1.113883.6.96", "Aflibercept (substance)", "Aflibercept",
			"Aflibercept", "aflibercept", "Aflibercept"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Agalsidase alfa</div>
	 * <div class="de">Agalsidase alfa</div>
	 * <div class="fr">agalsidase alfa</div>
	 * <div class="it">Agalsidasi alfa</div>
	 * <!-- @formatter:on -->
	 */
	AGALSIDASE_ALFA("424905009", "2.16.840.1.113883.6.96", "Agalsidase alfa (substance)",
			"Agalsidase alfa", "Agalsidase alfa", "agalsidase alfa", "Agalsidasi alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Agalsidase beta</div>
	 * <div class="de">Agalsidase beta</div>
	 * <div class="fr">agalsidase bêta</div>
	 * <div class="it">Agalsidasi beta</div>
	 * <!-- @formatter:on -->
	 */
	AGALSIDASE_BETA("424725004", "2.16.840.1.113883.6.96", "Agalsidase beta (substance)",
			"Agalsidase beta", "Agalsidase beta", "agalsidase bêta", "Agalsidasi beta"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Agomelatine</div>
	 * <div class="de">Agomelatin</div>
	 * <div class="fr">agomélatine</div>
	 * <div class="it">Agomelatina</div>
	 * <!-- @formatter:on -->
	 */
	AGOMELATINE("698012009", "2.16.840.1.113883.6.96", "Agomelatine (substance)", "Agomelatine",
			"Agomelatin", "agomélatine", "Agomelatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alanine</div>
	 * <div class="de">Alanin</div>
	 * <div class="fr">alanine</div>
	 * <div class="it">Alanina</div>
	 * <!-- @formatter:on -->
	 */
	ALANINE("58753009", "2.16.840.1.113883.6.96", "Alanine (substance)", "Alanine", "Alanin",
			"alanine", "Alanina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alanylglutamine</div>
	 * <div class="de">Alanyl glutamin</div>
	 * <div class="fr">alanyl glutamine</div>
	 * <div class="it">Alanil glutammina</div>
	 * <!-- @formatter:on -->
	 */
	ALANYLGLUTAMINE("703391005", "2.16.840.1.113883.6.96", "Alanylglutamine (substance)",
			"Alanylglutamine", "Alanyl glutamin", "alanyl glutamine", "Alanil glutammina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Albendazole</div>
	 * <div class="de">Albendazol</div>
	 * <div class="fr">albendazole</div>
	 * <div class="it">Albendazolo</div>
	 * <!-- @formatter:on -->
	 */
	ALBENDAZOLE("387558006", "2.16.840.1.113883.6.96", "Albendazole (substance)", "Albendazole",
			"Albendazol", "albendazole", "Albendazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Albiglutide</div>
	 * <div class="de">Albiglutid</div>
	 * <div class="fr">albiglutide</div>
	 * <div class="it">Albiglutide</div>
	 * <!-- @formatter:on -->
	 */
	ALBIGLUTIDE("703129009", "2.16.840.1.113883.6.96", "Albiglutide (substance)", "Albiglutide",
			"Albiglutid", "albiglutide", "Albiglutide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Albumin</div>
	 * <div class="de">Albumine</div>
	 * <div class="fr">albumine</div>
	 * <div class="it">Albumina</div>
	 * <!-- @formatter:on -->
	 */
	ALBUMIN("52454007", "2.16.840.1.113883.6.96", "Albumin (substance)", "Albumin", "Albumine",
			"albumine", "Albumina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Albutrepenonacog alfa</div>
	 * <div class="de">Albutrepenonacog alfa</div>
	 * <div class="fr">albutrépénonacog alfa</div>
	 * <div class="it">Albutrepenonacog alfa</div>
	 * <!-- @formatter:on -->
	 */
	ALBUTREPENONACOG_ALFA("718928008", "2.16.840.1.113883.6.96",
			"Albutrepenonacog alfa (substance)", "Albutrepenonacog alfa", "Albutrepenonacog alfa",
			"albutrépénonacog alfa", "Albutrepenonacog alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alcohol</div>
	 * <div class="de">Ethanol</div>
	 * <div class="fr">éthanol</div>
	 * <div class="it">Alcol etilico</div>
	 * <!-- @formatter:on -->
	 */
	ALCOHOL("53041004", "2.16.840.1.113883.6.96", "Alcohol (substance)", "Alcohol", "Ethanol",
			"éthanol", "Alcol etilico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aldesleukin</div>
	 * <div class="de">Aldesleukin</div>
	 * <div class="fr">aldesleukine</div>
	 * <div class="it">Aldesleuchina</div>
	 * <!-- @formatter:on -->
	 */
	ALDESLEUKIN("386917000", "2.16.840.1.113883.6.96", "Aldesleukin (substance)", "Aldesleukin",
			"Aldesleukin", "aldesleukine", "Aldesleuchina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alectinib</div>
	 * <div class="de">Alectinib</div>
	 * <div class="fr">alectinib</div>
	 * <div class="it">Alectinib</div>
	 * <!-- @formatter:on -->
	 */
	ALECTINIB("716039000", "2.16.840.1.113883.6.96", "Alectinib (substance)", "Alectinib",
			"Alectinib", "alectinib", "Alectinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alemtuzumab</div>
	 * <div class="de">Alemtuzumab</div>
	 * <div class="fr">alemtuzumab</div>
	 * <div class="it">Alemtuzumab</div>
	 * <!-- @formatter:on -->
	 */
	ALEMTUZUMAB("129472003", "2.16.840.1.113883.6.96", "Alemtuzumab (substance)", "Alemtuzumab",
			"Alemtuzumab", "alemtuzumab", "Alemtuzumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alendronic acid</div>
	 * <div class="de">Alendronsäure</div>
	 * <div class="fr">acide alendronique</div>
	 * <div class="it">Acido Alendronico</div>
	 * <!-- @formatter:on -->
	 */
	ALENDRONIC_ACID("391730008", "2.16.840.1.113883.6.96", "Alendronic acid (substance)",
			"Alendronic acid", "Alendronsäure", "acide alendronique", "Acido Alendronico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alfentanil</div>
	 * <div class="de">Alfentanil</div>
	 * <div class="fr">alfentanil</div>
	 * <div class="it">Alfentanil</div>
	 * <!-- @formatter:on -->
	 */
	ALFENTANIL("387560008", "2.16.840.1.113883.6.96", "Alfentanil (substance)", "Alfentanil",
			"Alfentanil", "alfentanil", "Alfentanil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alfuzosin</div>
	 * <div class="de">Alfuzosin</div>
	 * <div class="fr">alfuzosine</div>
	 * <div class="it">Alfuzosina</div>
	 * <!-- @formatter:on -->
	 */
	ALFUZOSIN("395954002", "2.16.840.1.113883.6.96", "Alfuzosin (substance)", "Alfuzosin",
			"Alfuzosin", "alfuzosine", "Alfuzosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alirocumab</div>
	 * <div class="de">Alirocumab</div>
	 * <div class="fr">alirocumab</div>
	 * <div class="it">Alirocumab</div>
	 * <!-- @formatter:on -->
	 */
	ALIROCUMAB("715186005", "2.16.840.1.113883.6.96", "Alirocumab (substance)", "Alirocumab",
			"Alirocumab", "alirocumab", "Alirocumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aliskiren</div>
	 * <div class="de">Aliskiren</div>
	 * <div class="fr">aliskirène</div>
	 * <div class="it">Aliskiren</div>
	 * <!-- @formatter:on -->
	 */
	ALISKIREN("426725002", "2.16.840.1.113883.6.96", "Aliskiren (substance)", "Aliskiren",
			"Aliskiren", "aliskirène", "Aliskiren"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Allopurinol</div>
	 * <div class="de">Allopurinol</div>
	 * <div class="fr">allopurinol</div>
	 * <div class="it">Allopurinolo</div>
	 * <!-- @formatter:on -->
	 */
	ALLOPURINOL("387135004", "2.16.840.1.113883.6.96", "Allopurinol (substance)", "Allopurinol",
			"Allopurinol", "allopurinol", "Allopurinolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Almotriptan</div>
	 * <div class="de">Almotriptan</div>
	 * <div class="fr">almotriptan</div>
	 * <div class="it">Almotriptan</div>
	 * <!-- @formatter:on -->
	 */
	ALMOTRIPTAN("363569003", "2.16.840.1.113883.6.96", "Almotriptan (substance)", "Almotriptan",
			"Almotriptan", "almotriptan", "Almotriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alogliptin</div>
	 * <div class="de">Alogliptin</div>
	 * <div class="fr">alogliptine</div>
	 * <div class="it">Alogliptin</div>
	 * <!-- @formatter:on -->
	 */
	ALOGLIPTIN("702799001", "2.16.840.1.113883.6.96", "Alogliptin (substance)", "Alogliptin",
			"Alogliptin", "alogliptine", "Alogliptin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alprazolam</div>
	 * <div class="de">Alprazolam</div>
	 * <div class="fr">alprazolam</div>
	 * <div class="it">Alprazolam</div>
	 * <!-- @formatter:on -->
	 */
	ALPRAZOLAM("386983007", "2.16.840.1.113883.6.96", "Alprazolam (substance)", "Alprazolam",
			"Alprazolam", "alprazolam", "Alprazolam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alprostadil</div>
	 * <div class="de">Alprostadil</div>
	 * <div class="fr">alprostadil</div>
	 * <div class="it">Alprostadil</div>
	 * <!-- @formatter:on -->
	 */
	ALPROSTADIL("48988008", "2.16.840.1.113883.6.96", "Alprostadil (substance)", "Alprostadil",
			"Alprostadil", "alprostadil", "Alprostadil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Alteplase</div>
	 * <div class="de">Alteplase</div>
	 * <div class="fr">altéplase</div>
	 * <div class="it">Alteplase</div>
	 * <!-- @formatter:on -->
	 */
	ALTEPLASE("387152000", "2.16.840.1.113883.6.96", "Alteplase (substance)", "Alteplase",
			"Alteplase", "altéplase", "Alteplase"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aluminium hydroxide</div>
	 * <div class="de">Aluminiumoxid, wasserhaltig (Algeldrat)</div>
	 * <div class="fr">aluminium oxyde hydrate (algeldrate)</div>
	 * <div class="it">Idrossido di alluminio</div>
	 * <!-- @formatter:on -->
	 */
	ALUMINIUM_HYDROXIDE("273944007", "2.16.840.1.113883.6.96", "Aluminium hydroxide (substance)",
			"Aluminium hydroxide", "Aluminiumoxid, wasserhaltig (Algeldrat)",
			"aluminium oxyde hydrate (algeldrate)", "Idrossido di alluminio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amantadine</div>
	 * <div class="de">Amantadin</div>
	 * <div class="fr">amantadine</div>
	 * <div class="it">Amantadina</div>
	 * <!-- @formatter:on -->
	 */
	AMANTADINE("372763006", "2.16.840.1.113883.6.96", "Amantadine (substance)", "Amantadine",
			"Amantadin", "amantadine", "Amantadina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ambrisentan</div>
	 * <div class="de">Ambrisentan</div>
	 * <div class="fr">ambrisentan</div>
	 * <div class="it">Ambrisentan</div>
	 * <!-- @formatter:on -->
	 */
	AMBRISENTAN("428159003", "2.16.840.1.113883.6.96", "Ambrisentan (substance)", "Ambrisentan",
			"Ambrisentan", "ambrisentan", "Ambrisentan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ambroxol</div>
	 * <div class="de">Ambroxol</div>
	 * <div class="fr">ambroxol</div>
	 * <div class="it">Ambroxolo</div>
	 * <!-- @formatter:on -->
	 */
	AMBROXOL("698024002", "2.16.840.1.113883.6.96", "Ambroxol (substance)", "Ambroxol", "Ambroxol",
			"ambroxol", "Ambroxolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amikacin</div>
	 * <div class="de">Amikacin</div>
	 * <div class="fr">amikacine</div>
	 * <div class="it">Amikacina</div>
	 * <!-- @formatter:on -->
	 */
	AMIKACIN("387266001", "2.16.840.1.113883.6.96", "Amikacin (substance)", "Amikacin", "Amikacin",
			"amikacine", "Amikacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amiloride</div>
	 * <div class="de">Amilorid</div>
	 * <div class="fr">amiloride</div>
	 * <div class="it">Amiloride</div>
	 * <!-- @formatter:on -->
	 */
	AMILORIDE("387503008", "2.16.840.1.113883.6.96", "Amiloride (substance)", "Amiloride",
			"Amilorid", "amiloride", "Amiloride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aminophylline</div>
	 * <div class="de">Aminophyllin</div>
	 * <div class="fr">aminophylline</div>
	 * <div class="it">Aminofillina</div>
	 * <!-- @formatter:on -->
	 */
	AMINOPHYLLINE("373508009", "2.16.840.1.113883.6.96", "Aminophylline (substance)",
			"Aminophylline", "Aminophyllin", "aminophylline", "Aminofillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amiodarone</div>
	 * <div class="de">Amiodaron</div>
	 * <div class="fr">amiodarone</div>
	 * <div class="it">Amiodarone</div>
	 * <!-- @formatter:on -->
	 */
	AMIODARONE("372821002", "2.16.840.1.113883.6.96", "Amiodarone (substance)", "Amiodarone",
			"Amiodaron", "amiodarone", "Amiodarone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amisulpride</div>
	 * <div class="de">Amisulprid</div>
	 * <div class="fr">amisulpride</div>
	 * <div class="it">Amisulpride</div>
	 * <!-- @formatter:on -->
	 */
	AMISULPRIDE("391761004", "2.16.840.1.113883.6.96", "Amisulpride (substance)", "Amisulpride",
			"Amisulprid", "amisulpride", "Amisulpride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amitriptyline</div>
	 * <div class="de">Amitriptylin</div>
	 * <div class="fr">amitriptyline</div>
	 * <div class="it">Amitriptilina</div>
	 * <!-- @formatter:on -->
	 */
	AMITRIPTYLINE("372726002", "2.16.840.1.113883.6.96", "Amitriptyline (substance)",
			"Amitriptyline", "Amitriptylin", "amitriptyline", "Amitriptilina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amlodipine</div>
	 * <div class="de">Amlodipin</div>
	 * <div class="fr">amlodipine</div>
	 * <div class="it">Amlodipina</div>
	 * <!-- @formatter:on -->
	 */
	AMLODIPINE("386864001", "2.16.840.1.113883.6.96", "Amlodipine (substance)", "Amlodipine",
			"Amlodipin", "amlodipine", "Amlodipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amorolfine</div>
	 * <div class="de">Amorolfin</div>
	 * <div class="fr">amorolfine</div>
	 * <div class="it">Amorolfina</div>
	 * <!-- @formatter:on -->
	 */
	AMOROLFINE("391769002", "2.16.840.1.113883.6.96", "Amorolfine (substance)", "Amorolfine",
			"Amorolfin", "amorolfine", "Amorolfina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amoxicillin</div>
	 * <div class="de">Amoxicillin</div>
	 * <div class="fr">amoxicilline</div>
	 * <div class="it">Amoxicillina</div>
	 * <!-- @formatter:on -->
	 */
	AMOXICILLIN("372687004", "2.16.840.1.113883.6.96", "Amoxicillin (substance)", "Amoxicillin",
			"Amoxicillin", "amoxicilline", "Amoxicillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amoxicillin sodium</div>
	 * <div class="de">Amoxicillin-Natrium</div>
	 * <div class="fr">amoxicilline sodique</div>
	 * <div class="it">Amoxicillina sodica</div>
	 * <!-- @formatter:on -->
	 */
	AMOXICILLIN_SODIUM("427483001", "2.16.840.1.113883.6.96", "Amoxicillin sodium (substance)",
			"Amoxicillin sodium", "Amoxicillin-Natrium", "amoxicilline sodique",
			"Amoxicillina sodica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amoxicillin trihydrate</div>
	 * <div class="de">Amoxicillin-3-Wasser</div>
	 * <div class="fr">amoxicilline trihydrate</div>
	 * <div class="it">Amoxicillina triidrato</div>
	 * <!-- @formatter:on -->
	 */
	AMOXICILLIN_TRIHYDRATE("96068000", "2.16.840.1.113883.6.96",
			"Amoxicillin trihydrate (substance)", "Amoxicillin trihydrate", "Amoxicillin-3-Wasser",
			"amoxicilline trihydrate", "Amoxicillina triidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amphotericin B</div>
	 * <div class="de">Amphotericin B</div>
	 * <div class="fr">amphotéricine B</div>
	 * <div class="it">Amfotericina B</div>
	 * <!-- @formatter:on -->
	 */
	AMPHOTERICIN_B("77703004", "2.16.840.1.113883.6.96", "Amphotericin B (substance)",
			"Amphotericin B", "Amphotericin B", "amphotéricine B", "Amfotericina B"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ampicillin</div>
	 * <div class="de">Ampicillin</div>
	 * <div class="fr">ampicilline</div>
	 * <div class="it">Ampicillina</div>
	 * <!-- @formatter:on -->
	 */
	AMPICILLIN("387170002", "2.16.840.1.113883.6.96", "Ampicillin (substance)", "Ampicillin",
			"Ampicillin", "ampicilline", "Ampicillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Amylase</div>
	 * <div class="de">Amylase</div>
	 * <div class="fr">amylase</div>
	 * <div class="it">Amilasi</div>
	 * <!-- @formatter:on -->
	 */
	AMYLASE("387031005", "2.16.840.1.113883.6.96", "Amylase (substance)", "Amylase", "Amylase",
			"amylase", "Amilasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Anagrelide</div>
	 * <div class="de">Anagrelid</div>
	 * <div class="fr">anagrélide</div>
	 * <div class="it">Anagrelide</div>
	 * <!-- @formatter:on -->
	 */
	ANAGRELIDE("372561005", "2.16.840.1.113883.6.96", "Anagrelide (substance)", "Anagrelide",
			"Anagrelid", "anagrélide", "Anagrelide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Anakinra</div>
	 * <div class="de">Anakinra</div>
	 * <div class="fr">anakinra</div>
	 * <div class="it">Anakinra</div>
	 * <!-- @formatter:on -->
	 */
	ANAKINRA("385549000", "2.16.840.1.113883.6.96", "Anakinra (substance)", "Anakinra", "Anakinra",
			"anakinra", "Anakinra"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Anastrozole</div>
	 * <div class="de">Anastrozol</div>
	 * <div class="fr">anastrozole</div>
	 * <div class="it">Anastrozolo</div>
	 * <!-- @formatter:on -->
	 */
	ANASTROZOLE("386910003", "2.16.840.1.113883.6.96", "Anastrozole (substance)", "Anastrozole",
			"Anastrozol", "anastrozole", "Anastrozolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Anetholtrithion</div>
	 * <div class="de">Anetholtrithion</div>
	 * <div class="fr">anétholtrithione</div>
	 * <div class="it">Anetoltritione</div>
	 * <!-- @formatter:on -->
	 */
	ANETHOLTRITHION("703112006", "2.16.840.1.113883.6.96", "Anetholtrithion (substance)",
			"Anetholtrithion", "Anetholtrithion", "anétholtrithione", "Anetoltritione"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Anidulafungin</div>
	 * <div class="de">Anidulafungin</div>
	 * <div class="fr">anidulafungine</div>
	 * <div class="it">Anidulafungina</div>
	 * <!-- @formatter:on -->
	 */
	ANIDULAFUNGIN("422157006", "2.16.840.1.113883.6.96", "Anidulafungin (substance)",
			"Anidulafungin", "Anidulafungin", "anidulafungine", "Anidulafungina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Antazoline</div>
	 * <div class="de">Antazolin</div>
	 * <div class="fr">antazoline</div>
	 * <div class="it">Antazolina</div>
	 * <!-- @formatter:on -->
	 */
	ANTAZOLINE("373544004", "2.16.840.1.113883.6.96", "Antazoline (substance)", "Antazoline",
			"Antazolin", "antazoline", "Antazolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Antilymphocyte immunoglobulin</div>
	 * <div class="de">Immunglobuline anti-Lymphozyten human</div>
	 * <div class="fr">immunoglobuline anti-lymphocytes humains</div>
	 * <div class="it">Immunoglobulina anti-linfociti T umani</div>
	 * <!-- @formatter:on -->
	 */
	ANTILYMPHOCYTE_IMMUNOGLOBULIN("391784006", "2.16.840.1.113883.6.96",
			"Antilymphocyte immunoglobulin (substance)", "Antilymphocyte immunoglobulin",
			"Immunglobuline anti-Lymphozyten human", "immunoglobuline anti-lymphocytes humains",
			"Immunoglobulina anti-linfociti T umani"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Antithymocyte immunoglobulin</div>
	 * <div class="de">Immunglobulin anti-T-Lymphozyten human</div>
	 * <div class="fr">immunoglobuline anti lymphocytes T humains</div>
	 * <div class="it">Immunoglobulina antitimociti umani</div>
	 * <!-- @formatter:on -->
	 */
	ANTITHYMOCYTE_IMMUNOGLOBULIN("768651008", "2.16.840.1.113883.6.96",
			"Antithymocyte immunoglobulin (substance)", "Antithymocyte immunoglobulin",
			"Immunglobulin anti-T-Lymphozyten human", "immunoglobuline anti lymphocytes T humains",
			"Immunoglobulina antitimociti umani"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Apixaban</div>
	 * <div class="de">Apixaban</div>
	 * <div class="fr">apixaban</div>
	 * <div class="it">Apixaban</div>
	 * <!-- @formatter:on -->
	 */
	APIXABAN("698090000", "2.16.840.1.113883.6.96", "Apixaban (substance)", "Apixaban", "Apixaban",
			"apixaban", "Apixaban"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Apomorphine</div>
	 * <div class="de">Apomorphin</div>
	 * <div class="fr">apomorphine</div>
	 * <div class="it">Apomorfina</div>
	 * <!-- @formatter:on -->
	 */
	APOMORPHINE("387375001", "2.16.840.1.113883.6.96", "Apomorphine (substance)", "Apomorphine",
			"Apomorphin", "apomorphine", "Apomorfina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aprepitant</div>
	 * <div class="de">Aprepitant</div>
	 * <div class="fr">aprépitant</div>
	 * <div class="it">Aprepitant</div>
	 * <!-- @formatter:on -->
	 */
	APREPITANT("409205009", "2.16.840.1.113883.6.96", "Aprepitant (substance)", "Aprepitant",
			"Aprepitant", "aprépitant", "Aprepitant"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aprotinin</div>
	 * <div class="de">Aprotinin</div>
	 * <div class="fr">aprotinine</div>
	 * <div class="it">Aprotinina</div>
	 * <!-- @formatter:on -->
	 */
	APROTININ("386961008", "2.16.840.1.113883.6.96", "Aprotinin (substance)", "Aprotinin",
			"Aprotinin", "aprotinine", "Aprotinina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Argatroban</div>
	 * <div class="de">Argatroban</div>
	 * <div class="fr">argatroban</div>
	 * <div class="it">Argatroban</div>
	 * <!-- @formatter:on -->
	 */
	ARGATROBAN("116508003", "2.16.840.1.113883.6.96", "Argatroban (substance)", "Argatroban",
			"Argatroban", "argatroban", "Argatroban"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Arginine</div>
	 * <div class="de">Arginin</div>
	 * <div class="fr">arginine</div>
	 * <div class="it">Arginina</div>
	 * <!-- @formatter:on -->
	 */
	ARGININE("52625008", "2.16.840.1.113883.6.96", "Arginine (substance)", "Arginine", "Arginin",
			"arginine", "Arginina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Argipressin</div>
	 * <div class="de">Argipressin (Vasopressin)</div>
	 * <div class="fr">argipressine (Vasopressine)</div>
	 * <div class="it">Argipressina (Vasopressina)</div>
	 * <!-- @formatter:on -->
	 */
	ARGIPRESSIN("421078009", "2.16.840.1.113883.6.96", "Argipressin (substance)", "Argipressin",
			"Argipressin (Vasopressin)", "argipressine (Vasopressine)",
			"Argipressina (Vasopressina)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aripiprazole</div>
	 * <div class="de">Aripiprazol</div>
	 * <div class="fr">aripiprazole</div>
	 * <div class="it">Aripirazolo</div>
	 * <!-- @formatter:on -->
	 */
	ARIPIPRAZOLE("406784005", "2.16.840.1.113883.6.96", "Aripiprazole (substance)", "Aripiprazole",
			"Aripiprazol", "aripiprazole", "Aripirazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Arsenic trioxide</div>
	 * <div class="de">Arsentrioxid</div>
	 * <div class="fr">trioxyde d'arsenic</div>
	 * <div class="it">Arsenico triossido</div>
	 * <!-- @formatter:on -->
	 */
	ARSENIC_TRIOXIDE("72251000", "2.16.840.1.113883.6.96", "Arsenic trioxide (substance)",
			"Arsenic trioxide", "Arsentrioxid", "trioxyde d'arsenic", "Arsenico triossido"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Artemether</div>
	 * <div class="de">Artemether</div>
	 * <div class="fr">artéméther</div>
	 * <div class="it">Artemetere</div>
	 * <!-- @formatter:on -->
	 */
	ARTEMETHER("420578008", "2.16.840.1.113883.6.96", "Artemether (substance)", "Artemether",
			"Artemether", "artéméther", "Artemetere"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Artesunate</div>
	 * <div class="de">Artesunat</div>
	 * <div class="fr">artésunate</div>
	 * <div class="it">Artesunato</div>
	 * <!-- @formatter:on -->
	 */
	ARTESUNATE("432410005", "2.16.840.1.113883.6.96", "Artesunate (substance)", "Artesunate",
			"Artesunat", "artésunate", "Artesunato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Articaine</div>
	 * <div class="de">Articain</div>
	 * <div class="fr">articaïne</div>
	 * <div class="it">Articaina</div>
	 * <!-- @formatter:on -->
	 */
	ARTICAINE("703107005", "2.16.840.1.113883.6.96", "Articaine (substance)", "Articaine",
			"Articain", "articaïne", "Articaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ascorbic acid</div>
	 * <div class="de">Ascorbinsäure (Vitamin C, E300)</div>
	 * <div class="fr">acide ascorbique (Vitamine C, E300)</div>
	 * <div class="it">Acido ascorbico (Vitamina C, E300)</div>
	 * <!-- @formatter:on -->
	 */
	ASCORBIC_ACID("43706004", "2.16.840.1.113883.6.96", "Ascorbic acid (substance)",
			"Ascorbic acid", "Ascorbinsäure (Vitamin C, E300)",
			"acide ascorbique (Vitamine C, E300)", "Acido ascorbico (Vitamina C, E300)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Asparaginase</div>
	 * <div class="de">Asparaginase</div>
	 * <div class="fr">asparaginase</div>
	 * <div class="it">Asparaginasi</div>
	 * <!-- @formatter:on -->
	 */
	ASPARAGINASE("371014004", "2.16.840.1.113883.6.96", "Asparaginase (substance)", "Asparaginase",
			"Asparaginase", "asparaginase", "Asparaginasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aspartic acid</div>
	 * <div class="de">Aspartinsäure</div>
	 * <div class="fr">acide aspartique</div>
	 * <div class="it">Acido aspartico</div>
	 * <!-- @formatter:on -->
	 */
	ASPARTIC_ACID("44970006", "2.16.840.1.113883.6.96", "Aspartic acid (substance)",
			"Aspartic acid", "Aspartinsäure", "acide aspartique", "Acido aspartico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aspirin</div>
	 * <div class="de">Acetylsalicylsäure</div>
	 * <div class="fr">acide acétylsalicylique</div>
	 * <div class="it">Acido acetilsalicilico</div>
	 * <!-- @formatter:on -->
	 */
	ASPIRIN("387458008", "2.16.840.1.113883.6.96", "Aspirin (substance)", "Aspirin",
			"Acetylsalicylsäure", "acide acétylsalicylique", "Acido acetilsalicilico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atazanavir</div>
	 * <div class="de">Atazanavir</div>
	 * <div class="fr">atazanavir</div>
	 * <div class="it">Atazanavir</div>
	 * <!-- @formatter:on -->
	 */
	ATAZANAVIR("413592000", "2.16.840.1.113883.6.96", "Atazanavir (substance)", "Atazanavir",
			"Atazanavir", "atazanavir", "Atazanavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atenolol</div>
	 * <div class="de">Atenolol</div>
	 * <div class="fr">aténolol</div>
	 * <div class="it">Atenololo</div>
	 * <!-- @formatter:on -->
	 */
	ATENOLOL("387506000", "2.16.840.1.113883.6.96", "Atenolol (substance)", "Atenolol", "Atenolol",
			"aténolol", "Atenololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atezolizumab</div>
	 * <div class="de">Atezolizumab</div>
	 * <div class="fr">atézolizumab</div>
	 * <div class="it">Atezolizumab</div>
	 * <!-- @formatter:on -->
	 */
	ATEZOLIZUMAB("719371003", "2.16.840.1.113883.6.96", "Atezolizumab (substance)", "Atezolizumab",
			"Atezolizumab", "atézolizumab", "Atezolizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atomoxetine</div>
	 * <div class="de">Atomoxetin</div>
	 * <div class="fr">atomoxétine</div>
	 * <div class="it">Atomoxetina</div>
	 * <!-- @formatter:on -->
	 */
	ATOMOXETINE("407037005", "2.16.840.1.113883.6.96", "Atomoxetine (substance)", "Atomoxetine",
			"Atomoxetin", "atomoxétine", "Atomoxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atorvastatin</div>
	 * <div class="de">Atorvastatin</div>
	 * <div class="fr">atorvastatine</div>
	 * <div class="it">Atorvastatina</div>
	 * <!-- @formatter:on -->
	 */
	ATORVASTATIN("373444002", "2.16.840.1.113883.6.96", "Atorvastatin (substance)", "Atorvastatin",
			"Atorvastatin", "atorvastatine", "Atorvastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atosiban</div>
	 * <div class="de">Atosiban</div>
	 * <div class="fr">atosiban</div>
	 * <div class="it">Atosiban</div>
	 * <!-- @formatter:on -->
	 */
	ATOSIBAN("391792002", "2.16.840.1.113883.6.96", "Atosiban (substance)", "Atosiban", "Atosiban",
			"atosiban", "Atosiban"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atovaquone</div>
	 * <div class="de">Atovaquon</div>
	 * <div class="fr">atovaquone</div>
	 * <div class="it">Atovaquone</div>
	 * <!-- @formatter:on -->
	 */
	ATOVAQUONE("386899002", "2.16.840.1.113883.6.96", "Atovaquone (substance)", "Atovaquone",
			"Atovaquon", "atovaquone", "Atovaquone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atracurium</div>
	 * <div class="de">Atracurium</div>
	 * <div class="fr">atracurium</div>
	 * <div class="it">Atracurio</div>
	 * <!-- @formatter:on -->
	 */
	ATRACURIUM("372835000", "2.16.840.1.113883.6.96", "Atracurium (substance)", "Atracurium",
			"Atracurium", "atracurium", "Atracurio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Atropine</div>
	 * <div class="de">Atropin</div>
	 * <div class="fr">atropine</div>
	 * <div class="it">Atropina</div>
	 * <!-- @formatter:on -->
	 */
	ATROPINE("372832002", "2.16.840.1.113883.6.96", "Atropine (substance)", "Atropine", "Atropin",
			"atropine", "Atropina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Avanafil</div>
	 * <div class="de">Avanafil</div>
	 * <div class="fr">avanafil</div>
	 * <div class="it">Avafanil</div>
	 * <!-- @formatter:on -->
	 */
	AVANAFIL("703956007", "2.16.840.1.113883.6.96", "Avanafil (substance)", "Avanafil", "Avanafil",
			"avanafil", "Avafanil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Avelumab</div>
	 * <div class="de">Avelumab</div>
	 * <div class="fr">avélumab</div>
	 * <div class="it">Avelumab</div>
	 * <!-- @formatter:on -->
	 */
	AVELUMAB("733055009", "2.16.840.1.113883.6.96", "Avelumab (substance)", "Avelumab", "Avelumab",
			"avélumab", "Avelumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Azacitidine</div>
	 * <div class="de">Azacitidin</div>
	 * <div class="fr">azacitidine</div>
	 * <div class="it">Azacitidina</div>
	 * <!-- @formatter:on -->
	 */
	AZACITIDINE("412328000", "2.16.840.1.113883.6.96", "Azacitidine (substance)", "Azacitidine",
			"Azacitidin", "azacitidine", "Azacitidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Azathioprine</div>
	 * <div class="de">Azathioprin</div>
	 * <div class="fr">azathioprine</div>
	 * <div class="it">Azatioprina</div>
	 * <!-- @formatter:on -->
	 */
	AZATHIOPRINE("372574004", "2.16.840.1.113883.6.96", "Azathioprine (substance)", "Azathioprine",
			"Azathioprin", "azathioprine", "Azatioprina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Azelaic acid</div>
	 * <div class="de">Azelainsäure</div>
	 * <div class="fr">acide azélaïque</div>
	 * <div class="it">Acido azelaico</div>
	 * <!-- @formatter:on -->
	 */
	AZELAIC_ACID("386936005", "2.16.840.1.113883.6.96", "Azelaic acid (substance)", "Azelaic acid",
			"Azelainsäure", "acide azélaïque", "Acido azelaico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Azelastine</div>
	 * <div class="de">Azelastin</div>
	 * <div class="fr">azélastine</div>
	 * <div class="it">Azelastina</div>
	 * <!-- @formatter:on -->
	 */
	AZELASTINE("372520005", "2.16.840.1.113883.6.96", "Azelastine (substance)", "Azelastine",
			"Azelastin", "azélastine", "Azelastina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Azithromycin</div>
	 * <div class="de">Azithromycin</div>
	 * <div class="fr">azithromycine</div>
	 * <div class="it">Azitromicina</div>
	 * <!-- @formatter:on -->
	 */
	AZITHROMYCIN("387531004", "2.16.840.1.113883.6.96", "Azithromycin (substance)", "Azithromycin",
			"Azithromycin", "azithromycine", "Azitromicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Aztreonam</div>
	 * <div class="de">Aztreonam</div>
	 * <div class="fr">aztréonam</div>
	 * <div class="it">Aztreonam</div>
	 * <!-- @formatter:on -->
	 */
	AZTREONAM("387386004", "2.16.840.1.113883.6.96", "Aztreonam (substance)", "Aztreonam",
			"Aztreonam", "aztréonam", "Aztreonam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bacitracin</div>
	 * <div class="de">Bacitracin</div>
	 * <div class="fr">bacitracine</div>
	 * <div class="it">Bacitracina</div>
	 * <!-- @formatter:on -->
	 */
	BACITRACIN("5220000", "2.16.840.1.113883.6.96", "Bacitracin (substance)", "Bacitracin",
			"Bacitracin", "bacitracine", "Bacitracina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Baclofen</div>
	 * <div class="de">Baclofen</div>
	 * <div class="fr">baclofène</div>
	 * <div class="it">Baclofene</div>
	 * <!-- @formatter:on -->
	 */
	BACLOFEN("387342009", "2.16.840.1.113883.6.96", "Baclofen (substance)", "Baclofen", "Baclofen",
			"baclofène", "Baclofene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Basiliximab</div>
	 * <div class="de">Basiliximab</div>
	 * <div class="fr">basiliximab</div>
	 * <div class="it">Basiliximab</div>
	 * <!-- @formatter:on -->
	 */
	BASILIXIMAB("386978004", "2.16.840.1.113883.6.96", "Basiliximab (substance)", "Basiliximab",
			"Basiliximab", "basiliximab", "Basiliximab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Beclometasone</div>
	 * <div class="de">Beclometason</div>
	 * <div class="fr">béclométasone</div>
	 * <div class="it">Beclometasone</div>
	 * <!-- @formatter:on -->
	 */
	BECLOMETASONE("116574000", "2.16.840.1.113883.6.96", "Beclometasone (substance)",
			"Beclometasone", "Beclometason", "béclométasone", "Beclometasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Beclometasone dipropionate</div>
	 * <div class="de">Beclometason dipropionat</div>
	 * <div class="fr">béclométasone dipropionate</div>
	 * <div class="it">Beclometasone dipropionato</div>
	 * <!-- @formatter:on -->
	 */
	BECLOMETASONE_DIPROPIONATE("116575004", "2.16.840.1.113883.6.96",
			"Beclometasone dipropionate (substance)", "Beclometasone dipropionate",
			"Beclometason dipropionat", "béclométasone dipropionate",
			"Beclometasone dipropionato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Belatacept</div>
	 * <div class="de">Belatacept</div>
	 * <div class="fr">bélatacept</div>
	 * <div class="it">Belatacept</div>
	 * <!-- @formatter:on -->
	 */
	BELATACEPT("713475001", "2.16.840.1.113883.6.96", "Belatacept (substance)", "Belatacept",
			"Belatacept", "bélatacept", "Belatacept"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Belimumab</div>
	 * <div class="de">Belimumab</div>
	 * <div class="fr">bélimumab</div>
	 * <div class="it">Belimumab</div>
	 * <!-- @formatter:on -->
	 */
	BELIMUMAB("449043000", "2.16.840.1.113883.6.96", "Belimumab (substance)", "Belimumab",
			"Belimumab", "bélimumab", "Belimumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Benazepril</div>
	 * <div class="de">Benazepril</div>
	 * <div class="fr">bénazépril</div>
	 * <div class="it">Benazepril</div>
	 * <!-- @formatter:on -->
	 */
	BENAZEPRIL("372511001", "2.16.840.1.113883.6.96", "Benazepril (substance)", "Benazepril",
			"Benazepril", "bénazépril", "Benazepril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bendamustine</div>
	 * <div class="de">Bendamustin</div>
	 * <div class="fr">bendamustine</div>
	 * <div class="it">Bendamustina</div>
	 * <!-- @formatter:on -->
	 */
	BENDAMUSTINE("428012008", "2.16.840.1.113883.6.96", "Bendamustine (substance)", "Bendamustine",
			"Bendamustin", "bendamustine", "Bendamustina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Benserazide</div>
	 * <div class="de">Benserazid</div>
	 * <div class="fr">bensérazide</div>
	 * <div class="it">Benserazide</div>
	 * <!-- @formatter:on -->
	 */
	BENSERAZIDE("391821005", "2.16.840.1.113883.6.96", "Benserazide (substance)", "Benserazide",
			"Benserazid", "bensérazide", "Benserazide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Benzocaine</div>
	 * <div class="de">Benzocain</div>
	 * <div class="fr">benzocaïne</div>
	 * <div class="it">Benzocaina</div>
	 * <!-- @formatter:on -->
	 */
	BENZOCAINE("387357002", "2.16.840.1.113883.6.96", "Benzocaine (substance)", "Benzocaine",
			"Benzocain", "benzocaïne", "Benzocaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Benzydamine</div>
	 * <div class="de">Benzydamin</div>
	 * <div class="fr">benzydamine</div>
	 * <div class="it">Benzidamina</div>
	 * <!-- @formatter:on -->
	 */
	BENZYDAMINE("421319000", "2.16.840.1.113883.6.96", "Benzydamine (substance)", "Benzydamine",
			"Benzydamin", "benzydamine", "Benzidamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Benzylpenicillin</div>
	 * <div class="de">Benzylpenicillin</div>
	 * <div class="fr">benzylpénicilline</div>
	 * <div class="it">Benzilpenicillina</div>
	 * <!-- @formatter:on -->
	 */
	BENZYLPENICILLIN("323389000", "2.16.840.1.113883.6.96", "Benzylpenicillin (substance)",
			"Benzylpenicillin", "Benzylpenicillin", "benzylpénicilline", "Benzilpenicillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Betahistine</div>
	 * <div class="de">Betahistin</div>
	 * <div class="fr">bétahistine</div>
	 * <div class="it">Betaistina</div>
	 * <!-- @formatter:on -->
	 */
	BETAHISTINE("418067008", "2.16.840.1.113883.6.96", "Betahistine (substance)", "Betahistine",
			"Betahistin", "bétahistine", "Betaistina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Betaine</div>
	 * <div class="de">Betain</div>
	 * <div class="fr">bétaine</div>
	 * <div class="it">Betaina</div>
	 * <!-- @formatter:on -->
	 */
	BETAINE("43356007", "2.16.840.1.113883.6.96", "Betaine (substance)", "Betaine", "Betain",
			"bétaine", "Betaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Betamethasone</div>
	 * <div class="de">Betamethason</div>
	 * <div class="fr">bétaméthasone</div>
	 * <div class="it">Betametasone</div>
	 * <!-- @formatter:on -->
	 */
	BETAMETHASONE("116571008", "2.16.840.1.113883.6.96", "Betamethasone (substance)",
			"Betamethasone", "Betamethason", "bétaméthasone", "Betametasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Betaxolol</div>
	 * <div class="de">Betaxolol</div>
	 * <div class="fr">bétaxolol</div>
	 * <div class="it">Betaxololo</div>
	 * <!-- @formatter:on -->
	 */
	BETAXOLOL("409276006", "2.16.840.1.113883.6.96", "Betaxolol (substance)", "Betaxolol",
			"Betaxolol", "bétaxolol", "Betaxololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Beta-galactosidase</div>
	 * <div class="de">Tilactase</div>
	 * <div class="fr">tilactase</div>
	 * <div class="it">Tilattasi</div>
	 * <!-- @formatter:on -->
	 */
	BETA_GALACTOSIDASE("28530008", "2.16.840.1.113883.6.96", "Beta-galactosidase (substance)",
			"Beta-galactosidase", "Tilactase", "tilactase", "Tilattasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bevacizumab</div>
	 * <div class="de">Bevacizumab</div>
	 * <div class="fr">bévacizumab</div>
	 * <div class="it">Bevacizumab</div>
	 * <!-- @formatter:on -->
	 */
	BEVACIZUMAB("409406007", "2.16.840.1.113883.6.96", "Bevacizumab (substance)", "Bevacizumab",
			"Bevacizumab", "bévacizumab", "Bevacizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bezafibrate</div>
	 * <div class="de">Bezafibrat</div>
	 * <div class="fr">bézafibrate</div>
	 * <div class="it">Bezafibrato</div>
	 * <!-- @formatter:on -->
	 */
	BEZAFIBRATE("396025003", "2.16.840.1.113883.6.96", "Bezafibrate (substance)", "Bezafibrate",
			"Bezafibrat", "bézafibrate", "Bezafibrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bicalutamide</div>
	 * <div class="de">Bicalutamid</div>
	 * <div class="fr">bicalutamide</div>
	 * <div class="it">Bicalutamide</div>
	 * <!-- @formatter:on -->
	 */
	BICALUTAMIDE("386908000", "2.16.840.1.113883.6.96", "Bicalutamide (substance)", "Bicalutamide",
			"Bicalutamid", "bicalutamide", "Bicalutamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bictegravir</div>
	 * <div class="de">Bictegravir</div>
	 * <div class="fr">bictégravir</div>
	 * <div class="it">Bictegravir</div>
	 * <!-- @formatter:on -->
	 */
	BICTEGRAVIR("772193003", "2.16.840.1.113883.6.96", "Bictegravir (substance)", "Bictegravir",
			"Bictegravir", "bictégravir", "Bictegravir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bilastine</div>
	 * <div class="de">Bilastin</div>
	 * <div class="fr">bilastine</div>
	 * <div class="it">Bilastina</div>
	 * <!-- @formatter:on -->
	 */
	BILASTINE("697973006", "2.16.840.1.113883.6.96", "Bilastine (substance)", "Bilastine",
			"Bilastin", "bilastine", "Bilastina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bimatoprost</div>
	 * <div class="de">Bimatoprost</div>
	 * <div class="fr">bimatoprost</div>
	 * <div class="it">Bimatoprost</div>
	 * <!-- @formatter:on -->
	 */
	BIMATOPROST("129492005", "2.16.840.1.113883.6.96", "Bimatoprost (substance)", "Bimatoprost",
			"Bimatoprost", "bimatoprost", "Bimatoprost"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Binimetinib</div>
	 * <div class="de">Binimetinib</div>
	 * <div class="fr">binimétinib</div>
	 * <div class="it">Binimetinib</div>
	 * <!-- @formatter:on -->
	 */
	BINIMETINIB("772195005", "2.16.840.1.113883.6.96", "Binimetinib (substance)", "Binimetinib",
			"Binimetinib", "binimétinib", "Binimetinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Biotin</div>
	 * <div class="de">Biotin</div>
	 * <div class="fr">biotine</div>
	 * <div class="it">Biotina</div>
	 * <!-- @formatter:on -->
	 */
	BIOTIN("8919000", "2.16.840.1.113883.6.96", "Biotin (substance)", "Biotin", "Biotin", "biotine",
			"Biotina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Biperiden</div>
	 * <div class="de">Biperiden</div>
	 * <div class="fr">bipéridène</div>
	 * <div class="it">Biperidene</div>
	 * <!-- @formatter:on -->
	 */
	BIPERIDEN("387359004", "2.16.840.1.113883.6.96", "Biperiden (substance)", "Biperiden",
			"Biperiden", "bipéridène", "Biperidene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bisacodyl</div>
	 * <div class="de">Bisacodyl</div>
	 * <div class="fr">bisacodyl</div>
	 * <div class="it">Bisacodile</div>
	 * <!-- @formatter:on -->
	 */
	BISACODYL("387075009", "2.16.840.1.113883.6.96", "Bisacodyl (substance)", "Bisacodyl",
			"Bisacodyl", "bisacodyl", "Bisacodile"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bisoprolol</div>
	 * <div class="de">Bisoprolol</div>
	 * <div class="fr">bisoprolol</div>
	 * <div class="it">Bisoprololo</div>
	 * <!-- @formatter:on -->
	 */
	BISOPROLOL("386868003", "2.16.840.1.113883.6.96", "Bisoprolol (substance)", "Bisoprolol",
			"Bisoprolol", "bisoprolol", "Bisoprololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bivalirudin</div>
	 * <div class="de">Bivalirudin</div>
	 * <div class="fr">bivalirudine</div>
	 * <div class="it">Bivalirudina</div>
	 * <!-- @formatter:on -->
	 */
	BIVALIRUDIN("129498009", "2.16.840.1.113883.6.96", "Bivalirudin (substance)", "Bivalirudin",
			"Bivalirudin", "bivalirudine", "Bivalirudina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bleomycin</div>
	 * <div class="de">Bleomycin</div>
	 * <div class="fr">bléomycine</div>
	 * <div class="it">Bleomicina</div>
	 * <!-- @formatter:on -->
	 */
	BLEOMYCIN("372843005", "2.16.840.1.113883.6.96", "Bleomycin (substance)", "Bleomycin",
			"Bleomycin", "bléomycine", "Bleomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bortezomib</div>
	 * <div class="de">Bortezomib</div>
	 * <div class="fr">bortézomib</div>
	 * <div class="it">Bortezomib</div>
	 * <!-- @formatter:on -->
	 */
	BORTEZOMIB("407097007", "2.16.840.1.113883.6.96", "Bortezomib (substance)", "Bortezomib",
			"Bortezomib", "bortézomib", "Bortezomib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bosentan</div>
	 * <div class="de">Bosentan</div>
	 * <div class="fr">bosentan</div>
	 * <div class="it">Bosentan</div>
	 * <!-- @formatter:on -->
	 */
	BOSENTAN("385559004", "2.16.840.1.113883.6.96", "Bosentan (substance)", "Bosentan", "Bosentan",
			"bosentan", "Bosentan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bosutinib</div>
	 * <div class="de">Bosutinib</div>
	 * <div class="fr">bosutinib</div>
	 * <div class="it">Bosutinib</div>
	 * <!-- @formatter:on -->
	 */
	BOSUTINIB("703128001", "2.16.840.1.113883.6.96", "Bosutinib (substance)", "Bosutinib",
			"Bosutinib", "bosutinib", "Bosutinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Botulinum toxin type A</div>
	 * <div class="de">Botulinumtoxin Typ A</div>
	 * <div class="fr">toxine botulique de type A</div>
	 * <div class="it">Tossina botulinica tipo A</div>
	 * <!-- @formatter:on -->
	 */
	BOTULINUM_TOXIN_TYPE_A("108890005", "2.16.840.1.113883.6.96",
			"Botulinum toxin type A (substance)", "Botulinum toxin type A", "Botulinumtoxin Typ A",
			"toxine botulique de type A", "Tossina botulinica tipo A"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Brentuximab vedotin</div>
	 * <div class="de">Brentuximab vedotin</div>
	 * <div class="fr">brentuximab védotine</div>
	 * <div class="it">Brentuximab</div>
	 * <!-- @formatter:on -->
	 */
	BRENTUXIMAB_VEDOTIN("713395006", "2.16.840.1.113883.6.96", "Brentuximab vedotin (substance)",
			"Brentuximab vedotin", "Brentuximab vedotin", "brentuximab védotine", "Brentuximab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Brexpiprazole</div>
	 * <div class="de">Brexpiprazol</div>
	 * <div class="fr">brexpiprazole</div>
	 * <div class="it">Brexpiprazolo</div>
	 * <!-- @formatter:on -->
	 */
	BREXPIPRAZOLE("716069007", "2.16.840.1.113883.6.96", "Brexpiprazole (substance)",
			"Brexpiprazole", "Brexpiprazol", "brexpiprazole", "Brexpiprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Brimonidine</div>
	 * <div class="de">Brimonidin</div>
	 * <div class="fr">brimonidine</div>
	 * <div class="it">Brimonidina</div>
	 * <!-- @formatter:on -->
	 */
	BRIMONIDINE("372547000", "2.16.840.1.113883.6.96", "Brimonidine (substance)", "Brimonidine",
			"Brimonidin", "brimonidine", "Brimonidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Brinzolamide</div>
	 * <div class="de">Brinzolamid</div>
	 * <div class="fr">brinzolamide</div>
	 * <div class="it">Brinzolamide</div>
	 * <!-- @formatter:on -->
	 */
	BRINZOLAMIDE("386925003", "2.16.840.1.113883.6.96", "Brinzolamide (substance)", "Brinzolamide",
			"Brinzolamid", "brinzolamide", "Brinzolamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Brivaracetam</div>
	 * <div class="de">Brivaracetam</div>
	 * <div class="fr">brivaracétam</div>
	 * <div class="it">Brivaracetam</div>
	 * <!-- @formatter:on -->
	 */
	BRIVARACETAM("420813001", "2.16.840.1.113883.6.96", "Brivaracetam (substance)", "Brivaracetam",
			"Brivaracetam", "brivaracétam", "Brivaracetam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Brivudine</div>
	 * <div class="de">Brivudin</div>
	 * <div class="fr">brivudine</div>
	 * <div class="it">Brivudina</div>
	 * <!-- @formatter:on -->
	 */
	BRIVUDINE("698049003", "2.16.840.1.113883.6.96", "Brivudine (substance)", "Brivudine",
			"Brivudin", "brivudine", "Brivudina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bromazepam</div>
	 * <div class="de">Bromazepam</div>
	 * <div class="fr">bromazépam</div>
	 * <div class="it">Bromazepam</div>
	 * <!-- @formatter:on -->
	 */
	BROMAZEPAM("387571009", "2.16.840.1.113883.6.96", "Bromazepam (substance)", "Bromazepam",
			"Bromazepam", "bromazépam", "Bromazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bromfenac</div>
	 * <div class="de">Bromfenac</div>
	 * <div class="fr">bromfénac</div>
	 * <div class="it">Bromfenac</div>
	 * <!-- @formatter:on -->
	 */
	BROMFENAC("108520008", "2.16.840.1.113883.6.96", "Bromfenac (substance)", "Bromfenac",
			"Bromfenac", "bromfénac", "Bromfenac"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bromocriptine</div>
	 * <div class="de">Bromocriptin</div>
	 * <div class="fr">bromocriptine</div>
	 * <div class="it">Bromocriptina</div>
	 * <!-- @formatter:on -->
	 */
	BROMOCRIPTINE("387039007", "2.16.840.1.113883.6.96", "Bromocriptine (substance)",
			"Bromocriptine", "Bromocriptin", "bromocriptine", "Bromocriptina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Budesonide</div>
	 * <div class="de">Budesonid</div>
	 * <div class="fr">budésonide</div>
	 * <div class="it">Budesonide</div>
	 * <!-- @formatter:on -->
	 */
	BUDESONIDE("395726003", "2.16.840.1.113883.6.96", "Budesonide (substance)", "Budesonide",
			"Budesonid", "budésonide", "Budesonide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bufexamac</div>
	 * <div class="de">Bufexamac</div>
	 * <div class="fr">bufexamac</div>
	 * <div class="it">Bufexamac</div>
	 * <!-- @formatter:on -->
	 */
	BUFEXAMAC("273952005", "2.16.840.1.113883.6.96", "Bufexamac (substance)", "Bufexamac",
			"Bufexamac", "bufexamac", "Bufexamac"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bupivacaine</div>
	 * <div class="de">Bupivacain</div>
	 * <div class="fr">bupivacaïne</div>
	 * <div class="it">Bupivacaina</div>
	 * <!-- @formatter:on -->
	 */
	BUPIVACAINE("387150008", "2.16.840.1.113883.6.96", "Bupivacaine (substance)", "Bupivacaine",
			"Bupivacain", "bupivacaïne", "Bupivacaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Buprenorphine</div>
	 * <div class="de">Buprenorphin</div>
	 * <div class="fr">buprénorphine</div>
	 * <div class="it">Buprenorfina</div>
	 * <!-- @formatter:on -->
	 */
	BUPRENORPHINE("387173000", "2.16.840.1.113883.6.96", "Buprenorphine (substance)",
			"Buprenorphine", "Buprenorphin", "buprénorphine", "Buprenorfina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Bupropion</div>
	 * <div class="de">Bupropion</div>
	 * <div class="fr">bupropion</div>
	 * <div class="it">Buproprione</div>
	 * <!-- @formatter:on -->
	 */
	BUPROPION("387564004", "2.16.840.1.113883.6.96", "Bupropion (substance)", "Bupropion",
			"Bupropion", "bupropion", "Buproprione"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Buserelin</div>
	 * <div class="de">Buserelin</div>
	 * <div class="fr">buséréline</div>
	 * <div class="it">Buserelina</div>
	 * <!-- @formatter:on -->
	 */
	BUSERELIN("395744006", "2.16.840.1.113883.6.96", "Buserelin (substance)", "Buserelin",
			"Buserelin", "buséréline", "Buserelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cabazitaxel</div>
	 * <div class="de">Cabazitaxel</div>
	 * <div class="fr">cabazitaxel</div>
	 * <div class="it">Cabazitaxel</div>
	 * <!-- @formatter:on -->
	 */
	CABAZITAXEL("446706007", "2.16.840.1.113883.6.96", "Cabazitaxel (substance)", "Cabazitaxel",
			"Cabazitaxel", "cabazitaxel", "Cabazitaxel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cabergoline</div>
	 * <div class="de">Cabergolin</div>
	 * <div class="fr">cabergoline</div>
	 * <div class="it">Cabergolina</div>
	 * <!-- @formatter:on -->
	 */
	CABERGOLINE("386979007", "2.16.840.1.113883.6.96", "Cabergoline (substance)", "Cabergoline",
			"Cabergolin", "cabergoline", "Cabergolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Caffeine</div>
	 * <div class="de">Coffein</div>
	 * <div class="fr">caféine</div>
	 * <div class="it">Caffeina</div>
	 * <!-- @formatter:on -->
	 */
	CAFFEINE("255641001", "2.16.840.1.113883.6.96", "Caffeine (substance)", "Caffeine", "Coffein",
			"caféine", "Caffeina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcipotriol</div>
	 * <div class="de">Calcipotriol</div>
	 * <div class="fr">calcipotriol</div>
	 * <div class="it">Calcipotriolo</div>
	 * <!-- @formatter:on -->
	 */
	CALCIPOTRIOL("395766004", "2.16.840.1.113883.6.96", "Calcipotriol (substance)", "Calcipotriol",
			"Calcipotriol", "calcipotriol", "Calcipotriolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcitriol</div>
	 * <div class="de">Calcitriol</div>
	 * <div class="fr">calcitriol</div>
	 * <div class="it">Calcitriolo</div>
	 * <!-- @formatter:on -->
	 */
	CALCITRIOL("259333003", "2.16.840.1.113883.6.96", "Calcitriol (substance)", "Calcitriol",
			"Calcitriol", "calcitriol", "Calcitriolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium acetate</div>
	 * <div class="de">Calcium acetat</div>
	 * <div class="fr">calcium acétate</div>
	 * <div class="it">Calcio acetato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_ACETATE("387019008", "2.16.840.1.113883.6.96", "Calcium acetate (substance)",
			"Calcium acetate", "Calcium acetat", "calcium acétate", "Calcio acetato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium alginate solution</div>
	 * <div class="de">Calcium alginat</div>
	 * <div class="fr">alginate calcique</div>
	 * <div class="it">Calcio alginato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_ALGINATE_SOLUTION("256620003", "2.16.840.1.113883.6.96",
			"Calcium alginate solution (substance)", "Calcium alginate solution", "Calcium alginat",
			"alginate calcique", "Calcio alginato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium carbaspirin</div>
	 * <div class="de">Carbasalat calcium</div>
	 * <div class="fr">carbasalate calcique</div>
	 * <div class="it">Calcio carbasalato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_CARBASPIRIN("111122008", "2.16.840.1.113883.6.96", "Calcium carbaspirin (substance)",
			"Calcium carbaspirin", "Carbasalat calcium", "carbasalate calcique",
			"Calcio carbasalato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium carbonate</div>
	 * <div class="de">Calcium carbonat</div>
	 * <div class="fr">calcium carbonate</div>
	 * <div class="it">Calcio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_CARBONATE("387307005", "2.16.840.1.113883.6.96", "Calcium carbonate (substance)",
			"Calcium carbonate", "Calcium carbonat", "calcium carbonate", "Calcio carbonato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium chloride</div>
	 * <div class="de">Calciumchlorid</div>
	 * <div class="fr">calcium chlorure</div>
	 * <div class="it">Calcio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_CHLORIDE("387377009", "2.16.840.1.113883.6.96", "Calcium chloride (substance)",
			"Calcium chloride", "Calciumchlorid", "calcium chlorure", "Calcio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium glubionate</div>
	 * <div class="de">Calcium glubionat</div>
	 * <div class="fr">calcium glubionate</div>
	 * <div class="it">Calcio glubionato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_GLUBIONATE("32445001", "2.16.840.1.113883.6.96", "Calcium glubionate (substance)",
			"Calcium glubionate", "Calcium glubionat", "calcium glubionate", "Calcio glubionato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium gluconate</div>
	 * <div class="de">Calcium gluconat</div>
	 * <div class="fr">calcium gluconate</div>
	 * <div class="it">Calcio gluconato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_GLUCONATE("387292008", "2.16.840.1.113883.6.96", "Calcium gluconate (substance)",
			"Calcium gluconate", "Calcium gluconat", "calcium gluconate", "Calcio gluconato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Calcium leucovorin</div>
	 * <div class="de">Calcium folinat</div>
	 * <div class="fr">acide folinique calcique</div>
	 * <div class="it">Calcio folinato</div>
	 * <!-- @formatter:on -->
	 */
	CALCIUM_LEUCOVORIN("126223008", "2.16.840.1.113883.6.96", "Calcium leucovorin (substance)",
			"Calcium leucovorin", "Calcium folinat", "acide folinique calcique", "Calcio folinato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Canagliflozin</div>
	 * <div class="de">Canagliflozin</div>
	 * <div class="fr">canagliflozine</div>
	 * <div class="it">Canaglifozin</div>
	 * <!-- @formatter:on -->
	 */
	CANAGLIFLOZIN("703676004", "2.16.840.1.113883.6.96", "Canagliflozin (substance)",
			"Canagliflozin", "Canagliflozin", "canagliflozine", "Canaglifozin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Canakinumab</div>
	 * <div class="de">Canakinumab</div>
	 * <div class="fr">canakinumab</div>
	 * <div class="it">Canakinumab</div>
	 * <!-- @formatter:on -->
	 */
	CANAKINUMAB("698091001", "2.16.840.1.113883.6.96", "Canakinumab (substance)", "Canakinumab",
			"Canakinumab", "canakinumab", "Canakinumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Candesartan</div>
	 * <div class="de">Candesartan</div>
	 * <div class="fr">candésartan</div>
	 * <div class="it">Candesartan</div>
	 * <!-- @formatter:on -->
	 */
	CANDESARTAN("372512008", "2.16.840.1.113883.6.96", "Candesartan (substance)", "Candesartan",
			"Candesartan", "candésartan", "Candesartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cangrelor</div>
	 * <div class="de">Cangrelor</div>
	 * <div class="fr">cangrélor</div>
	 * <div class="it">Cangrelor</div>
	 * <!-- @formatter:on -->
	 */
	CANGRELOR("716118009", "2.16.840.1.113883.6.96", "Cangrelor (substance)", "Cangrelor",
			"Cangrelor", "cangrélor", "Cangrelor"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cannabidiol</div>
	 * <div class="de">Cannabidiol (CBD)</div>
	 * <div class="fr">cannabidiol (CBD)</div>
	 * <div class="it">Cannabidiolo</div>
	 * <!-- @formatter:on -->
	 */
	CANNABIDIOL("96223000", "2.16.840.1.113883.6.96", "Cannabidiol (substance)", "Cannabidiol",
			"Cannabidiol (CBD)", "cannabidiol (CBD)", "Cannabidiolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Capecitabine</div>
	 * <div class="de">Capecitabin</div>
	 * <div class="fr">capécitabine</div>
	 * <div class="it">Capecitabina</div>
	 * <!-- @formatter:on -->
	 */
	CAPECITABINE("386906001", "2.16.840.1.113883.6.96", "Capecitabine (substance)", "Capecitabine",
			"Capecitabin", "capécitabine", "Capecitabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Capsaicin</div>
	 * <div class="de">Capsaicin</div>
	 * <div class="fr">capsaïcine</div>
	 * <div class="it">Capsaicina</div>
	 * <!-- @formatter:on -->
	 */
	CAPSAICIN("95995002", "2.16.840.1.113883.6.96", "Capsaicin (substance)", "Capsaicin",
			"Capsaicin", "capsaïcine", "Capsaicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Captopril</div>
	 * <div class="de">Captopril</div>
	 * <div class="fr">captopril</div>
	 * <div class="it">Captopril</div>
	 * <!-- @formatter:on -->
	 */
	CAPTOPRIL("387160004", "2.16.840.1.113883.6.96", "Captopril (substance)", "Captopril",
			"Captopril", "captopril", "Captopril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carbachol</div>
	 * <div class="de">Carbachol</div>
	 * <div class="fr">carbachol</div>
	 * <div class="it">Carbacolo</div>
	 * <!-- @formatter:on -->
	 */
	CARBACHOL("387183001", "2.16.840.1.113883.6.96", "Carbachol (substance)", "Carbachol",
			"Carbachol", "carbachol", "Carbacolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carbamazepine</div>
	 * <div class="de">Carbamazepin</div>
	 * <div class="fr">carbamazépine</div>
	 * <div class="it">Carbamazepina</div>
	 * <!-- @formatter:on -->
	 */
	CARBAMAZEPINE("387222003", "2.16.840.1.113883.6.96", "Carbamazepine (substance)",
			"Carbamazepine", "Carbamazepin", "carbamazépine", "Carbamazepina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carbetocin</div>
	 * <div class="de">Carbetocin</div>
	 * <div class="fr">carbétocine</div>
	 * <div class="it">Carbetocina</div>
	 * <!-- @formatter:on -->
	 */
	CARBETOCIN("425003007", "2.16.840.1.113883.6.96", "Carbetocin (substance)", "Carbetocin",
			"Carbetocin", "carbétocine", "Carbetocina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carbidopa</div>
	 * <div class="de">Carbidopa</div>
	 * <div class="fr">carbidopa</div>
	 * <div class="it">Carbidopa</div>
	 * <!-- @formatter:on -->
	 */
	CARBIDOPA("73579000", "2.16.840.1.113883.6.96", "Carbidopa (substance)", "Carbidopa",
			"Carbidopa", "carbidopa", "Carbidopa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carbimazole</div>
	 * <div class="de">Carbimazol</div>
	 * <div class="fr">carbimazole</div>
	 * <div class="it">Carbimazolo</div>
	 * <!-- @formatter:on -->
	 */
	CARBIMAZOLE("395831005", "2.16.840.1.113883.6.96", "Carbimazole (substance)", "Carbimazole",
			"Carbimazol", "carbimazole", "Carbimazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carbocisteine</div>
	 * <div class="de">Carbocistein</div>
	 * <div class="fr">carbocistéine</div>
	 * <div class="it">Carbocisteina</div>
	 * <!-- @formatter:on -->
	 */
	CARBOCISTEINE("395842001", "2.16.840.1.113883.6.96", "Carbocisteine (substance)",
			"Carbocisteine", "Carbocistein", "carbocistéine", "Carbocisteina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carboplatin</div>
	 * <div class="de">Carboplatin</div>
	 * <div class="fr">carboplatine</div>
	 * <div class="it">Carboplatino</div>
	 * <!-- @formatter:on -->
	 */
	CARBOPLATIN("386905002", "2.16.840.1.113883.6.96", "Carboplatin (substance)", "Carboplatin",
			"Carboplatin", "carboplatine", "Carboplatino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carfilzomib</div>
	 * <div class="de">Carfilzomib</div>
	 * <div class="fr">carfilzomib</div>
	 * <div class="it">Carfilzomib</div>
	 * <!-- @formatter:on -->
	 */
	CARFILZOMIB("713463006", "2.16.840.1.113883.6.96", "Carfilzomib (substance)", "Carfilzomib",
			"Carfilzomib", "carfilzomib", "Carfilzomib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cariprazine</div>
	 * <div class="de">Cariprazin</div>
	 * <div class="fr">cariprazine</div>
	 * <div class="it">Cariprazina</div>
	 * <!-- @formatter:on -->
	 */
	CARIPRAZINE("715295006", "2.16.840.1.113883.6.96", "Cariprazine (substance)", "Cariprazine",
			"Cariprazin", "cariprazine", "Cariprazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carmustine</div>
	 * <div class="de">Carmustin</div>
	 * <div class="fr">carmustine</div>
	 * <div class="it">Carmustina</div>
	 * <!-- @formatter:on -->
	 */
	CARMUSTINE("387281007", "2.16.840.1.113883.6.96", "Carmustine (substance)", "Carmustine",
			"Carmustin", "carmustine", "Carmustina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carteolol</div>
	 * <div class="de">Carteolol</div>
	 * <div class="fr">cartéolol</div>
	 * <div class="it">Carteololo</div>
	 * <!-- @formatter:on -->
	 */
	CARTEOLOL("386866004", "2.16.840.1.113883.6.96", "Carteolol (substance)", "Carteolol",
			"Carteolol", "cartéolol", "Carteololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Carvedilol</div>
	 * <div class="de">Carvedilol</div>
	 * <div class="fr">carvédilol</div>
	 * <div class="it">Carvedilolo</div>
	 * <!-- @formatter:on -->
	 */
	CARVEDILOL("386870007", "2.16.840.1.113883.6.96", "Carvedilol (substance)", "Carvedilol",
			"Carvedilol", "carvédilol", "Carvedilolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Caspofungin</div>
	 * <div class="de">Caspofungin</div>
	 * <div class="fr">caspofungine</div>
	 * <div class="it">Caspofungin</div>
	 * <!-- @formatter:on -->
	 */
	CASPOFUNGIN("413770001", "2.16.840.1.113883.6.96", "Caspofungin (substance)", "Caspofungin",
			"Caspofungin", "caspofungine", "Caspofungin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cefaclor</div>
	 * <div class="de">Cefaclor</div>
	 * <div class="fr">céfaclor</div>
	 * <div class="it">Cefaclor</div>
	 * <!-- @formatter:on -->
	 */
	CEFACLOR("387270009", "2.16.840.1.113883.6.96", "Cefaclor (substance)", "Cefaclor", "Cefaclor",
			"céfaclor", "Cefaclor"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cefazolin</div>
	 * <div class="de">Cefazolin</div>
	 * <div class="fr">céfazoline</div>
	 * <div class="it">Cefazolina</div>
	 * <!-- @formatter:on -->
	 */
	CEFAZOLIN("387470007", "2.16.840.1.113883.6.96", "Cefazolin (substance)", "Cefazolin",
			"Cefazolin", "céfazoline", "Cefazolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cefepime</div>
	 * <div class="de">Cefepim</div>
	 * <div class="fr">céfépime</div>
	 * <div class="it">Cefepime</div>
	 * <!-- @formatter:on -->
	 */
	CEFEPIME("96048006", "2.16.840.1.113883.6.96", "Cefepime (substance)", "Cefepime", "Cefepim",
			"céfépime", "Cefepime"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cefixime anhydrous</div>
	 * <div class="de">Cefixim</div>
	 * <div class="fr">céfixime</div>
	 * <div class="it">Cefixima</div>
	 * <!-- @formatter:on -->
	 */
	CEFIXIME_ANHYDROUS("785697003", "2.16.840.1.113883.6.96", "Cefixime anhydrous (substance)",
			"Cefixime anhydrous", "Cefixim", "céfixime", "Cefixima"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cefpodoxime</div>
	 * <div class="de">Cefpodoxim</div>
	 * <div class="fr">cefpodoxime</div>
	 * <div class="it">Cefpodoxima</div>
	 * <!-- @formatter:on -->
	 */
	CEFPODOXIME("387534007", "2.16.840.1.113883.6.96", "Cefpodoxime (substance)", "Cefpodoxime",
			"Cefpodoxim", "cefpodoxime", "Cefpodoxima"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ceftazidime</div>
	 * <div class="de">Ceftazidim</div>
	 * <div class="fr">ceftazidime</div>
	 * <div class="it">Ceftazidime</div>
	 * <!-- @formatter:on -->
	 */
	CEFTAZIDIME("387200005", "2.16.840.1.113883.6.96", "Ceftazidime (substance)", "Ceftazidime",
			"Ceftazidim", "ceftazidime", "Ceftazidime"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ceftriaxone</div>
	 * <div class="de">Ceftriaxon</div>
	 * <div class="fr">ceftriaxone</div>
	 * <div class="it">Ceftriaxone</div>
	 * <!-- @formatter:on -->
	 */
	CEFTRIAXONE("372670001", "2.16.840.1.113883.6.96", "Ceftriaxone (substance)", "Ceftriaxone",
			"Ceftriaxon", "ceftriaxone", "Ceftriaxone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cefuroxime</div>
	 * <div class="de">Cefuroxim</div>
	 * <div class="fr">céfuroxime</div>
	 * <div class="it">Cefuroxime</div>
	 * <!-- @formatter:on -->
	 */
	CEFUROXIME("372833007", "2.16.840.1.113883.6.96", "Cefuroxime (substance)", "Cefuroxime",
			"Cefuroxim", "céfuroxime", "Cefuroxime"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Celecoxib</div>
	 * <div class="de">Celecoxib</div>
	 * <div class="fr">célécoxib</div>
	 * <div class="it">Celecoxib</div>
	 * <!-- @formatter:on -->
	 */
	CELECOXIB("116081000", "2.16.840.1.113883.6.96", "Celecoxib (substance)", "Celecoxib",
			"Celecoxib", "célécoxib", "Celecoxib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cetirizine</div>
	 * <div class="de">Cetirizin</div>
	 * <div class="fr">cétirizine</div>
	 * <div class="it">Cetirizina</div>
	 * <!-- @formatter:on -->
	 */
	CETIRIZINE("372523007", "2.16.840.1.113883.6.96", "Cetirizine (substance)", "Cetirizine",
			"Cetirizin", "cétirizine", "Cetirizina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cetylpyridinium</div>
	 * <div class="de">Cetylpyridinium</div>
	 * <div class="fr">cétylpyridinium</div>
	 * <div class="it">Cetilpiridinio</div>
	 * <!-- @formatter:on -->
	 */
	CETYLPYRIDINIUM("387043006", "2.16.840.1.113883.6.96", "Cetylpyridinium (substance)",
			"Cetylpyridinium", "Cetylpyridinium", "cétylpyridinium", "Cetilpiridinio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chloramphenicol</div>
	 * <div class="de">Chloramphenicol</div>
	 * <div class="fr">chloramphénicol</div>
	 * <div class="it">Cloramfenicolo</div>
	 * <!-- @formatter:on -->
	 */
	CHLORAMPHENICOL("372777009", "2.16.840.1.113883.6.96", "Chloramphenicol (substance)",
			"Chloramphenicol", "Chloramphenicol", "chloramphénicol", "Cloramfenicolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlordiazepoxide</div>
	 * <div class="de">Chlordiazepoxid</div>
	 * <div class="fr">chlordiazépoxide</div>
	 * <div class="it">Clordiazepossido</div>
	 * <!-- @formatter:on -->
	 */
	CHLORDIAZEPOXIDE("372866006", "2.16.840.1.113883.6.96", "Chlordiazepoxide (substance)",
			"Chlordiazepoxide", "Chlordiazepoxid", "chlordiazépoxide", "Clordiazepossido"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlorhexidine</div>
	 * <div class="de">Chlorhexidin</div>
	 * <div class="fr">chlorhexidine</div>
	 * <div class="it">Clorexidina</div>
	 * <!-- @formatter:on -->
	 */
	CHLORHEXIDINE("373568007", "2.16.840.1.113883.6.96", "Chlorhexidine (substance)",
			"Chlorhexidine", "Chlorhexidin", "chlorhexidine", "Clorexidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlormadinone</div>
	 * <div class="de">Chlormadinon</div>
	 * <div class="fr">chlormadinone</div>
	 * <div class="it">Clormadinone</div>
	 * <!-- @formatter:on -->
	 */
	CHLORMADINONE("734645001", "2.16.840.1.113883.6.96", "Chlormadinone (substance)",
			"Chlormadinone", "Chlormadinon", "chlormadinone", "Clormadinone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chloroquine</div>
	 * <div class="de">Chloroquin</div>
	 * <div class="fr">chloroquine</div>
	 * <div class="it">Clorochina</div>
	 * <!-- @formatter:on -->
	 */
	CHLOROQUINE("373468005", "2.16.840.1.113883.6.96", "Chloroquine (substance)", "Chloroquine",
			"Chloroquin", "chloroquine", "Clorochina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlorphenamine</div>
	 * <div class="de">Chlorphenamin</div>
	 * <div class="fr">chlorphénamine</div>
	 * <div class="it">Clorfenamina</div>
	 * <!-- @formatter:on -->
	 */
	CHLORPHENAMINE("372914003", "2.16.840.1.113883.6.96", "Chlorphenamine (substance)",
			"Chlorphenamine", "Chlorphenamin", "chlorphénamine", "Clorfenamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlorpromazine</div>
	 * <div class="de">Chlorpromazin</div>
	 * <div class="fr">chlorpromazine</div>
	 * <div class="it">Clorpromazina</div>
	 * <!-- @formatter:on -->
	 */
	CHLORPROMAZINE("387258005", "2.16.840.1.113883.6.96", "Chlorpromazine (substance)",
			"Chlorpromazine", "Chlorpromazin", "chlorpromazine", "Clorpromazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlorprothixene</div>
	 * <div class="de">Chlorprothixen</div>
	 * <div class="fr">chlorprothixène</div>
	 * <div class="it">Clorprotixene</div>
	 * <!-- @formatter:on -->
	 */
	CHLORPROTHIXENE("387317000", "2.16.840.1.113883.6.96", "Chlorprothixene (substance)",
			"Chlorprothixene", "Chlorprothixen", "chlorprothixène", "Clorprotixene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chlortalidone</div>
	 * <div class="de">Chlortalidon</div>
	 * <div class="fr">chlortalidone</div>
	 * <div class="it">Clortalidone</div>
	 * <!-- @formatter:on -->
	 */
	CHLORTALIDONE("387324004", "2.16.840.1.113883.6.96", "Chlortalidone (substance)",
			"Chlortalidone", "Chlortalidon", "chlortalidone", "Clortalidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Chondroitin sulfate</div>
	 * <div class="de">Chondroitinsulfate-Gemisch</div>
	 * <div class="fr">chondroïtine sulfate</div>
	 * <div class="it">Condroitinsolfato</div>
	 * <!-- @formatter:on -->
	 */
	CHONDROITIN_SULFATE("4104007", "2.16.840.1.113883.6.96", "Chondroitin sulfate (substance)",
			"Chondroitin sulfate", "Chondroitinsulfate-Gemisch", "chondroïtine sulfate",
			"Condroitinsolfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Choriogonadotropin alfa</div>
	 * <div class="de">Choriogonadotropin alfa</div>
	 * <div class="fr">choriogonadotropine alfa</div>
	 * <div class="it">Coriogonadotropina alfa</div>
	 * <!-- @formatter:on -->
	 */
	CHORIOGONADOTROPIN_ALFA("129494006", "2.16.840.1.113883.6.96",
			"Choriogonadotropin alfa (substance)", "Choriogonadotropin alfa",
			"Choriogonadotropin alfa", "choriogonadotropine alfa", "Coriogonadotropina alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ciclesonide</div>
	 * <div class="de">Ciclesonid</div>
	 * <div class="fr">ciclésonide</div>
	 * <div class="it">Ciclesonide</div>
	 * <!-- @formatter:on -->
	 */
	CICLESONIDE("417420004", "2.16.840.1.113883.6.96", "Ciclesonide (substance)", "Ciclesonide",
			"Ciclesonid", "ciclésonide", "Ciclesonide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ciclopirox</div>
	 * <div class="de">Ciclopirox</div>
	 * <div class="fr">ciclopirox</div>
	 * <div class="it">Ciclopirox</div>
	 * <!-- @formatter:on -->
	 */
	CICLOPIROX("372854000", "2.16.840.1.113883.6.96", "Ciclopirox (substance)", "Ciclopirox",
			"Ciclopirox", "ciclopirox", "Ciclopirox"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ciclosporin</div>
	 * <div class="de">Ciclosporin</div>
	 * <div class="fr">ciclosporine</div>
	 * <div class="it">Ciclosporina</div>
	 * <!-- @formatter:on -->
	 */
	CICLOSPORIN("387467008", "2.16.840.1.113883.6.96", "Ciclosporin (substance)", "Ciclosporin",
			"Ciclosporin", "ciclosporine", "Ciclosporina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cilastatin</div>
	 * <div class="de">Cilastatin</div>
	 * <div class="fr">cilastatine</div>
	 * <div class="it">Cilastatina</div>
	 * <!-- @formatter:on -->
	 */
	CILASTATIN("96058005", "2.16.840.1.113883.6.96", "Cilastatin (substance)", "Cilastatin",
			"Cilastatin", "cilastatine", "Cilastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cilazapril</div>
	 * <div class="de">Cilazapril</div>
	 * <div class="fr">cilazapril</div>
	 * <div class="it">Cilazapril</div>
	 * <!-- @formatter:on -->
	 */
	CILAZAPRIL("395947008", "2.16.840.1.113883.6.96", "Cilazapril (substance)", "Cilazapril",
			"Cilazapril", "cilazapril", "Cilazapril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cimetidine</div>
	 * <div class="de">Cimetidin</div>
	 * <div class="fr">cimétidine</div>
	 * <div class="it">Cimetidina</div>
	 * <!-- @formatter:on -->
	 */
	CIMETIDINE("373541007", "2.16.840.1.113883.6.96", "Cimetidine (substance)", "Cimetidine",
			"Cimetidin", "cimétidine", "Cimetidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cinacalcet</div>
	 * <div class="de">Cinacalcet</div>
	 * <div class="fr">cinacalcet</div>
	 * <div class="it">Cinacalcet</div>
	 * <!-- @formatter:on -->
	 */
	CINACALCET("409392004", "2.16.840.1.113883.6.96", "Cinacalcet (substance)", "Cinacalcet",
			"Cinacalcet", "cinacalcet", "Cinacalcet"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cinchocaine</div>
	 * <div class="de">Cinchocain</div>
	 * <div class="fr">cinchocaïne</div>
	 * <div class="it">Cincocaina</div>
	 * <!-- @formatter:on -->
	 */
	CINCHOCAINE("395953008", "2.16.840.1.113883.6.96", "Cinchocaine (substance)", "Cinchocaine",
			"Cinchocain", "cinchocaïne", "Cincocaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cinnarizine</div>
	 * <div class="de">Cinnarizin</div>
	 * <div class="fr">cinnarizine</div>
	 * <div class="it">Cinnarizina</div>
	 * <!-- @formatter:on -->
	 */
	CINNARIZINE("395955001", "2.16.840.1.113883.6.96", "Cinnarizine (substance)", "Cinnarizine",
			"Cinnarizin", "cinnarizine", "Cinnarizina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ciprofloxacin</div>
	 * <div class="de">Ciprofloxacin</div>
	 * <div class="fr">ciprofloxacine</div>
	 * <div class="it">Ciprofloxacina</div>
	 * <!-- @formatter:on -->
	 */
	CIPROFLOXACIN("372840008", "2.16.840.1.113883.6.96", "Ciprofloxacin (substance)",
			"Ciprofloxacin", "Ciprofloxacin", "ciprofloxacine", "Ciprofloxacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cisatracurium</div>
	 * <div class="de">Cisatracurium</div>
	 * <div class="fr">cisatracurium</div>
	 * <div class="it">Cisatracurio</div>
	 * <!-- @formatter:on -->
	 */
	CISATRACURIUM("372495006", "2.16.840.1.113883.6.96", "Cisatracurium (substance)",
			"Cisatracurium", "Cisatracurium", "cisatracurium", "Cisatracurio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cisplatin</div>
	 * <div class="de">Cisplatin</div>
	 * <div class="fr">cisplatine</div>
	 * <div class="it">Cisplatino</div>
	 * <!-- @formatter:on -->
	 */
	CISPLATIN("387318005", "2.16.840.1.113883.6.96", "Cisplatin (substance)", "Cisplatin",
			"Cisplatin", "cisplatine", "Cisplatino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Citalopram</div>
	 * <div class="de">Citalopram</div>
	 * <div class="fr">citalopram</div>
	 * <div class="it">Citalopram</div>
	 * <!-- @formatter:on -->
	 */
	CITALOPRAM("372596005", "2.16.840.1.113883.6.96", "Citalopram (substance)", "Citalopram",
			"Citalopram", "citalopram", "Citalopram"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Citric acid monohydrate</div>
	 * <div class="de">Citronensäure-Monohydrat</div>
	 * <div class="fr">acide citrique monohydrate</div>
	 * <div class="it">Acido citrico monoidrato</div>
	 * <!-- @formatter:on -->
	 */
	CITRIC_ACID_MONOHYDRATE("725962006", "2.16.840.1.113883.6.96",
			"Citric acid monohydrate (substance)", "Citric acid monohydrate",
			"Citronensäure-Monohydrat", "acide citrique monohydrate", "Acido citrico monoidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cladribine</div>
	 * <div class="de">Cladribin</div>
	 * <div class="fr">cladribine</div>
	 * <div class="it">Cladribina</div>
	 * <!-- @formatter:on -->
	 */
	CLADRIBINE("386916009", "2.16.840.1.113883.6.96", "Cladribine (substance)", "Cladribine",
			"Cladribin", "cladribine", "Cladribina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clarithromycin</div>
	 * <div class="de">Clarithromycin</div>
	 * <div class="fr">clarithromycine</div>
	 * <div class="it">Claritromicina</div>
	 * <!-- @formatter:on -->
	 */
	CLARITHROMYCIN("387487009", "2.16.840.1.113883.6.96", "Clarithromycin (substance)",
			"Clarithromycin", "Clarithromycin", "clarithromycine", "Claritromicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clavulanic acid</div>
	 * <div class="de">Clavulansäure</div>
	 * <div class="fr">acide clavulanique</div>
	 * <div class="it">Acido clavulanico</div>
	 * <!-- @formatter:on -->
	 */
	CLAVULANIC_ACID("395939008", "2.16.840.1.113883.6.96", "Clavulanic acid (substance)",
			"Clavulanic acid", "Clavulansäure", "acide clavulanique", "Acido clavulanico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clemastine</div>
	 * <div class="de">Clemastin</div>
	 * <div class="fr">clémastine</div>
	 * <div class="it">Clemastina</div>
	 * <!-- @formatter:on -->
	 */
	CLEMASTINE("372744005", "2.16.840.1.113883.6.96", "Clemastine (substance)", "Clemastine",
			"Clemastin", "clémastine", "Clemastina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clevidipine</div>
	 * <div class="de">Clevidipin</div>
	 * <div class="fr">clévidipine</div>
	 * <div class="it">Clevidipina</div>
	 * <!-- @formatter:on -->
	 */
	CLEVIDIPINE("439471002", "2.16.840.1.113883.6.96", "Clevidipine (substance)", "Clevidipine",
			"Clevidipin", "clévidipine", "Clevidipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clindamycin</div>
	 * <div class="de">Clindamycin</div>
	 * <div class="fr">clindamycine</div>
	 * <div class="it">Clindamicina</div>
	 * <!-- @formatter:on -->
	 */
	CLINDAMYCIN("372786004", "2.16.840.1.113883.6.96", "Clindamycin (substance)", "Clindamycin",
			"Clindamycin", "clindamycine", "Clindamicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clioquinol</div>
	 * <div class="de">Clioquinol</div>
	 * <div class="fr">clioquinol</div>
	 * <div class="it">Cliochinolo</div>
	 * <!-- @formatter:on -->
	 */
	CLIOQUINOL("387291001", "2.16.840.1.113883.6.96", "Clioquinol (substance)", "Clioquinol",
			"Clioquinol", "clioquinol", "Cliochinolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clobazam</div>
	 * <div class="de">Clobazam</div>
	 * <div class="fr">clobazam</div>
	 * <div class="it">Clobazam</div>
	 * <!-- @formatter:on -->
	 */
	CLOBAZAM("387572002", "2.16.840.1.113883.6.96", "Clobazam (substance)", "Clobazam", "Clobazam",
			"clobazam", "Clobazam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clobetasol</div>
	 * <div class="de">Clobetasol</div>
	 * <div class="fr">clobétasol</div>
	 * <div class="it">Clobetasolo</div>
	 * <!-- @formatter:on -->
	 */
	CLOBETASOL("419129004", "2.16.840.1.113883.6.96", "Clobetasol (substance)", "Clobetasol",
			"Clobetasol", "clobétasol", "Clobetasolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clobetasone</div>
	 * <div class="de">Clobetason</div>
	 * <div class="fr">clobétasone</div>
	 * <div class="it">Clobetasone</div>
	 * <!-- @formatter:on -->
	 */
	CLOBETASONE("395963000", "2.16.840.1.113883.6.96", "Clobetasone (substance)", "Clobetasone",
			"Clobetason", "clobétasone", "Clobetasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clofarabine</div>
	 * <div class="de">Clofarabin</div>
	 * <div class="fr">clofarabine</div>
	 * <div class="it">Clofarabina</div>
	 * <!-- @formatter:on -->
	 */
	CLOFARABINE("413873006", "2.16.840.1.113883.6.96", "Clofarabine (substance)", "Clofarabine",
			"Clofarabin", "clofarabine", "Clofarabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clofazimine</div>
	 * <div class="de">Clofazimin</div>
	 * <div class="fr">clofazimine</div>
	 * <div class="it">Clofazimina</div>
	 * <!-- @formatter:on -->
	 */
	CLOFAZIMINE("387410004", "2.16.840.1.113883.6.96", "Clofazimine (substance)", "Clofazimine",
			"Clofazimin", "clofazimine", "Clofazimina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clomethiazole</div>
	 * <div class="de">Clomethiazol</div>
	 * <div class="fr">clométhiazole</div>
	 * <div class="it">Clometiazolo</div>
	 * <!-- @formatter:on -->
	 */
	CLOMETHIAZOLE("395978007", "2.16.840.1.113883.6.96", "Clomethiazole (substance)",
			"Clomethiazole", "Clomethiazol", "clométhiazole", "Clometiazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clomipramine</div>
	 * <div class="de">Clomipramin</div>
	 * <div class="fr">clomipramine</div>
	 * <div class="it">Clomipramina</div>
	 * <!-- @formatter:on -->
	 */
	CLOMIPRAMINE("372903001", "2.16.840.1.113883.6.96", "Clomipramine (substance)", "Clomipramine",
			"Clomipramin", "clomipramine", "Clomipramina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clonazepam</div>
	 * <div class="de">Clonazepam</div>
	 * <div class="fr">clonazépam</div>
	 * <div class="it">Clonazepam</div>
	 * <!-- @formatter:on -->
	 */
	CLONAZEPAM("387383007", "2.16.840.1.113883.6.96", "Clonazepam (substance)", "Clonazepam",
			"Clonazepam", "clonazépam", "Clonazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clonidine</div>
	 * <div class="de">Clonidin</div>
	 * <div class="fr">clonidine</div>
	 * <div class="it">Clonidina</div>
	 * <!-- @formatter:on -->
	 */
	CLONIDINE("372805007", "2.16.840.1.113883.6.96", "Clonidine (substance)", "Clonidine",
			"Clonidin", "clonidine", "Clonidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clopidogrel</div>
	 * <div class="de">Clopidogrel</div>
	 * <div class="fr">clopidogrel</div>
	 * <div class="it">Clopidogrel</div>
	 * <!-- @formatter:on -->
	 */
	CLOPIDOGREL("386952008", "2.16.840.1.113883.6.96", "Clopidogrel (substance)", "Clopidogrel",
			"Clopidogrel", "clopidogrel", "Clopidogrel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clotiapine</div>
	 * <div class="de">Clotiapin</div>
	 * <div class="fr">clotiapine</div>
	 * <div class="it">Clotiapina</div>
	 * <!-- @formatter:on -->
	 */
	CLOTIAPINE("698028004", "2.16.840.1.113883.6.96", "Clotiapine (substance)", "Clotiapine",
			"Clotiapin", "clotiapine", "Clotiapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clotrimazole</div>
	 * <div class="de">Clotrimazol</div>
	 * <div class="fr">clotrimazole</div>
	 * <div class="it">Clotrimazolo</div>
	 * <!-- @formatter:on -->
	 */
	CLOTRIMAZOLE("387325003", "2.16.840.1.113883.6.96", "Clotrimazole (substance)", "Clotrimazole",
			"Clotrimazol", "clotrimazole", "Clotrimazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Clozapine</div>
	 * <div class="de">Clozapin</div>
	 * <div class="fr">clozapine</div>
	 * <div class="it">Clozapina</div>
	 * <!-- @formatter:on -->
	 */
	CLOZAPINE("387568001", "2.16.840.1.113883.6.96", "Clozapine (substance)", "Clozapine",
			"Clozapin", "clozapine", "Clozapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Coagulation factor II</div>
	 * <div class="de">Blutgerinnungsfaktor II human (Prothrombin)</div>
	 * <div class="fr">facteur II de coagulation humain (prothrombine)</div>
	 * <div class="it">Fattore II di coagulazione umano (protrombina)</div>
	 * <!-- @formatter:on -->
	 */
	COAGULATION_FACTOR_II("7348004", "2.16.840.1.113883.6.96", "Coagulation factor II (substance)",
			"Coagulation factor II", "Blutgerinnungsfaktor II human (Prothrombin)",
			"facteur II de coagulation humain (prothrombine)",
			"Fattore II di coagulazione umano (protrombina)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Coagulation factor IX</div>
	 * <div class="de">Blutgerinnungsfaktor IX human</div>
	 * <div class="fr">facteur IX de coagulation humain</div>
	 * <div class="it">Fattore IX di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	COAGULATION_FACTOR_IX("54378000", "2.16.840.1.113883.6.96", "Coagulation factor IX (substance)",
			"Coagulation factor IX", "Blutgerinnungsfaktor IX human",
			"facteur IX de coagulation humain", "Fattore IX di coagulazione umano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Coagulation factor VII</div>
	 * <div class="de">Blutgerinnungsfaktor VII human</div>
	 * <div class="fr">facteur VII de coagulation humain</div>
	 * <div class="it">Fattore VII di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	COAGULATION_FACTOR_VII("30804005", "2.16.840.1.113883.6.96",
			"Coagulation factor VII (substance)", "Coagulation factor VII",
			"Blutgerinnungsfaktor VII human", "facteur VII de coagulation humain",
			"Fattore VII di coagulazione umano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Coagulation factor X</div>
	 * <div class="de">Blutgerinnungsfaktor X human</div>
	 * <div class="fr">facteur X de coagulation humain</div>
	 * <div class="it">Fattore X di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	COAGULATION_FACTOR_X("81444003", "2.16.840.1.113883.6.96", "Coagulation factor X (substance)",
			"Coagulation factor X", "Blutgerinnungsfaktor X human",
			"facteur X de coagulation humain", "Fattore X di coagulazione umano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Coagulation factor XIII</div>
	 * <div class="de">Blutgerinnungsfaktor XIII human</div>
	 * <div class="fr">facteur XIII de coagulation humain</div>
	 * <div class="it">Fattore XIII di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	COAGULATION_FACTOR_XIII("51161000", "2.16.840.1.113883.6.96",
			"Coagulation factor XIII (substance)", "Coagulation factor XIII",
			"Blutgerinnungsfaktor XIII human", "facteur XIII de coagulation humain",
			"Fattore XIII di coagulazione umano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cobicistat</div>
	 * <div class="de">Cobicistat</div>
	 * <div class="fr">cobicistat</div>
	 * <div class="it">Cobicistat</div>
	 * <!-- @formatter:on -->
	 */
	COBICISTAT("710109003", "2.16.840.1.113883.6.96", "Cobicistat (substance)", "Cobicistat",
			"Cobicistat", "cobicistat", "Cobicistat"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cocaine</div>
	 * <div class="de">Cocain</div>
	 * <div class="fr">cocaïne</div>
	 * <div class="it">Cocaina</div>
	 * <!-- @formatter:on -->
	 */
	COCAINE("387085005", "2.16.840.1.113883.6.96", "Cocaine (substance)", "Cocaine", "Cocain",
			"cocaïne", "Cocaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Codeine</div>
	 * <div class="de">Codein</div>
	 * <div class="fr">codéine</div>
	 * <div class="it">Codeina</div>
	 * <!-- @formatter:on -->
	 */
	CODEINE("387494007", "2.16.840.1.113883.6.96", "Codeine (substance)", "Codeine", "Codein",
			"codéine", "Codeina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Codeine phosphate hemihydrate</div>
	 * <div class="de">Codein phosphat hemihydrat</div>
	 * <div class="fr">codéine phosphate hémihydrate</div>
	 * <div class="it">Codeina fosfato emiidrato</div>
	 * <!-- @formatter:on -->
	 */
	CODEINE_PHOSPHATE_HEMIHYDRATE("725666006", "2.16.840.1.113883.6.96",
			"Codeine phosphate hemihydrate (substance)", "Codeine phosphate hemihydrate",
			"Codein phosphat hemihydrat", "codéine phosphate hémihydrate",
			"Codeina fosfato emiidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Colchicine</div>
	 * <div class="de">Colchicin</div>
	 * <div class="fr">colchicine</div>
	 * <div class="it">Colchicina</div>
	 * <!-- @formatter:on -->
	 */
	COLCHICINE("387413002", "2.16.840.1.113883.6.96", "Colchicine (substance)", "Colchicine",
			"Colchicin", "colchicine", "Colchicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Colecalciferol</div>
	 * <div class="de">Colecalciferol (Vitamin D3)</div>
	 * <div class="fr">colécalciférol (Vitamine D3)</div>
	 * <div class="it">Colecalciferolo</div>
	 * <!-- @formatter:on -->
	 */
	COLECALCIFEROL("18414002", "2.16.840.1.113883.6.96", "Colecalciferol (substance)",
			"Colecalciferol", "Colecalciferol (Vitamin D3)", "colécalciférol (Vitamine D3)",
			"Colecalciferolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Colestyramine</div>
	 * <div class="de">Colestyramin</div>
	 * <div class="fr">colestyramine</div>
	 * <div class="it">Colestiramina</div>
	 * <!-- @formatter:on -->
	 */
	COLESTYRAMINE("387408001", "2.16.840.1.113883.6.96", "Colestyramine (substance)",
			"Colestyramine", "Colestyramin", "colestyramine", "Colestiramina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Colistin</div>
	 * <div class="de">Colistin</div>
	 * <div class="fr">colistine</div>
	 * <div class="it">Colistina</div>
	 * <!-- @formatter:on -->
	 */
	COLISTIN("387412007", "2.16.840.1.113883.6.96", "Colistin (substance)", "Colistin", "Colistin",
			"colistine", "Colistina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Copper sulfate</div>
	 * <div class="de">Kupfer(II)-sulfat, wasserfreies</div>
	 * <div class="fr">cuivre sulfate</div>
	 * <div class="it">Rame solfato</div>
	 * <!-- @formatter:on -->
	 */
	COPPER_SULFATE("70168001", "2.16.840.1.113883.6.96", "Copper sulfate (substance)",
			"Copper sulfate", "Kupfer(II)-sulfat, wasserfreies", "cuivre sulfate", "Rame solfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cromoglicate sodium</div>
	 * <div class="de">Cromoglicinsäure, Dinatriumsalz</div>
	 * <div class="fr">cromoglicate sodique</div>
	 * <div class="it">Sodio cromoglicato</div>
	 * <!-- @formatter:on -->
	 */
	CROMOGLICATE_SODIUM("387221005", "2.16.840.1.113883.6.96", "Cromoglicate sodium (substance)",
			"Cromoglicate sodium", "Cromoglicinsäure, Dinatriumsalz", "cromoglicate sodique",
			"Sodio cromoglicato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cromoglicic acid</div>
	 * <div class="de">Cromoglicinsäure</div>
	 * <div class="fr">acide cromoglicique</div>
	 * <div class="it">Acido cromoglicico</div>
	 * <!-- @formatter:on -->
	 */
	CROMOGLICIC_ACID("372672009", "2.16.840.1.113883.6.96", "Cromoglicic acid (substance)",
			"Cromoglicic acid", "Cromoglicinsäure", "acide cromoglicique", "Acido cromoglicico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cyanocobalamin</div>
	 * <div class="de">Cyanocobalamin (Vitamin B12)</div>
	 * <div class="fr">cyanocobalamine (Vitamine B12)</div>
	 * <div class="it">Cianocobalamina</div>
	 * <!-- @formatter:on -->
	 */
	CYANOCOBALAMIN("419382002", "2.16.840.1.113883.6.96", "Cyanocobalamin (substance)",
			"Cyanocobalamin", "Cyanocobalamin (Vitamin B12)", "cyanocobalamine (Vitamine B12)",
			"Cianocobalamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cyclophosphamide</div>
	 * <div class="de">Cyclophosphamid</div>
	 * <div class="fr">cyclophosphamide</div>
	 * <div class="it">Ciclofosfamide</div>
	 * <!-- @formatter:on -->
	 */
	CYCLOPHOSPHAMIDE("387420009", "2.16.840.1.113883.6.96", "Cyclophosphamide (substance)",
			"Cyclophosphamide", "Cyclophosphamid", "cyclophosphamide", "Ciclofosfamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cycloserine</div>
	 * <div class="de">Cycloserin</div>
	 * <div class="fr">cycloserine</div>
	 * <div class="it">Cicloserina</div>
	 * <!-- @formatter:on -->
	 */
	CYCLOSERINE("387282000", "2.16.840.1.113883.6.96", "Cycloserine (substance)", "Cycloserine",
			"Cycloserin", "cycloserine", "Cicloserina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cyproterone</div>
	 * <div class="de">Cyproteron</div>
	 * <div class="fr">cyprotérone</div>
	 * <div class="it">Ciproterone</div>
	 * <!-- @formatter:on -->
	 */
	CYPROTERONE("126119006", "2.16.840.1.113883.6.96", "Cyproterone (substance)", "Cyproterone",
			"Cyproteron", "cyprotérone", "Ciproterone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cytarabine</div>
	 * <div class="de">Cytarabin</div>
	 * <div class="fr">cytarabine</div>
	 * <div class="it">Citarabina</div>
	 * <!-- @formatter:on -->
	 */
	CYTARABINE("387511003", "2.16.840.1.113883.6.96", "Cytarabine (substance)", "Cytarabine",
			"Cytarabin", "cytarabine", "Citarabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Cytomegalovirus antibody</div>
	 * <div class="de">Cytomegalie-Immunglobulin human</div>
	 * <div class="fr">immunoglobuline humaine anti cytomégalovirus</div>
	 * <div class="it">Immunoglobulina umana anti-citomegalovirus</div>
	 * <!-- @formatter:on -->
	 */
	CYTOMEGALOVIRUS_ANTIBODY("120941004", "2.16.840.1.113883.6.96",
			"Cytomegalovirus antibody (substance)", "Cytomegalovirus antibody",
			"Cytomegalie-Immunglobulin human", "immunoglobuline humaine anti cytomégalovirus",
			"Immunoglobulina umana anti-citomegalovirus"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dabigatran etexilate</div>
	 * <div class="de">Dabigatran etexilat</div>
	 * <div class="fr">dabigatran étexilate</div>
	 * <div class="it">Dabigratan etexilato</div>
	 * <!-- @formatter:on -->
	 */
	DABIGATRAN_ETEXILATE("700029008", "2.16.840.1.113883.6.96", "Dabigatran etexilate (substance)",
			"Dabigatran etexilate", "Dabigatran etexilat", "dabigatran étexilate",
			"Dabigratan etexilato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dabrafenib</div>
	 * <div class="de">Dabrafenib</div>
	 * <div class="fr">dabrafénib</div>
	 * <div class="it">Dabrafenib</div>
	 * <!-- @formatter:on -->
	 */
	DABRAFENIB("703641001", "2.16.840.1.113883.6.96", "Dabrafenib (substance)", "Dabrafenib",
			"Dabrafenib", "dabrafénib", "Dabrafenib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dacarbazine</div>
	 * <div class="de">Dacarbazin</div>
	 * <div class="fr">dacarbazine</div>
	 * <div class="it">Dacarbazina</div>
	 * <!-- @formatter:on -->
	 */
	DACARBAZINE("387441003", "2.16.840.1.113883.6.96", "Dacarbazine (substance)", "Dacarbazine",
			"Dacarbazin", "dacarbazine", "Dacarbazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Daclatasvir</div>
	 * <div class="de">Daclatasvir</div>
	 * <div class="fr">daclatasvir</div>
	 * <div class="it">Daclatasvir</div>
	 * <!-- @formatter:on -->
	 */
	DACLATASVIR("712519008", "2.16.840.1.113883.6.96", "Daclatasvir (substance)", "Daclatasvir",
			"Daclatasvir", "daclatasvir", "Daclatasvir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dactinomycin</div>
	 * <div class="de">Dactinomycin</div>
	 * <div class="fr">dactinomycine</div>
	 * <div class="it">Dactinomicina</div>
	 * <!-- @formatter:on -->
	 */
	DACTINOMYCIN("387353003", "2.16.840.1.113883.6.96", "Dactinomycin (substance)", "Dactinomycin",
			"Dactinomycin", "dactinomycine", "Dactinomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dalteparin sodium</div>
	 * <div class="de">Dalteparin natrium</div>
	 * <div class="fr">daltéparine sodique</div>
	 * <div class="it">Dalteparina sodica</div>
	 * <!-- @formatter:on -->
	 */
	DALTEPARIN_SODIUM("108987000", "2.16.840.1.113883.6.96", "Dalteparin sodium (substance)",
			"Dalteparin sodium", "Dalteparin natrium", "daltéparine sodique",
			"Dalteparina sodica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Danaparoid</div>
	 * <div class="de">Danaparoid</div>
	 * <div class="fr">danaparoïde</div>
	 * <div class="it">Danaparoid</div>
	 * <!-- @formatter:on -->
	 */
	DANAPAROID("372564002", "2.16.840.1.113883.6.96", "Danaparoid (substance)", "Danaparoid",
			"Danaparoid", "danaparoïde", "Danaparoid"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dantrolene</div>
	 * <div class="de">Dantrolen</div>
	 * <div class="fr">dantrolène</div>
	 * <div class="it">Dantrolene</div>
	 * <!-- @formatter:on -->
	 */
	DANTROLENE("372819007", "2.16.840.1.113883.6.96", "Dantrolene (substance)", "Dantrolene",
			"Dantrolen", "dantrolène", "Dantrolene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dapagliflozin</div>
	 * <div class="de">Dapagliflozin</div>
	 * <div class="fr">dapagliflozine</div>
	 * <div class="it">Dapaglifozin</div>
	 * <!-- @formatter:on -->
	 */
	DAPAGLIFLOZIN("703674001", "2.16.840.1.113883.6.96", "Dapagliflozin (substance)",
			"Dapagliflozin", "Dapagliflozin", "dapagliflozine", "Dapaglifozin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dapoxetine</div>
	 * <div class="de">Dapoxetin</div>
	 * <div class="fr">dapoxétine</div>
	 * <div class="it">Dapoxetina</div>
	 * <!-- @formatter:on -->
	 */
	DAPOXETINE("702794006", "2.16.840.1.113883.6.96", "Dapoxetine (substance)", "Dapoxetine",
			"Dapoxetin", "dapoxétine", "Dapoxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Daptomycin</div>
	 * <div class="de">Daptomycin</div>
	 * <div class="fr">daptomycine</div>
	 * <div class="it">Daptomicina</div>
	 * <!-- @formatter:on -->
	 */
	DAPTOMYCIN("406439009", "2.16.840.1.113883.6.96", "Daptomycin (substance)", "Daptomycin",
			"Daptomycin", "daptomycine", "Daptomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Daratumumab</div>
	 * <div class="de">Daratumumab</div>
	 * <div class="fr">daratumumab</div>
	 * <div class="it">Daratumumab</div>
	 * <!-- @formatter:on -->
	 */
	DARATUMUMAB("716016006", "2.16.840.1.113883.6.96", "Daratumumab (substance)", "Daratumumab",
			"Daratumumab", "daratumumab", "Daratumumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Darbepoetin alfa</div>
	 * <div class="de">Darbepoetin alfa</div>
	 * <div class="fr">darbépoétine alfa</div>
	 * <div class="it">Darbeaoetina alfa</div>
	 * <!-- @formatter:on -->
	 */
	DARBEPOETIN_ALFA("385608005", "2.16.840.1.113883.6.96", "Darbepoetin alfa (substance)",
			"Darbepoetin alfa", "Darbepoetin alfa", "darbépoétine alfa", "Darbeaoetina alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Darifenacin</div>
	 * <div class="de">Darifenacin</div>
	 * <div class="fr">darifénacine</div>
	 * <div class="it">Darifenacina</div>
	 * <!-- @formatter:on -->
	 */
	DARIFENACIN("416140008", "2.16.840.1.113883.6.96", "Darifenacin (substance)", "Darifenacin",
			"Darifenacin", "darifénacine", "Darifenacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Darunavir</div>
	 * <div class="de">Darunavir</div>
	 * <div class="fr">darunavir</div>
	 * <div class="it">Darunavir</div>
	 * <!-- @formatter:on -->
	 */
	DARUNAVIR("423888002", "2.16.840.1.113883.6.96", "Darunavir (substance)", "Darunavir",
			"Darunavir", "darunavir", "Darunavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dasatinib</div>
	 * <div class="de">Dasatinib</div>
	 * <div class="fr">dasatinib</div>
	 * <div class="it">Dasatinib</div>
	 * <!-- @formatter:on -->
	 */
	DASATINIB("423658008", "2.16.840.1.113883.6.96", "Dasatinib (substance)", "Dasatinib",
			"Dasatinib", "dasatinib", "Dasatinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Daunorubicin</div>
	 * <div class="de">Daunorubicin</div>
	 * <div class="fr">daunorubicine</div>
	 * <div class="it">Daunorubicina</div>
	 * <!-- @formatter:on -->
	 */
	DAUNORUBICIN("372715008", "2.16.840.1.113883.6.96", "Daunorubicin (substance)", "Daunorubicin",
			"Daunorubicin", "daunorubicine", "Daunorubicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Decitabine</div>
	 * <div class="de">Decitabin</div>
	 * <div class="fr">décitabine</div>
	 * <div class="it">Decitabina</div>
	 * <!-- @formatter:on -->
	 */
	DECITABINE("420759005", "2.16.840.1.113883.6.96", "Decitabine (substance)", "Decitabine",
			"Decitabin", "décitabine", "Decitabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Deferasirox</div>
	 * <div class="de">Deferasirox</div>
	 * <div class="fr">déférasirox</div>
	 * <div class="it">Deferasirox</div>
	 * <!-- @formatter:on -->
	 */
	DEFERASIROX("419985007", "2.16.840.1.113883.6.96", "Deferasirox (substance)", "Deferasirox",
			"Deferasirox", "déférasirox", "Deferasirox"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Deferiprone</div>
	 * <div class="de">Deferipron</div>
	 * <div class="fr">défériprone</div>
	 * <div class="it">Deferiprone</div>
	 * <!-- @formatter:on -->
	 */
	DEFERIPRONE("396011004", "2.16.840.1.113883.6.96", "Deferiprone (substance)", "Deferiprone",
			"Deferipron", "défériprone", "Deferiprone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Deferoxamine</div>
	 * <div class="de">Deferoxamin</div>
	 * <div class="fr">déféroxamine</div>
	 * <div class="it">Deferoxamina</div>
	 * <!-- @formatter:on -->
	 */
	DEFEROXAMINE("372825006", "2.16.840.1.113883.6.96", "Deferoxamine (substance)", "Deferoxamine",
			"Deferoxamin", "déféroxamine", "Deferoxamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Defibrotide</div>
	 * <div class="de">Defibrotid</div>
	 * <div class="fr">défibrotide</div>
	 * <div class="it">Defibrotide</div>
	 * <!-- @formatter:on -->
	 */
	DEFIBROTIDE("442263003", "2.16.840.1.113883.6.96", "Defibrotide (substance)", "Defibrotide",
			"Defibrotid", "défibrotide", "Defibrotide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Deflazacort</div>
	 * <div class="de">Deflazacort</div>
	 * <div class="fr">déflazacort</div>
	 * <div class="it">Deflazacort</div>
	 * <!-- @formatter:on -->
	 */
	DEFLAZACORT("396012006", "2.16.840.1.113883.6.96", "Deflazacort (substance)", "Deflazacort",
			"Deflazacort", "déflazacort", "Deflazacort"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Degarelix</div>
	 * <div class="de">Degarelix</div>
	 * <div class="fr">dégarélix</div>
	 * <div class="it">Degarelix</div>
	 * <!-- @formatter:on -->
	 */
	DEGARELIX("441864003", "2.16.840.1.113883.6.96", "Degarelix (substance)", "Degarelix",
			"Degarelix", "dégarélix", "Degarelix"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Delta-9-tetrahydrocannabinol</div>
	 * <div class="de">Delta-9-Tetrahydrocannabinol (THC)</div>
	 * <div class="fr">delta-9-tétrahydrocannabinol (THC)</div>
	 * <div class="it">Delta-9-tetracannabinolo (THC)</div>
	 * <!-- @formatter:on -->
	 */
	DELTA_9_TETRAHYDROCANNABINOL("96225007", "2.16.840.1.113883.6.96",
			"Delta-9-tetrahydrocannabinol (substance)", "Delta-9-tetrahydrocannabinol",
			"Delta-9-Tetrahydrocannabinol (THC)", "delta-9-tétrahydrocannabinol (THC)",
			"Delta-9-tetracannabinolo (THC)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Denosumab</div>
	 * <div class="de">Denosumab</div>
	 * <div class="fr">dénosumab</div>
	 * <div class="it">Denosumab</div>
	 * <!-- @formatter:on -->
	 */
	DENOSUMAB("446321003", "2.16.840.1.113883.6.96", "Denosumab (substance)", "Denosumab",
			"Denosumab", "dénosumab", "Denosumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Desflurane</div>
	 * <div class="de">Desfluran</div>
	 * <div class="fr">desflurane</div>
	 * <div class="it">Desflurano</div>
	 * <!-- @formatter:on -->
	 */
	DESFLURANE("386841003", "2.16.840.1.113883.6.96", "Desflurane (substance)", "Desflurane",
			"Desfluran", "desflurane", "Desflurano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Desloratadine</div>
	 * <div class="de">Desloratadin</div>
	 * <div class="fr">desloratadine</div>
	 * <div class="it">Desloratadina</div>
	 * <!-- @formatter:on -->
	 */
	DESLORATADINE("396015008", "2.16.840.1.113883.6.96", "Desloratadine (substance)",
			"Desloratadine", "Desloratadin", "desloratadine", "Desloratadina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Desmopressin</div>
	 * <div class="de">Desmopressin</div>
	 * <div class="fr">desmopressine</div>
	 * <div class="it">Desmopressina</div>
	 * <!-- @formatter:on -->
	 */
	DESMOPRESSIN("126189002", "2.16.840.1.113883.6.96", "Desmopressin (substance)", "Desmopressin",
			"Desmopressin", "desmopressine", "Desmopressina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Desogestrel</div>
	 * <div class="de">Desogestrel</div>
	 * <div class="fr">désogestrel</div>
	 * <div class="it">Desogestrel</div>
	 * <!-- @formatter:on -->
	 */
	DESOGESTREL("126108008", "2.16.840.1.113883.6.96", "Desogestrel (substance)", "Desogestrel",
			"Desogestrel", "désogestrel", "Desogestrel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexamethasone</div>
	 * <div class="de">Dexamethason</div>
	 * <div class="fr">dexaméthasone</div>
	 * <div class="it">Desametasone</div>
	 * <!-- @formatter:on -->
	 */
	DEXAMETHASONE("372584003", "2.16.840.1.113883.6.96", "Dexamethasone (substance)",
			"Dexamethasone", "Dexamethason", "dexaméthasone", "Desametasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexamfetamine</div>
	 * <div class="de">Dexamfetamin</div>
	 * <div class="fr">dexamfétamine</div>
	 * <div class="it">Dexamfetamina</div>
	 * <!-- @formatter:on -->
	 */
	DEXAMFETAMINE("387278002", "2.16.840.1.113883.6.96", "Dexamfetamine (substance)",
			"Dexamfetamine", "Dexamfetamin", "dexamfétamine", "Dexamfetamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexibuprofen</div>
	 * <div class="de">Dexibuprofen</div>
	 * <div class="fr">dexibuprofène</div>
	 * <div class="it">Dexibuprofene</div>
	 * <!-- @formatter:on -->
	 */
	DEXIBUPROFEN("418868002", "2.16.840.1.113883.6.96", "Dexibuprofen (substance)", "Dexibuprofen",
			"Dexibuprofen", "dexibuprofène", "Dexibuprofene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexketoprofen</div>
	 * <div class="de">Dexketoprofen</div>
	 * <div class="fr">dexkétoprofène</div>
	 * <div class="it">Desketoprofene</div>
	 * <!-- @formatter:on -->
	 */
	DEXKETOPROFEN("396018005", "2.16.840.1.113883.6.96", "Dexketoprofen (substance)",
			"Dexketoprofen", "Dexketoprofen", "dexkétoprofène", "Desketoprofene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexlansoprazole</div>
	 * <div class="de">Dexlansoprazol</div>
	 * <div class="fr">dexlansoprazole</div>
	 * <div class="it">Dexlansoprazolo</div>
	 * <!-- @formatter:on -->
	 */
	DEXLANSOPRAZOLE("441863009", "2.16.840.1.113883.6.96", "Dexlansoprazole (substance)",
			"Dexlansoprazole", "Dexlansoprazol", "dexlansoprazole", "Dexlansoprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexmedetomidine</div>
	 * <div class="de">Dexmedetomidin</div>
	 * <div class="fr">dexmédétomidine</div>
	 * <div class="it">Dexmedetomidina</div>
	 * <!-- @formatter:on -->
	 */
	DEXMEDETOMIDINE("437750002", "2.16.840.1.113883.6.96", "Dexmedetomidine (substance)",
			"Dexmedetomidine", "Dexmedetomidin", "dexmédétomidine", "Dexmedetomidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexmethylphenidate</div>
	 * <div class="de">Dexmethylphenidat</div>
	 * <div class="fr">dexméthylphénidate</div>
	 * <div class="it">Dexmetilfenidato</div>
	 * <!-- @formatter:on -->
	 */
	DEXMETHYLPHENIDATE("767715008", "2.16.840.1.113883.6.96", "Dexmethylphenidate (substance)",
			"Dexmethylphenidate", "Dexmethylphenidat", "dexméthylphénidate", "Dexmetilfenidato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexpanthenol</div>
	 * <div class="de">Dexpanthenol</div>
	 * <div class="fr">dexpanthénol</div>
	 * <div class="it">Despantenolo</div>
	 * <!-- @formatter:on -->
	 */
	DEXPANTHENOL("126226000", "2.16.840.1.113883.6.96", "Dexpanthenol (substance)", "Dexpanthenol",
			"Dexpanthenol", "dexpanthénol", "Despantenolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dexrazoxane</div>
	 * <div class="de">Dexrazoxan</div>
	 * <div class="fr">dexrazoxane</div>
	 * <div class="it">Dexrazoxano</div>
	 * <!-- @formatter:on -->
	 */
	DEXRAZOXANE("108825009", "2.16.840.1.113883.6.96", "Dexrazoxane (substance)", "Dexrazoxane",
			"Dexrazoxan", "dexrazoxane", "Dexrazoxano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dextromethorphan</div>
	 * <div class="de">Dextromethorphan</div>
	 * <div class="fr">dextrométhorphane</div>
	 * <div class="it">Destrometorfano</div>
	 * <!-- @formatter:on -->
	 */
	DEXTROMETHORPHAN("387114001", "2.16.840.1.113883.6.96", "Dextromethorphan (substance)",
			"Dextromethorphan", "Dextromethorphan", "dextrométhorphane", "Destrometorfano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diamorphine</div>
	 * <div class="de">Heroin</div>
	 * <div class="fr">héroïne</div>
	 * <div class="it">Eroina</div>
	 * <!-- @formatter:on -->
	 */
	DIAMORPHINE("387341002", "2.16.840.1.113883.6.96", "Diamorphine (substance)", "Diamorphine",
			"Heroin", "héroïne", "Eroina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diazepam</div>
	 * <div class="de">Diazepam</div>
	 * <div class="fr">diazépam</div>
	 * <div class="it">Diazepam</div>
	 * <!-- @formatter:on -->
	 */
	DIAZEPAM("387264003", "2.16.840.1.113883.6.96", "Diazepam (substance)", "Diazepam", "Diazepam",
			"diazépam", "Diazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diclofenac</div>
	 * <div class="de">Diclofenac</div>
	 * <div class="fr">diclofénac</div>
	 * <div class="it">Diclofenac</div>
	 * <!-- @formatter:on -->
	 */
	DICLOFENAC("7034005", "2.16.840.1.113883.6.96", "Diclofenac (substance)", "Diclofenac",
			"Diclofenac", "diclofénac", "Diclofenac"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dienogest</div>
	 * <div class="de">Dienogest</div>
	 * <div class="fr">diénogest</div>
	 * <div class="it">Dienogest</div>
	 * <!-- @formatter:on -->
	 */
	DIENOGEST("703097002", "2.16.840.1.113883.6.96", "Dienogest (substance)", "Dienogest",
			"Dienogest", "diénogest", "Dienogest"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diflucortolone</div>
	 * <div class="de">Diflucortolon</div>
	 * <div class="fr">diflucortolone</div>
	 * <div class="it">Diflucortolone</div>
	 * <!-- @formatter:on -->
	 */
	DIFLUCORTOLONE("395965007", "2.16.840.1.113883.6.96", "Diflucortolone (substance)",
			"Diflucortolone", "Diflucortolon", "diflucortolone", "Diflucortolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Digitoxin</div>
	 * <div class="de">Digitoxin</div>
	 * <div class="fr">digitoxine</div>
	 * <div class="it">Digitossina</div>
	 * <!-- @formatter:on -->
	 */
	DIGITOXIN("373534001", "2.16.840.1.113883.6.96", "Digitoxin (substance)", "Digitoxin",
			"Digitoxin", "digitoxine", "Digitossina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Digoxin</div>
	 * <div class="de">Digoxin</div>
	 * <div class="fr">digoxine</div>
	 * <div class="it">Digossina</div>
	 * <!-- @formatter:on -->
	 */
	DIGOXIN("387461009", "2.16.840.1.113883.6.96", "Digoxin (substance)", "Digoxin", "Digoxin",
			"digoxine", "Digossina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dihydralazine</div>
	 * <div class="de">Dihydralazin</div>
	 * <div class="fr">dihydralazine</div>
	 * <div class="it">Diidralazina</div>
	 * <!-- @formatter:on -->
	 */
	DIHYDRALAZINE("703113001", "2.16.840.1.113883.6.96", "Dihydralazine (substance)",
			"Dihydralazine", "Dihydralazin", "dihydralazine", "Diidralazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dihydrocodeine</div>
	 * <div class="de">Dihydrocodein</div>
	 * <div class="fr">dihydrocodéine</div>
	 * <div class="it">Diidrocodeina</div>
	 * <!-- @formatter:on -->
	 */
	DIHYDROCODEINE("387322000", "2.16.840.1.113883.6.96", "Dihydrocodeine (substance)",
			"Dihydrocodeine", "Dihydrocodein", "dihydrocodéine", "Diidrocodeina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diltiazem</div>
	 * <div class="de">Diltiazem</div>
	 * <div class="fr">diltiazem</div>
	 * <div class="it">Diltiazem</div>
	 * <!-- @formatter:on -->
	 */
	DILTIAZEM("372793000", "2.16.840.1.113883.6.96", "Diltiazem (substance)", "Diltiazem",
			"Diltiazem", "diltiazem", "Diltiazem"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dimenhydrinate</div>
	 * <div class="de">Dimenhydrinat</div>
	 * <div class="fr">diménhydrinate</div>
	 * <div class="it">Dimenidrinato</div>
	 * <!-- @formatter:on -->
	 */
	DIMENHYDRINATE("387469006", "2.16.840.1.113883.6.96", "Dimenhydrinate (substance)",
			"Dimenhydrinate", "Dimenhydrinat", "diménhydrinate", "Dimenidrinato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dimethyl sulfoxide</div>
	 * <div class="de">Dimethylsulfoxid</div>
	 * <div class="fr">diméthylsulfoxyde</div>
	 * <div class="it">Dimetilsolfossido (DMSO)</div>
	 * <!-- @formatter:on -->
	 */
	DIMETHYL_SULFOXIDE("115535002", "2.16.840.1.113883.6.96", "Dimethyl sulfoxide (substance)",
			"Dimethyl sulfoxide", "Dimethylsulfoxid", "diméthylsulfoxyde",
			"Dimetilsolfossido (DMSO)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dimeticone</div>
	 * <div class="de">Dimeticon</div>
	 * <div class="fr">diméticone</div>
	 * <div class="it">Dimeticone</div>
	 * <!-- @formatter:on -->
	 */
	DIMETICONE("396031000", "2.16.840.1.113883.6.96", "Dimeticone (substance)", "Dimeticone",
			"Dimeticon", "diméticone", "Dimeticone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dimetindene</div>
	 * <div class="de">Dimetinden</div>
	 * <div class="fr">dimétindène</div>
	 * <div class="it">Dimetindene</div>
	 * <!-- @formatter:on -->
	 */
	DIMETINDENE("387142004", "2.16.840.1.113883.6.96", "Dimetindene (substance)", "Dimetindene",
			"Dimetinden", "dimétindène", "Dimetindene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dinoprostone</div>
	 * <div class="de">Dinoproston</div>
	 * <div class="fr">dinoprostone</div>
	 * <div class="it">Dinoprostone</div>
	 * <!-- @formatter:on -->
	 */
	DINOPROSTONE("387245009", "2.16.840.1.113883.6.96", "Dinoprostone (substance)", "Dinoprostone",
			"Dinoproston", "dinoprostone", "Dinoprostone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diosmin</div>
	 * <div class="de">Diosmin</div>
	 * <div class="fr">diosmine</div>
	 * <div class="it">Diosmina</div>
	 * <!-- @formatter:on -->
	 */
	DIOSMIN("8143001", "2.16.840.1.113883.6.96", "Diosmin (substance)", "Diosmin", "Diosmin",
			"diosmine", "Diosmina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diphenhydramine</div>
	 * <div class="de">Diphenhydramin</div>
	 * <div class="fr">diphénhydramine</div>
	 * <div class="it">Difenidramina</div>
	 * <!-- @formatter:on -->
	 */
	DIPHENHYDRAMINE("372682005", "2.16.840.1.113883.6.96", "Diphenhydramine (substance)",
			"Diphenhydramine", "Diphenhydramin", "diphénhydramine", "Difenidramina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Diphtheria vaccine</div>
	 * <div class="de">Diphtherie-Impfstoff</div>
	 * <div class="fr">diphtérie vaccin</div>
	 * <div class="it">Difterite vaccino</div>
	 * <!-- @formatter:on -->
	 */
	DIPHTHERIA_VACCINE("428126001", "2.16.840.1.113883.6.96", "Diphtheria vaccine (substance)",
			"Diphtheria vaccine", "Diphtherie-Impfstoff", "diphtérie vaccin", "Difterite vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dipotassium clorazepate</div>
	 * <div class="de">Dikalium clorazepat</div>
	 * <div class="fr">clorazépate dipotassique</div>
	 * <div class="it">Clorazepato potassico</div>
	 * <!-- @formatter:on -->
	 */
	DIPOTASSIUM_CLORAZEPATE("387453004", "2.16.840.1.113883.6.96",
			"Dipotassium clorazepate (substance)", "Dipotassium clorazepate", "Dikalium clorazepat",
			"clorazépate dipotassique", "Clorazepato potassico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Disulfiram</div>
	 * <div class="de">Disulfiram</div>
	 * <div class="fr">disulfirame</div>
	 * <div class="it">Disulfiram</div>
	 * <!-- @formatter:on -->
	 */
	DISULFIRAM("387212009", "2.16.840.1.113883.6.96", "Disulfiram (substance)", "Disulfiram",
			"Disulfiram", "disulfirame", "Disulfiram"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dobesilate calcium</div>
	 * <div class="de">Calcium dobesilat</div>
	 * <div class="fr">dobésilate de calcium</div>
	 * <div class="it">Calcio dobesilato</div>
	 * <!-- @formatter:on -->
	 */
	DOBESILATE_CALCIUM("83438009", "2.16.840.1.113883.6.96", "Dobesilate calcium (substance)",
			"Dobesilate calcium", "Calcium dobesilat", "dobésilate de calcium",
			"Calcio dobesilato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dobutamine</div>
	 * <div class="de">Dobutamin</div>
	 * <div class="fr">dobutamine</div>
	 * <div class="it">Dobutamina</div>
	 * <!-- @formatter:on -->
	 */
	DOBUTAMINE("387145002", "2.16.840.1.113883.6.96", "Dobutamine (substance)", "Dobutamine",
			"Dobutamin", "dobutamine", "Dobutamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Docetaxel</div>
	 * <div class="de">Docetaxel</div>
	 * <div class="fr">docétaxel</div>
	 * <div class="it">Docetaxel</div>
	 * <!-- @formatter:on -->
	 */
	DOCETAXEL("386918005", "2.16.840.1.113883.6.96", "Docetaxel (substance)", "Docetaxel",
			"Docetaxel", "docétaxel", "Docetaxel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Docosahexaenoic acid</div>
	 * <div class="de">Docosahexaensäure DHA</div>
	 * <div class="fr">acide docosahexaénoïque DHA</div>
	 * <div class="it">Acido docosaesaenoico (DHA)</div>
	 * <!-- @formatter:on -->
	 */
	DOCOSAHEXAENOIC_ACID("226368001", "2.16.840.1.113883.6.96", "Docosahexaenoic acid (substance)",
			"Docosahexaenoic acid", "Docosahexaensäure DHA", "acide docosahexaénoïque DHA",
			"Acido docosaesaenoico (DHA)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dolutegravir</div>
	 * <div class="de">Dolutegravir</div>
	 * <div class="fr">dolutégravir</div>
	 * <div class="it">Dolutegravir</div>
	 * <!-- @formatter:on -->
	 */
	DOLUTEGRAVIR("713464000", "2.16.840.1.113883.6.96", "Dolutegravir (substance)", "Dolutegravir",
			"Dolutegravir", "dolutégravir", "Dolutegravir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Domperidone</div>
	 * <div class="de">Domperidon</div>
	 * <div class="fr">dompéridone</div>
	 * <div class="it">Domperidone</div>
	 * <!-- @formatter:on -->
	 */
	DOMPERIDONE("387181004", "2.16.840.1.113883.6.96", "Domperidone (substance)", "Domperidone",
			"Domperidon", "dompéridone", "Domperidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Donepezil</div>
	 * <div class="de">Donepezil</div>
	 * <div class="fr">donépézil</div>
	 * <div class="it">Donepezil</div>
	 * <!-- @formatter:on -->
	 */
	DONEPEZIL("386855006", "2.16.840.1.113883.6.96", "Donepezil (substance)", "Donepezil",
			"Donepezil", "donépézil", "Donepezil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dopamine</div>
	 * <div class="de">Dopamin</div>
	 * <div class="fr">dopamine</div>
	 * <div class="it">Dopamina</div>
	 * <!-- @formatter:on -->
	 */
	DOPAMINE("412383006", "2.16.840.1.113883.6.96", "Dopamine (substance)", "Dopamine", "Dopamin",
			"dopamine", "Dopamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dornase alfa</div>
	 * <div class="de">Dornase alfa</div>
	 * <div class="fr">dornase alfa</div>
	 * <div class="it">Dornase alfa</div>
	 * <!-- @formatter:on -->
	 */
	DORNASE_ALFA("386882003", "2.16.840.1.113883.6.96", "Dornase alfa (substance)", "Dornase alfa",
			"Dornase alfa", "dornase alfa", "Dornase alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dorzolamide</div>
	 * <div class="de">Dorzolamid</div>
	 * <div class="fr">dorzolamide</div>
	 * <div class="it">Dorzolamide</div>
	 * <!-- @formatter:on -->
	 */
	DORZOLAMIDE("373447009", "2.16.840.1.113883.6.96", "Dorzolamide (substance)", "Dorzolamide",
			"Dorzolamid", "dorzolamide", "Dorzolamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxapram</div>
	 * <div class="de">Doxapram</div>
	 * <div class="fr">doxapram</div>
	 * <div class="it">Doxapram</div>
	 * <!-- @formatter:on -->
	 */
	DOXAPRAM("373339005", "2.16.840.1.113883.6.96", "Doxapram (substance)", "Doxapram", "Doxapram",
			"doxapram", "Doxapram"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxazosin</div>
	 * <div class="de">Doxazosin</div>
	 * <div class="fr">doxazosine</div>
	 * <div class="it">Doxazosina</div>
	 * <!-- @formatter:on -->
	 */
	DOXAZOSIN("372508002", "2.16.840.1.113883.6.96", "Doxazosin (substance)", "Doxazosin",
			"Doxazosin", "doxazosine", "Doxazosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxepin</div>
	 * <div class="de">Doxepin</div>
	 * <div class="fr">doxépine</div>
	 * <div class="it">Doxepina</div>
	 * <!-- @formatter:on -->
	 */
	DOXEPIN("372587005", "2.16.840.1.113883.6.96", "Doxepin (substance)", "Doxepin", "Doxepin",
			"doxépine", "Doxepina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxorubicin</div>
	 * <div class="de">Doxorubicin</div>
	 * <div class="fr">doxorubicine</div>
	 * <div class="it">Doxorubicina</div>
	 * <!-- @formatter:on -->
	 */
	DOXORUBICIN("372817009", "2.16.840.1.113883.6.96", "Doxorubicin (substance)", "Doxorubicin",
			"Doxorubicin", "doxorubicine", "Doxorubicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxycycline</div>
	 * <div class="de">Doxycyclin</div>
	 * <div class="fr">doxycycline</div>
	 * <div class="it">Doxiciclina</div>
	 * <!-- @formatter:on -->
	 */
	DOXYCYCLINE("372478003", "2.16.840.1.113883.6.96", "Doxycycline (substance)", "Doxycycline",
			"Doxycyclin", "doxycycline", "Doxiciclina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxycycline hyclate</div>
	 * <div class="de">Doxycyclin hyclat</div>
	 * <div class="fr">doxycycline hyclate</div>
	 * <div class="it">Doxiciclina iclato</div>
	 * <!-- @formatter:on -->
	 */
	DOXYCYCLINE_HYCLATE("71417000", "2.16.840.1.113883.6.96", "Doxycycline hyclate (substance)",
			"Doxycycline hyclate", "Doxycyclin hyclat", "doxycycline hyclate",
			"Doxiciclina iclato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Doxylamine</div>
	 * <div class="de">Doxylamin</div>
	 * <div class="fr">doxylamine</div>
	 * <div class="it">Doxilamina</div>
	 * <!-- @formatter:on -->
	 */
	DOXYLAMINE("44068004", "2.16.840.1.113883.6.96", "Doxylamine (substance)", "Doxylamine",
			"Doxylamin", "doxylamine", "Doxilamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dronedarone</div>
	 * <div class="de">Dronedaron</div>
	 * <div class="fr">dronédarone</div>
	 * <div class="it">Dronedarone</div>
	 * <!-- @formatter:on -->
	 */
	DRONEDARONE("443195003", "2.16.840.1.113883.6.96", "Dronedarone (substance)", "Dronedarone",
			"Dronedaron", "dronédarone", "Dronedarone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Droperidol</div>
	 * <div class="de">Droperidol</div>
	 * <div class="fr">dropéridol</div>
	 * <div class="it">Droperidolo</div>
	 * <!-- @formatter:on -->
	 */
	DROPERIDOL("387146001", "2.16.840.1.113883.6.96", "Droperidol (substance)", "Droperidol",
			"Droperidol", "dropéridol", "Droperidolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Drospirenone</div>
	 * <div class="de">Drospirenon</div>
	 * <div class="fr">drospirénone</div>
	 * <div class="it">Drospirenone</div>
	 * <!-- @formatter:on -->
	 */
	DROSPIRENONE("410919000", "2.16.840.1.113883.6.96", "Drospirenone (substance)", "Drospirenone",
			"Drospirenon", "drospirénone", "Drospirenone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dulaglutide</div>
	 * <div class="de">Dulaglutid</div>
	 * <div class="fr">dulaglutide</div>
	 * <div class="it">Dulaglutide</div>
	 * <!-- @formatter:on -->
	 */
	DULAGLUTIDE("714080005", "2.16.840.1.113883.6.96", "Dulaglutide (substance)", "Dulaglutide",
			"Dulaglutid", "dulaglutide", "Dulaglutide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Duloxetine</div>
	 * <div class="de">Duloxetin</div>
	 * <div class="fr">duloxétine</div>
	 * <div class="it">Duloxetina</div>
	 * <!-- @formatter:on -->
	 */
	DULOXETINE("407032004", "2.16.840.1.113883.6.96", "Duloxetine (substance)", "Duloxetine",
			"Duloxetin", "duloxétine", "Duloxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dupilumab</div>
	 * <div class="de">Dupilumab</div>
	 * <div class="fr">dupilumab</div>
	 * <div class="it">Dupilumab</div>
	 * <!-- @formatter:on -->
	 */
	DUPILUMAB("733487000", "2.16.840.1.113883.6.96", "Dupilumab (substance)", "Dupilumab",
			"Dupilumab", "dupilumab", "Dupilumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Durvalumab</div>
	 * <div class="de">Durvalumab</div>
	 * <div class="fr">durvalumab</div>
	 * <div class="it">Durvalumab</div>
	 * <!-- @formatter:on -->
	 */
	DURVALUMAB("735230005", "2.16.840.1.113883.6.96", "Durvalumab (substance)", "Durvalumab",
			"Durvalumab", "durvalumab", "Durvalumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dutasteride</div>
	 * <div class="de">Dutasterid</div>
	 * <div class="fr">dutastéride</div>
	 * <div class="it">Dutasteride</div>
	 * <!-- @formatter:on -->
	 */
	DUTASTERIDE("385572003", "2.16.840.1.113883.6.96", "Dutasteride (substance)", "Dutasteride",
			"Dutasterid", "dutastéride", "Dutasteride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Dydrogesterone</div>
	 * <div class="de">Dydrogesteron</div>
	 * <div class="fr">dydrogestérone</div>
	 * <div class="it">Didrogesterone</div>
	 * <!-- @formatter:on -->
	 */
	DYDROGESTERONE("126093005", "2.16.840.1.113883.6.96", "Dydrogesterone (substance)",
			"Dydrogesterone", "Dydrogesteron", "dydrogestérone", "Didrogesterone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">D-alpha-tocopherol</div>
	 * <div class="de">Tocopherol D-alpha (Vitamin E)</div>
	 * <div class="fr">tocophérol D-alfa (Vitamine E)</div>
	 * <div class="it">D-alfa-tocoferolo (vitamina E)</div>
	 * <!-- @formatter:on -->
	 */
	D_ALPHA_TOCOPHEROL("116776001", "2.16.840.1.113883.6.96", "D-alpha-tocopherol (substance)",
			"D-alpha-tocopherol", "Tocopherol D-alpha (Vitamin E)",
			"tocophérol D-alfa (Vitamine E)", "D-alfa-tocoferolo (vitamina E)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Econazole</div>
	 * <div class="de">Econazol</div>
	 * <div class="fr">éconazole</div>
	 * <div class="it">Econazolo</div>
	 * <!-- @formatter:on -->
	 */
	ECONAZOLE("373471002", "2.16.840.1.113883.6.96", "Econazole (substance)", "Econazole",
			"Econazol", "éconazole", "Econazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eculizumab</div>
	 * <div class="de">Eculizumab</div>
	 * <div class="fr">éculizumab</div>
	 * <div class="it">Eculizumab</div>
	 * <!-- @formatter:on -->
	 */
	ECULIZUMAB("427429004", "2.16.840.1.113883.6.96", "Eculizumab (substance)", "Eculizumab",
			"Eculizumab", "éculizumab", "Eculizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Edoxaban</div>
	 * <div class="de">Edoxaban</div>
	 * <div class="fr">édoxaban</div>
	 * <div class="it">Edoxaban</div>
	 * <!-- @formatter:on -->
	 */
	EDOXABAN("712778008", "2.16.840.1.113883.6.96", "Edoxaban (substance)", "Edoxaban", "Edoxaban",
			"édoxaban", "Edoxaban"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Efavirenz</div>
	 * <div class="de">Efavirenz</div>
	 * <div class="fr">éfavirenz</div>
	 * <div class="it">Efavirenz</div>
	 * <!-- @formatter:on -->
	 */
	EFAVIRENZ("387001004", "2.16.840.1.113883.6.96", "Efavirenz (substance)", "Efavirenz",
			"Efavirenz", "éfavirenz", "Efavirenz"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eicosapentaenoic acid</div>
	 * <div class="de">Eicosapentaensäure EPA</div>
	 * <div class="fr">acide eicosapentaénoïque EPA</div>
	 * <div class="it">Acido eicosapentaenoico EPA</div>
	 * <!-- @formatter:on -->
	 */
	EICOSAPENTAENOIC_ACID("226367006", "2.16.840.1.113883.6.96",
			"Eicosapentaenoic acid (substance)", "Eicosapentaenoic acid", "Eicosapentaensäure EPA",
			"acide eicosapentaénoïque EPA", "Acido eicosapentaenoico EPA"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eletriptan</div>
	 * <div class="de">Eletriptan</div>
	 * <div class="fr">élétriptan</div>
	 * <div class="it">Eletriptan</div>
	 * <!-- @formatter:on -->
	 */
	ELETRIPTAN("410843003", "2.16.840.1.113883.6.96", "Eletriptan (substance)", "Eletriptan",
			"Eletriptan", "élétriptan", "Eletriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Elotuzumab</div>
	 * <div class="de">Elotuzumab</div>
	 * <div class="fr">élotuzumab</div>
	 * <div class="it">Elotuzumab</div>
	 * <!-- @formatter:on -->
	 */
	ELOTUZUMAB("715660001", "2.16.840.1.113883.6.96", "Elotuzumab (substance)", "Elotuzumab",
			"Elotuzumab", "élotuzumab", "Elotuzumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eltrombopag</div>
	 * <div class="de">Eltrombopag</div>
	 * <div class="fr">eltrombopag</div>
	 * <div class="it">Eltrombopag</div>
	 * <!-- @formatter:on -->
	 */
	ELTROMBOPAG("432005001", "2.16.840.1.113883.6.96", "Eltrombopag (substance)", "Eltrombopag",
			"Eltrombopag", "eltrombopag", "Eltrombopag"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Elvitegravir</div>
	 * <div class="de">Elvitegravir</div>
	 * <div class="fr">elvitégravir</div>
	 * <div class="it">Elvitegravir</div>
	 * <!-- @formatter:on -->
	 */
	ELVITEGRAVIR("708828000", "2.16.840.1.113883.6.96", "Elvitegravir (substance)", "Elvitegravir",
			"Elvitegravir", "elvitégravir", "Elvitegravir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Emedastine</div>
	 * <div class="de">Emedastin</div>
	 * <div class="fr">émédastine</div>
	 * <div class="it">Emedastina</div>
	 * <!-- @formatter:on -->
	 */
	EMEDASTINE("372551003", "2.16.840.1.113883.6.96", "Emedastine (substance)", "Emedastine",
			"Emedastin", "émédastine", "Emedastina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Emicizumab</div>
	 * <div class="de">Emicizumab</div>
	 * <div class="fr">emicizumab</div>
	 * <div class="it">Emicizumab</div>
	 * <!-- @formatter:on -->
	 */
	EMICIZUMAB("763611007", "2.16.840.1.113883.6.96", "Emicizumab (substance)", "Emicizumab",
			"Emicizumab", "emicizumab", "Emicizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Empagliflozin</div>
	 * <div class="de">Empagliflozin</div>
	 * <div class="fr">empagliflozine</div>
	 * <div class="it">Empagliflozin</div>
	 * <!-- @formatter:on -->
	 */
	EMPAGLIFLOZIN("703894008", "2.16.840.1.113883.6.96", "Empagliflozin (substance)",
			"Empagliflozin", "Empagliflozin", "empagliflozine", "Empagliflozin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Emtricitabine</div>
	 * <div class="de">Emtricitabin</div>
	 * <div class="fr">emtricitabine</div>
	 * <div class="it">Emtricitabina</div>
	 * <!-- @formatter:on -->
	 */
	EMTRICITABINE("404856006", "2.16.840.1.113883.6.96", "Emtricitabine (substance)",
			"Emtricitabine", "Emtricitabin", "emtricitabine", "Emtricitabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Enalapril</div>
	 * <div class="de">Enalapril</div>
	 * <div class="fr">énalapril</div>
	 * <div class="it">Enalapril</div>
	 * <!-- @formatter:on -->
	 */
	ENALAPRIL("372658000", "2.16.840.1.113883.6.96", "Enalapril (substance)", "Enalapril",
			"Enalapril", "énalapril", "Enalapril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Encorafenib</div>
	 * <div class="de">Encorafenib</div>
	 * <div class="fr">encorafénib</div>
	 * <div class="it">Encorafenib</div>
	 * <!-- @formatter:on -->
	 */
	ENCORAFENIB("772201002", "2.16.840.1.113883.6.96", "Encorafenib (substance)", "Encorafenib",
			"Encorafenib", "encorafénib", "Encorafenib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Enoxaparin sodium</div>
	 * <div class="de">Enoxaparin natrium</div>
	 * <div class="fr">énoxaparine sodique</div>
	 * <div class="it">Enoxaparina sodica</div>
	 * <!-- @formatter:on -->
	 */
	ENOXAPARIN_SODIUM("108983001", "2.16.840.1.113883.6.96", "Enoxaparin sodium (substance)",
			"Enoxaparin sodium", "Enoxaparin natrium", "énoxaparine sodique",
			"Enoxaparina sodica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Entacapone</div>
	 * <div class="de">Entacapon</div>
	 * <div class="fr">entacapone</div>
	 * <div class="it">Entacapone</div>
	 * <!-- @formatter:on -->
	 */
	ENTACAPONE("387018000", "2.16.840.1.113883.6.96", "Entacapone (substance)", "Entacapone",
			"Entacapon", "entacapone", "Entacapone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Entecavir</div>
	 * <div class="de">Entecavir</div>
	 * <div class="fr">entécavir</div>
	 * <div class="it">Entacavir</div>
	 * <!-- @formatter:on -->
	 */
	ENTECAVIR("416644000", "2.16.840.1.113883.6.96", "Entecavir (substance)", "Entecavir",
			"Entecavir", "entécavir", "Entacavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eosine</div>
	 * <div class="de">Eosin</div>
	 * <div class="fr">éosine</div>
	 * <div class="it">Eosina</div>
	 * <!-- @formatter:on -->
	 */
	EOSINE("256012001", "2.16.840.1.113883.6.96", "Eosine (substance)", "Eosine", "Eosin",
			"éosine", "Eosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ephedrine</div>
	 * <div class="de">Ephedrin</div>
	 * <div class="fr">éphédrine</div>
	 * <div class="it">Efedrina</div>
	 * <!-- @formatter:on -->
	 */
	EPHEDRINE("387358007", "2.16.840.1.113883.6.96", "Ephedrine (substance)", "Ephedrine",
			"Ephedrin", "éphédrine", "Efedrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ephedrine sulfate</div>
	 * <div class="de">Ephedrin sulfat</div>
	 * <div class="fr">éphédrine sulfate</div>
	 * <div class="it">Efedrina solfato</div>
	 * <!-- @formatter:on -->
	 */
	EPHEDRINE_SULFATE("76525000", "2.16.840.1.113883.6.96", "Ephedrine sulfate (substance)",
			"Ephedrine sulfate", "Ephedrin sulfat", "éphédrine sulfate", "Efedrina solfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epinastine</div>
	 * <div class="de">Epinastin</div>
	 * <div class="fr">épinastine</div>
	 * <div class="it">Epinastine</div>
	 * <!-- @formatter:on -->
	 */
	EPINASTINE("407068009", "2.16.840.1.113883.6.96", "Epinastine (substance)", "Epinastine",
			"Epinastin", "épinastine", "Epinastine"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epinephrine</div>
	 * <div class="de">Adrenalin (Epinephrin)</div>
	 * <div class="fr">adrénaline (épinéphrine)</div>
	 * <div class="it">Adrenalina (epinefrina)</div>
	 * <!-- @formatter:on -->
	 */
	EPINEPHRINE("387362001", "2.16.840.1.113883.6.96", "Epinephrine (substance)", "Epinephrine",
			"Adrenalin (Epinephrin)", "adrénaline (épinéphrine)", "Adrenalina (epinefrina)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epirubicin</div>
	 * <div class="de">Epirubicin</div>
	 * <div class="fr">épirubicine</div>
	 * <div class="it">Epirubicina</div>
	 * <!-- @formatter:on -->
	 */
	EPIRUBICIN("417916005", "2.16.840.1.113883.6.96", "Epirubicin (substance)", "Epirubicin",
			"Epirubicin", "épirubicine", "Epirubicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eplerenone</div>
	 * <div class="de">Eplerenon</div>
	 * <div class="fr">éplérénone</div>
	 * <div class="it">Eplerenone</div>
	 * <!-- @formatter:on -->
	 */
	EPLERENONE("407010008", "2.16.840.1.113883.6.96", "Eplerenone (substance)", "Eplerenone",
			"Eplerenon", "éplérénone", "Eplerenone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epoetin alfa</div>
	 * <div class="de">Epoetin alfa rekombiniert</div>
	 * <div class="fr">époétine alfa recombinante</div>
	 * <div class="it">Epoetina alfa ricombinante</div>
	 * <!-- @formatter:on -->
	 */
	EPOETIN_ALFA("386947003", "2.16.840.1.113883.6.96", "Epoetin alfa (substance)", "Epoetin alfa",
			"Epoetin alfa rekombiniert", "époétine alfa recombinante",
			"Epoetina alfa ricombinante"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epoetin beta</div>
	 * <div class="de">Epoetin beta rekombiniert</div>
	 * <div class="fr">époétine bêta recombinante</div>
	 * <div class="it">Epoetina beta ricombinante</div>
	 * <!-- @formatter:on -->
	 */
	EPOETIN_BETA("396043004", "2.16.840.1.113883.6.96", "Epoetin beta (substance)", "Epoetin beta",
			"Epoetin beta rekombiniert", "époétine bêta recombinante",
			"Epoetina beta ricombinante"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epoetin theta</div>
	 * <div class="de">Epoetin theta</div>
	 * <div class="fr">époétine thêta</div>
	 * <div class="it">Epoetina teta ricombinante</div>
	 * <!-- @formatter:on -->
	 */
	EPOETIN_THETA("708829008", "2.16.840.1.113883.6.96", "Epoetin theta (substance)",
			"Epoetin theta", "Epoetin theta", "époétine thêta", "Epoetina teta ricombinante"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Epoprostenol</div>
	 * <div class="de">Epoprostenol</div>
	 * <div class="fr">époprosténol</div>
	 * <div class="it">Epoprostenolo</div>
	 * <!-- @formatter:on -->
	 */
	EPOPROSTENOL("372513003", "2.16.840.1.113883.6.96", "Epoprostenol (substance)", "Epoprostenol",
			"Epoprostenol", "époprosténol", "Epoprostenolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eprosartan</div>
	 * <div class="de">Eprosartan</div>
	 * <div class="fr">éprosartan</div>
	 * <div class="it">Eprosartan</div>
	 * <!-- @formatter:on -->
	 */
	EPROSARTAN("396044005", "2.16.840.1.113883.6.96", "Eprosartan (substance)", "Eprosartan",
			"Eprosartan", "éprosartan", "Eprosartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eptacog alfa</div>
	 * <div class="de">Eptacog alfa (aktiviert)</div>
	 * <div class="fr">eptacogum alfa (activatum)</div>
	 * <div class="it">Eptacog alfa (attivato)</div>
	 * <!-- @formatter:on -->
	 */
	EPTACOG_ALFA("116066006", "2.16.840.1.113883.6.96", "Eptacog alfa (substance)", "Eptacog alfa",
			"Eptacog alfa (aktiviert)", "eptacogum alfa (activatum)", "Eptacog alfa (attivato)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eptifibatide</div>
	 * <div class="de">Eptifibatid</div>
	 * <div class="fr">eptifibatide</div>
	 * <div class="it">Eptifibatide</div>
	 * <!-- @formatter:on -->
	 */
	EPTIFIBATIDE("386998009", "2.16.840.1.113883.6.96", "Eptifibatide (substance)", "Eptifibatide",
			"Eptifibatid", "eptifibatide", "Eptifibatide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Erdosteine</div>
	 * <div class="de">Erdostein</div>
	 * <div class="fr">erdostéine</div>
	 * <div class="it">Erdosteina</div>
	 * <!-- @formatter:on -->
	 */
	ERDOSTEINE("426292005", "2.16.840.1.113883.6.96", "Erdosteine (substance)", "Erdosteine",
			"Erdostein", "erdostéine", "Erdosteina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Erenumab</div>
	 * <div class="de">Erenumab</div>
	 * <div class="fr">erénumab</div>
	 * <div class="it">Erenumab</div>
	 * <!-- @formatter:on -->
	 */
	ERENUMAB("771590007", "2.16.840.1.113883.6.96", "Erenumab (substance)", "Erenumab", "Erenumab",
			"erénumab", "Erenumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Eribulin</div>
	 * <div class="de">Eribulin</div>
	 * <div class="fr">éribuline</div>
	 * <div class="it">Eribulina</div>
	 * <!-- @formatter:on -->
	 */
	ERIBULIN("708166000", "2.16.840.1.113883.6.96", "Eribulin (substance)", "Eribulin", "Eribulin",
			"éribuline", "Eribulina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Erlotinib</div>
	 * <div class="de">Erlotinib</div>
	 * <div class="fr">erlotinib</div>
	 * <div class="it">Erlotinib</div>
	 * <!-- @formatter:on -->
	 */
	ERLOTINIB("414123001", "2.16.840.1.113883.6.96", "Erlotinib (substance)", "Erlotinib",
			"Erlotinib", "erlotinib", "Erlotinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ertapenem</div>
	 * <div class="de">Ertapenem</div>
	 * <div class="fr">ertapénem</div>
	 * <div class="it">Ertapenem</div>
	 * <!-- @formatter:on -->
	 */
	ERTAPENEM("396346003", "2.16.840.1.113883.6.96", "Ertapenem (substance)", "Ertapenem",
			"Ertapenem", "ertapénem", "Ertapenem"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ertugliflozin</div>
	 * <div class="de">Ertugliflozin</div>
	 * <div class="fr">ertugliflozine</div>
	 * <div class="it">Ertugliflozin</div>
	 * <!-- @formatter:on -->
	 */
	ERTUGLIFLOZIN("764274008", "2.16.840.1.113883.6.96", "Ertugliflozin (substance)",
			"Ertugliflozin", "Ertugliflozin", "ertugliflozine", "Ertugliflozin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Erythromycin</div>
	 * <div class="de">Erythromycin</div>
	 * <div class="fr">érythromycine</div>
	 * <div class="it">Eritromicina</div>
	 * <!-- @formatter:on -->
	 */
	ERYTHROMYCIN("372694001", "2.16.840.1.113883.6.96", "Erythromycin (substance)", "Erythromycin",
			"Erythromycin", "érythromycine", "Eritromicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Escitalopram</div>
	 * <div class="de">Escitalopram</div>
	 * <div class="fr">escitalopram</div>
	 * <div class="it">Escitalopram</div>
	 * <!-- @formatter:on -->
	 */
	ESCITALOPRAM("400447003", "2.16.840.1.113883.6.96", "Escitalopram (substance)", "Escitalopram",
			"Escitalopram", "escitalopram", "Escitalopram"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Esmolol</div>
	 * <div class="de">Esmolol</div>
	 * <div class="fr">esmolol</div>
	 * <div class="it">Esmololo</div>
	 * <!-- @formatter:on -->
	 */
	ESMOLOL("372847006", "2.16.840.1.113883.6.96", "Esmolol (substance)", "Esmolol", "Esmolol",
			"esmolol", "Esmololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Esomeprazole</div>
	 * <div class="de">Esomeprazol</div>
	 * <div class="fr">ésoméprazole</div>
	 * <div class="it">Esomeprazolo</div>
	 * <!-- @formatter:on -->
	 */
	ESOMEPRAZOLE("396047003", "2.16.840.1.113883.6.96", "Esomeprazole (substance)", "Esomeprazole",
			"Esomeprazol", "ésoméprazole", "Esomeprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Estradiol</div>
	 * <div class="de">Estradiol</div>
	 * <div class="fr">estradiol</div>
	 * <div class="it">Estradiolo</div>
	 * <!-- @formatter:on -->
	 */
	ESTRADIOL("126172005", "2.16.840.1.113883.6.96", "Estradiol (substance)", "Estradiol",
			"Estradiol", "estradiol", "Estradiolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Estradiol hemihydrate</div>
	 * <div class="de">Estradiol hemihydrat</div>
	 * <div class="fr">estradiol hémihydrate</div>
	 * <div class="it">Estradiolo emiidrato</div>
	 * <!-- @formatter:on -->
	 */
	ESTRADIOL_HEMIHYDRATE("116070003", "2.16.840.1.113883.6.96",
			"Estradiol hemihydrate (substance)", "Estradiol hemihydrate", "Estradiol hemihydrat",
			"estradiol hémihydrate", "Estradiolo emiidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Estradiol valerate</div>
	 * <div class="de">Estradiol valerat</div>
	 * <div class="fr">estradiol valérate</div>
	 * <div class="it">Estradiolo valerato</div>
	 * <!-- @formatter:on -->
	 */
	ESTRADIOL_VALERATE("96350008", "2.16.840.1.113883.6.96", "Estradiol valerate (substance)",
			"Estradiol valerate", "Estradiol valerat", "estradiol valérate",
			"Estradiolo valerato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Estriol</div>
	 * <div class="de">Estriol</div>
	 * <div class="fr">estriol</div>
	 * <div class="it">Estriolo</div>
	 * <!-- @formatter:on -->
	 */
	ESTRIOL("73723004", "2.16.840.1.113883.6.96", "Estriol (substance)", "Estriol", "Estriol",
			"estriol", "Estriolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etanercept</div>
	 * <div class="de">Etanercept</div>
	 * <div class="fr">étanercept</div>
	 * <div class="it">Etanercept</div>
	 * <!-- @formatter:on -->
	 */
	ETANERCEPT("387045004", "2.16.840.1.113883.6.96", "Etanercept (substance)", "Etanercept",
			"Etanercept", "étanercept", "Etanercept"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etelcalcetide</div>
	 * <div class="de">Etelcalcetid</div>
	 * <div class="fr">ételcalcétide</div>
	 * <div class="it">Etelcalcetide</div>
	 * <!-- @formatter:on -->
	 */
	ETELCALCETIDE("723539000", "2.16.840.1.113883.6.96", "Etelcalcetide (substance)",
			"Etelcalcetide", "Etelcalcetid", "ételcalcétide", "Etelcalcetide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ethacridine</div>
	 * <div class="de">Ethacridin</div>
	 * <div class="fr">éthacridine</div>
	 * <div class="it">Etacridina</div>
	 * <!-- @formatter:on -->
	 */
	ETHACRIDINE("711320003", "2.16.840.1.113883.6.96", "Ethacridine (substance)", "Ethacridine",
			"Ethacridin", "éthacridine", "Etacridina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ethambutol</div>
	 * <div class="de">Ethambutol</div>
	 * <div class="fr">éthambutol</div>
	 * <div class="it">Etambutolo</div>
	 * <!-- @formatter:on -->
	 */
	ETHAMBUTOL("387129004", "2.16.840.1.113883.6.96", "Ethambutol (substance)", "Ethambutol",
			"Ethambutol", "éthambutol", "Etambutolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ethinylestradiol</div>
	 * <div class="de">Ethinylestradiol</div>
	 * <div class="fr">éthinylestradiol</div>
	 * <div class="it">Etinilestradiolo</div>
	 * <!-- @formatter:on -->
	 */
	ETHINYLESTRADIOL("126097006", "2.16.840.1.113883.6.96", "Ethinylestradiol (substance)",
			"Ethinylestradiol", "Ethinylestradiol", "éthinylestradiol", "Etinilestradiolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ethionamide</div>
	 * <div class="de">Ethionamid</div>
	 * <div class="fr">ethionamide</div>
	 * <div class="it">Etionamide</div>
	 * <!-- @formatter:on -->
	 */
	ETHIONAMIDE("32800009", "2.16.840.1.113883.6.96", "Ethionamide (substance)", "Ethionamide",
			"Ethionamid", "ethionamide", "Etionamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ethosuximide</div>
	 * <div class="de">Ethosuximid</div>
	 * <div class="fr">éthosuximide</div>
	 * <div class="it">Etosuccimide</div>
	 * <!-- @formatter:on -->
	 */
	ETHOSUXIMIDE("387244008", "2.16.840.1.113883.6.96", "Ethosuximide (substance)", "Ethosuximide",
			"Ethosuximid", "éthosuximide", "Etosuccimide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ethyl chloride</div>
	 * <div class="de">Chlorethan</div>
	 * <div class="fr">éthyle chlorure</div>
	 * <div class="it">Cloruro di etile</div>
	 * <!-- @formatter:on -->
	 */
	ETHYL_CHLORIDE("22005007", "2.16.840.1.113883.6.96", "Ethyl chloride (substance)",
			"Ethyl chloride", "Chlorethan", "éthyle chlorure", "Cloruro di etile"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etilefrine</div>
	 * <div class="de">Etilefrin</div>
	 * <div class="fr">étiléfrine</div>
	 * <div class="it">Etilefrina</div>
	 * <!-- @formatter:on -->
	 */
	ETILEFRINE("96255000", "2.16.840.1.113883.6.96", "Etilefrine (substance)", "Etilefrine",
			"Etilefrin", "étiléfrine", "Etilefrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etodolac</div>
	 * <div class="de">Etodolac</div>
	 * <div class="fr">étodolac</div>
	 * <div class="it">Etodolac</div>
	 * <!-- @formatter:on -->
	 */
	ETODOLAC("386860005", "2.16.840.1.113883.6.96", "Etodolac (substance)", "Etodolac", "Etodolac",
			"étodolac", "Etodolac"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etomidate</div>
	 * <div class="de">Etomidat</div>
	 * <div class="fr">étomidate</div>
	 * <div class="it">Etomidato</div>
	 * <!-- @formatter:on -->
	 */
	ETOMIDATE("387218008", "2.16.840.1.113883.6.96", "Etomidate (substance)", "Etomidate",
			"Etomidat", "étomidate", "Etomidato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etonogestrel</div>
	 * <div class="de">Etonogestrel</div>
	 * <div class="fr">étonogestrel</div>
	 * <div class="it">Etonogestrel</div>
	 * <!-- @formatter:on -->
	 */
	ETONOGESTREL("396050000", "2.16.840.1.113883.6.96", "Etonogestrel (substance)", "Etonogestrel",
			"Etonogestrel", "étonogestrel", "Etonogestrel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etoposide</div>
	 * <div class="de">Etoposid</div>
	 * <div class="fr">étoposide</div>
	 * <div class="it">Etoposide</div>
	 * <!-- @formatter:on -->
	 */
	ETOPOSIDE("387316009", "2.16.840.1.113883.6.96", "Etoposide (substance)", "Etoposide",
			"Etoposid", "étoposide", "Etoposide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etoricoxib</div>
	 * <div class="de">Etoricoxib</div>
	 * <div class="fr">étoricoxib</div>
	 * <div class="it">Etoricoxib</div>
	 * <!-- @formatter:on -->
	 */
	ETORICOXIB("409134009", "2.16.840.1.113883.6.96", "Etoricoxib (substance)", "Etoricoxib",
			"Etoricoxib", "étoricoxib", "Etoricoxib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Etravirine</div>
	 * <div class="de">Etravirin</div>
	 * <div class="fr">étravirine</div>
	 * <div class="it">Etravirina</div>
	 * <!-- @formatter:on -->
	 */
	ETRAVIRINE("432121008", "2.16.840.1.113883.6.96", "Etravirine (substance)", "Etravirine",
			"Etravirin", "étravirine", "Etravirina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Everolimus</div>
	 * <div class="de">Everolimus</div>
	 * <div class="fr">évérolimus</div>
	 * <div class="it">Everolimus</div>
	 * <!-- @formatter:on -->
	 */
	EVEROLIMUS("428698007", "2.16.840.1.113883.6.96", "Everolimus (substance)", "Everolimus",
			"Everolimus", "évérolimus", "Everolimus"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Exemestane</div>
	 * <div class="de">Exemestan</div>
	 * <div class="fr">exémestane</div>
	 * <div class="it">Exemestane</div>
	 * <!-- @formatter:on -->
	 */
	EXEMESTANE("387017005", "2.16.840.1.113883.6.96", "Exemestane (substance)", "Exemestane",
			"Exemestan", "exémestane", "Exemestane"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Exenatide</div>
	 * <div class="de">Exenatid</div>
	 * <div class="fr">exénatide</div>
	 * <div class="it">Exenatide</div>
	 * <!-- @formatter:on -->
	 */
	EXENATIDE("416859008", "2.16.840.1.113883.6.96", "Exenatide (substance)", "Exenatide",
			"Exenatid", "exénatide", "Exenatide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ezetimibe</div>
	 * <div class="de">Ezetimib</div>
	 * <div class="fr">ézétimibe</div>
	 * <div class="it">Ezetimibe</div>
	 * <!-- @formatter:on -->
	 */
	EZETIMIBE("409149001", "2.16.840.1.113883.6.96", "Ezetimibe (substance)", "Ezetimibe",
			"Ezetimib", "ézétimibe", "Ezetimibe"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Factor VIII</div>
	 * <div class="de">Blutgerinnungsfaktor VIII human</div>
	 * <div class="fr">facteur VIII de coagulation humain</div>
	 * <div class="it">Fattore VIII di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	FACTOR_VIII("278910002", "2.16.840.1.113883.6.96", "Factor VIII (substance)", "Factor VIII",
			"Blutgerinnungsfaktor VIII human", "facteur VIII de coagulation humain",
			"Fattore VIII di coagulazione umano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Famciclovir</div>
	 * <div class="de">Famciclovir</div>
	 * <div class="fr">famciclovir</div>
	 * <div class="it">Famciclovir</div>
	 * <!-- @formatter:on -->
	 */
	FAMCICLOVIR("387557001", "2.16.840.1.113883.6.96", "Famciclovir (substance)", "Famciclovir",
			"Famciclovir", "famciclovir", "Famciclovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Febuxostat</div>
	 * <div class="de">Febuxostat</div>
	 * <div class="fr">fébuxostat</div>
	 * <div class="it">Febuxostat</div>
	 * <!-- @formatter:on -->
	 */
	FEBUXOSTAT("441743008", "2.16.840.1.113883.6.96", "Febuxostat (substance)", "Febuxostat",
			"Febuxostat", "fébuxostat", "Febuxostat"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Felbamate</div>
	 * <div class="de">Felbamat</div>
	 * <div class="fr">felbamate</div>
	 * <div class="it">Felbamato</div>
	 * <!-- @formatter:on -->
	 */
	FELBAMATE("96194006", "2.16.840.1.113883.6.96", "Felbamate (substance)", "Felbamate",
			"Felbamat", "felbamate", "Felbamato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Felodipine</div>
	 * <div class="de">Felodipin</div>
	 * <div class="fr">félodipine</div>
	 * <div class="it">Felodipina</div>
	 * <!-- @formatter:on -->
	 */
	FELODIPINE("386863007", "2.16.840.1.113883.6.96", "Felodipine (substance)", "Felodipine",
			"Felodipin", "félodipine", "Felodipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fenofibrate</div>
	 * <div class="de">Fenofibrat</div>
	 * <div class="fr">fénofibrate</div>
	 * <div class="it">Fenofibrato</div>
	 * <!-- @formatter:on -->
	 */
	FENOFIBRATE("386879008", "2.16.840.1.113883.6.96", "Fenofibrate (substance)", "Fenofibrate",
			"Fenofibrat", "fénofibrate", "Fenofibrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fenoterol</div>
	 * <div class="de">Fenoterol</div>
	 * <div class="fr">fénotérol</div>
	 * <div class="it">Fenoterolo</div>
	 * <!-- @formatter:on -->
	 */
	FENOTEROL("395976006", "2.16.840.1.113883.6.96", "Fenoterol (substance)", "Fenoterol",
			"Fenoterol", "fénotérol", "Fenoterolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fentanyl</div>
	 * <div class="de">Fentanyl</div>
	 * <div class="fr">fentanyl</div>
	 * <div class="it">Fentanil</div>
	 * <!-- @formatter:on -->
	 */
	FENTANYL("373492002", "2.16.840.1.113883.6.96", "Fentanyl (substance)", "Fentanyl", "Fentanyl",
			"fentanyl", "Fentanil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ferric hexacyanoferrate-II</div>
	 * <div class="de">Eisen(III)-hexacyanoferrat(II)</div>
	 * <div class="fr">hexacyanoferrate II ferrique III</div>
	 * <div class="it">Esacianoferrato (II) di ferro (III)</div>
	 * <!-- @formatter:on -->
	 */
	FERRIC_HEXACYANOFERRATE_II("406452004", "2.16.840.1.113883.6.96",
			"Ferric hexacyanoferrate-II (substance)", "Ferric hexacyanoferrate-II",
			"Eisen(III)-hexacyanoferrat(II)", "hexacyanoferrate II ferrique III",
			"Esacianoferrato (II) di ferro (III)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ferrous fumarate</div>
	 * <div class="de">Eisen(II) fumarat</div>
	 * <div class="fr">fer II fumarate</div>
	 * <div class="it">Ferro (II) fumarato</div>
	 * <!-- @formatter:on -->
	 */
	FERROUS_FUMARATE("387289009", "2.16.840.1.113883.6.96", "Ferrous fumarate (substance)",
			"Ferrous fumarate", "Eisen(II) fumarat", "fer II fumarate", "Ferro (II) fumarato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ferrous sulfate</div>
	 * <div class="de">Eisen(II)-sulfat</div>
	 * <div class="fr">fer sulfate</div>
	 * <div class="it">Solfato ferroso</div>
	 * <!-- @formatter:on -->
	 */
	FERROUS_SULFATE("387402000", "2.16.840.1.113883.6.96", "Ferrous sulfate (substance)",
			"Ferrous sulfate", "Eisen(II)-sulfat", "fer sulfate", "Solfato ferroso"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fesoterodine fumarate</div>
	 * <div class="de">Fesoterodin fumarat</div>
	 * <div class="fr">fésotérodine fumarate</div>
	 * <div class="it">Fesoterodine fumarato</div>
	 * <!-- @formatter:on -->
	 */
	FESOTERODINE_FUMARATE("441469003", "2.16.840.1.113883.6.96",
			"Fesoterodine fumarate (substance)", "Fesoterodine fumarate", "Fesoterodin fumarat",
			"fésotérodine fumarate", "Fesoterodine fumarato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fexofenadine</div>
	 * <div class="de">Fexofenadin</div>
	 * <div class="fr">fexofénadine</div>
	 * <div class="it">Fexofenadina</div>
	 * <!-- @formatter:on -->
	 */
	FEXOFENADINE("372522002", "2.16.840.1.113883.6.96", "Fexofenadine (substance)", "Fexofenadine",
			"Fexofenadin", "fexofénadine", "Fexofenadina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fidaxomicin</div>
	 * <div class="de">Fidaxomicin</div>
	 * <div class="fr">fidaxomicine</div>
	 * <div class="it">Fidaxomicina</div>
	 * <!-- @formatter:on -->
	 */
	FIDAXOMICIN("703664004", "2.16.840.1.113883.6.96", "Fidaxomicin (substance)", "Fidaxomicin",
			"Fidaxomicin", "fidaxomicine", "Fidaxomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Filgrastim</div>
	 * <div class="de">Filgrastim rekombiniert</div>
	 * <div class="fr">filgrastim recombinant</div>
	 * <div class="it">Filgrastim</div>
	 * <!-- @formatter:on -->
	 */
	FILGRASTIM("386948008", "2.16.840.1.113883.6.96", "Filgrastim (substance)", "Filgrastim",
			"Filgrastim rekombiniert", "filgrastim recombinant", "Filgrastim"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Finasteride</div>
	 * <div class="de">Finasterid</div>
	 * <div class="fr">finastéride</div>
	 * <div class="it">Finasteride</div>
	 * <!-- @formatter:on -->
	 */
	FINASTERIDE("386963006", "2.16.840.1.113883.6.96", "Finasteride (substance)", "Finasteride",
			"Finasterid", "finastéride", "Finasteride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fingolimod</div>
	 * <div class="de">Fingolimod</div>
	 * <div class="fr">fingolimod</div>
	 * <div class="it">Fingolimod</div>
	 * <!-- @formatter:on -->
	 */
	FINGOLIMOD("449000008", "2.16.840.1.113883.6.96", "Fingolimod (substance)", "Fingolimod",
			"Fingolimod", "fingolimod", "Fingolimod"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fish oil</div>
	 * <div class="de">Fischkörperöl</div>
	 * <div class="fr">poisson huile</div>
	 * <div class="it">Pesce olio</div>
	 * <!-- @formatter:on -->
	 */
	FISH_OIL("735341005", "2.16.840.1.113883.6.96", "Fish oil (substance)", "Fish oil",
			"Fischkörperöl", "poisson huile", "Pesce olio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">5-aminolevulinic acid</div>
	 * <div class="de">5-Aminolevulinsäure</div>
	 * <div class="fr">acide 5-aminolévulinique</div>
	 * <div class="it">Acido 5-aminolevulinico</div>
	 * <!-- @formatter:on -->
	 */
	FIVE_AMINOLEVULINIC_ACID("259496005", "2.16.840.1.113883.6.96",
			"5-aminolevulinic acid (substance)", "5-aminolevulinic acid", "5-Aminolevulinsäure",
			"acide 5-aminolévulinique", "Acido 5-aminolevulinico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flavoxate</div>
	 * <div class="de">Flavoxat</div>
	 * <div class="fr">flavoxate</div>
	 * <div class="it">Flavossato</div>
	 * <!-- @formatter:on -->
	 */
	FLAVOXATE("372768002", "2.16.840.1.113883.6.96", "Flavoxate (substance)", "Flavoxate",
			"Flavoxat", "flavoxate", "Flavossato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flecainide</div>
	 * <div class="de">Flecainid</div>
	 * <div class="fr">flécaïnide</div>
	 * <div class="it">Flecainide</div>
	 * <!-- @formatter:on -->
	 */
	FLECAINIDE("372751001", "2.16.840.1.113883.6.96", "Flecainide (substance)", "Flecainide",
			"Flecainid", "flécaïnide", "Flecainide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flucloxacillin</div>
	 * <div class="de">Flucloxacillin</div>
	 * <div class="fr">flucloxacilline</div>
	 * <div class="it">Flucloxacillina</div>
	 * <!-- @formatter:on -->
	 */
	FLUCLOXACILLIN("387544009", "2.16.840.1.113883.6.96", "Flucloxacillin (substance)",
			"Flucloxacillin", "Flucloxacillin", "flucloxacilline", "Flucloxacillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluconazole</div>
	 * <div class="de">Fluconazol</div>
	 * <div class="fr">fluconazole</div>
	 * <div class="it">Fluconazolo</div>
	 * <!-- @formatter:on -->
	 */
	FLUCONAZOLE("387174006", "2.16.840.1.113883.6.96", "Fluconazole (substance)", "Fluconazole",
			"Fluconazol", "fluconazole", "Fluconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fludarabine</div>
	 * <div class="de">Fludarabin</div>
	 * <div class="fr">fludarabine</div>
	 * <div class="it">Fludarabina</div>
	 * <!-- @formatter:on -->
	 */
	FLUDARABINE("386907005", "2.16.840.1.113883.6.96", "Fludarabine (substance)", "Fludarabine",
			"Fludarabin", "fludarabine", "Fludarabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fludrocortisone</div>
	 * <div class="de">Fludrocortison</div>
	 * <div class="fr">fludrocortisone</div>
	 * <div class="it">Fludrocortisone</div>
	 * <!-- @formatter:on -->
	 */
	FLUDROCORTISONE("116586002", "2.16.840.1.113883.6.96", "Fludrocortisone (substance)",
			"Fludrocortisone", "Fludrocortison", "fludrocortisone", "Fludrocortisone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flumazenil</div>
	 * <div class="de">Flumazenil</div>
	 * <div class="fr">flumazénil</div>
	 * <div class="it">Flumazenil</div>
	 * <!-- @formatter:on -->
	 */
	FLUMAZENIL("387575000", "2.16.840.1.113883.6.96", "Flumazenil (substance)", "Flumazenil",
			"Flumazenil", "flumazénil", "Flumazenil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flumetasone</div>
	 * <div class="de">Flumetason</div>
	 * <div class="fr">flumétasone</div>
	 * <div class="it">Flumetasone</div>
	 * <!-- @formatter:on -->
	 */
	FLUMETASONE("116598007", "2.16.840.1.113883.6.96", "Flumetasone (substance)", "Flumetasone",
			"Flumetason", "flumétasone", "Flumetasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flunarizine</div>
	 * <div class="de">Flunarizin</div>
	 * <div class="fr">flunarizine</div>
	 * <div class="it">Flunarizina</div>
	 * <!-- @formatter:on -->
	 */
	FLUNARIZINE("418221001", "2.16.840.1.113883.6.96", "Flunarizine (substance)", "Flunarizine",
			"Flunarizin", "flunarizine", "Flunarizina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flunitrazepam</div>
	 * <div class="de">Flunitrazepam</div>
	 * <div class="fr">flunitrazépam</div>
	 * <div class="it">Flunitrazepam</div>
	 * <!-- @formatter:on -->
	 */
	FLUNITRAZEPAM("387573007", "2.16.840.1.113883.6.96", "Flunitrazepam (substance)",
			"Flunitrazepam", "Flunitrazepam", "flunitrazépam", "Flunitrazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluocinonide</div>
	 * <div class="de">Fluocinonid</div>
	 * <div class="fr">fluocinonide</div>
	 * <div class="it">Fluocinonide</div>
	 * <!-- @formatter:on -->
	 */
	FLUOCINONIDE("396060009", "2.16.840.1.113883.6.96", "Fluocinonide (substance)", "Fluocinonide",
			"Fluocinonid", "fluocinonide", "Fluocinonide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluorometholone</div>
	 * <div class="de">Fluorometholon</div>
	 * <div class="fr">fluorométholone</div>
	 * <div class="it">Fluorometolone</div>
	 * <!-- @formatter:on -->
	 */
	FLUOROMETHOLONE("2925007", "2.16.840.1.113883.6.96", "Fluorometholone (substance)",
			"Fluorometholone", "Fluorometholon", "fluorométholone", "Fluorometolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluorouracil</div>
	 * <div class="de">Fluorouracil</div>
	 * <div class="fr">fluorouracil</div>
	 * <div class="it">Fluorouracile</div>
	 * <!-- @formatter:on -->
	 */
	FLUOROURACIL("387172005", "2.16.840.1.113883.6.96", "Fluorouracil (substance)", "Fluorouracil",
			"Fluorouracil", "fluorouracil", "Fluorouracile"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluoxetine</div>
	 * <div class="de">Fluoxetin</div>
	 * <div class="fr">fluoxétine</div>
	 * <div class="it">Fluoxetina</div>
	 * <!-- @formatter:on -->
	 */
	FLUOXETINE("372767007", "2.16.840.1.113883.6.96", "Fluoxetine (substance)", "Fluoxetine",
			"Fluoxetin", "fluoxétine", "Fluoxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flupentixol</div>
	 * <div class="de">Flupentixol</div>
	 * <div class="fr">flupentixol</div>
	 * <div class="it">Flupentixolo</div>
	 * <!-- @formatter:on -->
	 */
	FLUPENTIXOL("387567006", "2.16.840.1.113883.6.96", "Flupentixol (substance)", "Flupentixol",
			"Flupentixol", "flupentixol", "Flupentixolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flupentixol decanoate</div>
	 * <div class="de">Flupentixol decanoat</div>
	 * <div class="fr">flupentixol décanoate</div>
	 * <div class="it">Flupentixolo decanoato</div>
	 * <!-- @formatter:on -->
	 */
	FLUPENTIXOL_DECANOATE("396062001", "2.16.840.1.113883.6.96",
			"Flupentixol decanoate (substance)", "Flupentixol decanoate", "Flupentixol decanoat",
			"flupentixol décanoate", "Flupentixolo decanoato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flurazepam</div>
	 * <div class="de">Flurazepam</div>
	 * <div class="fr">flurazépam</div>
	 * <div class="it">Flurazepam</div>
	 * <!-- @formatter:on -->
	 */
	FLURAZEPAM("387109000", "2.16.840.1.113883.6.96", "Flurazepam (substance)", "Flurazepam",
			"Flurazepam", "flurazépam", "Flurazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Flurbiprofen</div>
	 * <div class="de">Flurbiprofen</div>
	 * <div class="fr">flurbiprofène</div>
	 * <div class="it">Flurbiprofene</div>
	 * <!-- @formatter:on -->
	 */
	FLURBIPROFEN("373506008", "2.16.840.1.113883.6.96", "Flurbiprofen (substance)", "Flurbiprofen",
			"Flurbiprofen", "flurbiprofène", "Flurbiprofene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluticasone</div>
	 * <div class="de">Fluticason</div>
	 * <div class="fr">fluticasone</div>
	 * <div class="it">Fluticasone</div>
	 * <!-- @formatter:on -->
	 */
	FLUTICASONE("397192001", "2.16.840.1.113883.6.96", "Fluticasone (substance)", "Fluticasone",
			"Fluticason", "fluticasone", "Fluticasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluvastatin</div>
	 * <div class="de">Fluvastatin</div>
	 * <div class="fr">fluvastatine</div>
	 * <div class="it">Fluvastatina</div>
	 * <!-- @formatter:on -->
	 */
	FLUVASTATIN("387585004", "2.16.840.1.113883.6.96", "Fluvastatin (substance)", "Fluvastatin",
			"Fluvastatin", "fluvastatine", "Fluvastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fluvoxamine</div>
	 * <div class="de">Fluvoxamin</div>
	 * <div class="fr">fluvoxamine</div>
	 * <div class="it">Fluvoxamina</div>
	 * <!-- @formatter:on -->
	 */
	FLUVOXAMINE("372905008", "2.16.840.1.113883.6.96", "Fluvoxamine (substance)", "Fluvoxamine",
			"Fluvoxamin", "fluvoxamine", "Fluvoxamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Folic acid</div>
	 * <div class="de">Folsäure</div>
	 * <div class="fr">acide folique</div>
	 * <div class="it">Acido folico</div>
	 * <!-- @formatter:on -->
	 */
	FOLIC_ACID("63718003", "2.16.840.1.113883.6.96", "Folic acid (substance)", "Folic acid",
			"Folsäure", "acide folique", "Acido folico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Folinic acid</div>
	 * <div class="de">Folinsäure</div>
	 * <div class="fr">acide folinique</div>
	 * <div class="it">Acido folinico</div>
	 * <!-- @formatter:on -->
	 */
	FOLINIC_ACID("396065004", "2.16.840.1.113883.6.96", "Folinic acid (substance)", "Folinic acid",
			"Folinsäure", "acide folinique", "Acido folinico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Follitropin alfa</div>
	 * <div class="de">Follitropin alfa</div>
	 * <div class="fr">follitropine alfa</div>
	 * <div class="it">Follitropina alfa</div>
	 * <!-- @formatter:on -->
	 */
	FOLLITROPIN_ALFA("395862009", "2.16.840.1.113883.6.96", "Follitropin alfa (substance)",
			"Follitropin alfa", "Follitropin alfa", "follitropine alfa", "Follitropina alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Follitropin beta</div>
	 * <div class="de">Follitropin beta</div>
	 * <div class="fr">follitropine bêta</div>
	 * <div class="it">Follitropina beta</div>
	 * <!-- @formatter:on -->
	 */
	FOLLITROPIN_BETA("103028007", "2.16.840.1.113883.6.96", "Follitropin beta (substance)",
			"Follitropin beta", "Follitropin beta", "follitropine bêta", "Follitropina beta"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fomepizole</div>
	 * <div class="de">Fomepizol</div>
	 * <div class="fr">fomépizole</div>
	 * <div class="it">Fomepizolo</div>
	 * <!-- @formatter:on -->
	 */
	FOMEPIZOLE("386970006", "2.16.840.1.113883.6.96", "Fomepizole (substance)", "Fomepizole",
			"Fomepizol", "fomépizole", "Fomepizolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fondaparinux sodium</div>
	 * <div class="de">Fondaparinux natrium</div>
	 * <div class="fr">fondaparinux sodique</div>
	 * <div class="it">Fondaparinux sodico</div>
	 * <!-- @formatter:on -->
	 */
	FONDAPARINUX_SODIUM("385517000", "2.16.840.1.113883.6.96", "Fondaparinux sodium (substance)",
			"Fondaparinux sodium", "Fondaparinux natrium", "fondaparinux sodique",
			"Fondaparinux sodico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Formoterol</div>
	 * <div class="de">Formoterol</div>
	 * <div class="fr">formotérol</div>
	 * <div class="it">Formoterolo</div>
	 * <!-- @formatter:on -->
	 */
	FORMOTEROL("414289007", "2.16.840.1.113883.6.96", "Formoterol (substance)", "Formoterol",
			"Formoterol", "formotérol", "Formoterolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fosamprenavir</div>
	 * <div class="de">Fosamprenavir</div>
	 * <div class="fr">fosamprénavir</div>
	 * <div class="it">Fosamprenavir</div>
	 * <!-- @formatter:on -->
	 */
	FOSAMPRENAVIR("407017006", "2.16.840.1.113883.6.96", "Fosamprenavir (substance)",
			"Fosamprenavir", "Fosamprenavir", "fosamprénavir", "Fosamprenavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Foscarnet</div>
	 * <div class="de">Foscarnet</div>
	 * <div class="fr">foscarnet</div>
	 * <div class="it">Foscarnet</div>
	 * <!-- @formatter:on -->
	 */
	FOSCARNET("372902006", "2.16.840.1.113883.6.96", "Foscarnet (substance)", "Foscarnet",
			"Foscarnet", "foscarnet", "Foscarnet"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fosfomycin</div>
	 * <div class="de">Fosfomycin</div>
	 * <div class="fr">fosfomycine</div>
	 * <div class="it">Fosfomicina</div>
	 * <!-- @formatter:on -->
	 */
	FOSFOMYCIN("372534005", "2.16.840.1.113883.6.96", "Fosfomycin (substance)", "Fosfomycin",
			"Fosfomycin", "fosfomycine", "Fosfomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fosinopril</div>
	 * <div class="de">Fosinopril</div>
	 * <div class="fr">fosinopril</div>
	 * <div class="it">Fosinopril</div>
	 * <!-- @formatter:on -->
	 */
	FOSINOPRIL("372510000", "2.16.840.1.113883.6.96", "Fosinopril (substance)", "Fosinopril",
			"Fosinopril", "fosinopril", "Fosinopril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Frovatriptan</div>
	 * <div class="de">Frovatriptan</div>
	 * <div class="fr">frovatriptan</div>
	 * <div class="it">Frovatriptan</div>
	 * <!-- @formatter:on -->
	 */
	FROVATRIPTAN("411990007", "2.16.840.1.113883.6.96", "Frovatriptan (substance)", "Frovatriptan",
			"Frovatriptan", "frovatriptan", "Frovatriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fruit bromelain</div>
	 * <div class="de">Bromelain</div>
	 * <div class="fr">bromélaïnes</div>
	 * <div class="it">Bromelina</div>
	 * <!-- @formatter:on -->
	 */
	FRUIT_BROMELAIN("130663004", "2.16.840.1.113883.6.96", "Fruit bromelain (substance)",
			"Fruit bromelain", "Bromelain", "bromélaïnes", "Bromelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fulvestrant</div>
	 * <div class="de">Fulvestrant</div>
	 * <div class="fr">fulvestrant</div>
	 * <div class="it">Fulvestrant</div>
	 * <!-- @formatter:on -->
	 */
	FULVESTRANT("385519002", "2.16.840.1.113883.6.96", "Fulvestrant (substance)", "Fulvestrant",
			"Fulvestrant", "fulvestrant", "Fulvestrant"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Furosemide</div>
	 * <div class="de">Furosemid</div>
	 * <div class="fr">furosémide</div>
	 * <div class="it">Furosemide</div>
	 * <!-- @formatter:on -->
	 */
	FUROSEMIDE("387475002", "2.16.840.1.113883.6.96", "Furosemide (substance)", "Furosemide",
			"Furosemid", "furosémide", "Furosemide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Fusidic acid</div>
	 * <div class="de">Fusidinsäure</div>
	 * <div class="fr">acide fusidique</div>
	 * <div class="it">Acido fusidico</div>
	 * <!-- @formatter:on -->
	 */
	FUSIDIC_ACID("387530003", "2.16.840.1.113883.6.96", "Fusidic acid (substance)", "Fusidic acid",
			"Fusidinsäure", "acide fusidique", "Acido fusidico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gabapentin</div>
	 * <div class="de">Gabapentin</div>
	 * <div class="fr">gabapentine</div>
	 * <div class="it">Gabapentin</div>
	 * <!-- @formatter:on -->
	 */
	GABAPENTIN("386845007", "2.16.840.1.113883.6.96", "Gabapentin (substance)", "Gabapentin",
			"Gabapentin", "gabapentine", "Gabapentin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gadobutrol</div>
	 * <div class="de">Gadobutrol</div>
	 * <div class="fr">gadobutrol</div>
	 * <div class="it">Gadobutrolo</div>
	 * <!-- @formatter:on -->
	 */
	GADOBUTROL("418351005", "2.16.840.1.113883.6.96", "Gadobutrol (substance)", "Gadobutrol",
			"Gadobutrol", "gadobutrol", "Gadobutrolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gadoteric acid</div>
	 * <div class="de">Gadotersäure</div>
	 * <div class="fr">acide gadotérique</div>
	 * <div class="it">Acido gadoterico</div>
	 * <!-- @formatter:on -->
	 */
	GADOTERIC_ACID("710812003", "2.16.840.1.113883.6.96", "Gadoteric acid (substance)",
			"Gadoteric acid", "Gadotersäure", "acide gadotérique", "Acido gadoterico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Galactose</div>
	 * <div class="de">Galactose</div>
	 * <div class="fr">galactose</div>
	 * <div class="it">Galattosio</div>
	 * <!-- @formatter:on -->
	 */
	GALACTOSE("38182007", "2.16.840.1.113883.6.96", "Galactose (substance)", "Galactose",
			"Galactose", "galactose", "Galattosio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Galantamine</div>
	 * <div class="de">Galantamin</div>
	 * <div class="fr">galantamine</div>
	 * <div class="it">Galantamina</div>
	 * <!-- @formatter:on -->
	 */
	GALANTAMINE("395727007", "2.16.840.1.113883.6.96", "Galantamine (substance)", "Galantamine",
			"Galantamin", "galantamine", "Galantamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ganciclovir</div>
	 * <div class="de">Ganciclovir</div>
	 * <div class="fr">ganciclovir</div>
	 * <div class="it">Ganciclovir</div>
	 * <!-- @formatter:on -->
	 */
	GANCICLOVIR("372848001", "2.16.840.1.113883.6.96", "Ganciclovir (substance)", "Ganciclovir",
			"Ganciclovir", "ganciclovir", "Ganciclovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ganirelix</div>
	 * <div class="de">Ganirelix</div>
	 * <div class="fr">ganirélix</div>
	 * <div class="it">Ganirelix</div>
	 * <!-- @formatter:on -->
	 */
	GANIRELIX("395728002", "2.16.840.1.113883.6.96", "Ganirelix (substance)", "Ganirelix",
			"Ganirelix", "ganirélix", "Ganirelix"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gemcitabine</div>
	 * <div class="de">Gemcitabin</div>
	 * <div class="fr">gemcitabine</div>
	 * <div class="it">Gemcitabina</div>
	 * <!-- @formatter:on -->
	 */
	GEMCITABINE("386920008", "2.16.840.1.113883.6.96", "Gemcitabine (substance)", "Gemcitabine",
			"Gemcitabin", "gemcitabine", "Gemcitabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gemfibrozil</div>
	 * <div class="de">Gemfibrozil</div>
	 * <div class="fr">gemfibrozil</div>
	 * <div class="it">Gemfibrozil</div>
	 * <!-- @formatter:on -->
	 */
	GEMFIBROZIL("387189002", "2.16.840.1.113883.6.96", "Gemfibrozil (substance)", "Gemfibrozil",
			"Gemfibrozil", "gemfibrozil", "Gemfibrozil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gentamicin</div>
	 * <div class="de">Gentamicin</div>
	 * <div class="fr">gentamicine</div>
	 * <div class="it">Gentamicina</div>
	 * <!-- @formatter:on -->
	 */
	GENTAMICIN("387321007", "2.16.840.1.113883.6.96", "Gentamicin (substance)", "Gentamicin",
			"Gentamicin", "gentamicine", "Gentamicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gestodene</div>
	 * <div class="de">Gestoden</div>
	 * <div class="fr">gestodène</div>
	 * <div class="it">Gestodene</div>
	 * <!-- @formatter:on -->
	 */
	GESTODENE("395945000", "2.16.840.1.113883.6.96", "Gestodene (substance)", "Gestodene",
			"Gestoden", "gestodène", "Gestodene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ginkgo biloba</div>
	 * <div class="de">Ginkgo (Ginkgo biloba L.)</div>
	 * <div class="fr">ginkgo (Ginkgo biloba L.)</div>
	 * <div class="it">Ginko (Ginko biloba L.)</div>
	 * <!-- @formatter:on -->
	 */
	GINKGO_BILOBA("420733007", "2.16.840.1.113883.6.96", "Ginkgo biloba (substance)",
			"Ginkgo biloba", "Ginkgo (Ginkgo biloba L.)", "ginkgo (Ginkgo biloba L.)",
			"Ginko (Ginko biloba L.)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glatiramer</div>
	 * <div class="de">Glatiramer</div>
	 * <div class="fr">glatiramère</div>
	 * <div class="it">Glatiramer</div>
	 * <!-- @formatter:on -->
	 */
	GLATIRAMER("372535006", "2.16.840.1.113883.6.96", "Glatiramer (substance)", "Glatiramer",
			"Glatiramer", "glatiramère", "Glatiramer"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glibenclamide</div>
	 * <div class="de">Glibenclamid</div>
	 * <div class="fr">glibenclamide</div>
	 * <div class="it">Glibenclamide</div>
	 * <!-- @formatter:on -->
	 */
	GLIBENCLAMIDE("384978002", "2.16.840.1.113883.6.96", "Glibenclamide (substance)",
			"Glibenclamide", "Glibenclamid", "glibenclamide", "Glibenclamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gliclazide</div>
	 * <div class="de">Gliclazid</div>
	 * <div class="fr">gliclazide</div>
	 * <div class="it">Gliclazide</div>
	 * <!-- @formatter:on -->
	 */
	GLICLAZIDE("395731001", "2.16.840.1.113883.6.96", "Gliclazide (substance)", "Gliclazide",
			"Gliclazid", "gliclazide", "Gliclazide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glimepiride</div>
	 * <div class="de">Glimepirid</div>
	 * <div class="fr">glimépiride</div>
	 * <div class="it">Glimepiride</div>
	 * <!-- @formatter:on -->
	 */
	GLIMEPIRIDE("386966003", "2.16.840.1.113883.6.96", "Glimepiride (substance)", "Glimepiride",
			"Glimepirid", "glimépiride", "Glimepiride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glucagon</div>
	 * <div class="de">Glucagon</div>
	 * <div class="fr">glucagon</div>
	 * <div class="it">Glucagone</div>
	 * <!-- @formatter:on -->
	 */
	GLUCAGON("66603002", "2.16.840.1.113883.6.96", "Glucagon (substance)", "Glucagon", "Glucagon",
			"glucagon", "Glucagone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glucose</div>
	 * <div class="de">Glucose</div>
	 * <div class="fr">glucose</div>
	 * <div class="it">Glucosio</div>
	 * <!-- @formatter:on -->
	 */
	GLUCOSE("67079006", "2.16.840.1.113883.6.96", "Glucose (substance)", "Glucose", "Glucose",
			"glucose", "Glucosio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glyceryl trinitrate</div>
	 * <div class="de">Nitroglycerin</div>
	 * <div class="fr">nitroglycérine</div>
	 * <div class="it">Nitroglicerina</div>
	 * <!-- @formatter:on -->
	 */
	GLYCERYL_TRINITRATE("387404004", "2.16.840.1.113883.6.96", "Glyceryl trinitrate (substance)",
			"Glyceryl trinitrate", "Nitroglycerin", "nitroglycérine", "Nitroglicerina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glycine</div>
	 * <div class="de">Glycin</div>
	 * <div class="fr">glycine</div>
	 * <div class="it">Glicina</div>
	 * <!-- @formatter:on -->
	 */
	GLYCINE("15331006", "2.16.840.1.113883.6.96", "Glycine (substance)", "Glycine", "Glycin",
			"glycine", "Glicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glycocholic acid</div>
	 * <div class="de">Glycocholsäure</div>
	 * <div class="fr">acide glycocholique</div>
	 * <div class="it">Acido glicocolico</div>
	 * <!-- @formatter:on -->
	 */
	GLYCOCHOLIC_ACID("96314001", "2.16.840.1.113883.6.96", "Glycocholic acid (substance)",
			"Glycocholic acid", "Glycocholsäure", "acide glycocholique", "Acido glicocolico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Glycopyrronium</div>
	 * <div class="de">Glycopyrronium-Kation</div>
	 * <div class="fr">glycopyrronium</div>
	 * <div class="it">Glicopirronio</div>
	 * <!-- @formatter:on -->
	 */
	GLYCOPYRRONIUM("769097000", "2.16.840.1.113883.6.96", "Glycopyrronium (substance)",
			"Glycopyrronium", "Glycopyrronium-Kation", "glycopyrronium", "Glicopirronio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Golimumab</div>
	 * <div class="de">Golimumab</div>
	 * <div class="fr">golimumab</div>
	 * <div class="it">Golimumab</div>
	 * <!-- @formatter:on -->
	 */
	GOLIMUMAB("442435002", "2.16.840.1.113883.6.96", "Golimumab (substance)", "Golimumab",
			"Golimumab", "golimumab", "Golimumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gonadorelin</div>
	 * <div class="de">Gonadorelin</div>
	 * <div class="fr">gonadoréline</div>
	 * <div class="it">Gonadorelina</div>
	 * <!-- @formatter:on -->
	 */
	GONADORELIN("397197007", "2.16.840.1.113883.6.96", "Gonadorelin (substance)", "Gonadorelin",
			"Gonadorelin", "gonadoréline", "Gonadorelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Goserelin</div>
	 * <div class="de">Goserelin</div>
	 * <div class="fr">goséréline</div>
	 * <div class="it">Goserelin</div>
	 * <!-- @formatter:on -->
	 */
	GOSERELIN("108771008", "2.16.840.1.113883.6.96", "Goserelin (substance)", "Goserelin",
			"Goserelin", "goséréline", "Goserelin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gramicidin</div>
	 * <div class="de">Gramicidin</div>
	 * <div class="fr">gramicidine</div>
	 * <div class="it">Gramicidina</div>
	 * <!-- @formatter:on -->
	 */
	GRAMICIDIN("387524003", "2.16.840.1.113883.6.96", "Gramicidin (substance)", "Gramicidin",
			"Gramicidin", "gramicidine", "Gramicidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Granisetron</div>
	 * <div class="de">Granisetron</div>
	 * <div class="fr">granisétron</div>
	 * <div class="it">Granisetron</div>
	 * <!-- @formatter:on -->
	 */
	GRANISETRON("372489005", "2.16.840.1.113883.6.96", "Granisetron (substance)", "Granisetron",
			"Granisetron", "granisétron", "Granisetron"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Guaifenesin</div>
	 * <div class="de">Guaifenesin</div>
	 * <div class="fr">guaïfénésine</div>
	 * <div class="it">Guaifenesina</div>
	 * <!-- @formatter:on -->
	 */
	GUAIFENESIN("87174009", "2.16.840.1.113883.6.96", "Guaifenesin (substance)", "Guaifenesin",
			"Guaifenesin", "guaïfénésine", "Guaifenesina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Guanfacine</div>
	 * <div class="de">Guanfacin</div>
	 * <div class="fr">guanfacine</div>
	 * <div class="it">Guanfacina</div>
	 * <!-- @formatter:on -->
	 */
	GUANFACINE("372507007", "2.16.840.1.113883.6.96", "Guanfacine (substance)", "Guanfacine",
			"Guanfacin", "guanfacine", "Guanfacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Haemophilus influenzae type b vaccine</div>
	 * <div class="de">Haemophilus-influenza-b-Impfstoff</div>
	 * <div class="fr">vaccin Haemophilus influenza type B</div>
	 * <div class="it">Anti-Hemophilus influenzae tipo B (Hib) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	HAEMOPHILUS_INFLUENZAE_TYPE_B_VACCINE("412374001", "2.16.840.1.113883.6.96",
			"Haemophilus influenzae type b vaccine (substance)",
			"Haemophilus influenzae type b vaccine", "Haemophilus-influenza-b-Impfstoff",
			"vaccin Haemophilus influenza type B",
			"Anti-Hemophilus influenzae tipo B (Hib) vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Halcinonide</div>
	 * <div class="de">Halcinonid</div>
	 * <div class="fr">halcinonide</div>
	 * <div class="it">Alcinonide</div>
	 * <!-- @formatter:on -->
	 */
	HALCINONIDE("395735005", "2.16.840.1.113883.6.96", "Halcinonide (substance)", "Halcinonide",
			"Halcinonid", "halcinonide", "Alcinonide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Halometasone</div>
	 * <div class="de">Halometason</div>
	 * <div class="fr">halométasone</div>
	 * <div class="it">Alometasone</div>
	 * <!-- @formatter:on -->
	 */
	HALOMETASONE("704673003", "2.16.840.1.113883.6.96", "Halometasone (substance)", "Halometasone",
			"Halometason", "halométasone", "Alometasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Haloperidol</div>
	 * <div class="de">Haloperidol</div>
	 * <div class="fr">halopéridol</div>
	 * <div class="it">Aloperidolo</div>
	 * <!-- @formatter:on -->
	 */
	HALOPERIDOL("386837002", "2.16.840.1.113883.6.96", "Haloperidol (substance)", "Haloperidol",
			"Haloperidol", "halopéridol", "Aloperidolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Heparin</div>
	 * <div class="de">Heparin</div>
	 * <div class="fr">héparine</div>
	 * <div class="it">Eparina</div>
	 * <!-- @formatter:on -->
	 */
	HEPARIN("372877000", "2.16.840.1.113883.6.96", "Heparin (substance)", "Heparin", "Heparin",
			"héparine", "Eparina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hepatitis A virus vaccine</div>
	 * <div class="de">Hepatitis-A-Impfstoff</div>
	 * <div class="fr">hépatite A vaccin</div>
	 * <div class="it">Epatite A (HAV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	HEPATITIS_A_VIRUS_VACCINE("396423004", "2.16.840.1.113883.6.96",
			"Hepatitis A virus vaccine (substance)", "Hepatitis A virus vaccine",
			"Hepatitis-A-Impfstoff", "hépatite A vaccin", "Epatite A (HAV) vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hepatitis B antigen</div>
	 * <div class="de">Hepatitis B Antigen</div>
	 * <div class="fr">hepatitis B antigène</div>
	 * <div class="it">Epatite B antigene purificato</div>
	 * <!-- @formatter:on -->
	 */
	HEPATITIS_B_ANTIGEN("303233001", "2.16.840.1.113883.6.96", "Hepatitis B antigen (substance)",
			"Hepatitis B antigen", "Hepatitis B Antigen", "hepatitis B antigène",
			"Epatite B antigene purificato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hepatitis B virus recombinant vaccine</div>
	 * <div class="de">Hepatitis-B-Impfstoff, rekombiniert, monovalent</div>
	 * <div class="fr">hépatite B vaccin recombiné, monovalent</div>
	 * <div class="it">Epatite B vaccino ricombinato (ADNr)</div>
	 * <!-- @formatter:on -->
	 */
	HEPATITIS_B_VIRUS_RECOMBINANT_VACCINE("412402004", "2.16.840.1.113883.6.96",
			"Hepatitis B virus recombinant vaccine (substance)",
			"Hepatitis B virus recombinant vaccine",
			"Hepatitis-B-Impfstoff, rekombiniert, monovalent",
			"hépatite B vaccin recombiné, monovalent", "Epatite B vaccino ricombinato (ADNr)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hepatitis B virus vaccine</div>
	 * <div class="de">Hepatitis-B-Impfstoff</div>
	 * <div class="fr">vaccin hépatite B</div>
	 * <div class="it">Epatite B (HBV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	HEPATITIS_B_VIRUS_VACCINE("396424005", "2.16.840.1.113883.6.96",
			"Hepatitis B virus vaccine (substance)", "Hepatitis B virus vaccine",
			"Hepatitis-B-Impfstoff", "vaccin hépatite B", "Epatite B (HBV) vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hexamidine</div>
	 * <div class="de">Hexamidin</div>
	 * <div class="fr">hexamidine</div>
	 * <div class="it">Esamidina</div>
	 * <!-- @formatter:on -->
	 */
	HEXAMIDINE("703831002", "2.16.840.1.113883.6.96", "Hexamidine (substance)", "Hexamidine",
			"Hexamidin", "hexamidine", "Esamidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hexetidine</div>
	 * <div class="de">Hexetidin</div>
	 * <div class="fr">hexétidine</div>
	 * <div class="it">Esetidina</div>
	 * <!-- @formatter:on -->
	 */
	HEXETIDINE("387132001", "2.16.840.1.113883.6.96", "Hexetidine (substance)", "Hexetidine",
			"Hexetidin", "hexétidine", "Esetidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hexoprenaline</div>
	 * <div class="de">Hexoprenalin</div>
	 * <div class="fr">hexoprénaline</div>
	 * <div class="it">Esoprenalina</div>
	 * <!-- @formatter:on -->
	 */
	HEXOPRENALINE("704987001", "2.16.840.1.113883.6.96", "Hexoprenaline (substance)",
			"Hexoprenaline", "Hexoprenalin", "hexoprénaline", "Esoprenalina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Histidine</div>
	 * <div class="de">Histidin</div>
	 * <div class="fr">histidine</div>
	 * <div class="it">Istidina</div>
	 * <!-- @formatter:on -->
	 */
	HISTIDINE("60260004", "2.16.840.1.113883.6.96", "Histidine (substance)", "Histidine",
			"Histidin", "histidine", "Istidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human antithrombin III</div>
	 * <div class="de">Antithrombin III human</div>
	 * <div class="fr">antithrombine III humaine</div>
	 * <div class="it">Antitrombina III umana</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_ANTITHROMBIN_III("412564003", "2.16.840.1.113883.6.96",
			"Human antithrombin III (substance)", "Human antithrombin III",
			"Antithrombin III human", "antithrombine III humaine", "Antitrombina III umana"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human anti-D immunoglobulin</div>
	 * <div class="de">Anti-D-Immunglobulin vom Menschen</div>
	 * <div class="fr">immunoglobuline humaine anti-D</div>
	 * <div class="it">Immunoglobulina umana anti-D</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_ANTI_D_IMMUNOGLOBULIN("769102002", "2.16.840.1.113883.6.96",
			"Human anti-D immunoglobulin (substance)", "Human anti-D immunoglobulin",
			"Anti-D-Immunglobulin vom Menschen", "immunoglobuline humaine anti-D",
			"Immunoglobulina umana anti-D"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human chorionic gonadotropin</div>
	 * <div class="de">Choriongonadotropin</div>
	 * <div class="fr">gonadotrophine chorionique</div>
	 * <div class="it">Gonadotropina corionica umana (HCG)</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_CHORIONIC_GONADOTROPIN("59433001", "2.16.840.1.113883.6.96",
			"Human chorionic gonadotropin (substance)", "Human chorionic gonadotropin",
			"Choriongonadotropin", "gonadotrophine chorionique",
			"Gonadotropina corionica umana (HCG)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human fibrinogen</div>
	 * <div class="de">Fibrinogen (human)</div>
	 * <div class="fr">fibrinogène humain</div>
	 * <div class="it">Fibrinogeno umano</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_FIBRINOGEN("418326009", "2.16.840.1.113883.6.96", "Human fibrinogen (substance)",
			"Human fibrinogen", "Fibrinogen (human)", "fibrinogène humain", "Fibrinogeno umano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human immunoglobulin</div>
	 * <div class="de">Immunglobulin vom Menschen</div>
	 * <div class="fr">immunoglobuline humaine</div>
	 * <div class="it">Immunoglobulina umana</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_IMMUNOGLOBULIN("420084002", "2.16.840.1.113883.6.96", "Human immunoglobulin (substance)",
			"Human immunoglobulin", "Immunglobulin vom Menschen", "immunoglobuline humaine",
			"Immunoglobulina umana"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human immunoglobulin G</div>
	 * <div class="de">Immunglobulin G human (IgG)</div>
	 * <div class="fr">iImmunoglobulinum gamma humanum (IgG)</div>
	 * <div class="it">Immunoglobulina G umana (IgG)</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_IMMUNOGLOBULIN_G("722197004", "2.16.840.1.113883.6.96",
			"Human immunoglobulin G (substance)", "Human immunoglobulin G",
			"Immunglobulin G human (IgG)", "iImmunoglobulinum gamma humanum (IgG)",
			"Immunoglobulina G umana (IgG)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human insulin</div>
	 * <div class="de">Insulin human</div>
	 * <div class="fr">insulines humaines</div>
	 * <div class="it">Insulina umana</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_INSULIN("96367001", "2.16.840.1.113883.6.96", "Human insulin (substance)",
			"Human insulin", "Insulin human", "insulines humaines", "Insulina umana"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human menopausal gonadotropin</div>
	 * <div class="de">Menotropin</div>
	 * <div class="fr">ménotropine</div>
	 * <div class="it">Menotropina</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_MENOPAUSAL_GONADOTROPIN("8203003", "2.16.840.1.113883.6.96",
			"Human menopausal gonadotropin (substance)", "Human menopausal gonadotropin",
			"Menotropin", "ménotropine", "Menotropina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human papilloma virus type 16 and type 18 vaccine</div>
	 * <div class="de">Papillomavirus (human)-Impfstoff, Typ 16 und Typ 18</div>
	 * <div class="fr">papillomavirus vaccin humain, type 16 et type 18</div>
	 * <div class="it">Papillomavirus umano tipo 16 e tipo 18 (HPV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_PAPILLOMA_VIRUS_TYPE_16_AND_TYPE_18_VACCINE("722219008", "2.16.840.1.113883.6.96",
			"Human papilloma virus type 16 and type 18 vaccine (substance)",
			"Human papilloma virus type 16 and type 18 vaccine",
			"Papillomavirus (human)-Impfstoff, Typ 16 und Typ 18",
			"papillomavirus vaccin humain, type 16 et type 18",
			"Papillomavirus umano tipo 16 e tipo 18 (HPV) vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Human papilloma virus type 9 vaccine</div>
	 * <div class="de">Papillomavirus (human)-Impfstoff, Typ 9</div>
	 * <div class="fr">papillomavirus vaccin humain, type 9</div>
	 * <div class="it">Papillomavirus umano tipo 9 (HPV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	HUMAN_PAPILLOMA_VIRUS_TYPE_9_VACCINE("724330005", "2.16.840.1.113883.6.96",
			"Human papilloma virus type 9 vaccine (substance)",
			"Human papilloma virus type 9 vaccine", "Papillomavirus (human)-Impfstoff, Typ 9",
			"papillomavirus vaccin humain, type 9", "Papillomavirus umano tipo 9 (HPV) vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hyaluronic acid</div>
	 * <div class="de">Hyaluronsäure</div>
	 * <div class="fr">acide hyaluronique</div>
	 * <div class="it">Acido ialuronico</div>
	 * <!-- @formatter:on -->
	 */
	HYALURONIC_ACID("38218009", "2.16.840.1.113883.6.96", "Hyaluronic acid (substance)",
			"Hyaluronic acid", "Hyaluronsäure", "acide hyaluronique", "Acido ialuronico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydrochlorothiazide</div>
	 * <div class="de">Hydrochlorothiazid</div>
	 * <div class="fr">hydrochlorothiazide</div>
	 * <div class="it">Idrocloratiazide</div>
	 * <!-- @formatter:on -->
	 */
	HYDROCHLOROTHIAZIDE("387525002", "2.16.840.1.113883.6.96", "Hydrochlorothiazide (substance)",
			"Hydrochlorothiazide", "Hydrochlorothiazid", "hydrochlorothiazide", "Idrocloratiazide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydrocodone</div>
	 * <div class="de">Hydrocodon</div>
	 * <div class="fr">hydrocodone</div>
	 * <div class="it">Idrocodone</div>
	 * <!-- @formatter:on -->
	 */
	HYDROCODONE("372671002", "2.16.840.1.113883.6.96", "Hydrocodone (substance)", "Hydrocodone",
			"Hydrocodon", "hydrocodone", "Idrocodone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydrocortisone</div>
	 * <div class="de">Hydrocortison</div>
	 * <div class="fr">hydrocortisone</div>
	 * <div class="it">Idrocortisone</div>
	 * <!-- @formatter:on -->
	 */
	HYDROCORTISONE("396458002", "2.16.840.1.113883.6.96", "Hydrocortisone (substance)",
			"Hydrocortisone", "Hydrocortison", "hydrocortisone", "Idrocortisone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydrocortisone acetate</div>
	 * <div class="de">Hydrocortison acetat</div>
	 * <div class="fr">hydrocortisone acétate</div>
	 * <div class="it">Idrocortisone acetato</div>
	 * <!-- @formatter:on -->
	 */
	HYDROCORTISONE_ACETATE("79380007", "2.16.840.1.113883.6.96",
			"Hydrocortisone acetate (substance)", "Hydrocortisone acetate", "Hydrocortison acetat",
			"hydrocortisone acétate", "Idrocortisone acetato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydrogen peroxide</div>
	 * <div class="de">Wasserstoffperoxid</div>
	 * <div class="fr">peroxyde d´hydrogène</div>
	 * <div class="it">Idrogeno perossido</div>
	 * <!-- @formatter:on -->
	 */
	HYDROGEN_PEROXIDE("387171003", "2.16.840.1.113883.6.96", "Hydrogen peroxide (substance)",
			"Hydrogen peroxide", "Wasserstoffperoxid", "peroxyde d´hydrogène",
			"Idrogeno perossido"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydromorphone</div>
	 * <div class="de">Hydromorphon</div>
	 * <div class="fr">hydromorphone</div>
	 * <div class="it">Idromorfone</div>
	 * <!-- @formatter:on -->
	 */
	HYDROMORPHONE("44508008", "2.16.840.1.113883.6.96", "Hydromorphone (substance)",
			"Hydromorphone", "Hydromorphon", "hydromorphone", "Idromorfone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydroquinone</div>
	 * <div class="de">Hydrochinon</div>
	 * <div class="fr">hydroquinone</div>
	 * <div class="it">Idrochinone</div>
	 * <!-- @formatter:on -->
	 */
	HYDROQUINONE("387422001", "2.16.840.1.113883.6.96", "Hydroquinone (substance)", "Hydroquinone",
			"Hydrochinon", "hydroquinone", "Idrochinone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydrotalcite</div>
	 * <div class="de">Hydrotalcit</div>
	 * <div class="fr">hydrotalcite</div>
	 * <div class="it">Idrotalcite</div>
	 * <!-- @formatter:on -->
	 */
	HYDROTALCITE("395738007", "2.16.840.1.113883.6.96", "Hydrotalcite (substance)", "Hydrotalcite",
			"Hydrotalcit", "hydrotalcite", "Idrotalcite"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydroxocobalamin</div>
	 * <div class="de">Hydroxocobalamin</div>
	 * <div class="fr">hydroxocobalamine</div>
	 * <div class="it">Idrossocobalamina</div>
	 * <!-- @formatter:on -->
	 */
	HYDROXOCOBALAMIN("409258004", "2.16.840.1.113883.6.96", "Hydroxocobalamin (substance)",
			"Hydroxocobalamin", "Hydroxocobalamin", "hydroxocobalamine", "Idrossocobalamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydroxycarbamide</div>
	 * <div class="de">Hydroxycarbamid</div>
	 * <div class="fr">hydroxycarbamide</div>
	 * <div class="it">Idrossicarbamide</div>
	 * <!-- @formatter:on -->
	 */
	HYDROXYCARBAMIDE("387314007", "2.16.840.1.113883.6.96", "Hydroxycarbamide (substance)",
			"Hydroxycarbamide", "Hydroxycarbamid", "hydroxycarbamide", "Idrossicarbamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydroxychloroquine</div>
	 * <div class="de">Hydroxychloroquin</div>
	 * <div class="fr">hydroxychloroquine</div>
	 * <div class="it">Idrossiclorochina</div>
	 * <!-- @formatter:on -->
	 */
	HYDROXYCHLOROQUINE("373540008", "2.16.840.1.113883.6.96", "Hydroxychloroquine (substance)",
			"Hydroxychloroquine", "Hydroxychloroquin", "hydroxychloroquine", "Idrossiclorochina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hydroxyzine</div>
	 * <div class="de">Hydroxyzin</div>
	 * <div class="fr">hydroxyzine</div>
	 * <div class="it">Idrossizina</div>
	 * <!-- @formatter:on -->
	 */
	HYDROXYZINE("372856003", "2.16.840.1.113883.6.96", "Hydroxyzine (substance)", "Hydroxyzine",
			"Hydroxyzin", "hydroxyzine", "Idrossizina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hypericin</div>
	 * <div class="de">Hypericin</div>
	 * <div class="fr">hypéricine</div>
	 * <div class="it">Ipericina</div>
	 * <!-- @formatter:on -->
	 */
	HYPERICIN("123681008", "2.16.840.1.113883.6.96", "Hypericin (substance)", "Hypericin",
			"Hypericin", "hypéricine", "Ipericina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Hypericum perforatum extract</div>
	 * <div class="de">Johanniskraut (Hypericum perforatum L.)</div>
	 * <div class="fr">millepertuis (Hypericum perforatum L.)</div>
	 * <div class="it">Iperico (Hypericum perforatum L.)</div>
	 * <!-- @formatter:on -->
	 */
	HYPERICUM_PERFORATUM_EXTRACT("412515006", "2.16.840.1.113883.6.96",
			"Hypericum perforatum extract (substance)", "Hypericum perforatum extract",
			"Johanniskraut (Hypericum perforatum L.)", "millepertuis (Hypericum perforatum L.)",
			"Iperico (Hypericum perforatum L.)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ibandronic acid</div>
	 * <div class="de">Ibandronsäure</div>
	 * <div class="fr">acide ibandronique (ibandronate)</div>
	 * <div class="it">Acido ibandronico</div>
	 * <!-- @formatter:on -->
	 */
	IBANDRONIC_ACID("420936009", "2.16.840.1.113883.6.96", "Ibandronic acid (substance)",
			"Ibandronic acid", "Ibandronsäure", "acide ibandronique (ibandronate)",
			"Acido ibandronico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ibuprofen</div>
	 * <div class="de">Ibuprofen</div>
	 * <div class="fr">ibuprofène</div>
	 * <div class="it">Ibuprofene</div>
	 * <!-- @formatter:on -->
	 */
	IBUPROFEN("387207008", "2.16.840.1.113883.6.96", "Ibuprofen (substance)", "Ibuprofen",
			"Ibuprofen", "ibuprofène", "Ibuprofene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ibuprofen lysine</div>
	 * <div class="de">Ibuprofen lysin</div>
	 * <div class="fr">ibuprofène lysine</div>
	 * <div class="it">Ibuprofene lisina</div>
	 * <!-- @formatter:on -->
	 */
	IBUPROFEN_LYSINE("425516000", "2.16.840.1.113883.6.96", "Ibuprofen lysine (substance)",
			"Ibuprofen lysine", "Ibuprofen lysin", "ibuprofène lysine", "Ibuprofene lisina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Icatibant</div>
	 * <div class="de">Icatibant</div>
	 * <div class="fr">icatibant</div>
	 * <div class="it">Icatibant</div>
	 * <!-- @formatter:on -->
	 */
	ICATIBANT("703834005", "2.16.840.1.113883.6.96", "Icatibant (substance)", "Icatibant",
			"Icatibant", "icatibant", "Icatibant"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Idarubicin</div>
	 * <div class="de">Idarubicin</div>
	 * <div class="fr">idarubicine</div>
	 * <div class="it">Idarubicina</div>
	 * <!-- @formatter:on -->
	 */
	IDARUBICIN("372539000", "2.16.840.1.113883.6.96", "Idarubicin (substance)", "Idarubicin",
			"Idarubicin", "idarubicine", "Idarubicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Idarucizumab</div>
	 * <div class="de">Idarucizumab</div>
	 * <div class="fr">idarucizumab</div>
	 * <div class="it">Idarucizumab</div>
	 * <!-- @formatter:on -->
	 */
	IDARUCIZUMAB("716017002", "2.16.840.1.113883.6.96", "Idarucizumab (substance)", "Idarucizumab",
			"Idarucizumab", "idarucizumab", "Idarucizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Idebenone</div>
	 * <div class="de">Idebenon</div>
	 * <div class="fr">idébénone</div>
	 * <div class="it">Idebenone</div>
	 * <!-- @formatter:on -->
	 */
	IDEBENONE("429666007", "2.16.840.1.113883.6.96", "Idebenone (substance)", "Idebenone",
			"Idebenon", "idébénone", "Idebenone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Idelalisib</div>
	 * <div class="de">Idelalisib</div>
	 * <div class="fr">idélalisib</div>
	 * <div class="it">Idelalisib</div>
	 * <!-- @formatter:on -->
	 */
	IDELALISIB("710278000", "2.16.840.1.113883.6.96", "Idelalisib (substance)", "Idelalisib",
			"Idelalisib", "idélalisib", "Idelalisib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ifosfamide</div>
	 * <div class="de">Ifosfamid</div>
	 * <div class="fr">ifosfamide</div>
	 * <div class="it">Ifosfamide</div>
	 * <!-- @formatter:on -->
	 */
	IFOSFAMIDE("386904003", "2.16.840.1.113883.6.96", "Ifosfamide (substance)", "Ifosfamide",
			"Ifosfamid", "ifosfamide", "Ifosfamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iloprost</div>
	 * <div class="de">Iloprost</div>
	 * <div class="fr">iloprost</div>
	 * <div class="it">Iloprost</div>
	 * <!-- @formatter:on -->
	 */
	ILOPROST("395740002", "2.16.840.1.113883.6.96", "Iloprost (substance)", "Iloprost", "Iloprost",
			"iloprost", "Iloprost"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Imatinib</div>
	 * <div class="de">Imatinib</div>
	 * <div class="fr">imatinib</div>
	 * <div class="it">Imatinib</div>
	 * <!-- @formatter:on -->
	 */
	IMATINIB("414460008", "2.16.840.1.113883.6.96", "Imatinib (substance)", "Imatinib", "Imatinib",
			"imatinib", "Imatinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Imiglucerase</div>
	 * <div class="de">Imiglucerase</div>
	 * <div class="fr">imiglucérase</div>
	 * <div class="it">Imiglucerasi</div>
	 * <!-- @formatter:on -->
	 */
	IMIGLUCERASE("386968002", "2.16.840.1.113883.6.96", "Imiglucerase (substance)", "Imiglucerase",
			"Imiglucerase", "imiglucérase", "Imiglucerasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Imipenem</div>
	 * <div class="de">Imipenem</div>
	 * <div class="fr">imipénem</div>
	 * <div class="it">Imipenem</div>
	 * <!-- @formatter:on -->
	 */
	IMIPENEM("46558003", "2.16.840.1.113883.6.96", "Imipenem (substance)", "Imipenem", "Imipenem",
			"imipénem", "Imipenem"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Imipramine</div>
	 * <div class="de">Imipramin</div>
	 * <div class="fr">imipramine</div>
	 * <div class="it">Imipramina</div>
	 * <!-- @formatter:on -->
	 */
	IMIPRAMINE("372718005", "2.16.840.1.113883.6.96", "Imipramine (substance)", "Imipramine",
			"Imipramin", "imipramine", "Imipramina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Imiquimod</div>
	 * <div class="de">Imiquimod</div>
	 * <div class="fr">imiquimod</div>
	 * <div class="it">Imiquimod</div>
	 * <!-- @formatter:on -->
	 */
	IMIQUIMOD("386941002", "2.16.840.1.113883.6.96", "Imiquimod (substance)", "Imiquimod",
			"Imiquimod", "imiquimod", "Imiquimod"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Immunoglobulin A</div>
	 * <div class="de">Immunglobulin A human (IgA)</div>
	 * <div class="fr">immunglobulin A human (IgA)</div>
	 * <div class="it">Immunoglobulina A umana (IgA)</div>
	 * <!-- @formatter:on -->
	 */
	IMMUNOGLOBULIN_A("46046006", "2.16.840.1.113883.6.96", "Immunoglobulin A (substance)",
			"Immunoglobulin A", "Immunglobulin A human (IgA)", "immunglobulin A human (IgA)",
			"Immunoglobulina A umana (IgA)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Immunoglobulin M</div>
	 * <div class="de">Immunglobulin M human (IgM)</div>
	 * <div class="fr">immunoglobuline M humaine (IgM)</div>
	 * <div class="it">Immunoglobulina M umana (IgM)</div>
	 * <!-- @formatter:on -->
	 */
	IMMUNOGLOBULIN_M("74889000", "2.16.840.1.113883.6.96", "Immunoglobulin M (substance)",
			"Immunoglobulin M", "Immunglobulin M human (IgM)", "immunoglobuline M humaine (IgM)",
			"Immunoglobulina M umana (IgM)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Inactivated Poliovirus vaccine</div>
	 * <div class="de">Poliomyelitis-Impfstoff inaktiviert</div>
	 * <div class="fr">poliomyélite vaccin inactivé</div>
	 * <div class="it">Poliomielite vaccino inattivato</div>
	 * <!-- @formatter:on -->
	 */
	INACTIVATED_POLIOVIRUS_VACCINE("396435000", "2.16.840.1.113883.6.96",
			"Inactivated Poliovirus vaccine (substance)", "Inactivated Poliovirus vaccine",
			"Poliomyelitis-Impfstoff inaktiviert", "poliomyélite vaccin inactivé",
			"Poliomielite vaccino inattivato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Indacaterol</div>
	 * <div class="de">Indacaterol</div>
	 * <div class="fr">indacatérol</div>
	 * <div class="it">Indacaterol</div>
	 * <!-- @formatter:on -->
	 */
	INDACATEROL("702801003", "2.16.840.1.113883.6.96", "Indacaterol (substance)", "Indacaterol",
			"Indacaterol", "indacatérol", "Indacaterol"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Indapamide</div>
	 * <div class="de">Indapamid</div>
	 * <div class="fr">indapamide</div>
	 * <div class="it">Indapamide</div>
	 * <!-- @formatter:on -->
	 */
	INDAPAMIDE("387419003", "2.16.840.1.113883.6.96", "Indapamide (substance)", "Indapamide",
			"Indapamid", "indapamide", "Indapamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Indometacin</div>
	 * <div class="de">Indometacin</div>
	 * <div class="fr">indométacine</div>
	 * <div class="it">Indometacina</div>
	 * <!-- @formatter:on -->
	 */
	INDOMETACIN("373513008", "2.16.840.1.113883.6.96", "Indometacin (substance)", "Indometacin",
			"Indometacin", "indométacine", "Indometacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Infliximab</div>
	 * <div class="de">Infliximab</div>
	 * <div class="fr">infliximab</div>
	 * <div class="it">Infliximab</div>
	 * <!-- @formatter:on -->
	 */
	INFLIXIMAB("386891004", "2.16.840.1.113883.6.96", "Infliximab (substance)", "Infliximab",
			"Infliximab", "infliximab", "Infliximab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Influenza split virion vaccine</div>
	 * <div class="de">Influenza-Spalt-Impfstoff, inaktiviert</div>
	 * <div class="fr">vaccin grippal inactivé, virion fragmenté</div>
	 * <div class="it">Influenza vaccino inattivato, virus frammentati</div>
	 * <!-- @formatter:on -->
	 */
	INFLUENZA_SPLIT_VIRION_VACCINE("419826009", "2.16.840.1.113883.6.96",
			"Influenza split virion vaccine (substance)", "Influenza split virion vaccine",
			"Influenza-Spalt-Impfstoff, inaktiviert", "vaccin grippal inactivé, virion fragmenté",
			"Influenza vaccino inattivato, virus frammentati"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Inositol</div>
	 * <div class="de">Inositol</div>
	 * <div class="fr">inositol</div>
	 * <div class="it">Inositolo</div>
	 * <!-- @formatter:on -->
	 */
	INOSITOL("72164009", "2.16.840.1.113883.6.96", "Inositol (substance)", "Inositol", "Inositol",
			"inositol", "Inositolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Insulin aspart</div>
	 * <div class="de">Insulin aspart</div>
	 * <div class="fr">insuline asparte</div>
	 * <div class="it">Insulina aspartat</div>
	 * <!-- @formatter:on -->
	 */
	INSULIN_ASPART("325072002", "2.16.840.1.113883.6.96", "Insulin aspart (substance)",
			"Insulin aspart", "Insulin aspart", "insuline asparte", "Insulina aspartat"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Insulin degludec</div>
	 * <div class="de">Insulin degludec</div>
	 * <div class="fr">insuline dégludec</div>
	 * <div class="it">Insulina degludec</div>
	 * <!-- @formatter:on -->
	 */
	INSULIN_DEGLUDEC("710281005", "2.16.840.1.113883.6.96", "Insulin degludec (substance)",
			"Insulin degludec", "Insulin degludec", "insuline dégludec", "Insulina degludec"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Insulin detemir</div>
	 * <div class="de">Insulin detemir</div>
	 * <div class="fr">insuline détémir</div>
	 * <div class="it">Insulina detemir</div>
	 * <!-- @formatter:on -->
	 */
	INSULIN_DETEMIR("414515005", "2.16.840.1.113883.6.96", "Insulin detemir (substance)",
			"Insulin detemir", "Insulin detemir", "insuline détémir", "Insulina detemir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Insulin glargine</div>
	 * <div class="de">Insulin glargin</div>
	 * <div class="fr">insuline glargine</div>
	 * <div class="it">Insulina glargine</div>
	 * <!-- @formatter:on -->
	 */
	INSULIN_GLARGINE("411529005", "2.16.840.1.113883.6.96", "Insulin glargine (substance)",
			"Insulin glargine", "Insulin glargin", "insuline glargine", "Insulina glargine"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Insulin glulisine</div>
	 * <div class="de">Insulin glulisin</div>
	 * <div class="fr">insuline glulisine</div>
	 * <div class="it">Insulina glulisina</div>
	 * <!-- @formatter:on -->
	 */
	INSULIN_GLULISINE("411530000", "2.16.840.1.113883.6.96", "Insulin glulisine (substance)",
			"Insulin glulisine", "Insulin glulisin", "insuline glulisine", "Insulina glulisina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Insulin lispro</div>
	 * <div class="de">Insulin lispro</div>
	 * <div class="fr">insuline lispro</div>
	 * <div class="it">Insulina lispro</div>
	 * <!-- @formatter:on -->
	 */
	INSULIN_LISPRO("412210000", "2.16.840.1.113883.6.96", "Insulin lispro (substance)",
			"Insulin lispro", "Insulin lispro", "insuline lispro", "Insulina lispro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Interferon alfa-2a</div>
	 * <div class="de">Interferon alfa-2a</div>
	 * <div class="fr">interféron alfa 2a</div>
	 * <div class="it">Interferone alfa 2a</div>
	 * <!-- @formatter:on -->
	 */
	INTERFERON_ALFA_2A("386914007", "2.16.840.1.113883.6.96", "Interferon alfa-2a (substance)",
			"Interferon alfa-2a", "Interferon alfa-2a", "interféron alfa 2a",
			"Interferone alfa 2a"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Interferon alfa-2b</div>
	 * <div class="de">Interferon alfa-2b</div>
	 * <div class="fr">interféron alfa 2b</div>
	 * <div class="it">Interferone alfa 2b</div>
	 * <!-- @formatter:on -->
	 */
	INTERFERON_ALFA_2B("386915008", "2.16.840.1.113883.6.96", "Interferon alfa-2b (substance)",
			"Interferon alfa-2b", "Interferon alfa-2b", "interféron alfa 2b",
			"Interferone alfa 2b"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Interferon beta-1a</div>
	 * <div class="de">Interferon beta-1a</div>
	 * <div class="fr">interféron bêta-1a</div>
	 * <div class="it">Interferone beta 1a</div>
	 * <!-- @formatter:on -->
	 */
	INTERFERON_BETA_1A("386902004", "2.16.840.1.113883.6.96", "Interferon beta-1a (substance)",
			"Interferon beta-1a", "Interferon beta-1a", "interféron bêta-1a",
			"Interferone beta 1a"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Interferon beta-1b</div>
	 * <div class="de">Interferon beta-1b</div>
	 * <div class="fr">interféron bêta 1b</div>
	 * <div class="it">Interferone beta 1b</div>
	 * <!-- @formatter:on -->
	 */
	INTERFERON_BETA_1B("386903009", "2.16.840.1.113883.6.96", "Interferon beta-1b (substance)",
			"Interferon beta-1b", "Interferon beta-1b", "interféron bêta 1b",
			"Interferone beta 1b"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Interferon gamma-1b</div>
	 * <div class="de">Interferon gamma-1b</div>
	 * <div class="fr">interféron gamma 1b</div>
	 * <div class="it">Interferone gamma 1b</div>
	 * <!-- @formatter:on -->
	 */
	INTERFERON_GAMMA_1B("386901006", "2.16.840.1.113883.6.96", "Interferon gamma-1b (substance)",
			"Interferon gamma-1b", "Interferon gamma-1b", "interféron gamma 1b",
			"Interferone gamma 1b"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Inulin</div>
	 * <div class="de">Inulin</div>
	 * <div class="fr">inuline</div>
	 * <div class="it">Inulina</div>
	 * <!-- @formatter:on -->
	 */
	INULIN("32154009", "2.16.840.1.113883.6.96", "Inulin (substance)", "Inulin", "Inulin",
			"inuline", "Inulina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iodixanol</div>
	 * <div class="de">Iodixanol</div>
	 * <div class="fr">iodixanol</div>
	 * <div class="it">Iodixanolo</div>
	 * <!-- @formatter:on -->
	 */
	IODIXANOL("395750001", "2.16.840.1.113883.6.96", "Iodixanol (substance)", "Iodixanol",
			"Iodixanol", "iodixanol", "Iodixanolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iohexol</div>
	 * <div class="de">Iohexol</div>
	 * <div class="fr">iohexol</div>
	 * <div class="it">Ioexolo</div>
	 * <!-- @formatter:on -->
	 */
	IOHEXOL("395751002", "2.16.840.1.113883.6.96", "Iohexol (substance)", "Iohexol", "Iohexol",
			"iohexol", "Ioexolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iopamidol</div>
	 * <div class="de">Iopamidol</div>
	 * <div class="fr">iopamidol</div>
	 * <div class="it">Iopamidolo</div>
	 * <!-- @formatter:on -->
	 */
	IOPAMIDOL("395754005", "2.16.840.1.113883.6.96", "Iopamidol (substance)", "Iopamidol",
			"Iopamidol", "iopamidol", "Iopamidolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iopromide</div>
	 * <div class="de">Iopromid</div>
	 * <div class="fr">iopromide</div>
	 * <div class="it">Iopromide</div>
	 * <!-- @formatter:on -->
	 */
	IOPROMIDE("395756007", "2.16.840.1.113883.6.96", "Iopromide (substance)", "Iopromide",
			"Iopromid", "iopromide", "Iopromide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ipilimumab</div>
	 * <div class="de">Ipilimumab</div>
	 * <div class="fr">ipilimumab</div>
	 * <div class="it">Ipilimumab</div>
	 * <!-- @formatter:on -->
	 */
	IPILIMUMAB("697995005", "2.16.840.1.113883.6.96", "Ipilimumab (substance)", "Ipilimumab",
			"Ipilimumab", "ipilimumab", "Ipilimumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ipratropium</div>
	 * <div class="de">Ipratropium</div>
	 * <div class="fr">ipratropium</div>
	 * <div class="it">Ipratropio</div>
	 * <!-- @formatter:on -->
	 */
	IPRATROPIUM("372518007", "2.16.840.1.113883.6.96", "Ipratropium (substance)", "Ipratropium",
			"Ipratropium", "ipratropium", "Ipratropio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Irbesartan</div>
	 * <div class="de">Irbesartan</div>
	 * <div class="fr">irbésartan</div>
	 * <div class="it">Irbesartan</div>
	 * <!-- @formatter:on -->
	 */
	IRBESARTAN("386877005", "2.16.840.1.113883.6.96", "Irbesartan (substance)", "Irbesartan",
			"Irbesartan", "irbésartan", "Irbesartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Irinotecan</div>
	 * <div class="de">Irinotecan</div>
	 * <div class="fr">irinotécan</div>
	 * <div class="it">Irinotecan</div>
	 * <!-- @formatter:on -->
	 */
	IRINOTECAN("372538008", "2.16.840.1.113883.6.96", "Irinotecan (substance)", "Irinotecan",
			"Irinotecan", "irinotécan", "Irinotecan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iron</div>
	 * <div class="de">Eisen</div>
	 * <div class="fr">fer</div>
	 * <div class="it">Ferro</div>
	 * <!-- @formatter:on -->
	 */
	IRON("3829006", "2.16.840.1.113883.6.96", "Iron (substance)", "Iron", "Eisen", "fer", "Ferro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Iron polymaltose</div>
	 * <div class="de">Eisen(III)-hydroxid-Polymaltose-Komplex</div>
	 * <div class="fr">fer hydroxyde polymalté</div>
	 * <div class="it">Ferro (III) idrossido polimaltosato</div>
	 * <!-- @formatter:on -->
	 */
	IRON_POLYMALTOSE("708805001", "2.16.840.1.113883.6.96", "Iron polymaltose (substance)",
			"Iron polymaltose", "Eisen(III)-hydroxid-Polymaltose-Komplex",
			"fer hydroxyde polymalté", "Ferro (III) idrossido polimaltosato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isavuconazole</div>
	 * <div class="de">Isavuconazol</div>
	 * <div class="fr">isavuconazole</div>
	 * <div class="it">Isavuconazolo</div>
	 * <!-- @formatter:on -->
	 */
	ISAVUCONAZOLE("765386003", "2.16.840.1.113883.6.96", "Isavuconazole (substance)",
			"Isavuconazole", "Isavuconazol", "isavuconazole", "Isavuconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isoconazole</div>
	 * <div class="de">Isoconazol</div>
	 * <div class="fr">isoconazole</div>
	 * <div class="it">Isoconazolo</div>
	 * <!-- @formatter:on -->
	 */
	ISOCONAZOLE("418371001", "2.16.840.1.113883.6.96", "Isoconazole (substance)", "Isoconazole",
			"Isoconazol", "isoconazole", "Isoconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isoleucine</div>
	 * <div class="de">Isoleucin</div>
	 * <div class="fr">isoleucine</div>
	 * <div class="it">Isoleucina</div>
	 * <!-- @formatter:on -->
	 */
	ISOLEUCINE("14971004", "2.16.840.1.113883.6.96", "Isoleucine (substance)", "Isoleucine",
			"Isoleucin", "isoleucine", "Isoleucina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isoniazid</div>
	 * <div class="de">Isoniazid</div>
	 * <div class="fr">isoniazide</div>
	 * <div class="it">Isoniazide</div>
	 * <!-- @formatter:on -->
	 */
	ISONIAZID("387472004", "2.16.840.1.113883.6.96", "Isoniazid (substance)", "Isoniazid",
			"Isoniazid", "isoniazide", "Isoniazide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isoprenaline</div>
	 * <div class="de">Isoprenalin</div>
	 * <div class="fr">isoprénaline</div>
	 * <div class="it">Isoprenalina</div>
	 * <!-- @formatter:on -->
	 */
	ISOPRENALINE("372781009", "2.16.840.1.113883.6.96", "Isoprenaline (substance)", "Isoprenaline",
			"Isoprenalin", "isoprénaline", "Isoprenalina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isopropyl alcohol</div>
	 * <div class="de">Isopropylalkohol</div>
	 * <div class="fr">isopropanol</div>
	 * <div class="it">Alcol isopropilico</div>
	 * <!-- @formatter:on -->
	 */
	ISOPROPYL_ALCOHOL("259268001", "2.16.840.1.113883.6.96", "Isopropyl alcohol (substance)",
			"Isopropyl alcohol", "Isopropylalkohol", "isopropanol", "Alcol isopropilico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isosorbide dinitrate</div>
	 * <div class="de">Isosorbid dinitrat</div>
	 * <div class="fr">isosorbide dinitrate</div>
	 * <div class="it">Isosorbide dinitrato</div>
	 * <!-- @formatter:on -->
	 */
	ISOSORBIDE_DINITRATE("387332007", "2.16.840.1.113883.6.96", "Isosorbide dinitrate (substance)",
			"Isosorbide dinitrate", "Isosorbid dinitrat", "isosorbide dinitrate",
			"Isosorbide dinitrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Isotretinoin</div>
	 * <div class="de">Isotretinoin</div>
	 * <div class="fr">isotrétinoïne</div>
	 * <div class="it">Isotretinoina</div>
	 * <!-- @formatter:on -->
	 */
	ISOTRETINOIN("387208003", "2.16.840.1.113883.6.96", "Isotretinoin (substance)", "Isotretinoin",
			"Isotretinoin", "isotrétinoïne", "Isotretinoina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Itraconazole</div>
	 * <div class="de">Itraconazol</div>
	 * <div class="fr">itraconazole</div>
	 * <div class="it">Itraconazolo</div>
	 * <!-- @formatter:on -->
	 */
	ITRACONAZOLE("387532006", "2.16.840.1.113883.6.96", "Itraconazole (substance)", "Itraconazole",
			"Itraconazol", "itraconazole", "Itraconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ivabradine</div>
	 * <div class="de">Ivabradin</div>
	 * <div class="fr">ivabradine</div>
	 * <div class="it">Ivabradina</div>
	 * <!-- @formatter:on -->
	 */
	IVABRADINE("421228002", "2.16.840.1.113883.6.96", "Ivabradine (substance)", "Ivabradine",
			"Ivabradin", "ivabradine", "Ivabradina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ivacaftor</div>
	 * <div class="de">Ivacaftor</div>
	 * <div class="fr">ivacaftor</div>
	 * <div class="it">Ivacaftor</div>
	 * <!-- @formatter:on -->
	 */
	IVACAFTOR("703823007", "2.16.840.1.113883.6.96", "Ivacaftor (substance)", "Ivacaftor",
			"Ivacaftor", "ivacaftor", "Ivacaftor"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ivermectin</div>
	 * <div class="de">Ivermectin</div>
	 * <div class="fr">ivermectine</div>
	 * <div class="it">Ivermectina</div>
	 * <!-- @formatter:on -->
	 */
	IVERMECTIN("387559003", "2.16.840.1.113883.6.96", "Ivermectin (substance)", "Ivermectin",
			"Ivermectin", "ivermectine", "Ivermectina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ixekizumab</div>
	 * <div class="de">Ixekizumab</div>
	 * <div class="fr">ixékizumab</div>
	 * <div class="it">Ixekizumab</div>
	 * <!-- @formatter:on -->
	 */
	IXEKIZUMAB("724037000", "2.16.840.1.113883.6.96", "Ixekizumab (substance)", "Ixekizumab",
			"Ixekizumab", "ixékizumab", "Ixekizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ketamine</div>
	 * <div class="de">Ketamin</div>
	 * <div class="fr">kétamine</div>
	 * <div class="it">Ketamina</div>
	 * <!-- @formatter:on -->
	 */
	KETAMINE("373464007", "2.16.840.1.113883.6.96", "Ketamine (substance)", "Ketamine", "Ketamin",
			"kétamine", "Ketamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ketoconazole</div>
	 * <div class="de">Ketoconazol</div>
	 * <div class="fr">kétoconazole</div>
	 * <div class="it">Ketoconazolo</div>
	 * <!-- @formatter:on -->
	 */
	KETOCONAZOLE("387216007", "2.16.840.1.113883.6.96", "Ketoconazole (substance)", "Ketoconazole",
			"Ketoconazol", "kétoconazole", "Ketoconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ketoprofen</div>
	 * <div class="de">Ketoprofen</div>
	 * <div class="fr">kétoprofène</div>
	 * <div class="it">Ketoprofene</div>
	 * <!-- @formatter:on -->
	 */
	KETOPROFEN("386832008", "2.16.840.1.113883.6.96", "Ketoprofen (substance)", "Ketoprofen",
			"Ketoprofen", "kétoprofène", "Ketoprofene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ketorolac</div>
	 * <div class="de">Ketorolac</div>
	 * <div class="fr">kétorolac</div>
	 * <div class="it">Ketorolac</div>
	 * <!-- @formatter:on -->
	 */
	KETOROLAC("372501008", "2.16.840.1.113883.6.96", "Ketorolac (substance)", "Ketorolac",
			"Ketorolac", "kétorolac", "Ketorolac"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ketotifen</div>
	 * <div class="de">Ketotifen</div>
	 * <div class="fr">kétotifène</div>
	 * <div class="it">Ketotifene</div>
	 * <!-- @formatter:on -->
	 */
	KETOTIFEN("372642003", "2.16.840.1.113883.6.96", "Ketotifen (substance)", "Ketotifen",
			"Ketotifen", "kétotifène", "Ketotifene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Labetalol</div>
	 * <div class="de">Labetalol</div>
	 * <div class="fr">labétalol</div>
	 * <div class="it">Labetalolo</div>
	 * <!-- @formatter:on -->
	 */
	LABETALOL("372750000", "2.16.840.1.113883.6.96", "Labetalol (substance)", "Labetalol",
			"Labetalol", "labétalol", "Labetalolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lacosamide</div>
	 * <div class="de">Lacosamid</div>
	 * <div class="fr">lacosamide</div>
	 * <div class="it">Lacosamide</div>
	 * <!-- @formatter:on -->
	 */
	LACOSAMIDE("441647003", "2.16.840.1.113883.6.96", "Lacosamide (substance)", "Lacosamide",
			"Lacosamid", "lacosamide", "Lacosamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lactitol</div>
	 * <div class="de">Lactitol</div>
	 * <div class="fr">lactitol</div>
	 * <div class="it">Lattilolo</div>
	 * <!-- @formatter:on -->
	 */
	LACTITOL("418929008", "2.16.840.1.113883.6.96", "Lactitol (substance)", "Lactitol", "Lactitol",
			"lactitol", "Lattilolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lactose</div>
	 * <div class="de">Lactose</div>
	 * <div class="fr">lactose</div>
	 * <div class="it">Lattosio</div>
	 * <!-- @formatter:on -->
	 */
	LACTOSE("47703008", "2.16.840.1.113883.6.96", "Lactose (substance)", "Lactose", "Lactose",
			"lactose", "Lattosio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lactulose</div>
	 * <div class="de">Lactulose</div>
	 * <div class="fr">lactulose</div>
	 * <div class="it">Lattulosio</div>
	 * <!-- @formatter:on -->
	 */
	LACTULOSE("273945008", "2.16.840.1.113883.6.96", "Lactulose (substance)", "Lactulose",
			"Lactulose", "lactulose", "Lattulosio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lamivudine</div>
	 * <div class="de">Lamivudin</div>
	 * <div class="fr">lamivudine</div>
	 * <div class="it">Lamivudina</div>
	 * <!-- @formatter:on -->
	 */
	LAMIVUDINE("386897000", "2.16.840.1.113883.6.96", "Lamivudine (substance)", "Lamivudine",
			"Lamivudin", "lamivudine", "Lamivudina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lamotrigine</div>
	 * <div class="de">Lamotrigin</div>
	 * <div class="fr">lamotrigine</div>
	 * <div class="it">Lamotrigina</div>
	 * <!-- @formatter:on -->
	 */
	LAMOTRIGINE("387562000", "2.16.840.1.113883.6.96", "Lamotrigine (substance)", "Lamotrigine",
			"Lamotrigin", "lamotrigine", "Lamotrigina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lanreotide</div>
	 * <div class="de">Lanreotid</div>
	 * <div class="fr">lanréotide</div>
	 * <div class="it">Lanreotide</div>
	 * <!-- @formatter:on -->
	 */
	LANREOTIDE("395765000", "2.16.840.1.113883.6.96", "Lanreotide (substance)", "Lanreotide",
			"Lanreotid", "lanréotide", "Lanreotide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lansoprazole</div>
	 * <div class="de">Lansoprazol</div>
	 * <div class="fr">lansoprazole</div>
	 * <div class="it">Lansoprazolo</div>
	 * <!-- @formatter:on -->
	 */
	LANSOPRAZOLE("386888004", "2.16.840.1.113883.6.96", "Lansoprazole (substance)", "Lansoprazole",
			"Lansoprazol", "lansoprazole", "Lansoprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lanthanum carbonate</div>
	 * <div class="de">Lanthan(III) carbonat</div>
	 * <div class="fr">lanthane(III) carbonate</div>
	 * <div class="it">Lantanio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	LANTHANUM_CARBONATE("414571007", "2.16.840.1.113883.6.96", "Lanthanum carbonate (substance)",
			"Lanthanum carbonate", "Lanthan(III) carbonat", "lanthane(III) carbonate",
			"Lantanio carbonato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Latanoprost</div>
	 * <div class="de">Latanoprost</div>
	 * <div class="fr">latanoprost</div>
	 * <div class="it">Latanoprost</div>
	 * <!-- @formatter:on -->
	 */
	LATANOPROST("386926002", "2.16.840.1.113883.6.96", "Latanoprost (substance)", "Latanoprost",
			"Latanoprost", "latanoprost", "Latanoprost"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lauromacrogol 400</div>
	 * <div class="de">Lauromacrogol 400</div>
	 * <div class="fr">lauromacrogol 400</div>
	 * <div class="it">Lauromacrogol</div>
	 * <!-- @formatter:on -->
	 */
	LAUROMACROGOL_400("427905004", "2.16.840.1.113883.6.96", "Lauromacrogol 400 (substance)",
			"Lauromacrogol 400", "Lauromacrogol 400", "lauromacrogol 400", "Lauromacrogol"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Leflunomide</div>
	 * <div class="de">Leflunomid</div>
	 * <div class="fr">léflunomide</div>
	 * <div class="it">Leflunomide</div>
	 * <!-- @formatter:on -->
	 */
	LEFLUNOMIDE("386981009", "2.16.840.1.113883.6.96", "Leflunomide (substance)", "Leflunomide",
			"Leflunomid", "léflunomide", "Leflunomide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lenalidomide</div>
	 * <div class="de">Lenalidomid</div>
	 * <div class="fr">lénalidomide</div>
	 * <div class="it">Lenalidomide</div>
	 * <!-- @formatter:on -->
	 */
	LENALIDOMIDE("421471009", "2.16.840.1.113883.6.96", "Lenalidomide (substance)", "Lenalidomide",
			"Lenalidomid", "lénalidomide", "Lenalidomide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lenograstime</div>
	 * <div class="de">Lenograstim</div>
	 * <div class="fr">lénograstim</div>
	 * <div class="it">Lenograstim</div>
	 * <!-- @formatter:on -->
	 */
	LENOGRASTIME("395767008", "2.16.840.1.113883.6.96", "Lenograstime (substance)", "Lenograstime",
			"Lenograstim", "lénograstim", "Lenograstim"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lercanidipine</div>
	 * <div class="de">Lercanidipin</div>
	 * <div class="fr">lercanidipine</div>
	 * <div class="it">Lercanidipina</div>
	 * <!-- @formatter:on -->
	 */
	LERCANIDIPINE("395986007", "2.16.840.1.113883.6.96", "Lercanidipine (substance)",
			"Lercanidipine", "Lercanidipin", "lercanidipine", "Lercanidipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Letrozole</div>
	 * <div class="de">Letrozol</div>
	 * <div class="fr">létrozole</div>
	 * <div class="it">Letrozolo</div>
	 * <!-- @formatter:on -->
	 */
	LETROZOLE("386911004", "2.16.840.1.113883.6.96", "Letrozole (substance)", "Letrozole",
			"Letrozol", "létrozole", "Letrozolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Leucine</div>
	 * <div class="de">Leucin</div>
	 * <div class="fr">leucine</div>
	 * <div class="it">Leucina</div>
	 * <!-- @formatter:on -->
	 */
	LEUCINE("83797003", "2.16.840.1.113883.6.96", "Leucine (substance)", "Leucine", "Leucin",
			"leucine", "Leucina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Leuprorelin</div>
	 * <div class="de">Leuprorelin</div>
	 * <div class="fr">leuproréline</div>
	 * <div class="it">Leuprorelina</div>
	 * <!-- @formatter:on -->
	 */
	LEUPRORELIN("397198002", "2.16.840.1.113883.6.96", "Leuprorelin (substance)", "Leuprorelin",
			"Leuprorelin", "leuproréline", "Leuprorelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levetiracetam</div>
	 * <div class="de">Levetiracetam</div>
	 * <div class="fr">lévétiracétam</div>
	 * <div class="it">Levetiracetam</div>
	 * <!-- @formatter:on -->
	 */
	LEVETIRACETAM("387000003", "2.16.840.1.113883.6.96", "Levetiracetam (substance)",
			"Levetiracetam", "Levetiracetam", "lévétiracétam", "Levetiracetam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levobupivacaine</div>
	 * <div class="de">Levobupivacain</div>
	 * <div class="fr">lévobupivacaïne</div>
	 * <div class="it">Levobupivacaina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOBUPIVACAINE("387011006", "2.16.840.1.113883.6.96", "Levobupivacaine (substance)",
			"Levobupivacaine", "Levobupivacain", "lévobupivacaïne", "Levobupivacaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levocabastine</div>
	 * <div class="de">Levocabastin</div>
	 * <div class="fr">lévocabastine</div>
	 * <div class="it">Levocabastina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOCABASTINE("372554006", "2.16.840.1.113883.6.96", "Levocabastine (substance)",
			"Levocabastine", "Levocabastin", "lévocabastine", "Levocabastina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levocarnitine</div>
	 * <div class="de">Levocarnitin</div>
	 * <div class="fr">lévocarnitine</div>
	 * <div class="it">Levocarnitina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOCARNITINE("372601001", "2.16.840.1.113883.6.96", "Levocarnitine (substance)",
			"Levocarnitine", "Levocarnitin", "lévocarnitine", "Levocarnitina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levocetirizine</div>
	 * <div class="de">Levocetirizin</div>
	 * <div class="fr">lévocétirizine</div>
	 * <div class="it">Levocetirizina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOCETIRIZINE("421889003", "2.16.840.1.113883.6.96", "Levocetirizine (substance)",
			"Levocetirizine", "Levocetirizin", "lévocétirizine", "Levocetirizina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levodopa</div>
	 * <div class="de">Levodopa</div>
	 * <div class="fr">lévodopa</div>
	 * <div class="it">Levodopa</div>
	 * <!-- @formatter:on -->
	 */
	LEVODOPA("387086006", "2.16.840.1.113883.6.96", "Levodopa (substance)", "Levodopa", "Levodopa",
			"lévodopa", "Levodopa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levofloxacin</div>
	 * <div class="de">Levofloxacin</div>
	 * <div class="fr">lévofloxacine</div>
	 * <div class="it">Levofloxacina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOFLOXACIN("387552007", "2.16.840.1.113883.6.96", "Levofloxacin (substance)", "Levofloxacin",
			"Levofloxacin", "lévofloxacine", "Levofloxacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levomepromazine</div>
	 * <div class="de">Levomepromazin</div>
	 * <div class="fr">lévomépromazine</div>
	 * <div class="it">Levomepromazina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOMEPROMAZINE("387509007", "2.16.840.1.113883.6.96", "Levomepromazine (substance)",
			"Levomepromazine", "Levomepromazin", "lévomépromazine", "Levomepromazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levonorgestrel</div>
	 * <div class="de">Levonorgestrel</div>
	 * <div class="fr">lévonorgestrel</div>
	 * <div class="it">Levonorgestrel</div>
	 * <!-- @formatter:on -->
	 */
	LEVONORGESTREL("126109000", "2.16.840.1.113883.6.96", "Levonorgestrel (substance)",
			"Levonorgestrel", "Levonorgestrel", "lévonorgestrel", "Levonorgestrel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levosimendan</div>
	 * <div class="de">Levosimendan</div>
	 * <div class="fr">lévosimendan</div>
	 * <div class="it">Levosimendan</div>
	 * <!-- @formatter:on -->
	 */
	LEVOSIMENDAN("442795003", "2.16.840.1.113883.6.96", "Levosimendan (substance)", "Levosimendan",
			"Levosimendan", "lévosimendan", "Levosimendan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Levothyroxine</div>
	 * <div class="de">Levothyroxin</div>
	 * <div class="fr">lévothyroxine</div>
	 * <div class="it">Levotiroxina</div>
	 * <!-- @formatter:on -->
	 */
	LEVOTHYROXINE("710809001", "2.16.840.1.113883.6.96", "Levothyroxine (substance)",
			"Levothyroxine", "Levothyroxin", "lévothyroxine", "Levotiroxina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lidocaine</div>
	 * <div class="de">Lidocain</div>
	 * <div class="fr">lidocaïne</div>
	 * <div class="it">Lidocaina</div>
	 * <!-- @formatter:on -->
	 */
	LIDOCAINE("387480006", "2.16.840.1.113883.6.96", "Lidocaine (substance)", "Lidocaine",
			"Lidocain", "lidocaïne", "Lidocaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Linagliptin</div>
	 * <div class="de">Linagliptin</div>
	 * <div class="fr">linagliptine</div>
	 * <div class="it">Linagliptin</div>
	 * <!-- @formatter:on -->
	 */
	LINAGLIPTIN("702798009", "2.16.840.1.113883.6.96", "Linagliptin (substance)", "Linagliptin",
			"Linagliptin", "linagliptine", "Linagliptin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Linezolid</div>
	 * <div class="de">Linezolid</div>
	 * <div class="fr">linézolide</div>
	 * <div class="it">Linezolid</div>
	 * <!-- @formatter:on -->
	 */
	LINEZOLID("387056004", "2.16.840.1.113883.6.96", "Linezolid (substance)", "Linezolid",
			"Linezolid", "linézolide", "Linezolid"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Liothyronine</div>
	 * <div class="de">Liothyronin</div>
	 * <div class="fr">liothyronine</div>
	 * <div class="it">Liotironina</div>
	 * <!-- @formatter:on -->
	 */
	LIOTHYRONINE("61275002", "2.16.840.1.113883.6.96", "Liothyronine (substance)", "Liothyronine",
			"Liothyronin", "liothyronine", "Liotironina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Liraglutide</div>
	 * <div class="de">Liraglutid</div>
	 * <div class="fr">liraglutide</div>
	 * <div class="it">Liraglutide</div>
	 * <!-- @formatter:on -->
	 */
	LIRAGLUTIDE("444828003", "2.16.840.1.113883.6.96", "Liraglutide (substance)", "Liraglutide",
			"Liraglutid", "liraglutide", "Liraglutide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lisdexamfetamine</div>
	 * <div class="de">Lisdexamfetamin</div>
	 * <div class="fr">lisdexamfétamine</div>
	 * <div class="it">Lisdexamfetamina</div>
	 * <!-- @formatter:on -->
	 */
	LISDEXAMFETAMINE("425597005", "2.16.840.1.113883.6.96", "Lisdexamfetamine (substance)",
			"Lisdexamfetamine", "Lisdexamfetamin", "lisdexamfétamine", "Lisdexamfetamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lisinopril</div>
	 * <div class="de">Lisinopril</div>
	 * <div class="fr">lisinopril</div>
	 * <div class="it">Lisinopril</div>
	 * <!-- @formatter:on -->
	 */
	LISINOPRIL("386873009", "2.16.840.1.113883.6.96", "Lisinopril (substance)", "Lisinopril",
			"Lisinopril", "lisinopril", "Lisinopril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lithium acetate</div>
	 * <div class="de">Lithium acetat</div>
	 * <div class="fr">lithium acétate</div>
	 * <div class="it">Litio acetato</div>
	 * <!-- @formatter:on -->
	 */
	LITHIUM_ACETATE("111080000", "2.16.840.1.113883.6.96", "Lithium acetate (substance)",
			"Lithium acetate", "Lithium acetat", "lithium acétate", "Litio acetato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lithium carbonate</div>
	 * <div class="de">Lithium carbonat</div>
	 * <div class="fr">lithium carbonate</div>
	 * <div class="it">Litio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	LITHIUM_CARBONATE("387095003", "2.16.840.1.113883.6.96", "Lithium carbonate (substance)",
			"Lithium carbonate", "Lithium carbonat", "lithium carbonate", "Litio carbonato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lithium sulfate</div>
	 * <div class="de">Lithiumsulfat</div>
	 * <div class="fr">lithium sulfate</div>
	 * <div class="it">Litio solfato</div>
	 * <!-- @formatter:on -->
	 */
	LITHIUM_SULFATE("708197001", "2.16.840.1.113883.6.96", "Lithium sulfate (substance)",
			"Lithium sulfate", "Lithiumsulfat", "lithium sulfate", "Litio solfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Live typhoid vaccine</div>
	 * <div class="de">Typhus-Lebend-Impfstoff</div>
	 * <div class="fr">typhus vaccin vivant</div>
	 * <div class="it">Tifo vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	LIVE_TYPHOID_VACCINE("764303004", "2.16.840.1.113883.6.96", "Live typhoid vaccine (substance)",
			"Live typhoid vaccine", "Typhus-Lebend-Impfstoff", "typhus vaccin vivant",
			"Tifo vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lixisenatide</div>
	 * <div class="de">Lixisenatid</div>
	 * <div class="fr">lixisénatide</div>
	 * <div class="it">Lixisenatide</div>
	 * <!-- @formatter:on -->
	 */
	LIXISENATIDE("708808004", "2.16.840.1.113883.6.96", "Lixisenatide (substance)", "Lixisenatide",
			"Lixisenatid", "lixisénatide", "Lixisenatide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lomustine</div>
	 * <div class="de">Lomustin</div>
	 * <div class="fr">lomustine</div>
	 * <div class="it">Lomustina</div>
	 * <!-- @formatter:on -->
	 */
	LOMUSTINE("387227009", "2.16.840.1.113883.6.96", "Lomustine (substance)", "Lomustine",
			"Lomustin", "lomustine", "Lomustina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lonoctocog alfa</div>
	 * <div class="de">Lonoctocog alfa</div>
	 * <div class="fr">lonoctocog alfa</div>
	 * <div class="it">Lonoctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	LONOCTOCOG_ALFA("1012961000168107", "2.16.840.1.113883.6.96", "Lonoctocog alfa (substance)",
			"Lonoctocog alfa", "Lonoctocog alfa", "lonoctocog alfa", "Lonoctocog alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Loperamide</div>
	 * <div class="de">Loperamid</div>
	 * <div class="fr">lopéramide</div>
	 * <div class="it">Loperamide</div>
	 * <!-- @formatter:on -->
	 */
	LOPERAMIDE("387040009", "2.16.840.1.113883.6.96", "Loperamide (substance)", "Loperamide",
			"Loperamid", "lopéramide", "Loperamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lopinavir</div>
	 * <div class="de">Lopinavir</div>
	 * <div class="fr">lopinavir</div>
	 * <div class="it">Lopinavir</div>
	 * <!-- @formatter:on -->
	 */
	LOPINAVIR("387067003", "2.16.840.1.113883.6.96", "Lopinavir (substance)", "Lopinavir",
			"Lopinavir", "lopinavir", "Lopinavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Loratadine</div>
	 * <div class="de">Loratadin</div>
	 * <div class="fr">loratadine</div>
	 * <div class="it">Loratadina</div>
	 * <!-- @formatter:on -->
	 */
	LORATADINE("386884002", "2.16.840.1.113883.6.96", "Loratadine (substance)", "Loratadine",
			"Loratadin", "loratadine", "Loratadina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lorazepam</div>
	 * <div class="de">Lorazepam</div>
	 * <div class="fr">lorazépam</div>
	 * <div class="it">Lorazepam</div>
	 * <!-- @formatter:on -->
	 */
	LORAZEPAM("387106007", "2.16.840.1.113883.6.96", "Lorazepam (substance)", "Lorazepam",
			"Lorazepam", "lorazépam", "Lorazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lormetazepam</div>
	 * <div class="de">Lormetazepam</div>
	 * <div class="fr">lormétazépam</div>
	 * <div class="it">Lormetazepam</div>
	 * <!-- @formatter:on -->
	 */
	LORMETAZEPAM("387570005", "2.16.840.1.113883.6.96", "Lormetazepam (substance)", "Lormetazepam",
			"Lormetazepam", "lormétazépam", "Lormetazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Losartan</div>
	 * <div class="de">Losartan</div>
	 * <div class="fr">losartan</div>
	 * <div class="it">Losartan</div>
	 * <!-- @formatter:on -->
	 */
	LOSARTAN("373567002", "2.16.840.1.113883.6.96", "Losartan (substance)", "Losartan", "Losartan",
			"losartan", "Losartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lumefantrine</div>
	 * <div class="de">Lumefantrin</div>
	 * <div class="fr">luméfantrine</div>
	 * <div class="it">Lumefantrina</div>
	 * <!-- @formatter:on -->
	 */
	LUMEFANTRINE("420307001", "2.16.840.1.113883.6.96", "Lumefantrine (substance)", "Lumefantrine",
			"Lumefantrin", "luméfantrine", "Lumefantrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lurasidone</div>
	 * <div class="de">Lurasidon</div>
	 * <div class="fr">lurasidone</div>
	 * <div class="it">Lurasidone</div>
	 * <!-- @formatter:on -->
	 */
	LURASIDONE("703115008", "2.16.840.1.113883.6.96", "Lurasidone (substance)", "Lurasidone",
			"Lurasidon", "lurasidone", "Lurasidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lutropin alfa</div>
	 * <div class="de">Lutropin alfa</div>
	 * <div class="fr">lutropine alfa</div>
	 * <div class="it">Lutropina alfa</div>
	 * <!-- @formatter:on -->
	 */
	LUTROPIN_ALFA("415248001", "2.16.840.1.113883.6.96", "Lutropin alfa (substance)",
			"Lutropin alfa", "Lutropin alfa", "lutropine alfa", "Lutropina alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Lysine</div>
	 * <div class="de">Lysin</div>
	 * <div class="fr">lysine</div>
	 * <div class="it">Lisina</div>
	 * <!-- @formatter:on -->
	 */
	LYSINE("75799006", "2.16.840.1.113883.6.96", "Lysine (substance)", "Lysine", "Lysin", "lysine",
			"Lisina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Macitentan</div>
	 * <div class="de">Macitentan</div>
	 * <div class="fr">macitentan</div>
	 * <div class="it">Macitentan</div>
	 * <!-- @formatter:on -->
	 */
	MACITENTAN("710283008", "2.16.840.1.113883.6.96", "Macitentan (substance)", "Macitentan",
			"Macitentan", "macitentan", "Macitentan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Macrogol</div>
	 * <div class="de">Macrogol</div>
	 * <div class="fr">macrogol</div>
	 * <div class="it">Macrogol</div>
	 * <!-- @formatter:on -->
	 */
	MACROGOL("8030004", "2.16.840.1.113883.6.96", "Macrogol (substance)", "Macrogol", "Macrogol",
			"macrogol", "Macrogol"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Macrogol 3350</div>
	 * <div class="de">Macrogol 3350</div>
	 * <div class="fr">macrogol 3350</div>
	 * <div class="it">Macrogol 3350</div>
	 * <!-- @formatter:on -->
	 */
	MACROGOL_3350("712566007", "2.16.840.1.113883.6.96", "Macrogol 3350 (substance)",
			"Macrogol 3350", "Macrogol 3350", "macrogol 3350", "Macrogol 3350"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Macrogol 4000</div>
	 * <div class="de">Macrogol 4000</div>
	 * <div class="fr">macrogol 4000</div>
	 * <div class="it">Macrogol 4000</div>
	 * <!-- @formatter:on -->
	 */
	MACROGOL_4000("712567003", "2.16.840.1.113883.6.96", "Macrogol 4000 (substance)",
			"Macrogol 4000", "Macrogol 4000", "macrogol 4000", "Macrogol 4000"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magaldrate</div>
	 * <div class="de">Magaldrat</div>
	 * <div class="fr">magaldrate</div>
	 * <div class="it">Magaldrato</div>
	 * <!-- @formatter:on -->
	 */
	MAGALDRATE("387240004", "2.16.840.1.113883.6.96", "Magaldrate (substance)", "Magaldrate",
			"Magaldrat", "magaldrate", "Magaldrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium</div>
	 * <div class="de">Magnesium</div>
	 * <div class="fr">magnésium</div>
	 * <div class="it">Magnesio</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM("72717003", "2.16.840.1.113883.6.96", "Magnesium (substance)", "Magnesium",
			"Magnesium", "magnésium", "Magnesio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium acetate tetrahydrate</div>
	 * <div class="de">Magnesium diacetat-4-Wasser</div>
	 * <div class="fr">magnésium acétate tétrahydrique</div>
	 * <div class="it">Magnesio acetato tetraidrato</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM_ACETATE_TETRAHYDRATE("723586001", "2.16.840.1.113883.6.96",
			"Magnesium acetate tetrahydrate (substance)", "Magnesium acetate tetrahydrate",
			"Magnesium diacetat-4-Wasser", "magnésium acétate tétrahydrique",
			"Magnesio acetato tetraidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium carbonate</div>
	 * <div class="de">Magnesium carbonat</div>
	 * <div class="fr">magnésium carbonate</div>
	 * <div class="it">Magnesio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM_CARBONATE("387401007", "2.16.840.1.113883.6.96", "Magnesium carbonate (substance)",
			"Magnesium carbonate", "Magnesium carbonat", "magnésium carbonate",
			"Magnesio carbonato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium chloride</div>
	 * <div class="de">Magnesiumchlorid</div>
	 * <div class="fr">magnésium chlorure</div>
	 * <div class="it">Magnesio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM_CHLORIDE("45733002", "2.16.840.1.113883.6.96", "Magnesium chloride (substance)",
			"Magnesium chloride", "Magnesiumchlorid", "magnésium chlorure", "Magnesio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium gluconate</div>
	 * <div class="de">Magnesium digluconat wasserfrei</div>
	 * <div class="fr">magnésium digluconate anhydre</div>
	 * <div class="it">Magnesio gluconato</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM_GLUCONATE("116126005", "2.16.840.1.113883.6.96", "Magnesium gluconate (substance)",
			"Magnesium gluconate", "Magnesium digluconat wasserfrei",
			"magnésium digluconate anhydre", "Magnesio gluconato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium hydroxide</div>
	 * <div class="de">Magnesiumhydroxid</div>
	 * <div class="fr">magnésium hydroxyde</div>
	 * <div class="it">Magnesio idrossido</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM_HYDROXIDE("387337001", "2.16.840.1.113883.6.96", "Magnesium hydroxide (substance)",
			"Magnesium hydroxide", "Magnesiumhydroxid", "magnésium hydroxyde",
			"Magnesio idrossido"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Magnesium sulfate</div>
	 * <div class="de">Magnesiumsulfat</div>
	 * <div class="fr">magnésium sulfate</div>
	 * <div class="it">Magnesio solfato</div>
	 * <!-- @formatter:on -->
	 */
	MAGNESIUM_SULFATE("387202002", "2.16.840.1.113883.6.96", "Magnesium sulfate (substance)",
			"Magnesium sulfate", "Magnesiumsulfat", "magnésium sulfate", "Magnesio solfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mannitol</div>
	 * <div class="de">Mannitol</div>
	 * <div class="fr">mannitol</div>
	 * <div class="it">Mannitolo</div>
	 * <!-- @formatter:on -->
	 */
	MANNITOL("387168006", "2.16.840.1.113883.6.96", "Mannitol (substance)", "Mannitol", "Mannitol",
			"mannitol", "Mannitolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Maraviroc</div>
	 * <div class="de">Maraviroc</div>
	 * <div class="fr">maraviroc</div>
	 * <div class="it">Maraviroc</div>
	 * <!-- @formatter:on -->
	 */
	MARAVIROC("429603001", "2.16.840.1.113883.6.96", "Maraviroc (substance)", "Maraviroc",
			"Maraviroc", "maraviroc", "Maraviroc"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Measles, mumps and rubella vaccine</div>
	 * <div class="de">Masern-Mumps-Röteln-Lebendimpfstoff</div>
	 * <div class="fr">rougeole-oreillons-rubéole vaccin vivant</div>
	 * <div class="it">Morbillo, parotite, rosolia vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	MEASLES_MUMPS_AND_RUBELLA_VACCINE("396429000", "2.16.840.1.113883.6.96",
			"Measles, mumps and rubella vaccine (substance)", "Measles, mumps and rubella vaccine",
			"Masern-Mumps-Röteln-Lebendimpfstoff", "rougeole-oreillons-rubéole vaccin vivant",
			"Morbillo, parotite, rosolia vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Measles vaccine</div>
	 * <div class="de">Masern-Lebend-Impfstoff</div>
	 * <div class="fr">rougeole vaccin vivant</div>
	 * <div class="it">Morbillo vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	MEASLES_VACCINE("396427003", "2.16.840.1.113883.6.96", "Measles vaccine (substance)",
			"Measles vaccine", "Masern-Lebend-Impfstoff", "rougeole vaccin vivant",
			"Morbillo vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mebendazole</div>
	 * <div class="de">Mebendazol</div>
	 * <div class="fr">mébendazole</div>
	 * <div class="it">Mebendazolo</div>
	 * <!-- @formatter:on -->
	 */
	MEBENDAZOLE("387311004", "2.16.840.1.113883.6.96", "Mebendazole (substance)", "Mebendazole",
			"Mebendazol", "mébendazole", "Mebendazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mebeverine</div>
	 * <div class="de">Mebeverin</div>
	 * <div class="fr">mébévérine</div>
	 * <div class="it">Mebeverina</div>
	 * <!-- @formatter:on -->
	 */
	MEBEVERINE("419830007", "2.16.840.1.113883.6.96", "Mebeverine (substance)", "Mebeverine",
			"Mebeverin", "mébévérine", "Mebeverina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Meclozine</div>
	 * <div class="de">Meclozin</div>
	 * <div class="fr">méclozine</div>
	 * <div class="it">Meclozina</div>
	 * <!-- @formatter:on -->
	 */
	MECLOZINE("372879002", "2.16.840.1.113883.6.96", "Meclozine (substance)", "Meclozine",
			"Meclozin", "méclozine", "Meclozina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Medium chain triglyceride</div>
	 * <div class="de">Triglyceride mittelkettige</div>
	 * <div class="fr">triglycérides à chaîne moyenne</div>
	 * <div class="it">Trigliceridi a catena media</div>
	 * <!-- @formatter:on -->
	 */
	MEDIUM_CHAIN_TRIGLYCERIDE("395781005", "2.16.840.1.113883.6.96",
			"Medium chain triglyceride (substance)", "Medium chain triglyceride",
			"Triglyceride mittelkettige", "triglycérides à chaîne moyenne",
			"Trigliceridi a catena media"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Medroxyprogesterone</div>
	 * <div class="de">Medroxyprogesteron</div>
	 * <div class="fr">médroxyprogestérone</div>
	 * <div class="it">Medrossiprogesterone acetato</div>
	 * <!-- @formatter:on -->
	 */
	MEDROXYPROGESTERONE("126113007", "2.16.840.1.113883.6.96", "Medroxyprogesterone (substance)",
			"Medroxyprogesterone", "Medroxyprogesteron", "médroxyprogestérone",
			"Medrossiprogesterone acetato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mefenamic acid</div>
	 * <div class="de">Mefenaminsäure</div>
	 * <div class="fr">acide méfénamique</div>
	 * <div class="it">Acido mefenamico</div>
	 * <!-- @formatter:on -->
	 */
	MEFENAMIC_ACID("387185008", "2.16.840.1.113883.6.96", "Mefenamic acid (substance)",
			"Mefenamic acid", "Mefenaminsäure", "acide méfénamique", "Acido mefenamico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mefloquine</div>
	 * <div class="de">Mefloquin</div>
	 * <div class="fr">méfloquine</div>
	 * <div class="it">Meflochina</div>
	 * <!-- @formatter:on -->
	 */
	MEFLOQUINE("387505001", "2.16.840.1.113883.6.96", "Mefloquine (substance)", "Mefloquine",
			"Mefloquin", "méfloquine", "Meflochina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Meglumine</div>
	 * <div class="de">Meglumin</div>
	 * <div class="fr">méglumine</div>
	 * <div class="it">Meglumina</div>
	 * <!-- @formatter:on -->
	 */
	MEGLUMINE("769091004", "2.16.840.1.113883.6.96", "Meglumine (substance)", "Meglumine",
			"Meglumin", "méglumine", "Meglumina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Melatonin</div>
	 * <div class="de">Melatonin</div>
	 * <div class="fr">mélatonine</div>
	 * <div class="it">Melatonina</div>
	 * <!-- @formatter:on -->
	 */
	MELATONIN("41199001", "2.16.840.1.113883.6.96", "Melatonin (substance)", "Melatonin",
			"Melatonin", "mélatonine", "Melatonina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Melitracen</div>
	 * <div class="de">Melitracen</div>
	 * <div class="fr">mélitracène</div>
	 * <div class="it">Melitracene</div>
	 * <!-- @formatter:on -->
	 */
	MELITRACEN("712683007", "2.16.840.1.113883.6.96", "Melitracen (substance)", "Melitracen",
			"Melitracen", "mélitracène", "Melitracene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Melperone</div>
	 * <div class="de">Melperon</div>
	 * <div class="fr">melpérone</div>
	 * <div class="it">Melperone</div>
	 * <!-- @formatter:on -->
	 */
	MELPERONE("442519006", "2.16.840.1.113883.6.96", "Melperone (substance)", "Melperone",
			"Melperon", "melpérone", "Melperone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Melphalan</div>
	 * <div class="de">Melphalan</div>
	 * <div class="fr">melphalan</div>
	 * <div class="it">Melphalan</div>
	 * <!-- @formatter:on -->
	 */
	MELPHALAN("387297002", "2.16.840.1.113883.6.96", "Melphalan (substance)", "Melphalan",
			"Melphalan", "melphalan", "Melphalan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Memantine</div>
	 * <div class="de">Memantin</div>
	 * <div class="fr">mémantine</div>
	 * <div class="it">Memantina</div>
	 * <!-- @formatter:on -->
	 */
	MEMANTINE("406458000", "2.16.840.1.113883.6.96", "Memantine (substance)", "Memantine",
			"Memantin", "mémantine", "Memantina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Meningococcus group C vaccine</div>
	 * <div class="de">Meningokokken C-Saccharid-T-Konjugat-Impfstoff</div>
	 * <div class="fr">méningite vaccin polysaccharidique C, conjugué T</div>
	 * <div class="it">Meningite vaccino polisaccaridico C, coniugato</div>
	 * <!-- @formatter:on -->
	 */
	MENINGOCOCCUS_GROUP_C_VACCINE("768366003", "2.16.840.1.113883.6.96",
			"Meningococcus group C vaccine (substance)", "Meningococcus group C vaccine",
			"Meningokokken C-Saccharid-T-Konjugat-Impfstoff",
			"méningite vaccin polysaccharidique C, conjugué T",
			"Meningite vaccino polisaccaridico C, coniugato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Meningococcus vaccine</div>
	 * <div class="de">Meningokokken-Impfstoff</div>
	 * <div class="fr">méningite vaccin</div>
	 * <div class="it">Meningite vaccino</div>
	 * <!-- @formatter:on -->
	 */
	MENINGOCOCCUS_VACCINE("424891007", "2.16.840.1.113883.6.96",
			"Meningococcus vaccine (substance)", "Meningococcus vaccine", "Meningokokken-Impfstoff",
			"méningite vaccin", "Meningite vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mepivacaine</div>
	 * <div class="de">Mepivacain</div>
	 * <div class="fr">mépivacaïne</div>
	 * <div class="it">Mepicacaina</div>
	 * <!-- @formatter:on -->
	 */
	MEPIVACAINE("59560006", "2.16.840.1.113883.6.96", "Mepivacaine (substance)", "Mepivacaine",
			"Mepivacain", "mépivacaïne", "Mepicacaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mercaptamine</div>
	 * <div class="de">Mercaptamin</div>
	 * <div class="fr">mercaptamine</div>
	 * <div class="it">Mercaptamina</div>
	 * <!-- @formatter:on -->
	 */
	MERCAPTAMINE("373457005", "2.16.840.1.113883.6.96", "Mercaptamine (substance)", "Mercaptamine",
			"Mercaptamin", "mercaptamine", "Mercaptamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Meropenem</div>
	 * <div class="de">Meropenem</div>
	 * <div class="fr">méropénem</div>
	 * <div class="it">Meropenem</div>
	 * <!-- @formatter:on -->
	 */
	MEROPENEM("387540000", "2.16.840.1.113883.6.96", "Meropenem (substance)", "Meropenem",
			"Meropenem", "méropénem", "Meropenem"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mesalazine</div>
	 * <div class="de">Mesalazin</div>
	 * <div class="fr">mésalazine</div>
	 * <div class="it">Mesalazina</div>
	 * <!-- @formatter:on -->
	 */
	MESALAZINE("387501005", "2.16.840.1.113883.6.96", "Mesalazine (substance)", "Mesalazine",
			"Mesalazin", "mésalazine", "Mesalazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mesna</div>
	 * <div class="de">Mesna</div>
	 * <div class="fr">mesna</div>
	 * <div class="it">Mesna</div>
	 * <!-- @formatter:on -->
	 */
	MESNA("386922000", "2.16.840.1.113883.6.96", "Mesna (substance)", "Mesna", "Mesna", "mesna",
			"Mesna"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Metamizole</div>
	 * <div class="de">Metamizol</div>
	 * <div class="fr">métamizole</div>
	 * <div class="it">Metamizolo</div>
	 * <!-- @formatter:on -->
	 */
	METAMIZOLE("780831000", "2.16.840.1.113883.6.96", "Metamizole (substance)", "Metamizole",
			"Metamizol", "métamizole", "Metamizolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Metformin</div>
	 * <div class="de">Metformin</div>
	 * <div class="fr">metformine</div>
	 * <div class="it">Metformina</div>
	 * <!-- @formatter:on -->
	 */
	METFORMIN("372567009", "2.16.840.1.113883.6.96", "Metformin (substance)", "Metformin",
			"Metformin", "metformine", "Metformina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methadone</div>
	 * <div class="de">Methadon</div>
	 * <div class="fr">méthadone</div>
	 * <div class="it">Metadone</div>
	 * <!-- @formatter:on -->
	 */
	METHADONE("387286002", "2.16.840.1.113883.6.96", "Methadone (substance)", "Methadone",
			"Methadon", "méthadone", "Metadone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methionine</div>
	 * <div class="de">Methionin</div>
	 * <div class="fr">méthionine</div>
	 * <div class="it">Metionina</div>
	 * <!-- @formatter:on -->
	 */
	METHIONINE("70288006", "2.16.840.1.113883.6.96", "Methionine (substance)", "Methionine",
			"Methionin", "méthionine", "Metionina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methotrexate</div>
	 * <div class="de">Methotrexat</div>
	 * <div class="fr">méthotrexate</div>
	 * <div class="it">Metotrexato</div>
	 * <!-- @formatter:on -->
	 */
	METHOTREXATE("387381009", "2.16.840.1.113883.6.96", "Methotrexate (substance)", "Methotrexate",
			"Methotrexat", "méthotrexate", "Metotrexato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methoxsalen</div>
	 * <div class="de">Methoxsalen</div>
	 * <div class="fr">méthoxsalène</div>
	 * <div class="it">Metoxsalene</div>
	 * <!-- @formatter:on -->
	 */
	METHOXSALEN("41062004", "2.16.840.1.113883.6.96", "Methoxsalen (substance)", "Methoxsalen",
			"Methoxsalen", "méthoxsalène", "Metoxsalene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methoxy polyethylene glycol-epoetin beta</div>
	 * <div class="de">PEG-Epoetin beta</div>
	 * <div class="fr">époétine bêta pégylée</div>
	 * <div class="it">Metossipolietilenglicole-epoetina beta</div>
	 * <!-- @formatter:on -->
	 */
	METHOXY_POLYETHYLENE_GLYCOL_EPOETIN_BETA("425913002", "2.16.840.1.113883.6.96",
			"Methoxy polyethylene glycol-epoetin beta (substance)",
			"Methoxy polyethylene glycol-epoetin beta", "PEG-Epoetin beta",
			"époétine bêta pégylée", "Metossipolietilenglicole-epoetina beta"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methyldopa anhydrous</div>
	 * <div class="de">Methyldopa</div>
	 * <div class="fr">méthyldopa</div>
	 * <div class="it">Metildopa</div>
	 * <!-- @formatter:on -->
	 */
	METHYLDOPA_ANHYDROUS("768043006", "2.16.840.1.113883.6.96", "Methyldopa anhydrous (substance)",
			"Methyldopa anhydrous", "Methyldopa", "méthyldopa", "Metildopa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methylene blue stain</div>
	 * <div class="de">Methylthioninium chlorid</div>
	 * <div class="fr">méthylthionine chlorure</div>
	 * <div class="it">Metiltioninio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	METHYLENE_BLUE_STAIN("6725000", "2.16.840.1.113883.6.96", "Methylene blue stain (substance)",
			"Methylene blue stain", "Methylthioninium chlorid", "méthylthionine chlorure",
			"Metiltioninio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methylergometrine</div>
	 * <div class="de">Methylergometrin</div>
	 * <div class="fr">méthylergométrine</div>
	 * <div class="it">Metilergometrina</div>
	 * <!-- @formatter:on -->
	 */
	METHYLERGOMETRINE("126074008", "2.16.840.1.113883.6.96", "Methylergometrine (substance)",
			"Methylergometrine", "Methylergometrin", "méthylergométrine", "Metilergometrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methylphenidate</div>
	 * <div class="de">Methylphenidat</div>
	 * <div class="fr">méthylphénidate</div>
	 * <div class="it">Metilfenidato</div>
	 * <!-- @formatter:on -->
	 */
	METHYLPHENIDATE("373337007", "2.16.840.1.113883.6.96", "Methylphenidate (substance)",
			"Methylphenidate", "Methylphenidat", "méthylphénidate", "Metilfenidato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Methylprednisolone</div>
	 * <div class="de">Methylprednisolon</div>
	 * <div class="fr">méthylprednisolone</div>
	 * <div class="it">Metilprednisolone</div>
	 * <!-- @formatter:on -->
	 */
	METHYLPREDNISOLONE("116593003", "2.16.840.1.113883.6.96", "Methylprednisolone (substance)",
			"Methylprednisolone", "Methylprednisolon", "méthylprednisolone", "Metilprednisolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Metoclopramide</div>
	 * <div class="de">Metoclopramid</div>
	 * <div class="fr">métoclopramide</div>
	 * <div class="it">Metoclopramide</div>
	 * <!-- @formatter:on -->
	 */
	METOCLOPRAMIDE("372776000", "2.16.840.1.113883.6.96", "Metoclopramide (substance)",
			"Metoclopramide", "Metoclopramid", "métoclopramide", "Metoclopramide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Metolazone</div>
	 * <div class="de">Metolazon</div>
	 * <div class="fr">métolazone</div>
	 * <div class="it">Metolazone</div>
	 * <!-- @formatter:on -->
	 */
	METOLAZONE("387123003", "2.16.840.1.113883.6.96", "Metolazone (substance)", "Metolazone",
			"Metolazon", "métolazone", "Metolazone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Metoprolol</div>
	 * <div class="de">Metoprolol</div>
	 * <div class="fr">métoprolol</div>
	 * <div class="it">Metoprololo</div>
	 * <!-- @formatter:on -->
	 */
	METOPROLOL("372826007", "2.16.840.1.113883.6.96", "Metoprolol (substance)", "Metoprolol",
			"Metoprolol", "métoprolol", "Metoprololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Metronidazole</div>
	 * <div class="de">Metronidazol</div>
	 * <div class="fr">métronidazole</div>
	 * <div class="it">Metronidazolo</div>
	 * <!-- @formatter:on -->
	 */
	METRONIDAZOLE("372602008", "2.16.840.1.113883.6.96", "Metronidazole (substance)",
			"Metronidazole", "Metronidazol", "métronidazole", "Metronidazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mianserin hydrochloride</div>
	 * <div class="de">Mianserin hydrochlorid</div>
	 * <div class="fr">miansérine chlorhydrate</div>
	 * <div class="it">Mianserina HCL</div>
	 * <!-- @formatter:on -->
	 */
	MIANSERIN_HYDROCHLORIDE("395795008", "2.16.840.1.113883.6.96",
			"Mianserin hydrochloride (substance)", "Mianserin hydrochloride",
			"Mianserin hydrochlorid", "miansérine chlorhydrate", "Mianserina HCL"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Miconazole</div>
	 * <div class="de">Miconazol</div>
	 * <div class="fr">miconazole</div>
	 * <div class="it">Miconazolo</div>
	 * <!-- @formatter:on -->
	 */
	MICONAZOLE("372738006", "2.16.840.1.113883.6.96", "Miconazole (substance)", "Miconazole",
			"Miconazol", "miconazole", "Miconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Midazolam</div>
	 * <div class="de">Midazolam</div>
	 * <div class="fr">midazolam</div>
	 * <div class="it">Midazolam</div>
	 * <!-- @formatter:on -->
	 */
	MIDAZOLAM("373476007", "2.16.840.1.113883.6.96", "Midazolam (substance)", "Midazolam",
			"Midazolam", "midazolam", "Midazolam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Midodrine</div>
	 * <div class="de">Midodrin</div>
	 * <div class="fr">midodrine</div>
	 * <div class="it">Midodrina</div>
	 * <!-- @formatter:on -->
	 */
	MIDODRINE("372504000", "2.16.840.1.113883.6.96", "Midodrine (substance)", "Midodrine",
			"Midodrin", "midodrine", "Midodrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mifepristone</div>
	 * <div class="de">Mifepriston</div>
	 * <div class="fr">mifépristone</div>
	 * <div class="it">Miferpristone</div>
	 * <!-- @formatter:on -->
	 */
	MIFEPRISTONE("395796009", "2.16.840.1.113883.6.96", "Mifepristone (substance)", "Mifepristone",
			"Mifepriston", "mifépristone", "Miferpristone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Milrinone</div>
	 * <div class="de">Milrinon</div>
	 * <div class="fr">milrinone</div>
	 * <div class="it">Milrinone</div>
	 * <!-- @formatter:on -->
	 */
	MILRINONE("373441005", "2.16.840.1.113883.6.96", "Milrinone (substance)", "Milrinone",
			"Milrinon", "milrinone", "Milrinone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Minocycline</div>
	 * <div class="de">Minocyclin</div>
	 * <div class="fr">minocycline</div>
	 * <div class="it">Minociclina</div>
	 * <!-- @formatter:on -->
	 */
	MINOCYCLINE("372653009", "2.16.840.1.113883.6.96", "Minocycline (substance)", "Minocycline",
			"Minocyclin", "minocycline", "Minociclina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Minoxidil</div>
	 * <div class="de">Minoxidil</div>
	 * <div class="fr">minoxidil</div>
	 * <div class="it">Minoxidil</div>
	 * <!-- @formatter:on -->
	 */
	MINOXIDIL("387272001", "2.16.840.1.113883.6.96", "Minoxidil (substance)", "Minoxidil",
			"Minoxidil", "minoxidil", "Minoxidil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mirabegron</div>
	 * <div class="de">Mirabegron</div>
	 * <div class="fr">mirabégron</div>
	 * <div class="it">Mirabegron</div>
	 * <!-- @formatter:on -->
	 */
	MIRABEGRON("703803006", "2.16.840.1.113883.6.96", "Mirabegron (substance)", "Mirabegron",
			"Mirabegron", "mirabégron", "Mirabegron"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mirtazapine</div>
	 * <div class="de">Mirtazapin</div>
	 * <div class="fr">mirtazapine</div>
	 * <div class="it">Mirtazapina</div>
	 * <!-- @formatter:on -->
	 */
	MIRTAZAPINE("386847004", "2.16.840.1.113883.6.96", "Mirtazapine (substance)", "Mirtazapine",
			"Mirtazapin", "mirtazapine", "Mirtazapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Misoprostol</div>
	 * <div class="de">Misoprostol</div>
	 * <div class="fr">misoprostol</div>
	 * <div class="it">Misoprostolo</div>
	 * <!-- @formatter:on -->
	 */
	MISOPROSTOL("387242007", "2.16.840.1.113883.6.96", "Misoprostol (substance)", "Misoprostol",
			"Misoprostol", "misoprostol", "Misoprostolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mitomycin</div>
	 * <div class="de">Mitomycin</div>
	 * <div class="fr">mitomycine</div>
	 * <div class="it">Mitomicina</div>
	 * <!-- @formatter:on -->
	 */
	MITOMYCIN("387331000", "2.16.840.1.113883.6.96", "Mitomycin (substance)", "Mitomycin",
			"Mitomycin", "mitomycine", "Mitomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mitoxantrone</div>
	 * <div class="de">Mitoxantron</div>
	 * <div class="fr">mitoxantrone</div>
	 * <div class="it">Mitoxantrone</div>
	 * <!-- @formatter:on -->
	 */
	MITOXANTRONE("386913001", "2.16.840.1.113883.6.96", "Mitoxantrone (substance)", "Mitoxantrone",
			"Mitoxantron", "mitoxantrone", "Mitoxantrone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mivacurium chloride</div>
	 * <div class="de">Mivacurium chlorid</div>
	 * <div class="fr">mivacurium chlorure</div>
	 * <div class="it">Mivacurio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	MIVACURIUM_CHLORIDE("108447000", "2.16.840.1.113883.6.96", "Mivacurium chloride (substance)",
			"Mivacurium chloride", "Mivacurium chlorid", "mivacurium chlorure",
			"Mivacurio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Moclobemide</div>
	 * <div class="de">Moclobemid</div>
	 * <div class="fr">moclobémide</div>
	 * <div class="it">Moclobemide</div>
	 * <!-- @formatter:on -->
	 */
	MOCLOBEMIDE("395800003", "2.16.840.1.113883.6.96", "Moclobemide (substance)", "Moclobemide",
			"Moclobemid", "moclobémide", "Moclobemide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Modafinil</div>
	 * <div class="de">Modafinil</div>
	 * <div class="fr">modafinil</div>
	 * <div class="it">Modafinil</div>
	 * <!-- @formatter:on -->
	 */
	MODAFINIL("387004007", "2.16.840.1.113883.6.96", "Modafinil (substance)", "Modafinil",
			"Modafinil", "modafinil", "Modafinil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Molsidomine</div>
	 * <div class="de">Molsidomin</div>
	 * <div class="fr">molsidomine</div>
	 * <div class="it">Molsidomina</div>
	 * <!-- @formatter:on -->
	 */
	MOLSIDOMINE("698196008", "2.16.840.1.113883.6.96", "Molsidomine (substance)", "Molsidomine",
			"Molsidomin", "molsidomine", "Molsidomina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mometasone</div>
	 * <div class="de">Mometason</div>
	 * <div class="fr">mométasone</div>
	 * <div class="it">Mometasone</div>
	 * <!-- @formatter:on -->
	 */
	MOMETASONE("395990009", "2.16.840.1.113883.6.96", "Mometasone (substance)", "Mometasone",
			"Mometason", "mométasone", "Mometasone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Montelukast</div>
	 * <div class="de">Montelukast</div>
	 * <div class="fr">montélukast</div>
	 * <div class="it">Montelukast</div>
	 * <!-- @formatter:on -->
	 */
	MONTELUKAST("373728005", "2.16.840.1.113883.6.96", "Montelukast (substance)", "Montelukast",
			"Montelukast", "montélukast", "Montelukast"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Moroctocog alfa</div>
	 * <div class="de">Moroctocog alfa</div>
	 * <div class="fr">moroctocog alfa</div>
	 * <div class="it">Moroctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	MOROCTOCOG_ALFA("441764007", "2.16.840.1.113883.6.96", "Moroctocog alfa (substance)",
			"Moroctocog alfa", "Moroctocog alfa", "moroctocog alfa", "Moroctocog alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Morphine</div>
	 * <div class="de">Morphin</div>
	 * <div class="fr">morphine</div>
	 * <div class="it">Morfina</div>
	 * <!-- @formatter:on -->
	 */
	MORPHINE("373529000", "2.16.840.1.113883.6.96", "Morphine (substance)", "Morphine", "Morphin",
			"morphine", "Morfina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Moxifloxacin</div>
	 * <div class="de">Moxifloxacin</div>
	 * <div class="fr">moxifloxacine</div>
	 * <div class="it">Moxifloxacina</div>
	 * <!-- @formatter:on -->
	 */
	MOXIFLOXACIN("412439003", "2.16.840.1.113883.6.96", "Moxifloxacin (substance)", "Moxifloxacin",
			"Moxifloxacin", "moxifloxacine", "Moxifloxacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Moxonidine</div>
	 * <div class="de">Moxonidin</div>
	 * <div class="fr">moxonidine</div>
	 * <div class="it">Moxonidina</div>
	 * <!-- @formatter:on -->
	 */
	MOXONIDINE("395805008", "2.16.840.1.113883.6.96", "Moxonidine (substance)", "Moxonidine",
			"Moxonidin", "moxonidine", "Moxonidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mumps vaccine</div>
	 * <div class="de">Mumps-Lebend-Impfstoff</div>
	 * <div class="fr">virus des oreillons vaccin vivant</div>
	 * <div class="it">Parotite vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	MUMPS_VACCINE("396431009", "2.16.840.1.113883.6.96", "Mumps vaccine (substance)",
			"Mumps vaccine", "Mumps-Lebend-Impfstoff", "virus des oreillons vaccin vivant",
			"Parotite vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mupirocin</div>
	 * <div class="de">Mupirocin</div>
	 * <div class="fr">mupirocine</div>
	 * <div class="it">Mupirocina</div>
	 * <!-- @formatter:on -->
	 */
	MUPIROCIN("387397004", "2.16.840.1.113883.6.96", "Mupirocin (substance)", "Mupirocin",
			"Mupirocin", "mupirocine", "Mupirocina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mycophenolate mofetil</div>
	 * <div class="de">Mycophenolat mofetil</div>
	 * <div class="fr">mycophénolate mofétil</div>
	 * <div class="it">Micofenolato mofetile</div>
	 * <!-- @formatter:on -->
	 */
	MYCOPHENOLATE_MOFETIL("386976000", "2.16.840.1.113883.6.96",
			"Mycophenolate mofetil (substance)", "Mycophenolate mofetil", "Mycophenolat mofetil",
			"mycophénolate mofétil", "Micofenolato mofetile"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Mycophenolic acid</div>
	 * <div class="de">Mycophenolsäure</div>
	 * <div class="fr">acide mycophénolique</div>
	 * <div class="it">Acido micofenolico</div>
	 * <!-- @formatter:on -->
	 */
	MYCOPHENOLIC_ACID("409330005", "2.16.840.1.113883.6.96", "Mycophenolic acid (substance)",
			"Mycophenolic acid", "Mycophenolsäure", "acide mycophénolique", "Acido micofenolico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nadolol</div>
	 * <div class="de">Nadolol</div>
	 * <div class="fr">nadolol</div>
	 * <div class="it">Nadololo</div>
	 * <!-- @formatter:on -->
	 */
	NADOLOL("387482003", "2.16.840.1.113883.6.96", "Nadolol (substance)", "Nadolol", "Nadolol",
			"nadolol", "Nadololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nadroparine</div>
	 * <div class="de">Nadroparin</div>
	 * <div class="fr">nadroparine</div>
	 * <div class="it">Nadroparina</div>
	 * <!-- @formatter:on -->
	 */
	NADROPARINE("699946002", "2.16.840.1.113883.6.96", "Nadroparine (substance)", "Nadroparine",
			"Nadroparin", "nadroparine", "Nadroparina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nadroparin calcium</div>
	 * <div class="de">Nadroparin calcium</div>
	 * <div class="fr">nadroparine calcique</div>
	 * <div class="it">Nadroparina calcica</div>
	 * <!-- @formatter:on -->
	 */
	NADROPARIN_CALCIUM("698278006", "2.16.840.1.113883.6.96", "Nadroparin calcium (substance)",
			"Nadroparin calcium", "Nadroparin calcium", "nadroparine calcique",
			"Nadroparina calcica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naftazone</div>
	 * <div class="de">Naftazon</div>
	 * <div class="fr">naftazone</div>
	 * <div class="it">Naftazone</div>
	 * <!-- @formatter:on -->
	 */
	NAFTAZONE("713428001", "2.16.840.1.113883.6.96", "Naftazone (substance)", "Naftazone",
			"Naftazon", "naftazone", "Naftazone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naftidrofuryl</div>
	 * <div class="de">Naftidrofuryl</div>
	 * <div class="fr">naftidrofuryl</div>
	 * <div class="it">Naftidrofurile</div>
	 * <!-- @formatter:on -->
	 */
	NAFTIDROFURYL("395992001", "2.16.840.1.113883.6.96", "Naftidrofuryl (substance)",
			"Naftidrofuryl", "Naftidrofuryl", "naftidrofuryl", "Naftidrofurile"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nalmefene</div>
	 * <div class="de">Nalmefen</div>
	 * <div class="fr">nalméfène</div>
	 * <div class="it">Nalmefene</div>
	 * <!-- @formatter:on -->
	 */
	NALMEFENE("109098006", "2.16.840.1.113883.6.96", "Nalmefene (substance)", "Nalmefene",
			"Nalmefen", "nalméfène", "Nalmefene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naloxone</div>
	 * <div class="de">Naloxon</div>
	 * <div class="fr">naloxone</div>
	 * <div class="it">Naloxone</div>
	 * <!-- @formatter:on -->
	 */
	NALOXONE("372890007", "2.16.840.1.113883.6.96", "Naloxone (substance)", "Naloxone", "Naloxon",
			"naloxone", "Naloxone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naltrexone</div>
	 * <div class="de">Naltrexon</div>
	 * <div class="fr">naltrexone</div>
	 * <div class="it">Naltrexone</div>
	 * <!-- @formatter:on -->
	 */
	NALTREXONE("373546002", "2.16.840.1.113883.6.96", "Naltrexone (substance)", "Naltrexone",
			"Naltrexon", "naltrexone", "Naltrexone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naphazoline</div>
	 * <div class="de">Naphazolin</div>
	 * <div class="fr">naphazoline</div>
	 * <div class="it">Nafazolina</div>
	 * <!-- @formatter:on -->
	 */
	NAPHAZOLINE("372803000", "2.16.840.1.113883.6.96", "Naphazoline (substance)", "Naphazoline",
			"Naphazolin", "naphazoline", "Nafazolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naproxen</div>
	 * <div class="de">Naproxen</div>
	 * <div class="fr">naproxène</div>
	 * <div class="it">Naprossene</div>
	 * <!-- @formatter:on -->
	 */
	NAPROXEN("372588000", "2.16.840.1.113883.6.96", "Naproxen (substance)", "Naproxen", "Naproxen",
			"naproxène", "Naprossene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Naratriptan</div>
	 * <div class="de">Naratriptan</div>
	 * <div class="fr">naratriptan</div>
	 * <div class="it">Naratriptan</div>
	 * <!-- @formatter:on -->
	 */
	NARATRIPTAN("363571003", "2.16.840.1.113883.6.96", "Naratriptan (substance)", "Naratriptan",
			"Naratriptan", "naratriptan", "Naratriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Natalizumab</div>
	 * <div class="de">Natalizumab</div>
	 * <div class="fr">natalizumab</div>
	 * <div class="it">Natalizumab</div>
	 * <!-- @formatter:on -->
	 */
	NATALIZUMAB("414805007", "2.16.840.1.113883.6.96", "Natalizumab (substance)", "Natalizumab",
			"Natalizumab", "natalizumab", "Natalizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nateglinide</div>
	 * <div class="de">Nateglinid</div>
	 * <div class="fr">natéglinide</div>
	 * <div class="it">Nateglinide</div>
	 * <!-- @formatter:on -->
	 */
	NATEGLINIDE("387070004", "2.16.840.1.113883.6.96", "Nateglinide (substance)", "Nateglinide",
			"Nateglinid", "natéglinide", "Nateglinide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nebivolol</div>
	 * <div class="de">Nebivolol</div>
	 * <div class="fr">nébivolol</div>
	 * <div class="it">Nebivololo</div>
	 * <!-- @formatter:on -->
	 */
	NEBIVOLOL("395808005", "2.16.840.1.113883.6.96", "Nebivolol (substance)", "Nebivolol",
			"Nebivolol", "nébivolol", "Nebivololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nelfinavir</div>
	 * <div class="de">Nelfinavir</div>
	 * <div class="fr">nelfinavir</div>
	 * <div class="it">Nelfinavir</div>
	 * <!-- @formatter:on -->
	 */
	NELFINAVIR("373445001", "2.16.840.1.113883.6.96", "Nelfinavir (substance)", "Nelfinavir",
			"Nelfinavir", "nelfinavir", "Nelfinavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Neomycin</div>
	 * <div class="de">Neomycin</div>
	 * <div class="fr">néomycine</div>
	 * <div class="it">Neomicina</div>
	 * <!-- @formatter:on -->
	 */
	NEOMYCIN("373528008", "2.16.840.1.113883.6.96", "Neomycin (substance)", "Neomycin", "Neomycin",
			"néomycine", "Neomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Neostigmine</div>
	 * <div class="de">Neostigmin</div>
	 * <div class="fr">néostigmine</div>
	 * <div class="it">Neostigmina</div>
	 * <!-- @formatter:on -->
	 */
	NEOSTIGMINE("373346001", "2.16.840.1.113883.6.96", "Neostigmine (substance)", "Neostigmine",
			"Neostigmin", "néostigmine", "Neostigmina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nevirapine</div>
	 * <div class="de">Nevirapin</div>
	 * <div class="fr">névirapine</div>
	 * <div class="it">Nevirapina</div>
	 * <!-- @formatter:on -->
	 */
	NEVIRAPINE("386898005", "2.16.840.1.113883.6.96", "Nevirapine (substance)", "Nevirapine",
			"Nevirapin", "névirapine", "Nevirapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nicardipine</div>
	 * <div class="de">Nicardipin</div>
	 * <div class="fr">nicardipine</div>
	 * <div class="it">Nicardipina</div>
	 * <!-- @formatter:on -->
	 */
	NICARDIPINE("372502001", "2.16.840.1.113883.6.96", "Nicardipine (substance)", "Nicardipine",
			"Nicardipin", "nicardipine", "Nicardipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nicorandil</div>
	 * <div class="de">Nicorandil</div>
	 * <div class="fr">nicorandil</div>
	 * <div class="it">Nicorandil</div>
	 * <!-- @formatter:on -->
	 */
	NICORANDIL("395809002", "2.16.840.1.113883.6.96", "Nicorandil (substance)", "Nicorandil",
			"Nicorandil", "nicorandil", "Nicorandil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nicotinamide</div>
	 * <div class="de">Nicotinamid</div>
	 * <div class="fr">nicotinamide</div>
	 * <div class="it">Nicotinamide</div>
	 * <!-- @formatter:on -->
	 */
	NICOTINAMIDE("173196005", "2.16.840.1.113883.6.96", "Nicotinamide (substance)", "Nicotinamide",
			"Nicotinamid", "nicotinamide", "Nicotinamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nifedipine</div>
	 * <div class="de">Nifedipin</div>
	 * <div class="fr">nifédipine</div>
	 * <div class="it">Nifedipina</div>
	 * <!-- @formatter:on -->
	 */
	NIFEDIPINE("387490003", "2.16.840.1.113883.6.96", "Nifedipine (substance)", "Nifedipine",
			"Nifedipin", "nifédipine", "Nifedipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nimesulide</div>
	 * <div class="de">Nimesulid</div>
	 * <div class="fr">nimésulide</div>
	 * <div class="it">Nimesulide</div>
	 * <!-- @formatter:on -->
	 */
	NIMESULIDE("703479000", "2.16.840.1.113883.6.96", "Nimesulide (substance)", "Nimesulide",
			"Nimesulid", "nimésulide", "Nimesulide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nimodipine</div>
	 * <div class="de">Nimodipin</div>
	 * <div class="fr">nimodipine</div>
	 * <div class="it">Nimodipina</div>
	 * <!-- @formatter:on -->
	 */
	NIMODIPINE("387502003", "2.16.840.1.113883.6.96", "Nimodipine (substance)", "Nimodipine",
			"Nimodipin", "nimodipine", "Nimodipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">nimésulide</div>
	 * <div class="de">Nicotin</div>
	 * <div class="fr">nicotine</div>
	 * <div class="it">Nicotina</div>
	 * <!-- @formatter:on -->
	 */
	NIM_SULIDE("68540007", "2.16.840.1.113883.6.96", "Nicotine (substance)", "nimésulide",
			"Nicotin", "nicotine", "Nicotina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nintedanib</div>
	 * <div class="de">Nintedanib</div>
	 * <div class="fr">nintédanib</div>
	 * <div class="it">Nintedanib</div>
	 * <!-- @formatter:on -->
	 */
	NINTEDANIB("712494002", "2.16.840.1.113883.6.96", "Nintedanib (substance)", "Nintedanib",
			"Nintedanib", "nintédanib", "Nintedanib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nitazoxanide</div>
	 * <div class="de">Nitazoxanid</div>
	 * <div class="fr">nitazoxanide</div>
	 * <div class="it">Nitazoxanide</div>
	 * <!-- @formatter:on -->
	 */
	NITAZOXANIDE("407148001", "2.16.840.1.113883.6.96", "Nitazoxanide (substance)", "Nitazoxanide",
			"Nitazoxanid", "nitazoxanide", "Nitazoxanide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nitisinone</div>
	 * <div class="de">Nitisinon</div>
	 * <div class="fr">nitisinone</div>
	 * <div class="it">Nitisinone</div>
	 * <!-- @formatter:on -->
	 */
	NITISINONE("385996000", "2.16.840.1.113883.6.96", "Nitisinone (substance)", "Nitisinone",
			"Nitisinon", "nitisinone", "Nitisinone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nitrazepam</div>
	 * <div class="de">Nitrazepam</div>
	 * <div class="fr">nitrazépam</div>
	 * <div class="it">Nitrazepam</div>
	 * <!-- @formatter:on -->
	 */
	NITRAZEPAM("387449001", "2.16.840.1.113883.6.96", "Nitrazepam (substance)", "Nitrazepam",
			"Nitrazepam", "nitrazépam", "Nitrazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nitrendipine</div>
	 * <div class="de">Nitrendipin</div>
	 * <div class="fr">nitrendipine</div>
	 * <div class="it">Nitrendipina</div>
	 * <!-- @formatter:on -->
	 */
	NITRENDIPINE("444757009", "2.16.840.1.113883.6.96", "Nitrendipine (substance)", "Nitrendipine",
			"Nitrendipin", "nitrendipine", "Nitrendipina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nitrofurantoin</div>
	 * <div class="de">Nitrofurantoin</div>
	 * <div class="fr">nitrofurantoïne</div>
	 * <div class="it">Nitrofurantoina</div>
	 * <!-- @formatter:on -->
	 */
	NITROFURANTOIN("373543005", "2.16.840.1.113883.6.96", "Nitrofurantoin (substance)",
			"Nitrofurantoin", "Nitrofurantoin", "nitrofurantoïne", "Nitrofurantoina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nivolumab</div>
	 * <div class="de">Nivolumab</div>
	 * <div class="fr">nivolumab</div>
	 * <div class="it">Nivolumab</div>
	 * <!-- @formatter:on -->
	 */
	NIVOLUMAB("704191007", "2.16.840.1.113883.6.96", "Nivolumab (substance)", "Nivolumab",
			"Nivolumab", "nivolumab", "Nivolumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nomegestrol acetate</div>
	 * <div class="de">Nomegestrol acetat</div>
	 * <div class="fr">nomégestrol acétate</div>
	 * <div class="it">Nomegestrolo acetato</div>
	 * <!-- @formatter:on -->
	 */
	NOMEGESTROL_ACETATE("698277001", "2.16.840.1.113883.6.96", "Nomegestrol acetate (substance)",
			"Nomegestrol acetate", "Nomegestrol acetat", "nomégestrol acétate",
			"Nomegestrolo acetato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Norepinephrine</div>
	 * <div class="de">Noradrenalin (Norepinephrin)</div>
	 * <div class="fr">noradrénaline (norépinéphrine)</div>
	 * <div class="it">Noradrenalina (norepinefrina)</div>
	 * <!-- @formatter:on -->
	 */
	NOREPINEPHRINE("45555007", "2.16.840.1.113883.6.96", "Norepinephrine (substance)",
			"Norepinephrine", "Noradrenalin (Norepinephrin)", "noradrénaline (norépinéphrine)",
			"Noradrenalina (norepinefrina)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Norethisterone</div>
	 * <div class="de">Norethisteron</div>
	 * <div class="fr">noréthistérone</div>
	 * <div class="it">Noretisterone</div>
	 * <!-- @formatter:on -->
	 */
	NORETHISTERONE("126102009", "2.16.840.1.113883.6.96", "Norethisterone (substance)",
			"Norethisterone", "Norethisteron", "noréthistérone", "Noretisterone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Norfloxacin</div>
	 * <div class="de">Norfloxacin</div>
	 * <div class="fr">norfloxacine</div>
	 * <div class="it">Norfloxacina</div>
	 * <!-- @formatter:on -->
	 */
	NORFLOXACIN("387271008", "2.16.840.1.113883.6.96", "Norfloxacin (substance)", "Norfloxacin",
			"Norfloxacin", "norfloxacine", "Norfloxacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Norgestimate</div>
	 * <div class="de">Norgestimat</div>
	 * <div class="fr">norgestimate</div>
	 * <div class="it">Norgestimato</div>
	 * <!-- @formatter:on -->
	 */
	NORGESTIMATE("126115000", "2.16.840.1.113883.6.96", "Norgestimate (substance)", "Norgestimate",
			"Norgestimat", "norgestimate", "Norgestimato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Norgestrel</div>
	 * <div class="de">Norgestrel</div>
	 * <div class="fr">norgestrel</div>
	 * <div class="it">Norgestrel</div>
	 * <!-- @formatter:on -->
	 */
	NORGESTREL("126106007", "2.16.840.1.113883.6.96", "Norgestrel (substance)", "Norgestrel",
			"Norgestrel", "norgestrel", "Norgestrel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Normal immunoglobulin human</div>
	 * <div class="de">Immunglobulin vom Menschen</div>
	 * <div class="fr">immunoglobuline humaine normale</div>
	 * <div class="it">Immunoglobulina umana normale</div>
	 * <!-- @formatter:on -->
	 */
	NORMAL_IMMUNOGLOBULIN_HUMAN("713355009", "2.16.840.1.113883.6.96",
			"Normal immunoglobulin human (substance)", "Normal immunoglobulin human",
			"Immunglobulin vom Menschen", "immunoglobuline humaine normale",
			"Immunoglobulina umana normale"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nortriptyline</div>
	 * <div class="de">Nortriptylin</div>
	 * <div class="fr">nortriptyline</div>
	 * <div class="it">Nortriptilina</div>
	 * <!-- @formatter:on -->
	 */
	NORTRIPTYLINE("372652004", "2.16.840.1.113883.6.96", "Nortriptyline (substance)",
			"Nortriptyline", "Nortriptylin", "nortriptyline", "Nortriptilina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Noscapine</div>
	 * <div class="de">Noscapin</div>
	 * <div class="fr">noscapine</div>
	 * <div class="it">Noscapina</div>
	 * <!-- @formatter:on -->
	 */
	NOSCAPINE("387437002", "2.16.840.1.113883.6.96", "Noscapine (substance)", "Noscapine",
			"Noscapin", "noscapine", "Noscapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Nystatin</div>
	 * <div class="de">Nystatin</div>
	 * <div class="fr">nystatine</div>
	 * <div class="it">Nistatina</div>
	 * <!-- @formatter:on -->
	 */
	NYSTATIN("387048002", "2.16.840.1.113883.6.96", "Nystatin (substance)", "Nystatin", "Nystatin",
			"nystatine", "Nistatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Obeticholic acid</div>
	 * <div class="de">Obeticholsäure</div>
	 * <div class="fr">acide obéticholique</div>
	 * <div class="it">Acido obeticolico</div>
	 * <!-- @formatter:on -->
	 */
	OBETICHOLIC_ACID("720257002", "2.16.840.1.113883.6.96", "Obeticholic acid (substance)",
			"Obeticholic acid", "Obeticholsäure", "acide obéticholique", "Acido obeticolico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Obinutuzumab</div>
	 * <div class="de">Obinutuzumab</div>
	 * <div class="fr">obinutuzumab</div>
	 * <div class="it">Obinutuzumab</div>
	 * <!-- @formatter:on -->
	 */
	OBINUTUZUMAB("710287009", "2.16.840.1.113883.6.96", "Obinutuzumab (substance)", "Obinutuzumab",
			"Obinutuzumab", "obinutuzumab", "Obinutuzumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ocrelizumab</div>
	 * <div class="de">Ocrelizumab</div>
	 * <div class="fr">ocrélizumab</div>
	 * <div class="it">Ocrelizumab</div>
	 * <!-- @formatter:on -->
	 */
	OCRELIZUMAB("733464008", "2.16.840.1.113883.6.96", "Ocrelizumab (substance)", "Ocrelizumab",
			"Ocrelizumab", "ocrélizumab", "Ocrelizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Octenidine</div>
	 * <div class="de">Octenidin</div>
	 * <div class="fr">octénidine</div>
	 * <div class="it">Octenidina</div>
	 * <!-- @formatter:on -->
	 */
	OCTENIDINE("430477008", "2.16.840.1.113883.6.96", "Octenidine (substance)", "Octenidine",
			"Octenidin", "octénidine", "Octenidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Octocog alfa</div>
	 * <div class="de">Octocog alfa</div>
	 * <div class="fr">octocog alfa</div>
	 * <div class="it">Octocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	OCTOCOG_ALFA("418888003", "2.16.840.1.113883.6.96", "Octocog alfa (substance)", "Octocog alfa",
			"Octocog alfa", "octocog alfa", "Octocog alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Octreotide</div>
	 * <div class="de">Octreotid</div>
	 * <div class="fr">octréotide</div>
	 * <div class="it">Octreotide</div>
	 * <!-- @formatter:on -->
	 */
	OCTREOTIDE("109053000", "2.16.840.1.113883.6.96", "Octreotide (substance)", "Octreotide",
			"Octreotid", "octréotide", "Octreotide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ofloxacin</div>
	 * <div class="de">Ofloxacin</div>
	 * <div class="fr">ofloxacine</div>
	 * <div class="it">Ofloxacina</div>
	 * <!-- @formatter:on -->
	 */
	OFLOXACIN("387551000", "2.16.840.1.113883.6.96", "Ofloxacin (substance)", "Ofloxacin",
			"Ofloxacin", "ofloxacine", "Ofloxacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Olanzapine</div>
	 * <div class="de">Olanzapin</div>
	 * <div class="fr">olanzapine</div>
	 * <div class="it">Olanzapina</div>
	 * <!-- @formatter:on -->
	 */
	OLANZAPINE("386849001", "2.16.840.1.113883.6.96", "Olanzapine (substance)", "Olanzapine",
			"Olanzapin", "olanzapine", "Olanzapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Olanzapine embonate</div>
	 * <div class="de">Olanzapin embonat-1-Wasser</div>
	 * <div class="fr">olanzapine embonate</div>
	 * <div class="it">Olanzapina embonato</div>
	 * <!-- @formatter:on -->
	 */
	OLANZAPINE_EMBONATE("725800004", "2.16.840.1.113883.6.96", "Olanzapine embonate (substance)",
			"Olanzapine embonate", "Olanzapin embonat-1-Wasser", "olanzapine embonate",
			"Olanzapina embonato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Olive oil</div>
	 * <div class="de">Olivenöl</div>
	 * <div class="fr">olive huile</div>
	 * <div class="it">Oliva olio</div>
	 * <!-- @formatter:on -->
	 */
	OLIVE_OIL("41834005", "2.16.840.1.113883.6.96", "Olive oil (substance)", "Olive oil",
			"Olivenöl", "olive huile", "Oliva olio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Olmesartan</div>
	 * <div class="de">Olmesartan</div>
	 * <div class="fr">olmésartan</div>
	 * <div class="it">Olmesartan</div>
	 * <!-- @formatter:on -->
	 */
	OLMESARTAN("412259001", "2.16.840.1.113883.6.96", "Olmesartan (substance)", "Olmesartan",
			"Olmesartan", "olmésartan", "Olmesartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Olodaterol</div>
	 * <div class="de">Olodaterol</div>
	 * <div class="fr">olodatérol</div>
	 * <div class="it">Olodaterolo</div>
	 * <!-- @formatter:on -->
	 */
	OLODATEROL("704459002", "2.16.840.1.113883.6.96", "Olodaterol (substance)", "Olodaterol",
			"Olodaterol", "olodatérol", "Olodaterolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Omalizumab</div>
	 * <div class="de">Omalizumab</div>
	 * <div class="fr">omalizumab</div>
	 * <div class="it">Omalizumab</div>
	 * <!-- @formatter:on -->
	 */
	OMALIZUMAB("406443008", "2.16.840.1.113883.6.96", "Omalizumab (substance)", "Omalizumab",
			"Omalizumab", "omalizumab", "Omalizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Omega 3 fatty acid</div>
	 * <div class="de">Omega-3-Fettsäuren</div>
	 * <div class="fr">acides gras oméga-3</div>
	 * <div class="it">Acidi grassi omega 3</div>
	 * <!-- @formatter:on -->
	 */
	OMEGA_3_FATTY_ACID("226365003", "2.16.840.1.113883.6.96", "Omega 3 fatty acid (substance)",
			"Omega 3 fatty acid", "Omega-3-Fettsäuren", "acides gras oméga-3",
			"Acidi grassi omega 3"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Omeprazole</div>
	 * <div class="de">Omeprazol</div>
	 * <div class="fr">oméprazole</div>
	 * <div class="it">Omeprazolo</div>
	 * <!-- @formatter:on -->
	 */
	OMEPRAZOLE("387137007", "2.16.840.1.113883.6.96", "Omeprazole (substance)", "Omeprazole",
			"Omeprazol", "oméprazole", "Omeprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ondansetron</div>
	 * <div class="de">Ondansetron</div>
	 * <div class="fr">ondansétron</div>
	 * <div class="it">Ondansetron</div>
	 * <!-- @formatter:on -->
	 */
	ONDANSETRON("372487007", "2.16.840.1.113883.6.96", "Ondansetron (substance)", "Ondansetron",
			"Ondansetron", "ondansétron", "Ondansetron"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Orlistat</div>
	 * <div class="de">Orlistat</div>
	 * <div class="fr">orlistat</div>
	 * <div class="it">Orlistat</div>
	 * <!-- @formatter:on -->
	 */
	ORLISTAT("387007000", "2.16.840.1.113883.6.96", "Orlistat (substance)", "Orlistat", "Orlistat",
			"orlistat", "Orlistat"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ornidazole</div>
	 * <div class="de">Ornidazol</div>
	 * <div class="fr">ornidazole</div>
	 * <div class="it">Ornidazolo</div>
	 * <!-- @formatter:on -->
	 */
	ORNIDAZOLE("442924004", "2.16.840.1.113883.6.96", "Ornidazole (substance)", "Ornidazole",
			"Ornidazol", "ornidazole", "Ornidazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oseltamivir</div>
	 * <div class="de">Oseltamivir</div>
	 * <div class="fr">oseltamivir</div>
	 * <div class="it">Oseltamivir</div>
	 * <!-- @formatter:on -->
	 */
	OSELTAMIVIR("412261005", "2.16.840.1.113883.6.96", "Oseltamivir (substance)", "Oseltamivir",
			"Oseltamivir", "oseltamivir", "Oseltamivir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxaliplatin</div>
	 * <div class="de">Oxaliplatin</div>
	 * <div class="fr">oxaliplatine</div>
	 * <div class="it">Oxaliplatino</div>
	 * <!-- @formatter:on -->
	 */
	OXALIPLATIN("395814003", "2.16.840.1.113883.6.96", "Oxaliplatin (substance)", "Oxaliplatin",
			"Oxaliplatin", "oxaliplatine", "Oxaliplatino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxandrolone</div>
	 * <div class="de">Oxandrolon</div>
	 * <div class="fr">oxandrolone</div>
	 * <div class="it">Oxandrolone</div>
	 * <!-- @formatter:on -->
	 */
	OXANDROLONE("126128007", "2.16.840.1.113883.6.96", "Oxandrolone (substance)", "Oxandrolone",
			"Oxandrolon", "oxandrolone", "Oxandrolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxazepam</div>
	 * <div class="de">Oxazepam</div>
	 * <div class="fr">oxazépam</div>
	 * <div class="it">Oxazepam</div>
	 * <!-- @formatter:on -->
	 */
	OXAZEPAM("387455006", "2.16.840.1.113883.6.96", "Oxazepam (substance)", "Oxazepam", "Oxazepam",
			"oxazépam", "Oxazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxcarbazepine</div>
	 * <div class="de">Oxcarbazepin</div>
	 * <div class="fr">oxcarbazépine</div>
	 * <div class="it">Oxcarbazepina</div>
	 * <!-- @formatter:on -->
	 */
	OXCARBAZEPINE("387025007", "2.16.840.1.113883.6.96", "Oxcarbazepine (substance)",
			"Oxcarbazepine", "Oxcarbazepin", "oxcarbazépine", "Oxcarbazepina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxomemazine</div>
	 * <div class="de">Oxomemazin</div>
	 * <div class="fr">oxomémazine</div>
	 * <div class="it">Oxomemazina</div>
	 * <!-- @formatter:on -->
	 */
	OXOMEMAZINE("772837001", "2.16.840.1.113883.6.96", "Oxomemazine (substance)", "Oxomemazine",
			"Oxomemazin", "oxomémazine", "Oxomemazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxybuprocaine</div>
	 * <div class="de">Oxybuprocain</div>
	 * <div class="fr">oxybuprocaïne</div>
	 * <div class="it">Oxibuprocaina</div>
	 * <!-- @formatter:on -->
	 */
	OXYBUPROCAINE("52140009", "2.16.840.1.113883.6.96", "Oxybuprocaine (substance)",
			"Oxybuprocaine", "Oxybuprocain", "oxybuprocaïne", "Oxibuprocaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxybutynin</div>
	 * <div class="de">Oxybutynin</div>
	 * <div class="fr">oxybutynine</div>
	 * <div class="it">Ossibutinina</div>
	 * <!-- @formatter:on -->
	 */
	OXYBUTYNIN("372717000", "2.16.840.1.113883.6.96", "Oxybutynin (substance)", "Oxybutynin",
			"Oxybutynin", "oxybutynine", "Ossibutinina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxycodone</div>
	 * <div class="de">Oxycodon</div>
	 * <div class="fr">oxycodone</div>
	 * <div class="it">Ossicodone</div>
	 * <!-- @formatter:on -->
	 */
	OXYCODONE("55452001", "2.16.840.1.113883.6.96", "Oxycodone (substance)", "Oxycodone",
			"Oxycodon", "oxycodone", "Ossicodone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxymetazoline</div>
	 * <div class="de">Oxymetazolin</div>
	 * <div class="fr">oxymétazoline</div>
	 * <div class="it">Ossimetazolina</div>
	 * <!-- @formatter:on -->
	 */
	OXYMETAZOLINE("387158001", "2.16.840.1.113883.6.96", "Oxymetazoline (substance)",
			"Oxymetazoline", "Oxymetazolin", "oxymétazoline", "Ossimetazolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxytetracycline</div>
	 * <div class="de">Oxytetracyclin</div>
	 * <div class="fr">oxytétracycline</div>
	 * <div class="it">Ossitetraciclina</div>
	 * <!-- @formatter:on -->
	 */
	OXYTETRACYCLINE("372675006", "2.16.840.1.113883.6.96", "Oxytetracycline (substance)",
			"Oxytetracycline", "Oxytetracyclin", "oxytétracycline", "Ossitetraciclina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Oxytocin</div>
	 * <div class="de">Oxytocin</div>
	 * <div class="fr">oxytocine</div>
	 * <div class="it">Ossitocina</div>
	 * <!-- @formatter:on -->
	 */
	OXYTOCIN("112115002", "2.16.840.1.113883.6.96", "Oxytocin (substance)", "Oxytocin", "Oxytocin",
			"oxytocine", "Ossitocina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Paclitaxel</div>
	 * <div class="de">Paclitaxel</div>
	 * <div class="fr">paclitaxel</div>
	 * <div class="it">Paclitaxel</div>
	 * <!-- @formatter:on -->
	 */
	PACLITAXEL("387374002", "2.16.840.1.113883.6.96", "Paclitaxel (substance)", "Paclitaxel",
			"Paclitaxel", "paclitaxel", "Paclitaxel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Paliperidone</div>
	 * <div class="de">Paliperidon</div>
	 * <div class="fr">palipéridone</div>
	 * <div class="it">Paliperidone</div>
	 * <!-- @formatter:on -->
	 */
	PALIPERIDONE("426276000", "2.16.840.1.113883.6.96", "Paliperidone (substance)", "Paliperidone",
			"Paliperidon", "palipéridone", "Paliperidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Palivizumab</div>
	 * <div class="de">Palivizumab</div>
	 * <div class="fr">palivizumab</div>
	 * <div class="it">Palivizumab</div>
	 * <!-- @formatter:on -->
	 */
	PALIVIZUMAB("386900007", "2.16.840.1.113883.6.96", "Palivizumab (substance)", "Palivizumab",
			"Palivizumab", "palivizumab", "Palivizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Palonosetron</div>
	 * <div class="de">Palonosetron</div>
	 * <div class="fr">palonosétron</div>
	 * <div class="it">Palonosetron</div>
	 * <!-- @formatter:on -->
	 */
	PALONOSETRON("404852008", "2.16.840.1.113883.6.96", "Palonosetron (substance)", "Palonosetron",
			"Palonosetron", "palonosétron", "Palonosetron"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pancuronium</div>
	 * <div class="de">Pancuronium</div>
	 * <div class="fr">pancuronium</div>
	 * <div class="it">Pancuronio</div>
	 * <!-- @formatter:on -->
	 */
	PANCURONIUM("373738000", "2.16.840.1.113883.6.96", "Pancuronium (substance)", "Pancuronium",
			"Pancuronium", "pancuronium", "Pancuronio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pantoprazole</div>
	 * <div class="de">Pantoprazol</div>
	 * <div class="fr">pantoprazole</div>
	 * <div class="it">Pantoprazolo</div>
	 * <!-- @formatter:on -->
	 */
	PANTOPRAZOLE("395821003", "2.16.840.1.113883.6.96", "Pantoprazole (substance)", "Pantoprazole",
			"Pantoprazol", "pantoprazole", "Pantoprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pantothenic acid</div>
	 * <div class="de">Pantothensäure</div>
	 * <div class="fr">acide pantothénique</div>
	 * <div class="it">Acido pantotenico</div>
	 * <!-- @formatter:on -->
	 */
	PANTOTHENIC_ACID("86431009", "2.16.840.1.113883.6.96", "Pantothenic acid (substance)",
			"Pantothenic acid", "Pantothensäure", "acide pantothénique", "Acido pantotenico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Papaverine</div>
	 * <div class="de">Papaverin</div>
	 * <div class="fr">papavérine</div>
	 * <div class="it">Papaverina</div>
	 * <!-- @formatter:on -->
	 */
	PAPAVERINE("372784001", "2.16.840.1.113883.6.96", "Papaverine (substance)", "Papaverine",
			"Papaverin", "papavérine", "Papaverina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Paracetamol</div>
	 * <div class="de">Paracetamol</div>
	 * <div class="fr">paracétamol</div>
	 * <div class="it">Paracetamolo</div>
	 * <!-- @formatter:on -->
	 */
	PARACETAMOL("387517004", "2.16.840.1.113883.6.96", "Paracetamol (substance)", "Paracetamol",
			"Paracetamol", "paracétamol", "Paracetamolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Paraffin</div>
	 * <div class="de">Paraffin</div>
	 * <div class="fr">paraffine</div>
	 * <div class="it">Paraffina</div>
	 * <!-- @formatter:on -->
	 */
	PARAFFIN("255667006", "2.16.840.1.113883.6.96", "Paraffin (substance)", "Paraffin", "Paraffin",
			"paraffine", "Paraffina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Paricalcitol</div>
	 * <div class="de">Paricalcitol</div>
	 * <div class="fr">paricalcitol</div>
	 * <div class="it">Paracalcitolo</div>
	 * <!-- @formatter:on -->
	 */
	PARICALCITOL("108946001", "2.16.840.1.113883.6.96", "Paricalcitol (substance)", "Paricalcitol",
			"Paricalcitol", "paricalcitol", "Paracalcitolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Paroxetine</div>
	 * <div class="de">Paroxetin</div>
	 * <div class="fr">paroxétine</div>
	 * <div class="it">Paroxetina</div>
	 * <!-- @formatter:on -->
	 */
	PAROXETINE("372595009", "2.16.840.1.113883.6.96", "Paroxetine (substance)", "Paroxetine",
			"Paroxetin", "paroxétine", "Paroxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pegaspargase</div>
	 * <div class="de">Pegaspargase</div>
	 * <div class="fr">pégaspargase</div>
	 * <div class="it">Pegaspargase</div>
	 * <!-- @formatter:on -->
	 */
	PEGASPARGASE("108814000", "2.16.840.1.113883.6.96", "Pegaspargase (substance)", "Pegaspargase",
			"Pegaspargase", "pégaspargase", "Pegaspargase"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pegfilgrastim</div>
	 * <div class="de">Pegfilgrastim</div>
	 * <div class="fr">pegfilgrastim</div>
	 * <div class="it">Pegfilgrastim</div>
	 * <!-- @formatter:on -->
	 */
	PEGFILGRASTIM("385544005", "2.16.840.1.113883.6.96", "Pegfilgrastim (substance)",
			"Pegfilgrastim", "Pegfilgrastim", "pegfilgrastim", "Pegfilgrastim"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Peginterferon alfa-2a</div>
	 * <div class="de">Peginterferon alfa-2a</div>
	 * <div class="fr">peginterféron alfa-2a</div>
	 * <div class="it">Peginterferone alfa-2a</div>
	 * <!-- @formatter:on -->
	 */
	PEGINTERFERON_ALFA_2A("421559001", "2.16.840.1.113883.6.96",
			"Peginterferon alfa-2a (substance)", "Peginterferon alfa-2a", "Peginterferon alfa-2a",
			"peginterféron alfa-2a", "Peginterferone alfa-2a"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pembrolizumab</div>
	 * <div class="de">Pembrolizumab</div>
	 * <div class="fr">pembrolizumab</div>
	 * <div class="it">Pembrolizumab</div>
	 * <!-- @formatter:on -->
	 */
	PEMBROLIZUMAB("716125002", "2.16.840.1.113883.6.96", "Pembrolizumab (substance)",
			"Pembrolizumab", "Pembrolizumab", "pembrolizumab", "Pembrolizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pemetrexed</div>
	 * <div class="de">Pemetrexed</div>
	 * <div class="fr">pémétrexed</div>
	 * <div class="it">Pemetrexed</div>
	 * <!-- @formatter:on -->
	 */
	PEMETREXED("409159000", "2.16.840.1.113883.6.96", "Pemetrexed (substance)", "Pemetrexed",
			"Pemetrexed", "pémétrexed", "Pemetrexed"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Penciclovir</div>
	 * <div class="de">Penciclovir</div>
	 * <div class="fr">penciclovir</div>
	 * <div class="it">Penciclovir</div>
	 * <!-- @formatter:on -->
	 */
	PENCICLOVIR("386939003", "2.16.840.1.113883.6.96", "Penciclovir (substance)", "Penciclovir",
			"Penciclovir", "penciclovir", "Penciclovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pentamidine isethionate</div>
	 * <div class="de">Pentamidin diisetionat</div>
	 * <div class="fr">pentamidine diiséthionate</div>
	 * <div class="it">Pentamidina</div>
	 * <!-- @formatter:on -->
	 */
	PENTAMIDINE_ISETHIONATE("16826009", "2.16.840.1.113883.6.96",
			"Pentamidine isethionate (substance)", "Pentamidine isethionate",
			"Pentamidin diisetionat", "pentamidine diiséthionate", "Pentamidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pentoxifylline</div>
	 * <div class="de">Pentoxifyllin</div>
	 * <div class="fr">pentoxyfylline</div>
	 * <div class="it">Pentossifillina</div>
	 * <!-- @formatter:on -->
	 */
	PENTOXIFYLLINE("387522004", "2.16.840.1.113883.6.96", "Pentoxifylline (substance)",
			"Pentoxifylline", "Pentoxifyllin", "pentoxyfylline", "Pentossifillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Perampanel</div>
	 * <div class="de">Perampanel</div>
	 * <div class="fr">pérampanel</div>
	 * <div class="it">Perampanel</div>
	 * <!-- @formatter:on -->
	 */
	PERAMPANEL("703127006", "2.16.840.1.113883.6.96", "Perampanel (substance)", "Perampanel",
			"Perampanel", "pérampanel", "Perampanel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Perindopril</div>
	 * <div class="de">Perindopril</div>
	 * <div class="fr">périndopril</div>
	 * <div class="it">Perindopril</div>
	 * <!-- @formatter:on -->
	 */
	PERINDOPRIL("372916001", "2.16.840.1.113883.6.96", "Perindopril (substance)", "Perindopril",
			"Perindopril", "périndopril", "Perindopril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Permethrin</div>
	 * <div class="de">Permethrin</div>
	 * <div class="fr">perméthrine</div>
	 * <div class="it">Permetrina</div>
	 * <!-- @formatter:on -->
	 */
	PERMETHRIN("410457007", "2.16.840.1.113883.6.96", "Permethrin (substance)", "Permethrin",
			"Permethrin", "perméthrine", "Permetrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pertussis vaccine</div>
	 * <div class="de">Pertussis-Impfstoff</div>
	 * <div class="fr">vaccin anticoquelucheux</div>
	 * <div class="it">Pertosse vaccino</div>
	 * <!-- @formatter:on -->
	 */
	PERTUSSIS_VACCINE("396433007", "2.16.840.1.113883.6.96", "Pertussis vaccine (substance)",
			"Pertussis vaccine", "Pertussis-Impfstoff", "vaccin anticoquelucheux",
			"Pertosse vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pertuzumab</div>
	 * <div class="de">Pertuzumab</div>
	 * <div class="fr">pertuzumab</div>
	 * <div class="it">Pertuzumab</div>
	 * <!-- @formatter:on -->
	 */
	PERTUZUMAB("704226002", "2.16.840.1.113883.6.96", "Pertuzumab (substance)", "Pertuzumab",
			"Pertuzumab", "pertuzumab", "Pertuzumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pethidine</div>
	 * <div class="de">Pethidin</div>
	 * <div class="fr">péthidine</div>
	 * <div class="it">Petidina</div>
	 * <!-- @formatter:on -->
	 */
	PETHIDINE("387298007", "2.16.840.1.113883.6.96", "Pethidine (substance)", "Pethidine",
			"Pethidin", "péthidine", "Petidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenazone</div>
	 * <div class="de">Phenazon</div>
	 * <div class="fr">phénazone</div>
	 * <div class="it">Fenazone</div>
	 * <!-- @formatter:on -->
	 */
	PHENAZONE("55486005", "2.16.840.1.113883.6.96", "Phenazone (substance)", "Phenazone",
			"Phenazon", "phénazone", "Fenazone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pheniramine</div>
	 * <div class="de">Pheniramin</div>
	 * <div class="fr">phéniramine</div>
	 * <div class="it">Feniramina</div>
	 * <!-- @formatter:on -->
	 */
	PHENIRAMINE("373500002", "2.16.840.1.113883.6.96", "Pheniramine (substance)", "Pheniramine",
			"Pheniramin", "phéniramine", "Feniramina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenobarbital</div>
	 * <div class="de">Phenobarbital</div>
	 * <div class="fr">phénobarbital</div>
	 * <div class="it">Fenobarbital</div>
	 * <!-- @formatter:on -->
	 */
	PHENOBARBITAL("373505007", "2.16.840.1.113883.6.96", "Phenobarbital (substance)",
			"Phenobarbital", "Phenobarbital", "phénobarbital", "Fenobarbital"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenoxybenzamine</div>
	 * <div class="de">Phenoxybenzamin</div>
	 * <div class="fr">phénoxybenzamine</div>
	 * <div class="it">Fenossibenzamina</div>
	 * <!-- @formatter:on -->
	 */
	PHENOXYBENZAMINE("372838003", "2.16.840.1.113883.6.96", "Phenoxybenzamine (substance)",
			"Phenoxybenzamine", "Phenoxybenzamin", "phénoxybenzamine", "Fenossibenzamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenoxymethylpenicillin potassium</div>
	 * <div class="de">Phenoxymethylpenicillin kalium</div>
	 * <div class="fr">phénoxyméthylpénicilline potassique</div>
	 * <div class="it">Fenossimetilpenicillina potassica</div>
	 * <!-- @formatter:on -->
	 */
	PHENOXYMETHYLPENICILLIN_POTASSIUM("56723006", "2.16.840.1.113883.6.96",
			"Phenoxymethylpenicillin potassium (substance)", "Phenoxymethylpenicillin potassium",
			"Phenoxymethylpenicillin kalium", "phénoxyméthylpénicilline potassique",
			"Fenossimetilpenicillina potassica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenprocoumon</div>
	 * <div class="de">Phenprocoumon</div>
	 * <div class="fr">phenprocoumone</div>
	 * <div class="it">Fenprocumone</div>
	 * <!-- @formatter:on -->
	 */
	PHENPROCOUMON("59488002", "2.16.840.1.113883.6.96", "Phenprocoumon (substance)",
			"Phenprocoumon", "Phenprocoumon", "phenprocoumone", "Fenprocumone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phentolamine</div>
	 * <div class="de">Phentolamin</div>
	 * <div class="fr">phentolamine</div>
	 * <div class="it">Fentolamina</div>
	 * <!-- @formatter:on -->
	 */
	PHENTOLAMINE("372863003", "2.16.840.1.113883.6.96", "Phentolamine (substance)", "Phentolamine",
			"Phentolamin", "phentolamine", "Fentolamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenylalanine</div>
	 * <div class="de">Phenylalanin</div>
	 * <div class="fr">phénylalanine</div>
	 * <div class="it">Fenilalanina</div>
	 * <!-- @formatter:on -->
	 */
	PHENYLALANINE("63004003", "2.16.840.1.113883.6.96", "Phenylalanine (substance)",
			"Phenylalanine", "Phenylalanin", "phénylalanine", "Fenilalanina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenylephrine</div>
	 * <div class="de">Phenylephrin</div>
	 * <div class="fr">phényléphrine</div>
	 * <div class="it">Fenilefrina</div>
	 * <!-- @formatter:on -->
	 */
	PHENYLEPHRINE("372771005", "2.16.840.1.113883.6.96", "Phenylephrine (substance)",
			"Phenylephrine", "Phenylephrin", "phényléphrine", "Fenilefrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phenytoin</div>
	 * <div class="de">Phenytoin</div>
	 * <div class="fr">phénytoïne</div>
	 * <div class="it">Fenitoina</div>
	 * <!-- @formatter:on -->
	 */
	PHENYTOIN("387220006", "2.16.840.1.113883.6.96", "Phenytoin (substance)", "Phenytoin",
			"Phenytoin", "phénytoïne", "Fenitoina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pholcodine</div>
	 * <div class="de">Pholcodin</div>
	 * <div class="fr">pholcodine</div>
	 * <div class="it">Folcodina</div>
	 * <!-- @formatter:on -->
	 */
	PHOLCODINE("396486005", "2.16.840.1.113883.6.96", "Pholcodine (substance)", "Pholcodine",
			"Pholcodin", "pholcodine", "Folcodina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phospholipid</div>
	 * <div class="de">Phospholipide</div>
	 * <div class="fr">phospholipides</div>
	 * <div class="it">Fosfolipidi</div>
	 * <!-- @formatter:on -->
	 */
	PHOSPHOLIPID("78447009", "2.16.840.1.113883.6.96", "Phospholipid (substance)", "Phospholipid",
			"Phospholipide", "phospholipides", "Fosfolipidi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Physostigmine</div>
	 * <div class="de">Physostigmin</div>
	 * <div class="fr">physostigmine</div>
	 * <div class="it">Fisostigmina</div>
	 * <!-- @formatter:on -->
	 */
	PHYSOSTIGMINE("373347005", "2.16.840.1.113883.6.96", "Physostigmine (substance)",
			"Physostigmine", "Physostigmin", "physostigmine", "Fisostigmina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Phytomenadione</div>
	 * <div class="de">Phytomenadion (Vitamin K1)</div>
	 * <div class="fr">phytoménadione (Vitamine K1)</div>
	 * <div class="it">Fitomenadione (vitamina K1)</div>
	 * <!-- @formatter:on -->
	 */
	PHYTOMENADIONE("66656000", "2.16.840.1.113883.6.96", "Phytomenadione (substance)",
			"Phytomenadione", "Phytomenadion (Vitamin K1)", "phytoménadione (Vitamine K1)",
			"Fitomenadione (vitamina K1)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pilocarpine</div>
	 * <div class="de">Pilocarpin</div>
	 * <div class="fr">pilocarpine</div>
	 * <div class="it">Pilocarpina</div>
	 * <!-- @formatter:on -->
	 */
	PILOCARPINE("372895002", "2.16.840.1.113883.6.96", "Pilocarpine (substance)", "Pilocarpine",
			"Pilocarpin", "pilocarpine", "Pilocarpina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pimecrolimus</div>
	 * <div class="de">Pimecrolimus</div>
	 * <div class="fr">pimécrolimus</div>
	 * <div class="it">Pimecrolimus</div>
	 * <!-- @formatter:on -->
	 */
	PIMECROLIMUS("385580005", "2.16.840.1.113883.6.96", "Pimecrolimus (substance)", "Pimecrolimus",
			"Pimecrolimus", "pimécrolimus", "Pimecrolimus"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pioglitazone</div>
	 * <div class="de">Pioglitazon</div>
	 * <div class="fr">pioglitazone</div>
	 * <div class="it">Pioglitazone</div>
	 * <!-- @formatter:on -->
	 */
	PIOGLITAZONE("395828009", "2.16.840.1.113883.6.96", "Pioglitazone (substance)", "Pioglitazone",
			"Pioglitazon", "pioglitazone", "Pioglitazone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pipamperone</div>
	 * <div class="de">Pipamperon</div>
	 * <div class="fr">pipampérone</div>
	 * <div class="it">Pipamperone</div>
	 * <!-- @formatter:on -->
	 */
	PIPAMPERONE("703362007", "2.16.840.1.113883.6.96", "Pipamperone (substance)", "Pipamperone",
			"Pipamperon", "pipampérone", "Pipamperone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Piperacillin</div>
	 * <div class="de">Piperacillin</div>
	 * <div class="fr">pipéracilline</div>
	 * <div class="it">Piperacillina</div>
	 * <!-- @formatter:on -->
	 */
	PIPERACILLIN("372836004", "2.16.840.1.113883.6.96", "Piperacillin (substance)", "Piperacillin",
			"Piperacillin", "pipéracilline", "Piperacillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Piracetam</div>
	 * <div class="de">Piracetam</div>
	 * <div class="fr">piracétam</div>
	 * <div class="it">Piracetam</div>
	 * <!-- @formatter:on -->
	 */
	PIRACETAM("395833008", "2.16.840.1.113883.6.96", "Piracetam (substance)", "Piracetam",
			"Piracetam", "piracétam", "Piracetam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Piretanide</div>
	 * <div class="de">Piretanid</div>
	 * <div class="fr">pirétanide</div>
	 * <div class="it">Piretanide</div>
	 * <!-- @formatter:on -->
	 */
	PIRETANIDE("419451002", "2.16.840.1.113883.6.96", "Piretanide (substance)", "Piretanide",
			"Piretanid", "pirétanide", "Piretanide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Piroxicam</div>
	 * <div class="de">Piroxicam</div>
	 * <div class="fr">piroxicam</div>
	 * <div class="it">Piroxicam</div>
	 * <!-- @formatter:on -->
	 */
	PIROXICAM("387153005", "2.16.840.1.113883.6.96", "Piroxicam (substance)", "Piroxicam",
			"Piroxicam", "piroxicam", "Piroxicam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pitavastatin</div>
	 * <div class="de">Pitavastatin</div>
	 * <div class="fr">pitavastatine</div>
	 * <div class="it">Pitavastatina</div>
	 * <!-- @formatter:on -->
	 */
	PITAVASTATIN("443586000", "2.16.840.1.113883.6.96", "Pitavastatin (substance)", "Pitavastatin",
			"Pitavastatin", "pitavastatine", "Pitavastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pituitary luteinizing hormone</div>
	 * <div class="de">Lutropin (hLH)</div>
	 * <div class="fr">lutropine (hLH)</div>
	 * <div class="it">Ormone luteinizzante umano, hLH</div>
	 * <!-- @formatter:on -->
	 */
	PITUITARY_LUTEINIZING_HORMONE("64182005", "2.16.840.1.113883.6.96",
			"Pituitary luteinizing hormone (substance)", "Pituitary luteinizing hormone",
			"Lutropin (hLH)", "lutropine (hLH)", "Ormone luteinizzante umano, hLH"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Plerixafor</div>
	 * <div class="de">Plerixafor</div>
	 * <div class="fr">plérixafor</div>
	 * <div class="it">Plerixafor</div>
	 * <!-- @formatter:on -->
	 */
	PLERIXAFOR("442264009", "2.16.840.1.113883.6.96", "Plerixafor (substance)", "Plerixafor",
			"Plerixafor", "plérixafor", "Plerixafor"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pneumococcal polysaccharide vaccine</div>
	 * <div class="de">Pneumokokken-Polysaccharid-Impfstoff</div>
	 * <div class="fr">pneumocoque vaccin polysaccharidique</div>
	 * <div class="it">Pneumococchi vaccino polisaccaridico</div>
	 * <!-- @formatter:on -->
	 */
	PNEUMOCOCCAL_POLYSACCHARIDE_VACCINE("417011002", "2.16.840.1.113883.6.96",
			"Pneumococcal polysaccharide vaccine (substance)",
			"Pneumococcal polysaccharide vaccine", "Pneumokokken-Polysaccharid-Impfstoff",
			"pneumocoque vaccin polysaccharidique", "Pneumococchi vaccino polisaccaridico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Polihexanide</div>
	 * <div class="de">Polihexanid</div>
	 * <div class="fr">polyhexanide</div>
	 * <div class="it">Poliesanide</div>
	 * <!-- @formatter:on -->
	 */
	POLIHEXANIDE("421952002", "2.16.840.1.113883.6.96", "Polihexanide (substance)", "Polihexanide",
			"Polihexanid", "polyhexanide", "Poliesanide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Polymyxin</div>
	 * <div class="de">Polymyxin B</div>
	 * <div class="fr">polymyxine B</div>
	 * <div class="it">Polimixina B</div>
	 * <!-- @formatter:on -->
	 */
	POLYMYXIN("373224006", "2.16.840.1.113883.6.96", "Polymyxin (substance)", "Polymyxin",
			"Polymyxin B", "polymyxine B", "Polimixina B"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Posaconazole</div>
	 * <div class="de">Posaconazol</div>
	 * <div class="fr">posaconazole</div>
	 * <div class="it">Posaconazolo</div>
	 * <!-- @formatter:on -->
	 */
	POSACONAZOLE("421747003", "2.16.840.1.113883.6.96", "Posaconazole (substance)", "Posaconazole",
			"Posaconazol", "posaconazole", "Posaconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Potassium acetate</div>
	 * <div class="de">Kaliumacetat</div>
	 * <div class="fr">potassium acétate</div>
	 * <div class="it">Potassio acetato</div>
	 * <!-- @formatter:on -->
	 */
	POTASSIUM_ACETATE("52394008", "2.16.840.1.113883.6.96", "Potassium acetate (substance)",
			"Potassium acetate", "Kaliumacetat", "potassium acétate", "Potassio acetato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Potassium chloride</div>
	 * <div class="de">Kaliumchlorid</div>
	 * <div class="fr">potassium chlorure</div>
	 * <div class="it">Potassio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	POTASSIUM_CHLORIDE("8631001", "2.16.840.1.113883.6.96", "Potassium chloride (substance)",
			"Potassium chloride", "Kaliumchlorid", "potassium chlorure", "Potassio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Potassium citrate</div>
	 * <div class="de">Kalium citrat (E332)</div>
	 * <div class="fr">potassium citrate (E332)</div>
	 * <div class="it">Potassio citrato (E332)</div>
	 * <!-- @formatter:on -->
	 */
	POTASSIUM_CITRATE("387450001", "2.16.840.1.113883.6.96", "Potassium citrate (substance)",
			"Potassium citrate", "Kalium citrat (E332)", "potassium citrate (E332)",
			"Potassio citrato (E332)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Potassium gluconate</div>
	 * <div class="de">Kalium D-gluconat wasserfrei</div>
	 * <div class="fr">potassium D-gluconate anhydre</div>
	 * <div class="it">Potassio gluconato</div>
	 * <!-- @formatter:on -->
	 */
	POTASSIUM_GLUCONATE("89219006", "2.16.840.1.113883.6.96", "Potassium gluconate (substance)",
			"Potassium gluconate", "Kalium D-gluconat wasserfrei", "potassium D-gluconate anhydre",
			"Potassio gluconato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Potassium permanganate</div>
	 * <div class="de">Kaliumpermanganat</div>
	 * <div class="fr">permanganate de potassium</div>
	 * <div class="it">Potassio permanganato</div>
	 * <!-- @formatter:on -->
	 */
	POTASSIUM_PERMANGANATE("4681002", "2.16.840.1.113883.6.96",
			"Potassium permanganate (substance)", "Potassium permanganate", "Kaliumpermanganat",
			"permanganate de potassium", "Potassio permanganato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Potassium phosphate</div>
	 * <div class="de">Kalium phosphat</div>
	 * <div class="fr">potassium phosphate</div>
	 * <div class="it">Potassio fosfato</div>
	 * <!-- @formatter:on -->
	 */
	POTASSIUM_PHOSPHATE("80916004", "2.16.840.1.113883.6.96", "Potassium phosphate (substance)",
			"Potassium phosphate", "Kalium phosphat", "potassium phosphate", "Potassio fosfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Povidone iodine</div>
	 * <div class="de">Povidon iod</div>
	 * <div class="fr">povidone iodée</div>
	 * <div class="it">Iodopovidone</div>
	 * <!-- @formatter:on -->
	 */
	POVIDONE_IODINE("386989006", "2.16.840.1.113883.6.96", "Povidone iodine (substance)",
			"Povidone iodine", "Povidon iod", "povidone iodée", "Iodopovidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pramipexole</div>
	 * <div class="de">Pramipexol</div>
	 * <div class="fr">pramipexole</div>
	 * <div class="it">Pramipexolo</div>
	 * <!-- @formatter:on -->
	 */
	PRAMIPEXOLE("386852009", "2.16.840.1.113883.6.96", "Pramipexole (substance)", "Pramipexole",
			"Pramipexol", "pramipexole", "Pramipexolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prasugrel</div>
	 * <div class="de">Prasugrel</div>
	 * <div class="fr">prasugrel</div>
	 * <div class="it">Prasugrel</div>
	 * <!-- @formatter:on -->
	 */
	PRASUGREL("443129001", "2.16.840.1.113883.6.96", "Prasugrel (substance)", "Prasugrel",
			"Prasugrel", "prasugrel", "Prasugrel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pravastatin</div>
	 * <div class="de">Pravastatin</div>
	 * <div class="fr">pravastatine</div>
	 * <div class="it">Pravastatina</div>
	 * <!-- @formatter:on -->
	 */
	PRAVASTATIN("373566006", "2.16.840.1.113883.6.96", "Pravastatin (substance)", "Pravastatin",
			"Pravastatin", "pravastatine", "Pravastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prazepam</div>
	 * <div class="de">Prazepam</div>
	 * <div class="fr">prazépam</div>
	 * <div class="it">Prazepam</div>
	 * <!-- @formatter:on -->
	 */
	PRAZEPAM("387417001", "2.16.840.1.113883.6.96", "Prazepam (substance)", "Prazepam", "Prazepam",
			"prazépam", "Prazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Praziquantel</div>
	 * <div class="de">Praziquantel</div>
	 * <div class="fr">praziquantel</div>
	 * <div class="it">Praziquantel</div>
	 * <!-- @formatter:on -->
	 */
	PRAZIQUANTEL("387310003", "2.16.840.1.113883.6.96", "Praziquantel (substance)", "Praziquantel",
			"Praziquantel", "praziquantel", "Praziquantel"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prednicarbate</div>
	 * <div class="de">Prednicarbat</div>
	 * <div class="fr">prednicarbate</div>
	 * <div class="it">Prednicarbato</div>
	 * <!-- @formatter:on -->
	 */
	PREDNICARBATE("126086006", "2.16.840.1.113883.6.96", "Prednicarbate (substance)",
			"Prednicarbate", "Prednicarbat", "prednicarbate", "Prednicarbato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prednisolone</div>
	 * <div class="de">Prednisolon</div>
	 * <div class="fr">prednisolone</div>
	 * <div class="it">Prednisolone</div>
	 * <!-- @formatter:on -->
	 */
	PREDNISOLONE("116601002", "2.16.840.1.113883.6.96", "Prednisolone (substance)", "Prednisolone",
			"Prednisolon", "prednisolone", "Prednisolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prednisone</div>
	 * <div class="de">Prednison</div>
	 * <div class="fr">prednisone</div>
	 * <div class="it">Prednisone</div>
	 * <!-- @formatter:on -->
	 */
	PREDNISONE("116602009", "2.16.840.1.113883.6.96", "Prednisone (substance)", "Prednisone",
			"Prednison", "prednisone", "Prednisone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pregabalin</div>
	 * <div class="de">Pregabalin</div>
	 * <div class="fr">prégabaline</div>
	 * <div class="it">Pregabalin</div>
	 * <!-- @formatter:on -->
	 */
	PREGABALIN("415160008", "2.16.840.1.113883.6.96", "Pregabalin (substance)", "Pregabalin",
			"Pregabalin", "prégabaline", "Pregabalin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prilocaine</div>
	 * <div class="de">Prilocain</div>
	 * <div class="fr">prilocaïne</div>
	 * <div class="it">Prilocaina</div>
	 * <!-- @formatter:on -->
	 */
	PRILOCAINE("387107003", "2.16.840.1.113883.6.96", "Prilocaine (substance)", "Prilocaine",
			"Prilocain", "prilocaïne", "Prilocaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Primaquine</div>
	 * <div class="de">Primaquin</div>
	 * <div class="fr">primaquine</div>
	 * <div class="it">Primachina</div>
	 * <!-- @formatter:on -->
	 */
	PRIMAQUINE("429663004", "2.16.840.1.113883.6.96", "Primaquine (substance)", "Primaquine",
			"Primaquin", "primaquine", "Primachina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Primidone</div>
	 * <div class="de">Primidon</div>
	 * <div class="fr">primidone</div>
	 * <div class="it">Primidone</div>
	 * <!-- @formatter:on -->
	 */
	PRIMIDONE("387256009", "2.16.840.1.113883.6.96", "Primidone (substance)", "Primidone",
			"Primidon", "primidone", "Primidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Probenecid</div>
	 * <div class="de">Probenecid</div>
	 * <div class="fr">probénécide</div>
	 * <div class="it">Probenecid</div>
	 * <!-- @formatter:on -->
	 */
	PROBENECID("387365004", "2.16.840.1.113883.6.96", "Probenecid (substance)", "Probenecid",
			"Probenecid", "probénécide", "Probenecid"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Procainamide</div>
	 * <div class="de">Procainamid</div>
	 * <div class="fr">procaïnamide</div>
	 * <div class="it">Procainamide</div>
	 * <!-- @formatter:on -->
	 */
	PROCAINAMIDE("372589008", "2.16.840.1.113883.6.96", "Procainamide (substance)", "Procainamide",
			"Procainamid", "procaïnamide", "Procainamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Procaine</div>
	 * <div class="de">Procain</div>
	 * <div class="fr">procaïne</div>
	 * <div class="it">Procaina</div>
	 * <!-- @formatter:on -->
	 */
	PROCAINE("387238009", "2.16.840.1.113883.6.96", "Procaine (substance)", "Procaine", "Procain",
			"procaïne", "Procaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Procyclidine</div>
	 * <div class="de">Procyclidin</div>
	 * <div class="fr">procyclidine</div>
	 * <div class="it">Prociclidina</div>
	 * <!-- @formatter:on -->
	 */
	PROCYCLIDINE("387247001", "2.16.840.1.113883.6.96", "Procyclidine (substance)", "Procyclidine",
			"Procyclidin", "procyclidine", "Prociclidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Progesterone</div>
	 * <div class="de">Progesteron</div>
	 * <div class="fr">progestérone</div>
	 * <div class="it">Progesterone</div>
	 * <!-- @formatter:on -->
	 */
	PROGESTERONE("16683002", "2.16.840.1.113883.6.96", "Progesterone (substance)", "Progesterone",
			"Progesteron", "progestérone", "Progesterone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Proguanil</div>
	 * <div class="de">Proguanil</div>
	 * <div class="fr">proguanil</div>
	 * <div class="it">Proguanil</div>
	 * <!-- @formatter:on -->
	 */
	PROGUANIL("387094004", "2.16.840.1.113883.6.96", "Proguanil (substance)", "Proguanil",
			"Proguanil", "proguanil", "Proguanil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Proline</div>
	 * <div class="de">Prolin</div>
	 * <div class="fr">proline</div>
	 * <div class="it">Prolina</div>
	 * <!-- @formatter:on -->
	 */
	PROLINE("52541003", "2.16.840.1.113883.6.96", "Proline (substance)", "Proline", "Prolin",
			"proline", "Prolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Promazine hydrochloride</div>
	 * <div class="de">Promazin hydrochlorid</div>
	 * <div class="fr">promazine chlorhydrate</div>
	 * <div class="it">Promazina</div>
	 * <!-- @formatter:on -->
	 */
	PROMAZINE_HYDROCHLORIDE("79135001", "2.16.840.1.113883.6.96",
			"Promazine hydrochloride (substance)", "Promazine hydrochloride",
			"Promazin hydrochlorid", "promazine chlorhydrate", "Promazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Promethazine</div>
	 * <div class="de">Promethazin</div>
	 * <div class="fr">prométhazine</div>
	 * <div class="it">Prometazina</div>
	 * <!-- @formatter:on -->
	 */
	PROMETHAZINE("372871004", "2.16.840.1.113883.6.96", "Promethazine (substance)", "Promethazine",
			"Promethazin", "prométhazine", "Prometazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Propafenone</div>
	 * <div class="de">Propafenon</div>
	 * <div class="fr">propafénone</div>
	 * <div class="it">Propafenone</div>
	 * <!-- @formatter:on -->
	 */
	PROPAFENONE("372910007", "2.16.840.1.113883.6.96", "Propafenone (substance)", "Propafenone",
			"Propafenon", "propafénone", "Propafenone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Propofol</div>
	 * <div class="de">Propofol</div>
	 * <div class="fr">propofol</div>
	 * <div class="it">Propofol</div>
	 * <!-- @formatter:on -->
	 */
	PROPOFOL("387423006", "2.16.840.1.113883.6.96", "Propofol (substance)", "Propofol", "Propofol",
			"propofol", "Propofol"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Propranolol</div>
	 * <div class="de">Propranolol</div>
	 * <div class="fr">propranolol</div>
	 * <div class="it">Propranololo</div>
	 * <!-- @formatter:on -->
	 */
	PROPRANOLOL("372772003", "2.16.840.1.113883.6.96", "Propranolol (substance)", "Propranolol",
			"Propranolol", "propranolol", "Propranololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Propyphenazone</div>
	 * <div class="de">Propyphenazon</div>
	 * <div class="fr">propyphénazone</div>
	 * <div class="it">Propifenazone</div>
	 * <!-- @formatter:on -->
	 */
	PROPYPHENAZONE("699188007", "2.16.840.1.113883.6.96", "Propyphenazone (substance)",
			"Propyphenazone", "Propyphenazon", "propyphénazone", "Propifenazone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Protamine</div>
	 * <div class="de">Protamin</div>
	 * <div class="fr">protamine</div>
	 * <div class="it">Protamina</div>
	 * <!-- @formatter:on -->
	 */
	PROTAMINE("372630008", "2.16.840.1.113883.6.96", "Protamine (substance)", "Protamine",
			"Protamin", "protamine", "Protamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Protein C</div>
	 * <div class="de">Protein C (human)</div>
	 * <div class="fr">protéine C humaine</div>
	 * <div class="it">Proteina C (umana)</div>
	 * <!-- @formatter:on -->
	 */
	PROTEIN_C("25525005", "2.16.840.1.113883.6.96", "Protein C (substance)", "Protein C",
			"Protein C (human)", "protéine C humaine", "Proteina C (umana)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Protein S</div>
	 * <div class="de">Protein S (human)</div>
	 * <div class="fr">protéine S humaine</div>
	 * <div class="it">Proteina S (umana)</div>
	 * <!-- @formatter:on -->
	 */
	PROTEIN_S("56898001", "2.16.840.1.113883.6.96", "Protein S (substance)", "Protein S",
			"Protein S (human)", "protéine S humaine", "Proteina S (umana)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Protionamide</div>
	 * <div class="de">Protionamid</div>
	 * <div class="fr">protionamide</div>
	 * <div class="it">Protionamide</div>
	 * <!-- @formatter:on -->
	 */
	PROTIONAMIDE("703589003", "2.16.840.1.113883.6.96", "Protionamide (substance)", "Protionamide",
			"Protionamid", "protionamide", "Protionamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Protireline</div>
	 * <div class="de">Protirelin</div>
	 * <div class="fr">protiréline</div>
	 * <div class="it">Protirelina</div>
	 * <!-- @formatter:on -->
	 */
	PROTIRELINE("412495007", "2.16.840.1.113883.6.96", "Protireline (substance)", "Protireline",
			"Protirelin", "protiréline", "Protirelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Prucalopride</div>
	 * <div class="de">Prucaloprid</div>
	 * <div class="fr">prucalopride</div>
	 * <div class="it">Prucalopride</div>
	 * <!-- @formatter:on -->
	 */
	PRUCALOPRIDE("699273008", "2.16.840.1.113883.6.96", "Prucalopride (substance)", "Prucalopride",
			"Prucaloprid", "prucalopride", "Prucalopride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pseudoephedrine</div>
	 * <div class="de">Pseudoephedrin</div>
	 * <div class="fr">pseudoéphédrine</div>
	 * <div class="it">Pseudoefedrina</div>
	 * <!-- @formatter:on -->
	 */
	PSEUDOEPHEDRINE("372900003", "2.16.840.1.113883.6.96", "Pseudoephedrine (substance)",
			"Pseudoephedrine", "Pseudoephedrin", "pseudoéphédrine", "Pseudoefedrina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pyrazinamide</div>
	 * <div class="de">Pyrazinamid</div>
	 * <div class="fr">pyrazinamide</div>
	 * <div class="it">Pirazinamide</div>
	 * <!-- @formatter:on -->
	 */
	PYRAZINAMIDE("387076005", "2.16.840.1.113883.6.96", "Pyrazinamide (substance)", "Pyrazinamide",
			"Pyrazinamid", "pyrazinamide", "Pirazinamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pyridostigmine bromide</div>
	 * <div class="de">Pyridostigmin bromid</div>
	 * <div class="fr">pyridostigmine bromure</div>
	 * <div class="it">Piridostigmina bromuro</div>
	 * <!-- @formatter:on -->
	 */
	PYRIDOSTIGMINE_BROMIDE("34915005", "2.16.840.1.113883.6.96",
			"Pyridostigmine bromide (substance)", "Pyridostigmine bromide", "Pyridostigmin bromid",
			"pyridostigmine bromure", "Piridostigmina bromuro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pyridoxal phosphate</div>
	 * <div class="de">Pyridoxal-5-phosphat</div>
	 * <div class="fr">pyridoxal-5-phosphate</div>
	 * <div class="it">Piridossal fosfato</div>
	 * <!-- @formatter:on -->
	 */
	PYRIDOXAL_PHOSPHATE("259663004", "2.16.840.1.113883.6.96", "Pyridoxal phosphate (substance)",
			"Pyridoxal phosphate", "Pyridoxal-5-phosphat", "pyridoxal-5-phosphate",
			"Piridossal fosfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pyridoxine</div>
	 * <div class="de">Pyridoxin (Vitamin B6)</div>
	 * <div class="fr">pyridoxine (Vitamine B6)</div>
	 * <div class="it">Piridossina (vitamina B6)</div>
	 * <!-- @formatter:on -->
	 */
	PYRIDOXINE("430469009", "2.16.840.1.113883.6.96", "Pyridoxine (substance)", "Pyridoxine",
			"Pyridoxin (Vitamin B6)", "pyridoxine (Vitamine B6)", "Piridossina (vitamina B6)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Pyrimethamine</div>
	 * <div class="de">Pyrimethamin</div>
	 * <div class="fr">pyriméthamine</div>
	 * <div class="it">Pirimetamina</div>
	 * <!-- @formatter:on -->
	 */
	PYRIMETHAMINE("373769001", "2.16.840.1.113883.6.96", "Pyrimethamine (substance)",
			"Pyrimethamine", "Pyrimethamin", "pyriméthamine", "Pirimetamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Quetiapine</div>
	 * <div class="de">Quetiapin</div>
	 * <div class="fr">quétiapine</div>
	 * <div class="it">Quetiapina</div>
	 * <!-- @formatter:on -->
	 */
	QUETIAPINE("386850001", "2.16.840.1.113883.6.96", "Quetiapine (substance)", "Quetiapine",
			"Quetiapin", "quétiapine", "Quetiapina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Quinapril</div>
	 * <div class="de">Quinapril</div>
	 * <div class="fr">quinapril</div>
	 * <div class="it">Quinapril</div>
	 * <!-- @formatter:on -->
	 */
	QUINAPRIL("386874003", "2.16.840.1.113883.6.96", "Quinapril (substance)", "Quinapril",
			"Quinapril", "quinapril", "Quinapril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Quinine</div>
	 * <div class="de">Chinin</div>
	 * <div class="fr">quinine</div>
	 * <div class="it">Chinina</div>
	 * <!-- @formatter:on -->
	 */
	QUININE("373497008", "2.16.840.1.113883.6.96", "Quinine (substance)", "Quinine", "Chinin",
			"quinine", "Chinina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rabeprazole</div>
	 * <div class="de">Rabeprazol</div>
	 * <div class="fr">rabéprazole</div>
	 * <div class="it">Rabeprazolo</div>
	 * <!-- @formatter:on -->
	 */
	RABEPRAZOLE("422225001", "2.16.840.1.113883.6.96", "Rabeprazole (substance)", "Rabeprazole",
			"Rabeprazol", "rabéprazole", "Rabeprazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rabies human immune globulin</div>
	 * <div class="de">Tollwut-Immunglobulin vom Menschen</div>
	 * <div class="fr">immunoglobuline humaine anti-rabique</div>
	 * <div class="it">Immunoglobulina umana antirabbica</div>
	 * <!-- @formatter:on -->
	 */
	RABIES_HUMAN_IMMUNE_GLOBULIN("422303009", "2.16.840.1.113883.6.96",
			"Rabies human immune globulin (substance)", "Rabies human immune globulin",
			"Tollwut-Immunglobulin vom Menschen", "immunoglobuline humaine anti-rabique",
			"Immunoglobulina umana antirabbica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Raloxifene</div>
	 * <div class="de">Raloxifen</div>
	 * <div class="fr">raloxifène</div>
	 * <div class="it">Raloxifene</div>
	 * <!-- @formatter:on -->
	 */
	RALOXIFENE("109029006", "2.16.840.1.113883.6.96", "Raloxifene (substance)", "Raloxifene",
			"Raloxifen", "raloxifène", "Raloxifene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Raltegravir</div>
	 * <div class="de">Raltegravir</div>
	 * <div class="fr">raltégravir</div>
	 * <div class="it">Raltegravir</div>
	 * <!-- @formatter:on -->
	 */
	RALTEGRAVIR("429707008", "2.16.840.1.113883.6.96", "Raltegravir (substance)", "Raltegravir",
			"Raltegravir", "raltégravir", "Raltegravir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Raltitrexed</div>
	 * <div class="de">Raltitrexed</div>
	 * <div class="fr">raltitrexed</div>
	 * <div class="it">Raltitrexed</div>
	 * <!-- @formatter:on -->
	 */
	RALTITREXED("395857008", "2.16.840.1.113883.6.96", "Raltitrexed (substance)", "Raltitrexed",
			"Raltitrexed", "raltitrexed", "Raltitrexed"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ramipril</div>
	 * <div class="de">Ramipril</div>
	 * <div class="fr">ramipril</div>
	 * <div class="it">Ramipril</div>
	 * <!-- @formatter:on -->
	 */
	RAMIPRIL("386872004", "2.16.840.1.113883.6.96", "Ramipril (substance)", "Ramipril", "Ramipril",
			"ramipril", "Ramipril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ramucirumab</div>
	 * <div class="de">Ramucirumab</div>
	 * <div class="fr">ramucirumab</div>
	 * <div class="it">Ramucirumab</div>
	 * <!-- @formatter:on -->
	 */
	RAMUCIRUMAB("704259004", "2.16.840.1.113883.6.96", "Ramucirumab (substance)", "Ramucirumab",
			"Ramucirumab", "ramucirumab", "Ramucirumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ranitidine</div>
	 * <div class="de">Ranitidin</div>
	 * <div class="fr">ranitidine</div>
	 * <div class="it">Ranitidina</div>
	 * <!-- @formatter:on -->
	 */
	RANITIDINE("372755005", "2.16.840.1.113883.6.96", "Ranitidine (substance)", "Ranitidine",
			"Ranitidin", "ranitidine", "Ranitidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ranolazine</div>
	 * <div class="de">Ranolazin</div>
	 * <div class="fr">ranolazine</div>
	 * <div class="it">Ranolazina</div>
	 * <!-- @formatter:on -->
	 */
	RANOLAZINE("420365007", "2.16.840.1.113883.6.96", "Ranolazine (substance)", "Ranolazine",
			"Ranolazin", "ranolazine", "Ranolazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rasagiline</div>
	 * <div class="de">Rasagilin</div>
	 * <div class="fr">rasagiline</div>
	 * <div class="it">Rasagilina</div>
	 * <!-- @formatter:on -->
	 */
	RASAGILINE("418734001", "2.16.840.1.113883.6.96", "Rasagiline (substance)", "Rasagiline",
			"Rasagilin", "rasagiline", "Rasagilina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rasburicase</div>
	 * <div class="de">Rasburicase</div>
	 * <div class="fr">rasburicase</div>
	 * <div class="it">Rasburicase</div>
	 * <!-- @formatter:on -->
	 */
	RASBURICASE("395858003", "2.16.840.1.113883.6.96", "Rasburicase (substance)", "Rasburicase",
			"Rasburicase", "rasburicase", "Rasburicase"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Reboxetine</div>
	 * <div class="de">Reboxetin</div>
	 * <div class="fr">réboxétine</div>
	 * <div class="it">Reboxetina</div>
	 * <!-- @formatter:on -->
	 */
	REBOXETINE("395859006", "2.16.840.1.113883.6.96", "Reboxetine (substance)", "Reboxetine",
			"Reboxetin", "réboxétine", "Reboxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Remifentanil</div>
	 * <div class="de">Remifentanil</div>
	 * <div class="fr">rémifentanil</div>
	 * <div class="it">Remifentanil</div>
	 * <!-- @formatter:on -->
	 */
	REMIFENTANIL("386839004", "2.16.840.1.113883.6.96", "Remifentanil (substance)", "Remifentanil",
			"Remifentanil", "rémifentanil", "Remifentanil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Repaglinide</div>
	 * <div class="de">Repaglinid</div>
	 * <div class="fr">répaglinide</div>
	 * <div class="it">Repaglinide</div>
	 * <!-- @formatter:on -->
	 */
	REPAGLINIDE("386964000", "2.16.840.1.113883.6.96", "Repaglinide (substance)", "Repaglinide",
			"Repaglinid", "répaglinide", "Repaglinide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Retigabine</div>
	 * <div class="de">Retigabin</div>
	 * <div class="fr">rétigabine</div>
	 * <div class="it">Retigabina</div>
	 * <!-- @formatter:on -->
	 */
	RETIGABINE("699271005", "2.16.840.1.113883.6.96", "Retigabine (substance)", "Retigabine",
			"Retigabin", "rétigabine", "Retigabina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Retinol</div>
	 * <div class="de">Retinol (Vitamin A)</div>
	 * <div class="fr">rétinol (Vitamine a)</div>
	 * <div class="it">Retinolo (vitamina A)</div>
	 * <!-- @formatter:on -->
	 */
	RETINOL("82622003", "2.16.840.1.113883.6.96", "Retinol (substance)", "Retinol",
			"Retinol (Vitamin A)", "rétinol (Vitamine a)", "Retinolo (vitamina A)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ribavirin</div>
	 * <div class="de">Ribavirin</div>
	 * <div class="fr">ribavirine</div>
	 * <div class="it">Ribavirina</div>
	 * <!-- @formatter:on -->
	 */
	RIBAVIRIN("387188005", "2.16.840.1.113883.6.96", "Ribavirin (substance)", "Ribavirin",
			"Ribavirin", "ribavirine", "Ribavirina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Riboflavin</div>
	 * <div class="de">Riboflavin (Vitamin B2, E101)</div>
	 * <div class="fr">riboflavine (Vitamine B2, E101)</div>
	 * <div class="it">Riboflavina (vitamina B2, E 101)</div>
	 * <!-- @formatter:on -->
	 */
	RIBOFLAVIN("13235001", "2.16.840.1.113883.6.96", "Riboflavin (substance)", "Riboflavin",
			"Riboflavin (Vitamin B2, E101)", "riboflavine (Vitamine B2, E101)",
			"Riboflavina (vitamina B2, E 101)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rifabutin</div>
	 * <div class="de">Rifabutin</div>
	 * <div class="fr">rifabutine</div>
	 * <div class="it">Rifabutina</div>
	 * <!-- @formatter:on -->
	 */
	RIFABUTIN("386893001", "2.16.840.1.113883.6.96", "Rifabutin (substance)", "Rifabutin",
			"Rifabutin", "rifabutine", "Rifabutina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rifampicin</div>
	 * <div class="de">Rifampicin</div>
	 * <div class="fr">rifampicine</div>
	 * <div class="it">Rifampicina</div>
	 * <!-- @formatter:on -->
	 */
	RIFAMPICIN("387159009", "2.16.840.1.113883.6.96", "Rifampicin (substance)", "Rifampicin",
			"Rifampicin", "rifampicine", "Rifampicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rifaximin</div>
	 * <div class="de">Rifaximin</div>
	 * <div class="fr">rifaximine</div>
	 * <div class="it">Rifaximina</div>
	 * <!-- @formatter:on -->
	 */
	RIFAXIMIN("412553001", "2.16.840.1.113883.6.96", "Rifaximin (substance)", "Rifaximin",
			"Rifaximin", "rifaximine", "Rifaximina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rilpivirine</div>
	 * <div class="de">Rilpivirin</div>
	 * <div class="fr">rilpivirine</div>
	 * <div class="it">Rilpivirina</div>
	 * <!-- @formatter:on -->
	 */
	RILPIVIRINE("703123005", "2.16.840.1.113883.6.96", "Rilpivirine (substance)", "Rilpivirine",
			"Rilpivirin", "rilpivirine", "Rilpivirina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Riluzole</div>
	 * <div class="de">Riluzol</div>
	 * <div class="fr">riluzole</div>
	 * <div class="it">Riluzolo</div>
	 * <!-- @formatter:on -->
	 */
	RILUZOLE("386980005", "2.16.840.1.113883.6.96", "Riluzole (substance)", "Riluzole", "Riluzol",
			"riluzole", "Riluzolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rimexolone</div>
	 * <div class="de">Rimexolon</div>
	 * <div class="fr">rimexolone</div>
	 * <div class="it">Rimexolone</div>
	 * <!-- @formatter:on -->
	 */
	RIMEXOLONE("387046003", "2.16.840.1.113883.6.96", "Rimexolone (substance)", "Rimexolone",
			"Rimexolon", "rimexolone", "Rimexolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Riociguat</div>
	 * <div class="de">Riociguat</div>
	 * <div class="fr">riociguat</div>
	 * <div class="it">Riociguat</div>
	 * <!-- @formatter:on -->
	 */
	RIOCIGUAT("713333001", "2.16.840.1.113883.6.96", "Riociguat (substance)", "Riociguat",
			"Riociguat", "riociguat", "Riociguat"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Risedronate sodium</div>
	 * <div class="de">Risedronat natrium</div>
	 * <div class="fr">risédronate sodique</div>
	 * <div class="it">Sodio risedronato</div>
	 * <!-- @formatter:on -->
	 */
	RISEDRONATE_SODIUM("387064005", "2.16.840.1.113883.6.96", "Risedronate sodium (substance)",
			"Risedronate sodium", "Risedronat natrium", "risédronate sodique",
			"Sodio risedronato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Risedronic acid</div>
	 * <div class="de">Risedronsäure</div>
	 * <div class="fr">acide risédronique</div>
	 * <div class="it">Acido risedronico</div>
	 * <!-- @formatter:on -->
	 */
	RISEDRONIC_ACID("768539002", "2.16.840.1.113883.6.96", "Risedronic acid (substance)",
			"Risedronic acid", "Risedronsäure", "acide risédronique", "Acido risedronico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Risperidone</div>
	 * <div class="de">Risperidon</div>
	 * <div class="fr">rispéridone</div>
	 * <div class="it">Risperidone</div>
	 * <!-- @formatter:on -->
	 */
	RISPERIDONE("386840002", "2.16.840.1.113883.6.96", "Risperidone (substance)", "Risperidone",
			"Risperidon", "rispéridone", "Risperidone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ritonavir</div>
	 * <div class="de">Ritonavir</div>
	 * <div class="fr">ritonavir</div>
	 * <div class="it">Ritonavir</div>
	 * <!-- @formatter:on -->
	 */
	RITONAVIR("386896009", "2.16.840.1.113883.6.96", "Ritonavir (substance)", "Ritonavir",
			"Ritonavir", "ritonavir", "Ritonavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rituximab</div>
	 * <div class="de">Rituximab</div>
	 * <div class="fr">rituximab</div>
	 * <div class="it">Rituximab</div>
	 * <!-- @formatter:on -->
	 */
	RITUXIMAB("386919002", "2.16.840.1.113883.6.96", "Rituximab (substance)", "Rituximab",
			"Rituximab", "rituximab", "Rituximab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rivaroxaban</div>
	 * <div class="de">Rivaroxaban</div>
	 * <div class="fr">rivaroxaban</div>
	 * <div class="it">Rivaroxaban</div>
	 * <!-- @formatter:on -->
	 */
	RIVAROXABAN("442031002", "2.16.840.1.113883.6.96", "Rivaroxaban (substance)", "Rivaroxaban",
			"Rivaroxaban", "rivaroxaban", "Rivaroxaban"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rivastigmine</div>
	 * <div class="de">Rivastigmin</div>
	 * <div class="fr">rivastigmine</div>
	 * <div class="it">Rivastigmina</div>
	 * <!-- @formatter:on -->
	 */
	RIVASTIGMINE("395868008", "2.16.840.1.113883.6.96", "Rivastigmine (substance)", "Rivastigmine",
			"Rivastigmin", "rivastigmine", "Rivastigmina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rizatriptan</div>
	 * <div class="de">Rizatriptan</div>
	 * <div class="fr">rizatriptan</div>
	 * <div class="it">Rizatriptan</div>
	 * <!-- @formatter:on -->
	 */
	RIZATRIPTAN("363573000", "2.16.840.1.113883.6.96", "Rizatriptan (substance)", "Rizatriptan",
			"Rizatriptan", "rizatriptan", "Rizatriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rocuronium bromide</div>
	 * <div class="de">Rocuronium bromid</div>
	 * <div class="fr">rocuronium bromure</div>
	 * <div class="it">Rocuronio bromuro</div>
	 * <!-- @formatter:on -->
	 */
	ROCURONIUM_BROMIDE("108450002", "2.16.840.1.113883.6.96", "Rocuronium bromide (substance)",
			"Rocuronium bromide", "Rocuronium bromid", "rocuronium bromure", "Rocuronio bromuro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Roflumilast</div>
	 * <div class="de">Roflumilast</div>
	 * <div class="fr">roflumilast</div>
	 * <div class="it">Roflumilast</div>
	 * <!-- @formatter:on -->
	 */
	ROFLUMILAST("448971002", "2.16.840.1.113883.6.96", "Roflumilast (substance)", "Roflumilast",
			"Roflumilast", "roflumilast", "Roflumilast"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Romiplostim</div>
	 * <div class="de">Romiplostim</div>
	 * <div class="fr">romiplostim</div>
	 * <div class="it">Romiplostim</div>
	 * <!-- @formatter:on -->
	 */
	ROMIPLOSTIM("439122000", "2.16.840.1.113883.6.96", "Romiplostim (substance)", "Romiplostim",
			"Romiplostim", "romiplostim", "Romiplostim"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ropinirole</div>
	 * <div class="de">Ropinirol</div>
	 * <div class="fr">ropinirole</div>
	 * <div class="it">Ropinirolo</div>
	 * <!-- @formatter:on -->
	 */
	ROPINIROLE("372499000", "2.16.840.1.113883.6.96", "Ropinirole (substance)", "Ropinirole",
			"Ropinirol", "ropinirole", "Ropinirolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ropivacaine</div>
	 * <div class="de">Ropivacain</div>
	 * <div class="fr">ropivacaïne</div>
	 * <div class="it">Ropivacaina</div>
	 * <!-- @formatter:on -->
	 */
	ROPIVACAINE("386969005", "2.16.840.1.113883.6.96", "Ropivacaine (substance)", "Ropivacaine",
			"Ropivacain", "ropivacaïne", "Ropivacaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rosuvastatin</div>
	 * <div class="de">Rosuvastatin</div>
	 * <div class="fr">rosuvastatine</div>
	 * <div class="it">Rosuvastatina</div>
	 * <!-- @formatter:on -->
	 */
	ROSUVASTATIN("700067006", "2.16.840.1.113883.6.96", "Rosuvastatin (substance)", "Rosuvastatin",
			"Rosuvastatin", "rosuvastatine", "Rosuvastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rotigotine</div>
	 * <div class="de">Rotigotin</div>
	 * <div class="fr">rotigotine</div>
	 * <div class="it">Rotigotina</div>
	 * <!-- @formatter:on -->
	 */
	ROTIGOTINE("421924006", "2.16.840.1.113883.6.96", "Rotigotine (substance)", "Rotigotine",
			"Rotigotin", "rotigotine", "Rotigotina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rubella vaccine</div>
	 * <div class="de">Röteln-Lebend-Impfstoff</div>
	 * <div class="fr">rubéole vaccin vivant</div>
	 * <div class="it">Rosolia vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	RUBELLA_VACCINE("396438003", "2.16.840.1.113883.6.96", "Rubella vaccine (substance)",
			"Rubella vaccine", "Röteln-Lebend-Impfstoff", "rubéole vaccin vivant",
			"Rosolia vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Rufinamide</div>
	 * <div class="de">Rufinamid</div>
	 * <div class="fr">rufinamide</div>
	 * <div class="it">Rufinamide</div>
	 * <!-- @formatter:on -->
	 */
	RUFINAMIDE("429835003", "2.16.840.1.113883.6.96", "Rufinamide (substance)", "Rufinamide",
			"Rufinamid", "rufinamide", "Rufinamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Saccharomyces boulardii</div>
	 * <div class="de">Saccharomyces boulardii</div>
	 * <div class="fr">saccharomyces boulardii</div>
	 * <div class="it">Saccharomyces boulardii</div>
	 * <!-- @formatter:on -->
	 */
	SACCHAROMYCES_BOULARDII("700441006", "2.16.840.1.113883.6.96",
			"Saccharomyces boulardii (substance)", "Saccharomyces boulardii",
			"Saccharomyces boulardii", "saccharomyces boulardii", "Saccharomyces boulardii"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sacubitril</div>
	 * <div class="de">Sacubitril</div>
	 * <div class="fr">sacubitril</div>
	 * <div class="it">Sacubitril</div>
	 * <!-- @formatter:on -->
	 */
	SACUBITRIL("716072000", "2.16.840.1.113883.6.96", "Sacubitril (substance)", "Sacubitril",
			"Sacubitril", "sacubitril", "Sacubitril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Safinamide</div>
	 * <div class="de">Safinamid</div>
	 * <div class="fr">safinamide</div>
	 * <div class="it">Safinamide</div>
	 * <!-- @formatter:on -->
	 */
	SAFINAMIDE("718852000", "2.16.840.1.113883.6.96", "Safinamide (substance)", "Safinamide",
			"Safinamid", "safinamide", "Safinamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Salbutamol</div>
	 * <div class="de">Salbutamol</div>
	 * <div class="fr">salbutamol</div>
	 * <div class="it">Salbutamolo</div>
	 * <!-- @formatter:on -->
	 */
	SALBUTAMOL("372897005", "2.16.840.1.113883.6.96", "Salbutamol (substance)", "Salbutamol",
			"Salbutamol", "salbutamol", "Salbutamolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Salicylamide</div>
	 * <div class="de">Salicylamid</div>
	 * <div class="fr">salicylamide</div>
	 * <div class="it">Salicilamide</div>
	 * <!-- @formatter:on -->
	 */
	SALICYLAMIDE("22192002", "2.16.840.1.113883.6.96", "Salicylamide (substance)", "Salicylamide",
			"Salicylamid", "salicylamide", "Salicilamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Salicylic acid</div>
	 * <div class="de">Salicylsäure</div>
	 * <div class="fr">acide salicylique</div>
	 * <div class="it">Acido salicilico</div>
	 * <!-- @formatter:on -->
	 */
	SALICYLIC_ACID("387253001", "2.16.840.1.113883.6.96", "Salicylic acid (substance)",
			"Salicylic acid", "Salicylsäure", "acide salicylique", "Acido salicilico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Salmeterol</div>
	 * <div class="de">Salmeterol</div>
	 * <div class="fr">salmétérol</div>
	 * <div class="it">Salmeterolo</div>
	 * <!-- @formatter:on -->
	 */
	SALMETEROL("372515005", "2.16.840.1.113883.6.96", "Salmeterol (substance)", "Salmeterol",
			"Salmeterol", "salmétérol", "Salmeterolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sapropterin</div>
	 * <div class="de">Sapropterin</div>
	 * <div class="fr">saproptérine</div>
	 * <div class="it">Sapropterina</div>
	 * <!-- @formatter:on -->
	 */
	SAPROPTERIN("432859002", "2.16.840.1.113883.6.96", "Sapropterin (substance)", "Sapropterin",
			"Sapropterin", "saproptérine", "Sapropterina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Saquinavir</div>
	 * <div class="de">Saquinavir</div>
	 * <div class="fr">saquinavir</div>
	 * <div class="it">Saquinavir</div>
	 * <!-- @formatter:on -->
	 */
	SAQUINAVIR("372530001", "2.16.840.1.113883.6.96", "Saquinavir (substance)", "Saquinavir",
			"Saquinavir", "saquinavir", "Saquinavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sarilumab</div>
	 * <div class="de">Sarilumab</div>
	 * <div class="fr">sarilumab</div>
	 * <div class="it">Sarilumab</div>
	 * <!-- @formatter:on -->
	 */
	SARILUMAB("735231009", "2.16.840.1.113883.6.96", "Sarilumab (substance)", "Sarilumab",
			"Sarilumab", "sarilumab", "Sarilumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Saxagliptin</div>
	 * <div class="de">Saxagliptin</div>
	 * <div class="fr">saxagliptine</div>
	 * <div class="it">Saxagliptin</div>
	 * <!-- @formatter:on -->
	 */
	SAXAGLIPTIN("443087004", "2.16.840.1.113883.6.96", "Saxagliptin (substance)", "Saxagliptin",
			"Saxagliptin", "saxagliptine", "Saxagliptin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Scopolamine</div>
	 * <div class="de">Scopolamin</div>
	 * <div class="fr">scopolamine</div>
	 * <div class="it">Scopolamina</div>
	 * <!-- @formatter:on -->
	 */
	SCOPOLAMINE("387409009", "2.16.840.1.113883.6.96", "Scopolamine (substance)", "Scopolamine",
			"Scopolamin", "scopolamine", "Scopolamina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Scopolamine butylbromide</div>
	 * <div class="de">Scopolamin butylbromid</div>
	 * <div class="fr">scopolamine butylbromure</div>
	 * <div class="it">Scopolamina butilbromuro</div>
	 * <!-- @formatter:on -->
	 */
	SCOPOLAMINE_BUTYLBROMIDE("395739004", "2.16.840.1.113883.6.96",
			"Scopolamine butylbromide (substance)", "Scopolamine butylbromide",
			"Scopolamin butylbromid", "scopolamine butylbromure", "Scopolamina butilbromuro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Secretin</div>
	 * <div class="de">Secretin</div>
	 * <div class="fr">sécrétine</div>
	 * <div class="it">Secretina</div>
	 * <!-- @formatter:on -->
	 */
	SECRETIN("19205004", "2.16.840.1.113883.6.96", "Secretin (substance)", "Secretin", "Secretin",
			"sécrétine", "Secretina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Secukinumab</div>
	 * <div class="de">Secukinumab</div>
	 * <div class="fr">sécukinumab</div>
	 * <div class="it">Secukinumab</div>
	 * <!-- @formatter:on -->
	 */
	SECUKINUMAB("708822004", "2.16.840.1.113883.6.96", "Secukinumab (substance)", "Secukinumab",
			"Secukinumab", "sécukinumab", "Secukinumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Serine</div>
	 * <div class="de">L-Serin</div>
	 * <div class="fr">l-sérine</div>
	 * <div class="it">Serina</div>
	 * <!-- @formatter:on -->
	 */
	SERINE("14125007", "2.16.840.1.113883.6.96", "Serine (substance)", "Serine", "L-Serin",
			"l-sérine", "Serina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sertraline</div>
	 * <div class="de">Sertralin</div>
	 * <div class="fr">sertraline</div>
	 * <div class="it">Sertralina</div>
	 * <!-- @formatter:on -->
	 */
	SERTRALINE("372594008", "2.16.840.1.113883.6.96", "Sertraline (substance)", "Sertraline",
			"Sertralin", "sertraline", "Sertralina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sevelamer</div>
	 * <div class="de">Sevelamer</div>
	 * <div class="fr">sévélamer</div>
	 * <div class="it">Sevelamer</div>
	 * <!-- @formatter:on -->
	 */
	SEVELAMER("395871000", "2.16.840.1.113883.6.96", "Sevelamer (substance)", "Sevelamer",
			"Sevelamer", "sévélamer", "Sevelamer"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sevoflurane</div>
	 * <div class="de">Sevofluran</div>
	 * <div class="fr">sévoflurane</div>
	 * <div class="it">Sevoflurano</div>
	 * <!-- @formatter:on -->
	 */
	SEVOFLURANE("386842005", "2.16.840.1.113883.6.96", "Sevoflurane (substance)", "Sevoflurane",
			"Sevofluran", "sévoflurane", "Sevoflurano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sildenafil</div>
	 * <div class="de">Sildenafil</div>
	 * <div class="fr">sildénafil</div>
	 * <div class="it">Sildenafil</div>
	 * <!-- @formatter:on -->
	 */
	SILDENAFIL("372572000", "2.16.840.1.113883.6.96", "Sildenafil (substance)", "Sildenafil",
			"Sildenafil", "sildénafil", "Sildenafil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Silibinin</div>
	 * <div class="de">Silibinin</div>
	 * <div class="fr">silibinine</div>
	 * <div class="it">Silibina</div>
	 * <!-- @formatter:on -->
	 */
	SILIBININ("720527007", "2.16.840.1.113883.6.96", "Silibinin (substance)", "Silibinin",
			"Silibinin", "silibinine", "Silibina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Silodosin</div>
	 * <div class="de">Silodosin</div>
	 * <div class="fr">silodosine</div>
	 * <div class="it">Silodosina</div>
	 * <!-- @formatter:on -->
	 */
	SILODOSIN("442042006", "2.16.840.1.113883.6.96", "Silodosin (substance)", "Silodosin",
			"Silodosin", "silodosine", "Silodosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Simeticone</div>
	 * <div class="de">Simeticon</div>
	 * <div class="fr">siméticone</div>
	 * <div class="it">Simeticone</div>
	 * <!-- @formatter:on -->
	 */
	SIMETICONE("387442005", "2.16.840.1.113883.6.96", "Simeticone (substance)", "Simeticone",
			"Simeticon", "siméticone", "Simeticone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Simoctocog alfa</div>
	 * <div class="de">Simoctocog alfa</div>
	 * <div class="fr">simoctocog alfa</div>
	 * <div class="it">Simoctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	SIMOCTOCOG_ALFA("718853005", "2.16.840.1.113883.6.96", "Simoctocog alfa (substance)",
			"Simoctocog alfa", "Simoctocog alfa", "simoctocog alfa", "Simoctocog alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Simvastatin</div>
	 * <div class="de">Simvastatin</div>
	 * <div class="fr">simvastatine</div>
	 * <div class="it">Simvastatina</div>
	 * <!-- @formatter:on -->
	 */
	SIMVASTATIN("387584000", "2.16.840.1.113883.6.96", "Simvastatin (substance)", "Simvastatin",
			"Simvastatin", "simvastatine", "Simvastatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sirolimus</div>
	 * <div class="de">Sirolimus</div>
	 * <div class="fr">sirolimus</div>
	 * <div class="it">Sirolimus</div>
	 * <!-- @formatter:on -->
	 */
	SIROLIMUS("387014003", "2.16.840.1.113883.6.96", "Sirolimus (substance)", "Sirolimus",
			"Sirolimus", "sirolimus", "Sirolimus"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sitagliptin</div>
	 * <div class="de">Sitagliptin</div>
	 * <div class="fr">sitagliptine</div>
	 * <div class="it">Sitagliptin</div>
	 * <!-- @formatter:on -->
	 */
	SITAGLIPTIN("423307000", "2.16.840.1.113883.6.96", "Sitagliptin (substance)", "Sitagliptin",
			"Sitagliptin", "sitagliptine", "Sitagliptin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium acetate trihydrat</div>
	 * <div class="de">Natrium acetat-3-Wasser</div>
	 * <div class="fr">sodium acétate trihydrate</div>
	 * <div class="it">Sodio acetato triidrato</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_ACETATE_TRIHYDRAT("726006002", "2.16.840.1.113883.6.96",
			"Sodium acetate trihydrat (substance)", "Sodium acetate trihydrat",
			"Natrium acetat-3-Wasser", "sodium acétate trihydrate", "Sodio acetato triidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium bicarbonate</div>
	 * <div class="de">Natriumhydrogencarbonat</div>
	 * <div class="fr">sodium bicarbonate</div>
	 * <div class="it">Bicarbonato di sodio</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_BICARBONATE("387319002", "2.16.840.1.113883.6.96", "Sodium bicarbonate (substance)",
			"Sodium bicarbonate", "Natriumhydrogencarbonat", "sodium bicarbonate",
			"Bicarbonato di sodio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium chloride</div>
	 * <div class="de">Natriumchlorid</div>
	 * <div class="fr">sodium chlorure</div>
	 * <div class="it">Sodio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_CHLORIDE("387390002", "2.16.840.1.113883.6.96", "Sodium chloride (substance)",
			"Sodium chloride", "Natriumchlorid", "sodium chlorure", "Sodio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium citrate</div>
	 * <div class="de">Natriumcitrat</div>
	 * <div class="fr">sodium citrate</div>
	 * <div class="it">Sodio citrato</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_CITRATE("412546005", "2.16.840.1.113883.6.96", "Sodium citrate (substance)",
			"Sodium citrate", "Natriumcitrat", "sodium citrate", "Sodio citrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium dihydrogen phosphate dihydrate</div>
	 * <div class="de">Natrium dihydrogenphosphat-2-Wasser</div>
	 * <div class="fr">sodium dihydrogénophosphate dihydrate</div>
	 * <div class="it">Sodio fosfato monobasico diidrato</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_DIHYDROGEN_PHOSPHATE_DIHYDRATE("726716000", "2.16.840.1.113883.6.96",
			"Sodium dihydrogen phosphate dihydrate (substance)",
			"Sodium dihydrogen phosphate dihydrate", "Natrium dihydrogenphosphat-2-Wasser",
			"sodium dihydrogénophosphate dihydrate", "Sodio fosfato monobasico diidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium hydroxide</div>
	 * <div class="de">Natriumhydroxid</div>
	 * <div class="fr">sodium hydroxyde</div>
	 * <div class="it">Sodio idrossido</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_HYDROXIDE("23423003", "2.16.840.1.113883.6.96", "Sodium hydroxide (substance)",
			"Sodium hydroxide", "Natriumhydroxid", "sodium hydroxyde", "Sodio idrossido"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium nitroprusside</div>
	 * <div class="de">Nitroprussidnatrium, wasserfrei</div>
	 * <div class="fr">nitroprussiate de sodium anhydre</div>
	 * <div class="it">Sodio nitroprussiato</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_NITROPRUSSIDE("387139005", "2.16.840.1.113883.6.96", "Sodium nitroprusside (substance)",
			"Sodium nitroprusside", "Nitroprussidnatrium, wasserfrei",
			"nitroprussiate de sodium anhydre", "Sodio nitroprussiato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium picosulfate</div>
	 * <div class="de">Natrium picosulfat</div>
	 * <div class="fr">picosulfate sodique</div>
	 * <div class="it">Sodio picosolfato</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_PICOSULFATE("395881001", "2.16.840.1.113883.6.96", "Sodium picosulfate (substance)",
			"Sodium picosulfate", "Natrium picosulfat", "picosulfate sodique", "Sodio picosolfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium selenite</div>
	 * <div class="de">Dinatrium-selenit</div>
	 * <div class="fr">sélénite disodique</div>
	 * <div class="it">Sodio selenito</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_SELENITE("96277001", "2.16.840.1.113883.6.96", "Sodium selenite (substance)",
			"Sodium selenite", "Dinatrium-selenit", "sélénite disodique", "Sodio selenito"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sodium thiosulfate</div>
	 * <div class="de">Dinatrium-thiosulfat</div>
	 * <div class="fr">sodium thiosulfate</div>
	 * <div class="it">Sodio tiosolfato</div>
	 * <!-- @formatter:on -->
	 */
	SODIUM_THIOSULFATE("387209006", "2.16.840.1.113883.6.96", "Sodium thiosulfate (substance)",
			"Sodium thiosulfate", "Dinatrium-thiosulfat", "sodium thiosulfate", "Sodio tiosolfato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sofosbuvir</div>
	 * <div class="de">Sofosbuvir</div>
	 * <div class="fr">sofosbuvir</div>
	 * <div class="it">Sofosbuvir</div>
	 * <!-- @formatter:on -->
	 */
	SOFOSBUVIR("710806008", "2.16.840.1.113883.6.96", "Sofosbuvir (substance)", "Sofosbuvir",
			"Sofosbuvir", "sofosbuvir", "Sofosbuvir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Solifenacin</div>
	 * <div class="de">Solifenacin</div>
	 * <div class="fr">solifénacine</div>
	 * <div class="it">Solifenacina</div>
	 * <!-- @formatter:on -->
	 */
	SOLIFENACIN("407030007", "2.16.840.1.113883.6.96", "Solifenacin (substance)", "Solifenacin",
			"Solifenacin", "solifénacine", "Solifenacina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Somatostatin</div>
	 * <div class="de">Somatostatin</div>
	 * <div class="fr">somatostatine</div>
	 * <div class="it">Somatostatina</div>
	 * <!-- @formatter:on -->
	 */
	SOMATOSTATIN("49722008", "2.16.840.1.113883.6.96", "Somatostatin (substance)", "Somatostatin",
			"Somatostatin", "somatostatine", "Somatostatina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Somatotropin releasing factor</div>
	 * <div class="de">Somatorelin</div>
	 * <div class="fr">somatoréline</div>
	 * <div class="it">Somatorelina</div>
	 * <!-- @formatter:on -->
	 */
	SOMATOTROPIN_RELEASING_FACTOR("16628008", "2.16.840.1.113883.6.96",
			"Somatotropin releasing factor (substance)", "Somatotropin releasing factor",
			"Somatorelin", "somatoréline", "Somatorelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Somatropin</div>
	 * <div class="de">Somatropin</div>
	 * <div class="fr">somatropine</div>
	 * <div class="it">Somatropina</div>
	 * <!-- @formatter:on -->
	 */
	SOMATROPIN("395883003", "2.16.840.1.113883.6.96", "Somatropin (substance)", "Somatropin",
			"Somatropin", "somatropine", "Somatropina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sorafenib</div>
	 * <div class="de">Sorafenib</div>
	 * <div class="fr">sorafénib</div>
	 * <div class="it">Sorafenib</div>
	 * <!-- @formatter:on -->
	 */
	SORAFENIB("422042001", "2.16.840.1.113883.6.96", "Sorafenib (substance)", "Sorafenib",
			"Sorafenib", "sorafénib", "Sorafenib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sotalol</div>
	 * <div class="de">Sotalol</div>
	 * <div class="fr">sotalol</div>
	 * <div class="it">Sotalolo</div>
	 * <!-- @formatter:on -->
	 */
	SOTALOL("372911006", "2.16.840.1.113883.6.96", "Sotalol (substance)", "Sotalol", "Sotalol",
			"sotalol", "Sotalolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Soy bean oil</div>
	 * <div class="de">Sojabohnenöl</div>
	 * <div class="fr">soja fèves huile</div>
	 * <div class="it">Soia olio</div>
	 * <!-- @formatter:on -->
	 */
	SOY_BEAN_OIL("226911007", "2.16.840.1.113883.6.96", "Soy bean oil (substance)", "Soy bean oil",
			"Sojabohnenöl", "soja fèves huile", "Soia olio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Spironolactone</div>
	 * <div class="de">Spironolacton</div>
	 * <div class="fr">spironolactone</div>
	 * <div class="it">Spironolattone</div>
	 * <!-- @formatter:on -->
	 */
	SPIRONOLACTONE("387078006", "2.16.840.1.113883.6.96", "Spironolactone (substance)",
			"Spironolactone", "Spironolacton", "spironolactone", "Spironolattone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Stiripentol</div>
	 * <div class="de">Stiripentol</div>
	 * <div class="fr">stiripentol</div>
	 * <div class="it">Stiripentolo</div>
	 * <!-- @formatter:on -->
	 */
	STIRIPENTOL("428221002", "2.16.840.1.113883.6.96", "Stiripentol (substance)", "Stiripentol",
			"Stiripentol", "stiripentol", "Stiripentolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Substance with protease mechanism of action</div>
	 * <div class="de">Protease</div>
	 * <div class="fr">protéase</div>
	 * <div class="it">Proteasi</div>
	 * <!-- @formatter:on -->
	 */
	SUBSTANCE_WITH_PROTEASE_MECHANISM_OF_ACTION("387033008", "2.16.840.1.113883.6.96",
			"Substance with protease mechanism of action (substance)",
			"Substance with protease mechanism of action", "Protease", "protéase", "Proteasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Succinylcholine chloride</div>
	 * <div class="de">Suxamethonium chlorid</div>
	 * <div class="fr">suxaméthonium chlorure</div>
	 * <div class="it">Succinilcolina cloruro</div>
	 * <!-- @formatter:on -->
	 */
	SUCCINYLCHOLINE_CHLORIDE("58907007", "2.16.840.1.113883.6.96",
			"Succinylcholine chloride (substance)", "Succinylcholine chloride",
			"Suxamethonium chlorid", "suxaméthonium chlorure", "Succinilcolina cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sufentanil</div>
	 * <div class="de">Sufentanil</div>
	 * <div class="fr">sufentanil</div>
	 * <div class="it">Sufentanil</div>
	 * <!-- @formatter:on -->
	 */
	SUFENTANIL("49998007", "2.16.840.1.113883.6.96", "Sufentanil (substance)", "Sufentanil",
			"Sufentanil", "sufentanil", "Sufentanil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sugammadex</div>
	 * <div class="de">Sugammadex</div>
	 * <div class="fr">sugammadex</div>
	 * <div class="it">Sugammadex</div>
	 * <!-- @formatter:on -->
	 */
	SUGAMMADEX("442340006", "2.16.840.1.113883.6.96", "Sugammadex (substance)", "Sugammadex",
			"Sugammadex", "sugammadex", "Sugammadex"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulfadiazine</div>
	 * <div class="de">Sulfadiazin</div>
	 * <div class="fr">sulfadiazine</div>
	 * <div class="it">Sulfadiazina</div>
	 * <!-- @formatter:on -->
	 */
	SULFADIAZINE("74523009", "2.16.840.1.113883.6.96", "Sulfadiazine (substance)", "Sulfadiazine",
			"Sulfadiazin", "sulfadiazine", "Sulfadiazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulfamethoxazole</div>
	 * <div class="de">Sulfamethoxazol</div>
	 * <div class="fr">sulfaméthoxazole</div>
	 * <div class="it">Sulfametossazolo</div>
	 * <!-- @formatter:on -->
	 */
	SULFAMETHOXAZOLE("363528007", "2.16.840.1.113883.6.96", "Sulfamethoxazole (substance)",
			"Sulfamethoxazole", "Sulfamethoxazol", "sulfaméthoxazole", "Sulfametossazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulfamethoxazole and trimethoprim</div>
	 * <div class="de">Sulfamethoxazol und Trimethoprim</div>
	 * <div class="fr">sulfaméthoxazole et triméthoprime</div>
	 * <div class="it">Sulfametossazolo e trimetoprim</div>
	 * <!-- @formatter:on -->
	 */
	SULFAMETHOXAZOLE_AND_TRIMETHOPRIM("703745000", "2.16.840.1.113883.6.96",
			"Sulfamethoxazole and trimethoprim (substance)", "Sulfamethoxazole and trimethoprim",
			"Sulfamethoxazol und Trimethoprim", "sulfaméthoxazole et triméthoprime",
			"Sulfametossazolo e trimetoprim"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulfasalazine</div>
	 * <div class="de">Sulfasalazin</div>
	 * <div class="fr">sulfasalazine</div>
	 * <div class="it">Sulfasalazina</div>
	 * <!-- @formatter:on -->
	 */
	SULFASALAZINE("387248006", "2.16.840.1.113883.6.96", "Sulfasalazine (substance)",
			"Sulfasalazine", "Sulfasalazin", "sulfasalazine", "Sulfasalazina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulfur hexafluoride</div>
	 * <div class="de">Schwefelhexafluorid</div>
	 * <div class="fr">soufre hexafluorure</div>
	 * <div class="it">Zolfo esafluoruro</div>
	 * <!-- @formatter:on -->
	 */
	SULFUR_HEXAFLUORIDE("259276004", "2.16.840.1.113883.6.96", "Sulfur hexafluoride (substance)",
			"Sulfur hexafluoride", "Schwefelhexafluorid", "soufre hexafluorure",
			"Zolfo esafluoruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulpiride</div>
	 * <div class="de">Sulpirid</div>
	 * <div class="fr">sulpiride</div>
	 * <div class="it">Sulpiride</div>
	 * <!-- @formatter:on -->
	 */
	SULPIRIDE("395891007", "2.16.840.1.113883.6.96", "Sulpiride (substance)", "Sulpiride",
			"Sulpirid", "sulpiride", "Sulpiride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulprostone</div>
	 * <div class="de">Sulproston</div>
	 * <div class="fr">sulprostone</div>
	 * <div class="it">Sulprostone</div>
	 * <!-- @formatter:on -->
	 */
	SULPROSTONE("713461008", "2.16.840.1.113883.6.96", "Sulprostone (substance)", "Sulprostone",
			"Sulproston", "sulprostone", "Sulprostone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sulthiamine</div>
	 * <div class="de">Sultiam</div>
	 * <div class="fr">sultiame</div>
	 * <div class="it">Sultiame</div>
	 * <!-- @formatter:on -->
	 */
	SULTHIAMINE("50580004", "2.16.840.1.113883.6.96", "Sulthiamine (substance)", "Sulthiamine",
			"Sultiam", "sultiame", "Sultiame"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Sumatriptan</div>
	 * <div class="de">Sumatriptan</div>
	 * <div class="fr">sumatriptan</div>
	 * <div class="it">Sumatriptan</div>
	 * <!-- @formatter:on -->
	 */
	SUMATRIPTAN("395892000", "2.16.840.1.113883.6.96", "Sumatriptan (substance)", "Sumatriptan",
			"Sumatriptan", "sumatriptan", "Sumatriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tacrolimus</div>
	 * <div class="de">Tacrolimus</div>
	 * <div class="fr">tacrolimus</div>
	 * <div class="it">Tacrolimus</div>
	 * <!-- @formatter:on -->
	 */
	TACROLIMUS("386975001", "2.16.840.1.113883.6.96", "Tacrolimus (substance)", "Tacrolimus",
			"Tacrolimus", "tacrolimus", "Tacrolimus"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tadalafil</div>
	 * <div class="de">Tadalafil</div>
	 * <div class="fr">tadalafil</div>
	 * <div class="it">Tadalafil</div>
	 * <!-- @formatter:on -->
	 */
	TADALAFIL("407111005", "2.16.840.1.113883.6.96", "Tadalafil (substance)", "Tadalafil",
			"Tadalafil", "tadalafil", "Tadalafil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tafluprost</div>
	 * <div class="de">Tafluprost</div>
	 * <div class="fr">tafluprost</div>
	 * <div class="it">Tafluprost</div>
	 * <!-- @formatter:on -->
	 */
	TAFLUPROST("699181001", "2.16.840.1.113883.6.96", "Tafluprost (substance)", "Tafluprost",
			"Tafluprost", "tafluprost", "Tafluprost"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tamoxifen</div>
	 * <div class="de">Tamoxifen</div>
	 * <div class="fr">tamoxifène</div>
	 * <div class="it">Tamoxifene</div>
	 * <!-- @formatter:on -->
	 */
	TAMOXIFEN("373345002", "2.16.840.1.113883.6.96", "Tamoxifen (substance)", "Tamoxifen",
			"Tamoxifen", "tamoxifène", "Tamoxifene"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tamsulosin</div>
	 * <div class="de">Tamsulosin</div>
	 * <div class="fr">tamsulosine</div>
	 * <div class="it">Tamsulosina</div>
	 * <!-- @formatter:on -->
	 */
	TAMSULOSIN("372509005", "2.16.840.1.113883.6.96", "Tamsulosin (substance)", "Tamsulosin",
			"Tamsulosin", "tamsulosine", "Tamsulosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tapentadol</div>
	 * <div class="de">Tapentadol</div>
	 * <div class="fr">tapentadol</div>
	 * <div class="it">Tapentadolo</div>
	 * <!-- @formatter:on -->
	 */
	TAPENTADOL("441757005", "2.16.840.1.113883.6.96", "Tapentadol (substance)", "Tapentadol",
			"Tapentadol", "tapentadol", "Tapentadolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Taurine</div>
	 * <div class="de">Taurin</div>
	 * <div class="fr">taurine</div>
	 * <div class="it">Taurina</div>
	 * <!-- @formatter:on -->
	 */
	TAURINE("10944007", "2.16.840.1.113883.6.96", "Taurine (substance)", "Taurine", "Taurin",
			"taurine", "Taurina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tazobactam</div>
	 * <div class="de">Tazobactam</div>
	 * <div class="fr">tazobactam</div>
	 * <div class="it">Tazobactam</div>
	 * <!-- @formatter:on -->
	 */
	TAZOBACTAM("96007008", "2.16.840.1.113883.6.96", "Tazobactam (substance)", "Tazobactam",
			"Tazobactam", "tazobactam", "Tazobactam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Teicoplanin</div>
	 * <div class="de">Teicoplanin</div>
	 * <div class="fr">téicoplanine</div>
	 * <div class="it">Teicoplanina</div>
	 * <!-- @formatter:on -->
	 */
	TEICOPLANIN("387529008", "2.16.840.1.113883.6.96", "Teicoplanin (substance)", "Teicoplanin",
			"Teicoplanin", "téicoplanine", "Teicoplanina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Telmisartan</div>
	 * <div class="de">Telmisartan</div>
	 * <div class="fr">telmisartan</div>
	 * <div class="it">Telmisartan</div>
	 * <!-- @formatter:on -->
	 */
	TELMISARTAN("387069000", "2.16.840.1.113883.6.96", "Telmisartan (substance)", "Telmisartan",
			"Telmisartan", "telmisartan", "Telmisartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Temazepam</div>
	 * <div class="de">Temazepam</div>
	 * <div class="fr">témazépam</div>
	 * <div class="it">Temazepam</div>
	 * <!-- @formatter:on -->
	 */
	TEMAZEPAM("387300007", "2.16.840.1.113883.6.96", "Temazepam (substance)", "Temazepam",
			"Temazepam", "témazépam", "Temazepam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Temozolomide</div>
	 * <div class="de">Temozolomid</div>
	 * <div class="fr">témozolomide</div>
	 * <div class="it">Temozolomide</div>
	 * <!-- @formatter:on -->
	 */
	TEMOZOLOMIDE("387009002", "2.16.840.1.113883.6.96", "Temozolomide (substance)", "Temozolomide",
			"Temozolomid", "témozolomide", "Temozolomide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tenofovir</div>
	 * <div class="de">Tenofovir</div>
	 * <div class="fr">ténofovir</div>
	 * <div class="it">Tenofovir</div>
	 * <!-- @formatter:on -->
	 */
	TENOFOVIR("422091007", "2.16.840.1.113883.6.96", "Tenofovir (substance)", "Tenofovir",
			"Tenofovir", "ténofovir", "Tenofovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Terbinafine</div>
	 * <div class="de">Terbinafin</div>
	 * <div class="fr">terbinafine</div>
	 * <div class="it">Terbinafina</div>
	 * <!-- @formatter:on -->
	 */
	TERBINAFINE("373450007", "2.16.840.1.113883.6.96", "Terbinafine (substance)", "Terbinafine",
			"Terbinafin", "terbinafine", "Terbinafina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Terbutaline sulfate</div>
	 * <div class="de">Terbutalin sulfat</div>
	 * <div class="fr">terbutaline sulfate</div>
	 * <div class="it">Terbutalina</div>
	 * <!-- @formatter:on -->
	 */
	TERBUTALINE_SULFATE("24583009", "2.16.840.1.113883.6.96", "Terbutaline sulfate (substance)",
			"Terbutaline sulfate", "Terbutalin sulfat", "terbutaline sulfate", "Terbutalina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Teriparatide</div>
	 * <div class="de">Teriparatid</div>
	 * <div class="fr">tériparatide</div>
	 * <div class="it">Teriparatide</div>
	 * <!-- @formatter:on -->
	 */
	TERIPARATIDE("425438001", "2.16.840.1.113883.6.96", "Teriparatide (substance)", "Teriparatide",
			"Teriparatid", "tériparatide", "Teriparatide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Terlipressin</div>
	 * <div class="de">Terlipressin</div>
	 * <div class="fr">terlipressine</div>
	 * <div class="it">Terlipressina</div>
	 * <!-- @formatter:on -->
	 */
	TERLIPRESSIN("395899009", "2.16.840.1.113883.6.96", "Terlipressin (substance)", "Terlipressin",
			"Terlipressin", "terlipressine", "Terlipressina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Testosterone</div>
	 * <div class="de">Testosteron</div>
	 * <div class="fr">testostérone</div>
	 * <div class="it">Testosterone</div>
	 * <!-- @formatter:on -->
	 */
	TESTOSTERONE("43688007", "2.16.840.1.113883.6.96", "Testosterone (substance)", "Testosterone",
			"Testosteron", "testostérone", "Testosterone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tetanus immunoglobulin of human origin</div>
	 * <div class="de">Tetanus-Immunglobulin vom Menschen</div>
	 * <div class="fr">immunoglobuline humaine anti-tétanique</div>
	 * <div class="it">Immunoglobulina umana antitetanica</div>
	 * <!-- @formatter:on -->
	 */
	TETANUS_IMMUNOGLOBULIN_OF_HUMAN_ORIGIN("428527002", "2.16.840.1.113883.6.96",
			"Tetanus immunoglobulin of human origin (substance)",
			"Tetanus immunoglobulin of human origin", "Tetanus-Immunglobulin vom Menschen",
			"immunoglobuline humaine anti-tétanique", "Immunoglobulina umana antitetanica"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tetanus vaccine</div>
	 * <div class="de">Tetanus-Adsorbat-Impfstoff</div>
	 * <div class="fr">tétanos vaccin adsorbé</div>
	 * <div class="it">Tetano vaccino</div>
	 * <!-- @formatter:on -->
	 */
	TETANUS_VACCINE("412375000", "2.16.840.1.113883.6.96", "Tetanus vaccine (substance)",
			"Tetanus vaccine", "Tetanus-Adsorbat-Impfstoff", "tétanos vaccin adsorbé",
			"Tetano vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tetracaine</div>
	 * <div class="de">Tetracain</div>
	 * <div class="fr">tétracaïne</div>
	 * <div class="it">Tetracaina</div>
	 * <!-- @formatter:on -->
	 */
	TETRACAINE("387309008", "2.16.840.1.113883.6.96", "Tetracaine (substance)", "Tetracaine",
			"Tetracain", "tétracaïne", "Tetracaina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tetracosactide</div>
	 * <div class="de">Tetracosactid</div>
	 * <div class="fr">tétracosactide</div>
	 * <div class="it">Tetracosactide</div>
	 * <!-- @formatter:on -->
	 */
	TETRACOSACTIDE("96363002", "2.16.840.1.113883.6.96", "Tetracosactide (substance)",
			"Tetracosactide", "Tetracosactid", "tétracosactide", "Tetracosactide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tetracycline</div>
	 * <div class="de">Tetracyclin</div>
	 * <div class="fr">tétracycline</div>
	 * <div class="it">Tetraciclina</div>
	 * <!-- @formatter:on -->
	 */
	TETRACYCLINE("372809001", "2.16.840.1.113883.6.96", "Tetracycline (substance)", "Tetracycline",
			"Tetracyclin", "tétracycline", "Tetraciclina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tetryzoline</div>
	 * <div class="de">Tetryzolin</div>
	 * <div class="fr">tétryzoline</div>
	 * <div class="it">Tetrizolina</div>
	 * <!-- @formatter:on -->
	 */
	TETRYZOLINE("372673004", "2.16.840.1.113883.6.96", "Tetryzoline (substance)", "Tetryzoline",
			"Tetryzolin", "tétryzoline", "Tetrizolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Theophylline</div>
	 * <div class="de">Theophyllin</div>
	 * <div class="fr">théophylline</div>
	 * <div class="it">Teofillina</div>
	 * <!-- @formatter:on -->
	 */
	THEOPHYLLINE("372810006", "2.16.840.1.113883.6.96", "Theophylline (substance)", "Theophylline",
			"Theophyllin", "théophylline", "Teofillina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Thiamine</div>
	 * <div class="de">Thiamin (Vitamin B1)</div>
	 * <div class="fr">thiamine (Vitamine B1)</div>
	 * <div class="it">Tiamina (vitamina B1)</div>
	 * <!-- @formatter:on -->
	 */
	THIAMINE("259659006", "2.16.840.1.113883.6.96", "Thiamine (substance)", "Thiamine",
			"Thiamin (Vitamin B1)", "thiamine (Vitamine B1)", "Tiamina (vitamina B1)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Thiotepa</div>
	 * <div class="de">Thiotepa</div>
	 * <div class="fr">thiotépa</div>
	 * <div class="it">Tiotepa</div>
	 * <!-- @formatter:on -->
	 */
	THIOTEPA("387508004", "2.16.840.1.113883.6.96", "Thiotepa (substance)", "Thiotepa", "Thiotepa",
			"thiotépa", "Tiotepa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Threonine</div>
	 * <div class="de">Threonin</div>
	 * <div class="fr">thréonine</div>
	 * <div class="it">Treonina</div>
	 * <!-- @formatter:on -->
	 */
	THREONINE("52736009", "2.16.840.1.113883.6.96", "Threonine (substance)", "Threonine",
			"Threonin", "thréonine", "Treonina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tiapride</div>
	 * <div class="de">Tiaprid</div>
	 * <div class="fr">tiapride</div>
	 * <div class="it">Tiapride</div>
	 * <!-- @formatter:on -->
	 */
	TIAPRIDE("699180000", "2.16.840.1.113883.6.96", "Tiapride (substance)", "Tiapride", "Tiaprid",
			"tiapride", "Tiapride"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tibolone</div>
	 * <div class="de">Tibolon</div>
	 * <div class="fr">tibolone</div>
	 * <div class="it">Tibolone</div>
	 * <!-- @formatter:on -->
	 */
	TIBOLONE("395903002", "2.16.840.1.113883.6.96", "Tibolone (substance)", "Tibolone", "Tibolon",
			"tibolone", "Tibolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ticagrelor</div>
	 * <div class="de">Ticagrelor</div>
	 * <div class="fr">ticagrélor</div>
	 * <div class="it">Ticagrelor</div>
	 * <!-- @formatter:on -->
	 */
	TICAGRELOR("698805004", "2.16.840.1.113883.6.96", "Ticagrelor (substance)", "Ticagrelor",
			"Ticagrelor", "ticagrélor", "Ticagrelor"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tick-borne encephalitis vaccine</div>
	 * <div class="de">FSME-Impfstoff, inaktiviert</div>
	 * <div class="fr">encéphalite verno-estivale FSME inactivé vaccin</div>
	 * <div class="it">Meningoencefalite verno-estiva (FSME) vaccino inattivato</div>
	 * <!-- @formatter:on -->
	 */
	TICK_BORNE_ENCEPHALITIS_VACCINE("398783009", "2.16.840.1.113883.6.96",
			"Tick-borne encephalitis vaccine (substance)", "Tick-borne encephalitis vaccine",
			"FSME-Impfstoff, inaktiviert", "encéphalite verno-estivale FSME inactivé vaccin",
			"Meningoencefalite verno-estiva (FSME) vaccino inattivato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tigecycline</div>
	 * <div class="de">Tigecyclin</div>
	 * <div class="fr">tigécycline</div>
	 * <div class="it">Tigeciclina</div>
	 * <!-- @formatter:on -->
	 */
	TIGECYCLINE("418313005", "2.16.840.1.113883.6.96", "Tigecycline (substance)", "Tigecycline",
			"Tigecyclin", "tigécycline", "Tigeciclina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tilidine hydrochloride</div>
	 * <div class="de">Tilidin hydrochlorid</div>
	 * <div class="fr">tilidine chlorhydrate</div>
	 * <div class="it">Tilidina cloridrato</div>
	 * <!-- @formatter:on -->
	 */
	TILIDINE_HYDROCHLORIDE("96186004", "2.16.840.1.113883.6.96",
			"Tilidine hydrochloride (substance)", "Tilidine hydrochloride", "Tilidin hydrochlorid",
			"tilidine chlorhydrate", "Tilidina cloridrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Timolol</div>
	 * <div class="de">Timolol</div>
	 * <div class="fr">timolol</div>
	 * <div class="it">Timololo</div>
	 * <!-- @formatter:on -->
	 */
	TIMOLOL("372880004", "2.16.840.1.113883.6.96", "Timolol (substance)", "Timolol", "Timolol",
			"timolol", "Timololo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tioguanine</div>
	 * <div class="de">Tioguanin</div>
	 * <div class="fr">tioguanine</div>
	 * <div class="it">Tioguanina</div>
	 * <!-- @formatter:on -->
	 */
	TIOGUANINE("387407006", "2.16.840.1.113883.6.96", "Tioguanine (substance)", "Tioguanine",
			"Tioguanin", "tioguanine", "Tioguanina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tiotropium</div>
	 * <div class="de">Tiotropium</div>
	 * <div class="fr">tiotropium</div>
	 * <div class="it">Tiotropio</div>
	 * <!-- @formatter:on -->
	 */
	TIOTROPIUM("409169006", "2.16.840.1.113883.6.96", "Tiotropium (substance)", "Tiotropium",
			"Tiotropium", "tiotropium", "Tiotropio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tipranavir</div>
	 * <div class="de">Tipranavir</div>
	 * <div class="fr">tipranavir</div>
	 * <div class="it">Tipranavir</div>
	 * <!-- @formatter:on -->
	 */
	TIPRANAVIR("419409009", "2.16.840.1.113883.6.96", "Tipranavir (substance)", "Tipranavir",
			"Tipranavir", "tipranavir", "Tipranavir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tizanidine</div>
	 * <div class="de">Tizanidin</div>
	 * <div class="fr">tizanidine</div>
	 * <div class="it">Tizanidina</div>
	 * <!-- @formatter:on -->
	 */
	TIZANIDINE("373440006", "2.16.840.1.113883.6.96", "Tizanidine (substance)", "Tizanidine",
			"Tizanidin", "tizanidine", "Tizanidina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tobramycine</div>
	 * <div class="de">Tobramycin</div>
	 * <div class="fr">tobramycine</div>
	 * <div class="it">Tobramicina</div>
	 * <!-- @formatter:on -->
	 */
	TOBRAMYCINE("373548001", "2.16.840.1.113883.6.96", "Tobramycine (substance)", "Tobramycine",
			"Tobramycin", "tobramycine", "Tobramicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tocilizumab</div>
	 * <div class="de">Tocilizumab</div>
	 * <div class="fr">tocilizumab</div>
	 * <div class="it">Tocilizumab</div>
	 * <!-- @formatter:on -->
	 */
	TOCILIZUMAB("444648007", "2.16.840.1.113883.6.96", "Tocilizumab (substance)", "Tocilizumab",
			"Tocilizumab", "tocilizumab", "Tocilizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tolcapone</div>
	 * <div class="de">Tolcapon</div>
	 * <div class="fr">tolcapone</div>
	 * <div class="it">Tolcapone</div>
	 * <!-- @formatter:on -->
	 */
	TOLCAPONE("386851002", "2.16.840.1.113883.6.96", "Tolcapone (substance)", "Tolcapone",
			"Tolcapon", "tolcapone", "Tolcapone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tolperisone</div>
	 * <div class="de">Tolperison</div>
	 * <div class="fr">tolpérisone</div>
	 * <div class="it">Tolperisone</div>
	 * <!-- @formatter:on -->
	 */
	TOLPERISONE("703717006", "2.16.840.1.113883.6.96", "Tolperisone (substance)", "Tolperisone",
			"Tolperison", "tolpérisone", "Tolperisone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tolterodine</div>
	 * <div class="de">Tolterodin</div>
	 * <div class="fr">toltérodine</div>
	 * <div class="it">Tolterodina</div>
	 * <!-- @formatter:on -->
	 */
	TOLTERODINE("372570008", "2.16.840.1.113883.6.96", "Tolterodine (substance)", "Tolterodine",
			"Tolterodin", "toltérodine", "Tolterodina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tolvaptan</div>
	 * <div class="de">Tolvaptan</div>
	 * <div class="fr">tolvaptan</div>
	 * <div class="it">Tolvaptan</div>
	 * <!-- @formatter:on -->
	 */
	TOLVAPTAN("443058000", "2.16.840.1.113883.6.96", "Tolvaptan (substance)", "Tolvaptan",
			"Tolvaptan", "tolvaptan", "Tolvaptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Topiramate</div>
	 * <div class="de">Topiramat</div>
	 * <div class="fr">topiramate</div>
	 * <div class="it">Topiramato</div>
	 * <!-- @formatter:on -->
	 */
	TOPIRAMATE("386844006", "2.16.840.1.113883.6.96", "Topiramate (substance)", "Topiramate",
			"Topiramat", "topiramate", "Topiramato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Topotecan</div>
	 * <div class="de">Topotecan</div>
	 * <div class="fr">topotécan</div>
	 * <div class="it">Topotecan</div>
	 * <!-- @formatter:on -->
	 */
	TOPOTECAN("372536007", "2.16.840.1.113883.6.96", "Topotecan (substance)", "Topotecan",
			"Topotecan", "topotécan", "Topotecan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Torasemide</div>
	 * <div class="de">Torasemid</div>
	 * <div class="fr">torasémide</div>
	 * <div class="it">Torasemide</div>
	 * <!-- @formatter:on -->
	 */
	TORASEMIDE("108476002", "2.16.840.1.113883.6.96", "Torasemide (substance)", "Torasemide",
			"Torasemid", "torasémide", "Torasemide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trabectedin</div>
	 * <div class="de">Trabectedin</div>
	 * <div class="fr">trabectédine</div>
	 * <div class="it">Trabectedina</div>
	 * <!-- @formatter:on -->
	 */
	TRABECTEDIN("433127001", "2.16.840.1.113883.6.96", "Trabectedin (substance)", "Trabectedin",
			"Trabectedin", "trabectédine", "Trabectedina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tramadol</div>
	 * <div class="de">Tramadol</div>
	 * <div class="fr">tramadol</div>
	 * <div class="it">Tramadol</div>
	 * <!-- @formatter:on -->
	 */
	TRAMADOL("386858008", "2.16.840.1.113883.6.96", "Tramadol (substance)", "Tramadol", "Tramadol",
			"tramadol", "Tramadol"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trametinib</div>
	 * <div class="de">Trametinib</div>
	 * <div class="fr">tramétinib</div>
	 * <div class="it">Trametinib</div>
	 * <!-- @formatter:on -->
	 */
	TRAMETINIB("708711009", "2.16.840.1.113883.6.96", "Trametinib (substance)", "Trametinib",
			"Trametinib", "tramétinib", "Trametinib"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trandolapril</div>
	 * <div class="de">Trandolapril</div>
	 * <div class="fr">trandolapril</div>
	 * <div class="it">Trandolapril</div>
	 * <!-- @formatter:on -->
	 */
	TRANDOLAPRIL("386871006", "2.16.840.1.113883.6.96", "Trandolapril (substance)", "Trandolapril",
			"Trandolapril", "trandolapril", "Trandolapril"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tranexamic acid</div>
	 * <div class="de">Tranexamsäure</div>
	 * <div class="fr">acide tranexamique</div>
	 * <div class="it">Acido tranexamico</div>
	 * <!-- @formatter:on -->
	 */
	TRANEXAMIC_ACID("386960009", "2.16.840.1.113883.6.96", "Tranexamic acid (substance)",
			"Tranexamic acid", "Tranexamsäure", "acide tranexamique", "Acido tranexamico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tranylcypromine</div>
	 * <div class="de">Tranylcypromin</div>
	 * <div class="fr">tranylcypromine</div>
	 * <div class="it">Tranilcipromina</div>
	 * <!-- @formatter:on -->
	 */
	TRANYLCYPROMINE("372891006", "2.16.840.1.113883.6.96", "Tranylcypromine (substance)",
			"Tranylcypromine", "Tranylcypromin", "tranylcypromine", "Tranilcipromina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trastuzumab</div>
	 * <div class="de">Trastuzumab</div>
	 * <div class="fr">trastuzumab</div>
	 * <div class="it">Trastuzumab</div>
	 * <!-- @formatter:on -->
	 */
	TRASTUZUMAB("387003001", "2.16.840.1.113883.6.96", "Trastuzumab (substance)", "Trastuzumab",
			"Trastuzumab", "trastuzumab", "Trastuzumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Travoprost</div>
	 * <div class="de">Travoprost</div>
	 * <div class="fr">travoprost</div>
	 * <div class="it">Travoprost</div>
	 * <!-- @formatter:on -->
	 */
	TRAVOPROST("129493000", "2.16.840.1.113883.6.96", "Travoprost (substance)", "Travoprost",
			"Travoprost", "travoprost", "Travoprost"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trazodone</div>
	 * <div class="de">Trazodon</div>
	 * <div class="fr">trazodone</div>
	 * <div class="it">Trazodone</div>
	 * <!-- @formatter:on -->
	 */
	TRAZODONE("372829000", "2.16.840.1.113883.6.96", "Trazodone (substance)", "Trazodone",
			"Trazodon", "trazodone", "Trazodone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Treprostinil</div>
	 * <div class="de">Treprostinil</div>
	 * <div class="fr">tréprostinil</div>
	 * <div class="it">Treprostinil</div>
	 * <!-- @formatter:on -->
	 */
	TREPROSTINIL("443570007", "2.16.840.1.113883.6.96", "Treprostinil (substance)", "Treprostinil",
			"Treprostinil", "tréprostinil", "Treprostinil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tretinoin</div>
	 * <div class="de">Tretinoin</div>
	 * <div class="fr">trétinoïne</div>
	 * <div class="it">Tretinoina</div>
	 * <!-- @formatter:on -->
	 */
	TRETINOIN("387305002", "2.16.840.1.113883.6.96", "Tretinoin (substance)", "Tretinoin",
			"Tretinoin", "trétinoïne", "Tretinoina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Triacylglycerol lipase</div>
	 * <div class="de">Lipase</div>
	 * <div class="fr">lipase</div>
	 * <div class="it">Lipasi</div>
	 * <!-- @formatter:on -->
	 */
	TRIACYLGLYCEROL_LIPASE("72993008", "2.16.840.1.113883.6.96",
			"Triacylglycerol lipase (substance)", "Triacylglycerol lipase", "Lipase", "lipase",
			"Lipasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Triamcinolone</div>
	 * <div class="de">Triamcinolon</div>
	 * <div class="fr">triamcinolone</div>
	 * <div class="it">Triamcinolone</div>
	 * <!-- @formatter:on -->
	 */
	TRIAMCINOLONE("116594009", "2.16.840.1.113883.6.96", "Triamcinolone (substance)",
			"Triamcinolone", "Triamcinolon", "triamcinolone", "Triamcinolone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Triamcinolone acetonide</div>
	 * <div class="de">Triamcinolon acetonid</div>
	 * <div class="fr">triamcinolone acétonide</div>
	 * <div class="it">Triamcinolone acetonide</div>
	 * <!-- @formatter:on -->
	 */
	TRIAMCINOLONE_ACETONIDE("395913005", "2.16.840.1.113883.6.96",
			"Triamcinolone acetonide (substance)", "Triamcinolone acetonide",
			"Triamcinolon acetonid", "triamcinolone acétonide", "Triamcinolone acetonide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Triazolam</div>
	 * <div class="de">Triazolam</div>
	 * <div class="fr">triazolam</div>
	 * <div class="it">Triazolam</div>
	 * <!-- @formatter:on -->
	 */
	TRIAZOLAM("386984001", "2.16.840.1.113883.6.96", "Triazolam (substance)", "Triazolam",
			"Triazolam", "triazolam", "Triazolam"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Triclosan</div>
	 * <div class="de">Triclosan</div>
	 * <div class="fr">triclosan</div>
	 * <div class="it">Triclosan</div>
	 * <!-- @formatter:on -->
	 */
	TRICLOSAN("387054001", "2.16.840.1.113883.6.96", "Triclosan (substance)", "Triclosan",
			"Triclosan", "triclosan", "Triclosan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trimethoprim</div>
	 * <div class="de">Trimethoprim</div>
	 * <div class="fr">triméthoprime</div>
	 * <div class="it">Trimetoprim</div>
	 * <!-- @formatter:on -->
	 */
	TRIMETHOPRIM("387179001", "2.16.840.1.113883.6.96", "Trimethoprim (substance)", "Trimethoprim",
			"Trimethoprim", "triméthoprime", "Trimetoprim"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trimipramine</div>
	 * <div class="de">Trimipramin</div>
	 * <div class="fr">trimipramine</div>
	 * <div class="it">Trimipramina</div>
	 * <!-- @formatter:on -->
	 */
	TRIMIPRAMINE("373550009", "2.16.840.1.113883.6.96", "Trimipramine (substance)", "Trimipramine",
			"Trimipramin", "trimipramine", "Trimipramina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Triptorelin</div>
	 * <div class="de">Triptorelin</div>
	 * <div class="fr">triptoréline</div>
	 * <div class="it">Triptorelina</div>
	 * <!-- @formatter:on -->
	 */
	TRIPTORELIN("395915003", "2.16.840.1.113883.6.96", "Triptorelin (substance)", "Triptorelin",
			"Triptorelin", "triptoréline", "Triptorelina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tropicamide</div>
	 * <div class="de">Tropicamid</div>
	 * <div class="fr">tropicamide</div>
	 * <div class="it">Tropicamide</div>
	 * <!-- @formatter:on -->
	 */
	TROPICAMIDE("387526001", "2.16.840.1.113883.6.96", "Tropicamide (substance)", "Tropicamide",
			"Tropicamid", "tropicamide", "Tropicamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Trospium chloride</div>
	 * <div class="de">Trospium chlorid</div>
	 * <div class="fr">trospium chlorure</div>
	 * <div class="it">Trospio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	TROSPIUM_CHLORIDE("326557004", "2.16.840.1.113883.6.96", "Trospium chloride (substance)",
			"Trospium chloride", "Trospium chlorid", "trospium chlorure", "Trospio cloruro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tryptophan</div>
	 * <div class="de">Tryptophan</div>
	 * <div class="fr">tryptophane</div>
	 * <div class="it">Triptofano</div>
	 * <!-- @formatter:on -->
	 */
	TRYPTOPHAN("54821000", "2.16.840.1.113883.6.96", "Tryptophan (substance)", "Tryptophan",
			"Tryptophan", "tryptophane", "Triptofano"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tuberculin purified protein derivative</div>
	 * <div class="de">Tuberkulin, gereinigtes PPD</div>
	 * <div class="fr">tuberculine dérivé protéinique purifié</div>
	 * <div class="it">Tubercolina derivato proteico purificato (PPD)</div>
	 * <!-- @formatter:on -->
	 */
	TUBERCULIN_PURIFIED_PROTEIN_DERIVATIVE("108731003", "2.16.840.1.113883.6.96",
			"Tuberculin purified protein derivative (substance)",
			"Tuberculin purified protein derivative", "Tuberkulin, gereinigtes PPD",
			"tuberculine dérivé protéinique purifié",
			"Tubercolina derivato proteico purificato (PPD)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Turoctocog alfa</div>
	 * <div class="de">Turoctocog alfa</div>
	 * <div class="fr">turoctocog alfa</div>
	 * <div class="it">Turoctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	TUROCTOCOG_ALFA("735055007", "2.16.840.1.113883.6.96", "Turoctocog alfa (substance)",
			"Turoctocog alfa", "Turoctocog alfa", "turoctocog alfa", "Turoctocog alfa"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tyrosine</div>
	 * <div class="de">Tyrosin</div>
	 * <div class="fr">tyrosine</div>
	 * <div class="it">Tirosina</div>
	 * <!-- @formatter:on -->
	 */
	TYROSINE("27378009", "2.16.840.1.113883.6.96", "Tyrosine (substance)", "Tyrosine", "Tyrosin",
			"tyrosine", "Tirosina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Tyrothricin</div>
	 * <div class="de">Tyrothricin</div>
	 * <div class="fr">tyrothricine</div>
	 * <div class="it">Tirotricina</div>
	 * <!-- @formatter:on -->
	 */
	TYROTHRICIN("36661005", "2.16.840.1.113883.6.96", "Tyrothricin (substance)", "Tyrothricin",
			"Tyrothricin", "tyrothricine", "Tirotricina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ulipristal</div>
	 * <div class="de">Ulipristal</div>
	 * <div class="fr">ulipristal</div>
	 * <div class="it">Ulipristal</div>
	 * <!-- @formatter:on -->
	 */
	ULIPRISTAL("703249005", "2.16.840.1.113883.6.96", "Ulipristal (substance)", "Ulipristal",
			"Ulipristal", "ulipristal", "Ulipristal"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Umeclidinium</div>
	 * <div class="de">Umeclidinium</div>
	 * <div class="fr">uméclidinium</div>
	 * <div class="it">Umeclidinio</div>
	 * <!-- @formatter:on -->
	 */
	UMECLIDINIUM("706898002", "2.16.840.1.113883.6.96", "Umeclidinium (substance)", "Umeclidinium",
			"Umeclidinium", "uméclidinium", "Umeclidinio"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Urapidil</div>
	 * <div class="de">Urapidil</div>
	 * <div class="fr">urapidil</div>
	 * <div class="it">Urapidil</div>
	 * <!-- @formatter:on -->
	 */
	URAPIDIL("698807007", "2.16.840.1.113883.6.96", "Urapidil (substance)", "Urapidil", "Urapidil",
			"urapidil", "Urapidil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Urokinase</div>
	 * <div class="de">Urokinase</div>
	 * <div class="fr">urokinase</div>
	 * <div class="it">Urochinasi</div>
	 * <!-- @formatter:on -->
	 */
	UROKINASE("59082006", "2.16.840.1.113883.6.96", "Urokinase (substance)", "Urokinase",
			"Urokinase", "urokinase", "Urochinasi"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ursodeoxycholic acid</div>
	 * <div class="de">Ursodeoxycholsäure</div>
	 * <div class="fr">acide ursodésoxycholique</div>
	 * <div class="it">Acido ursodesossicolico</div>
	 * <!-- @formatter:on -->
	 */
	URSODEOXYCHOLIC_ACID("41143004", "2.16.840.1.113883.6.96", "Ursodeoxycholic acid (substance)",
			"Ursodeoxycholic acid", "Ursodeoxycholsäure", "acide ursodésoxycholique",
			"Acido ursodesossicolico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Ustekinumab</div>
	 * <div class="de">Ustekinumab</div>
	 * <div class="fr">ustékinumab</div>
	 * <div class="it">Ustekinumab</div>
	 * <!-- @formatter:on -->
	 */
	USTEKINUMAB("443465002", "2.16.840.1.113883.6.96", "Ustekinumab (substance)", "Ustekinumab",
			"Ustekinumab", "ustékinumab", "Ustekinumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valaciclovir</div>
	 * <div class="de">Valaciclovir</div>
	 * <div class="fr">valaciclovir</div>
	 * <div class="it">Valaciclovir</div>
	 * <!-- @formatter:on -->
	 */
	VALACICLOVIR("96098007", "2.16.840.1.113883.6.96", "Valaciclovir (substance)", "Valaciclovir",
			"Valaciclovir", "valaciclovir", "Valaciclovir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valerian extract</div>
	 * <div class="de">Baldrianwurzel-Extrakt</div>
	 * <div class="fr">valériane extrait</div>
	 * <div class="it">Valeriana estratto</div>
	 * <!-- @formatter:on -->
	 */
	VALERIAN_EXTRACT("412266000", "2.16.840.1.113883.6.96", "Valerian extract (substance)",
			"Valerian extract", "Baldrianwurzel-Extrakt", "valériane extrait",
			"Valeriana estratto"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valganciclovir</div>
	 * <div class="de">Valganciclovir</div>
	 * <div class="fr">valganciclovir</div>
	 * <div class="it">Valganciclovit</div>
	 * <!-- @formatter:on -->
	 */
	VALGANCICLOVIR("129476000", "2.16.840.1.113883.6.96", "Valganciclovir (substance)",
			"Valganciclovir", "Valganciclovir", "valganciclovir", "Valganciclovit"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valine</div>
	 * <div class="de">Valin</div>
	 * <div class="fr">valine</div>
	 * <div class="it">Valina</div>
	 * <!-- @formatter:on -->
	 */
	VALINE("72840006", "2.16.840.1.113883.6.96", "Valine (substance)", "Valine", "Valin", "valine",
			"Valina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valproate semisodium</div>
	 * <div class="de">Valproat seminatrium</div>
	 * <div class="fr">valproate semisodique</div>
	 * <div class="it">Valproato semisodico</div>
	 * <!-- @formatter:on -->
	 */
	VALPROATE_SEMISODIUM("5641004", "2.16.840.1.113883.6.96", "Valproate semisodium (substance)",
			"Valproate semisodium", "Valproat seminatrium", "valproate semisodique",
			"Valproato semisodico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valproate sodium</div>
	 * <div class="de">Valproat natrium</div>
	 * <div class="fr">valproate sodique</div>
	 * <div class="it">Valproato sodico</div>
	 * <!-- @formatter:on -->
	 */
	VALPROATE_SODIUM("387481005", "2.16.840.1.113883.6.96", "Valproate sodium (substance)",
			"Valproate sodium", "Valproat natrium", "valproate sodique", "Valproato sodico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valproic acid</div>
	 * <div class="de">Valproinsäure</div>
	 * <div class="fr">acide valproïque</div>
	 * <div class="it">Acido valproico</div>
	 * <!-- @formatter:on -->
	 */
	VALPROIC_ACID("387080000", "2.16.840.1.113883.6.96", "Valproic acid (substance)",
			"Valproic acid", "Valproinsäure", "acide valproïque", "Acido valproico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Valsartan</div>
	 * <div class="de">Valsartan</div>
	 * <div class="fr">valsartan</div>
	 * <div class="it">Valsartan</div>
	 * <!-- @formatter:on -->
	 */
	VALSARTAN("386876001", "2.16.840.1.113883.6.96", "Valsartan (substance)", "Valsartan",
			"Valsartan", "valsartan", "Valsartan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vancomycin</div>
	 * <div class="de">Vancomycin</div>
	 * <div class="fr">vancomycine</div>
	 * <div class="it">Vancomicina</div>
	 * <!-- @formatter:on -->
	 */
	VANCOMYCIN("372735009", "2.16.840.1.113883.6.96", "Vancomycin (substance)", "Vancomycin",
			"Vancomycin", "vancomycine", "Vancomicina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vardenafil</div>
	 * <div class="de">Vardenafil</div>
	 * <div class="fr">vardénafil</div>
	 * <div class="it">Vardenafil</div>
	 * <!-- @formatter:on -->
	 */
	VARDENAFIL("404858007", "2.16.840.1.113883.6.96", "Vardenafil (substance)", "Vardenafil",
			"Vardenafil", "vardénafil", "Vardenafil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Varenicline</div>
	 * <div class="de">Vareniclin</div>
	 * <div class="fr">varénicline</div>
	 * <div class="it">Vareniclina</div>
	 * <!-- @formatter:on -->
	 */
	VARENICLINE("421772003", "2.16.840.1.113883.6.96", "Varenicline (substance)", "Varenicline",
			"Vareniclin", "varénicline", "Vareniclina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Varicella virus live vaccine</div>
	 * <div class="de">Varizellen-Lebend-Impfstoff</div>
	 * <div class="fr">vaccin varicelle vivant</div>
	 * <div class="it">Varicella vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	VARICELLA_VIRUS_LIVE_VACCINE("412529007", "2.16.840.1.113883.6.96",
			"Varicella virus live vaccine (substance)", "Varicella virus live vaccine",
			"Varizellen-Lebend-Impfstoff", "vaccin varicelle vivant", "Varicella vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Varicella virus vaccine</div>
	 * <div class="de">Varizellen-Impfstoff</div>
	 * <div class="fr">vaccin varicelle</div>
	 * <div class="it">Varicella vaccino</div>
	 * <!-- @formatter:on -->
	 */
	VARICELLA_VIRUS_VACCINE("396442000", "2.16.840.1.113883.6.96",
			"Varicella virus vaccine (substance)", "Varicella virus vaccine",
			"Varizellen-Impfstoff", "vaccin varicelle", "Varicella vaccino"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Varicella-zoster virus antibody</div>
	 * <div class="de">Varizellen-Immunglobulin vom Menschen</div>
	 * <div class="fr">immunoglobuline humaine antivaricelle</div>
	 * <div class="it">Immunoglobulina umana antivaricella</div>
	 * <!-- @formatter:on -->
	 */
	VARICELLA_ZOSTER_VIRUS_ANTIBODY("259858000", "2.16.840.1.113883.6.96",
			"Varicella-zoster virus antibody (substance)", "Varicella-zoster virus antibody",
			"Varizellen-Immunglobulin vom Menschen", "immunoglobuline humaine antivaricelle",
			"Immunoglobulina umana antivaricella"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vecuronium bromide</div>
	 * <div class="de">Vecuronium bromid</div>
	 * <div class="fr">vécuronium bromure</div>
	 * <div class="it">Vecuronio bromuro</div>
	 * <!-- @formatter:on -->
	 */
	VECURONIUM_BROMIDE("87472002", "2.16.840.1.113883.6.96", "Vecuronium bromide (substance)",
			"Vecuronium bromide", "Vecuronium bromid", "vécuronium bromure", "Vecuronio bromuro"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vedolizumab</div>
	 * <div class="de">Vedolizumab</div>
	 * <div class="fr">védolizumab</div>
	 * <div class="it">Vedolizumab</div>
	 * <!-- @formatter:on -->
	 */
	VEDOLIZUMAB("704256006", "2.16.840.1.113883.6.96", "Vedolizumab (substance)", "Vedolizumab",
			"Vedolizumab", "védolizumab", "Vedolizumab"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Venlafaxine</div>
	 * <div class="de">Venlafaxin</div>
	 * <div class="fr">venlafaxine</div>
	 * <div class="it">Venlafaxina</div>
	 * <!-- @formatter:on -->
	 */
	VENLAFAXINE("372490001", "2.16.840.1.113883.6.96", "Venlafaxine (substance)", "Venlafaxine",
			"Venlafaxin", "venlafaxine", "Venlafaxina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Verapamil</div>
	 * <div class="de">Verapamil</div>
	 * <div class="fr">vérapamil</div>
	 * <div class="it">Verapamil</div>
	 * <!-- @formatter:on -->
	 */
	VERAPAMIL("372754009", "2.16.840.1.113883.6.96", "Verapamil (substance)", "Verapamil",
			"Verapamil", "vérapamil", "Verapamil"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vigabatrin</div>
	 * <div class="de">Vigabatrin</div>
	 * <div class="fr">vigabatrine</div>
	 * <div class="it">Vigabatrin</div>
	 * <!-- @formatter:on -->
	 */
	VIGABATRIN("310283001", "2.16.840.1.113883.6.96", "Vigabatrin (substance)", "Vigabatrin",
			"Vigabatrin", "vigabatrine", "Vigabatrin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vilanterol</div>
	 * <div class="de">Vilanterol</div>
	 * <div class="fr">vilantérol</div>
	 * <div class="it">Vilanterolo</div>
	 * <!-- @formatter:on -->
	 */
	VILANTEROL("702408004", "2.16.840.1.113883.6.96", "Vilanterol (substance)", "Vilanterol",
			"Vilanterol", "vilantérol", "Vilanterolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vildagliptin</div>
	 * <div class="de">Vildagliptin</div>
	 * <div class="fr">vildagliptine</div>
	 * <div class="it">Vildagliptin</div>
	 * <!-- @formatter:on -->
	 */
	VILDAGLIPTIN("428611002", "2.16.840.1.113883.6.96", "Vildagliptin (substance)", "Vildagliptin",
			"Vildagliptin", "vildagliptine", "Vildagliptin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vinblastine</div>
	 * <div class="de">Vinblastin</div>
	 * <div class="fr">vinblastine</div>
	 * <div class="it">Vinblastina</div>
	 * <!-- @formatter:on -->
	 */
	VINBLASTINE("387051009", "2.16.840.1.113883.6.96", "Vinblastine (substance)", "Vinblastine",
			"Vinblastin", "vinblastine", "Vinblastina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vincristine</div>
	 * <div class="de">Vincristin</div>
	 * <div class="fr">vincristine</div>
	 * <div class="it">Vincristina</div>
	 * <!-- @formatter:on -->
	 */
	VINCRISTINE("387126006", "2.16.840.1.113883.6.96", "Vincristine (substance)", "Vincristine",
			"Vincristin", "vincristine", "Vincristina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vindesine</div>
	 * <div class="de">Vindesin</div>
	 * <div class="fr">vindésine</div>
	 * <div class="it">Vindesina</div>
	 * <!-- @formatter:on -->
	 */
	VINDESINE("409198005", "2.16.840.1.113883.6.96", "Vindesine (substance)", "Vindesine",
			"Vindesin", "vindésine", "Vindesina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vinorelbine</div>
	 * <div class="de">Vinorelbin</div>
	 * <div class="fr">vinorelbine</div>
	 * <div class="it">Vinorelbina</div>
	 * <!-- @formatter:on -->
	 */
	VINORELBINE("372541004", "2.16.840.1.113883.6.96", "Vinorelbine (substance)", "Vinorelbine",
			"Vinorelbin", "vinorelbine", "Vinorelbina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vitamin E</div>
	 * <div class="de">Tocopherol DL-alpha (E307)</div>
	 * <div class="fr">tocophérol DL-alfa (E307)</div>
	 * <div class="it">Alfa-Tocoferolo (vitamina E, E307)</div>
	 * <!-- @formatter:on -->
	 */
	VITAMIN_E("37237003", "2.16.840.1.113883.6.96", "Vitamin E (substance)", "Vitamin E",
			"Tocopherol DL-alpha (E307)", "tocophérol DL-alfa (E307)",
			"Alfa-Tocoferolo (vitamina E, E307)"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Voriconazole</div>
	 * <div class="de">Voriconazol</div>
	 * <div class="fr">voriconazole</div>
	 * <div class="it">Voriconazolo</div>
	 * <!-- @formatter:on -->
	 */
	VORICONAZOLE("385469007", "2.16.840.1.113883.6.96", "Voriconazole (substance)", "Voriconazole",
			"Voriconazol", "voriconazole", "Voriconazolo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Vortioxetine</div>
	 * <div class="de">Vortioxetin</div>
	 * <div class="fr">vortioxétine</div>
	 * <div class="it">Vortioxetina</div>
	 * <!-- @formatter:on -->
	 */
	VORTIOXETINE("708717008", "2.16.840.1.113883.6.96", "Vortioxetine (substance)", "Vortioxetine",
			"Vortioxetin", "vortioxétine", "Vortioxetina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Warfarin</div>
	 * <div class="de">Warfarin</div>
	 * <div class="fr">warfarine</div>
	 * <div class="it">Warfarin</div>
	 * <!-- @formatter:on -->
	 */
	WARFARIN("372756006", "2.16.840.1.113883.6.96", "Warfarin (substance)", "Warfarin", "Warfarin",
			"warfarine", "Warfarin"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Xylometazoline</div>
	 * <div class="de">Xylometazolin</div>
	 * <div class="fr">xylométazoline</div>
	 * <div class="it">Xilometazolina</div>
	 * <!-- @formatter:on -->
	 */
	XYLOMETAZOLINE("372841007", "2.16.840.1.113883.6.96", "Xylometazoline (substance)",
			"Xylometazoline", "Xylometazolin", "xylométazoline", "Xilometazolina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Yellow fever vaccine</div>
	 * <div class="de">Gelbfieber-Lebend-Impfstoff</div>
	 * <div class="fr">fièvre jaune vaccin vivant</div>
	 * <div class="it">Febbre gialla vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	YELLOW_FEVER_VACCINE("396444004", "2.16.840.1.113883.6.96", "Yellow fever vaccine (substance)",
			"Yellow fever vaccine", "Gelbfieber-Lebend-Impfstoff", "fièvre jaune vaccin vivant",
			"Febbre gialla vaccino vivo"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zanamivir</div>
	 * <div class="de">Zanamivir</div>
	 * <div class="fr">zanamivir</div>
	 * <div class="it">Zanamivir</div>
	 * <!-- @formatter:on -->
	 */
	ZANAMIVIR("387010007", "2.16.840.1.113883.6.96", "Zanamivir (substance)", "Zanamivir",
			"Zanamivir", "zanamivir", "Zanamivir"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zidovudine</div>
	 * <div class="de">Zidovudin</div>
	 * <div class="fr">zidovudine</div>
	 * <div class="it">Zidovudina</div>
	 * <!-- @formatter:on -->
	 */
	ZIDOVUDINE("387151007", "2.16.840.1.113883.6.96", "Zidovudine (substance)", "Zidovudine",
			"Zidovudin", "zidovudine", "Zidovudina"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zinc acetate dihydrate</div>
	 * <div class="de">Zinkdiacetat-2-Wasser</div>
	 * <div class="fr">zinc acétate dihydrate</div>
	 * <div class="it">Zinco acetato diidrato</div>
	 * <!-- @formatter:on -->
	 */
	ZINC_ACETATE_DIHYDRATE("725761005", "2.16.840.1.113883.6.96",
			"Zinc acetate dihydrate (substance)", "Zinc acetate dihydrate", "Zinkdiacetat-2-Wasser",
			"zinc acétate dihydrate", "Zinco acetato diidrato"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zinc pyrithione</div>
	 * <div class="de">Pyrithion zink</div>
	 * <div class="fr">pyrithione zinc</div>
	 * <div class="it">Zinco piritione</div>
	 * <!-- @formatter:on -->
	 */
	ZINC_PYRITHIONE("255954005", "2.16.840.1.113883.6.96", "Zinc pyrithione (substance)",
			"Zinc pyrithione", "Pyrithion zink", "pyrithione zinc", "Zinco piritione"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zoledronic acid</div>
	 * <div class="de">Zoledronsäure</div>
	 * <div class="fr">acide zolédronique (zolédronate)</div>
	 * <div class="it">Acido zoledronico</div>
	 * <!-- @formatter:on -->
	 */
	ZOLEDRONIC_ACID("395926009", "2.16.840.1.113883.6.96", "Zoledronic acid (substance)",
			"Zoledronic acid", "Zoledronsäure", "acide zolédronique (zolédronate)",
			"Acido zoledronico"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zolmitriptan</div>
	 * <div class="de">Zolmitriptan</div>
	 * <div class="fr">zolmitriptan</div>
	 * <div class="it">Zolmitriptan</div>
	 * <!-- @formatter:on -->
	 */
	ZOLMITRIPTAN("363582006", "2.16.840.1.113883.6.96", "Zolmitriptan (substance)", "Zolmitriptan",
			"Zolmitriptan", "zolmitriptan", "Zolmitriptan"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zolpidem</div>
	 * <div class="de">Zolpidem</div>
	 * <div class="fr">zolpidem</div>
	 * <div class="it">Zolpidem</div>
	 * <!-- @formatter:on -->
	 */
	ZOLPIDEM("387569009", "2.16.840.1.113883.6.96", "Zolpidem (substance)", "Zolpidem", "Zolpidem",
			"zolpidem", "Zolpidem"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zonisamide</div>
	 * <div class="de">Zonisamid</div>
	 * <div class="fr">zonisamide</div>
	 * <div class="it">Zonisamide</div>
	 * <!-- @formatter:on -->
	 */
	ZONISAMIDE("125693002", "2.16.840.1.113883.6.96", "Zonisamide (substance)", "Zonisamide",
			"Zonisamid", "zonisamide", "Zonisamide"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zopiclone</div>
	 * <div class="de">Zopiclon</div>
	 * <div class="fr">zopiclone</div>
	 * <div class="it">Zopiclone</div>
	 * <!-- @formatter:on -->
	 */
	ZOPICLONE("395929002", "2.16.840.1.113883.6.96", "Zopiclone (substance)", "Zopiclone",
			"Zopiclon", "zopiclone", "Zopiclone"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Zuclopenthixol</div>
	 * <div class="de">Zuclopenthixol</div>
	 * <div class="fr">zuclopenthixol</div>
	 * <div class="it">Zuclopentixolo</div>
	 * <!-- @formatter:on -->
	 */
	ZUCLOPENTHIXOL("428715002", "2.16.840.1.113883.6.96", "Zuclopenthixol (substance)",
			"Zuclopenthixol", "Zuclopenthixol", "zuclopenthixol", "Zuclopentixolo");

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Abacavir</div>
	 * <div class="de">Code für Abacavir</div>
	 * <div class="fr">Code de abacavir</div>
	 * <div class="it">Code per Abacavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ABACAVIR_CODE = "387005008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Abatacept</div>
	 * <div class="de">Code für Abatacept</div>
	 * <div class="fr">Code de abatacept</div>
	 * <div class="it">Code per Abatacept</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ABATACEPT_CODE = "421777009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Abciximab</div>
	 * <div class="de">Code für Abciximab</div>
	 * <div class="fr">Code de abciximab</div>
	 * <div class="it">Code per Abciximab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ABCIXIMAB_CODE = "386951001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Abemaciclib</div>
	 * <div class="de">Code für Abemaciclib</div>
	 * <div class="fr">Code de abémaciclib</div>
	 * <div class="it">Code per Abemaciclib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ABEMACICLIB_CODE = "761851004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Abiraterone</div>
	 * <div class="de">Code für Abirateron</div>
	 * <div class="fr">Code de abiratérone</div>
	 * <div class="it">Code per Abiraterone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ABIRATERONE_CODE = "699678007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acamprosate</div>
	 * <div class="de">Code für Acamprosat</div>
	 * <div class="fr">Code de acamprosate</div>
	 * <div class="it">Code per Acamprosato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACAMPROSATE_CODE = "391698009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acarbose</div>
	 * <div class="de">Code für Acarbose</div>
	 * <div class="fr">Code de acarbose</div>
	 * <div class="it">Code per Acarbosio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACARBOSE_CODE = "386965004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acemetacin</div>
	 * <div class="de">Code für Acemetacin</div>
	 * <div class="fr">Code de acémétacine</div>
	 * <div class="it">Code per Acemetacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACEMETACIN_CODE = "391704009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acenocoumarol</div>
	 * <div class="de">Code für Acenocoumarol</div>
	 * <div class="fr">Code de acénocoumarol</div>
	 * <div class="it">Code per Acenocumarolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACENOCOUMAROL_CODE = "387457003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acetazolamide</div>
	 * <div class="de">Code für Acetazolamid</div>
	 * <div class="fr">Code de acétazolamide</div>
	 * <div class="it">Code per Acetazolamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACETAZOLAMIDE_CODE = "372709008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acetylcysteine</div>
	 * <div class="de">Code für Acetylcystein</div>
	 * <div class="fr">Code de acétylcystéine</div>
	 * <div class="it">Code per Acetilcisteina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACETYLCYSTEINE_CODE = "387440002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aciclovir</div>
	 * <div class="de">Code für Aciclovir</div>
	 * <div class="fr">Code de aciclovir</div>
	 * <div class="it">Code per Aciclovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACICLOVIR_CODE = "372729009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acipimox</div>
	 * <div class="de">Code für Acipimox</div>
	 * <div class="fr">Code de acipimox</div>
	 * <div class="it">Code per Acipimox</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACIPIMOX_CODE = "391711008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Acitretin</div>
	 * <div class="de">Code für Acitretin</div>
	 * <div class="fr">Code de acitrétine</div>
	 * <div class="it">Code per Acitretina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACITRETIN_CODE = "386938006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aclidinium</div>
	 * <div class="de">Code für Aclidinium-Kation</div>
	 * <div class="fr">Code de aclidinium</div>
	 * <div class="it">Code per Aclidinio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACLIDINIUM_CODE = "703921008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Activated charcoal</div>
	 * <div class="de">Code für Kohle, medizinische</div>
	 * <div class="fr">Code de charbon activé</div>
	 * <div class="it">Code per Carbone attivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ACTIVATED_CHARCOAL_CODE = "32519007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Adalimumab</div>
	 * <div class="de">Code für Adalimumab</div>
	 * <div class="fr">Code de adalimumab</div>
	 * <div class="it">Code per Adalimumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ADALIMUMAB_CODE = "407317001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Adapalene</div>
	 * <div class="de">Code für Adapalen</div>
	 * <div class="fr">Code de adapalène</div>
	 * <div class="it">Code per Adapalene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ADAPALENE_CODE = "386934008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Adefovir</div>
	 * <div class="de">Code für Adefovir</div>
	 * <div class="fr">Code de adéfovir</div>
	 * <div class="it">Code per Adefovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ADEFOVIR_CODE = "412072006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Adenosine</div>
	 * <div class="de">Code für Adenosin</div>
	 * <div class="fr">Code de adénosine</div>
	 * <div class="it">Code per Adenosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ADENOSINE_CODE = "35431001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Afatinib</div>
	 * <div class="de">Code für Afatinib</div>
	 * <div class="fr">Code de afatinib</div>
	 * <div class="it">Code per Afatinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AFATINIB_CODE = "703579002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aflibercept</div>
	 * <div class="de">Code für Aflibercept</div>
	 * <div class="fr">Code de aflibercept</div>
	 * <div class="it">Code per Aflibercept</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AFLIBERCEPT_CODE = "703840003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Agalsidase alfa</div>
	 * <div class="de">Code für Agalsidase alfa</div>
	 * <div class="fr">Code de agalsidase alfa</div>
	 * <div class="it">Code per Agalsidasi alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AGALSIDASE_ALFA_CODE = "424905009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Agalsidase beta</div>
	 * <div class="de">Code für Agalsidase beta</div>
	 * <div class="fr">Code de agalsidase bêta</div>
	 * <div class="it">Code per Agalsidasi beta</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AGALSIDASE_BETA_CODE = "424725004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Agomelatine</div>
	 * <div class="de">Code für Agomelatin</div>
	 * <div class="fr">Code de agomélatine</div>
	 * <div class="it">Code per Agomelatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AGOMELATINE_CODE = "698012009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alanine</div>
	 * <div class="de">Code für Alanin</div>
	 * <div class="fr">Code de alanine</div>
	 * <div class="it">Code per Alanina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALANINE_CODE = "58753009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alanylglutamine</div>
	 * <div class="de">Code für Alanyl glutamin</div>
	 * <div class="fr">Code de alanyl glutamine</div>
	 * <div class="it">Code per Alanil glutammina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALANYLGLUTAMINE_CODE = "703391005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Albendazole</div>
	 * <div class="de">Code für Albendazol</div>
	 * <div class="fr">Code de albendazole</div>
	 * <div class="it">Code per Albendazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALBENDAZOLE_CODE = "387558006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Albiglutide</div>
	 * <div class="de">Code für Albiglutid</div>
	 * <div class="fr">Code de albiglutide</div>
	 * <div class="it">Code per Albiglutide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALBIGLUTIDE_CODE = "703129009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Albumin</div>
	 * <div class="de">Code für Albumine</div>
	 * <div class="fr">Code de albumine</div>
	 * <div class="it">Code per Albumina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALBUMIN_CODE = "52454007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Albutrepenonacog alfa</div>
	 * <div class="de">Code für Albutrepenonacog alfa</div>
	 * <div class="fr">Code de albutrépénonacog alfa</div>
	 * <div class="it">Code per Albutrepenonacog alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALBUTREPENONACOG_ALFA_CODE = "718928008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alcohol</div>
	 * <div class="de">Code für Ethanol</div>
	 * <div class="fr">Code de éthanol</div>
	 * <div class="it">Code per Alcol etilico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALCOHOL_CODE = "53041004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aldesleukin</div>
	 * <div class="de">Code für Aldesleukin</div>
	 * <div class="fr">Code de aldesleukine</div>
	 * <div class="it">Code per Aldesleuchina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALDESLEUKIN_CODE = "386917000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alectinib</div>
	 * <div class="de">Code für Alectinib</div>
	 * <div class="fr">Code de alectinib</div>
	 * <div class="it">Code per Alectinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALECTINIB_CODE = "716039000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alemtuzumab</div>
	 * <div class="de">Code für Alemtuzumab</div>
	 * <div class="fr">Code de alemtuzumab</div>
	 * <div class="it">Code per Alemtuzumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALEMTUZUMAB_CODE = "129472003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alendronic acid</div>
	 * <div class="de">Code für Alendronsäure</div>
	 * <div class="fr">Code de acide alendronique</div>
	 * <div class="it">Code per Acido Alendronico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALENDRONIC_ACID_CODE = "391730008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alfentanil</div>
	 * <div class="de">Code für Alfentanil</div>
	 * <div class="fr">Code de alfentanil</div>
	 * <div class="it">Code per Alfentanil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALFENTANIL_CODE = "387560008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alfuzosin</div>
	 * <div class="de">Code für Alfuzosin</div>
	 * <div class="fr">Code de alfuzosine</div>
	 * <div class="it">Code per Alfuzosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALFUZOSIN_CODE = "395954002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alirocumab</div>
	 * <div class="de">Code für Alirocumab</div>
	 * <div class="fr">Code de alirocumab</div>
	 * <div class="it">Code per Alirocumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALIROCUMAB_CODE = "715186005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aliskiren</div>
	 * <div class="de">Code für Aliskiren</div>
	 * <div class="fr">Code de aliskirène</div>
	 * <div class="it">Code per Aliskiren</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALISKIREN_CODE = "426725002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Allopurinol</div>
	 * <div class="de">Code für Allopurinol</div>
	 * <div class="fr">Code de allopurinol</div>
	 * <div class="it">Code per Allopurinolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALLOPURINOL_CODE = "387135004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Almotriptan</div>
	 * <div class="de">Code für Almotriptan</div>
	 * <div class="fr">Code de almotriptan</div>
	 * <div class="it">Code per Almotriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALMOTRIPTAN_CODE = "363569003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alogliptin</div>
	 * <div class="de">Code für Alogliptin</div>
	 * <div class="fr">Code de alogliptine</div>
	 * <div class="it">Code per Alogliptin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALOGLIPTIN_CODE = "702799001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alprazolam</div>
	 * <div class="de">Code für Alprazolam</div>
	 * <div class="fr">Code de alprazolam</div>
	 * <div class="it">Code per Alprazolam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALPRAZOLAM_CODE = "386983007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alprostadil</div>
	 * <div class="de">Code für Alprostadil</div>
	 * <div class="fr">Code de alprostadil</div>
	 * <div class="it">Code per Alprostadil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALPROSTADIL_CODE = "48988008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Alteplase</div>
	 * <div class="de">Code für Alteplase</div>
	 * <div class="fr">Code de altéplase</div>
	 * <div class="it">Code per Alteplase</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALTEPLASE_CODE = "387152000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aluminium hydroxide</div>
	 * <div class="de">Code für Aluminiumoxid, wasserhaltig (Algeldrat)</div>
	 * <div class="fr">Code de aluminium oxyde hydrate (algeldrate)</div>
	 * <div class="it">Code per Idrossido di alluminio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ALUMINIUM_HYDROXIDE_CODE = "273944007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amantadine</div>
	 * <div class="de">Code für Amantadin</div>
	 * <div class="fr">Code de amantadine</div>
	 * <div class="it">Code per Amantadina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMANTADINE_CODE = "372763006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ambrisentan</div>
	 * <div class="de">Code für Ambrisentan</div>
	 * <div class="fr">Code de ambrisentan</div>
	 * <div class="it">Code per Ambrisentan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMBRISENTAN_CODE = "428159003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ambroxol</div>
	 * <div class="de">Code für Ambroxol</div>
	 * <div class="fr">Code de ambroxol</div>
	 * <div class="it">Code per Ambroxolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMBROXOL_CODE = "698024002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amikacin</div>
	 * <div class="de">Code für Amikacin</div>
	 * <div class="fr">Code de amikacine</div>
	 * <div class="it">Code per Amikacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMIKACIN_CODE = "387266001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amiloride</div>
	 * <div class="de">Code für Amilorid</div>
	 * <div class="fr">Code de amiloride</div>
	 * <div class="it">Code per Amiloride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMILORIDE_CODE = "387503008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aminophylline</div>
	 * <div class="de">Code für Aminophyllin</div>
	 * <div class="fr">Code de aminophylline</div>
	 * <div class="it">Code per Aminofillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMINOPHYLLINE_CODE = "373508009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amiodarone</div>
	 * <div class="de">Code für Amiodaron</div>
	 * <div class="fr">Code de amiodarone</div>
	 * <div class="it">Code per Amiodarone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMIODARONE_CODE = "372821002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amisulpride</div>
	 * <div class="de">Code für Amisulprid</div>
	 * <div class="fr">Code de amisulpride</div>
	 * <div class="it">Code per Amisulpride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMISULPRIDE_CODE = "391761004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amitriptyline</div>
	 * <div class="de">Code für Amitriptylin</div>
	 * <div class="fr">Code de amitriptyline</div>
	 * <div class="it">Code per Amitriptilina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMITRIPTYLINE_CODE = "372726002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amlodipine</div>
	 * <div class="de">Code für Amlodipin</div>
	 * <div class="fr">Code de amlodipine</div>
	 * <div class="it">Code per Amlodipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMLODIPINE_CODE = "386864001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amorolfine</div>
	 * <div class="de">Code für Amorolfin</div>
	 * <div class="fr">Code de amorolfine</div>
	 * <div class="it">Code per Amorolfina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMOROLFINE_CODE = "391769002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amoxicillin</div>
	 * <div class="de">Code für Amoxicillin</div>
	 * <div class="fr">Code de amoxicilline</div>
	 * <div class="it">Code per Amoxicillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMOXICILLIN_CODE = "372687004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amoxicillin sodium</div>
	 * <div class="de">Code für Amoxicillin-Natrium</div>
	 * <div class="fr">Code de amoxicilline sodique</div>
	 * <div class="it">Code per Amoxicillina sodica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMOXICILLIN_SODIUM_CODE = "427483001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amoxicillin trihydrate</div>
	 * <div class="de">Code für Amoxicillin-3-Wasser</div>
	 * <div class="fr">Code de amoxicilline trihydrate</div>
	 * <div class="it">Code per Amoxicillina triidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMOXICILLIN_TRIHYDRATE_CODE = "96068000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amphotericin B</div>
	 * <div class="de">Code für Amphotericin B</div>
	 * <div class="fr">Code de amphotéricine B</div>
	 * <div class="it">Code per Amfotericina B</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMPHOTERICIN_B_CODE = "77703004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ampicillin</div>
	 * <div class="de">Code für Ampicillin</div>
	 * <div class="fr">Code de ampicilline</div>
	 * <div class="it">Code per Ampicillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMPICILLIN_CODE = "387170002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Amylase</div>
	 * <div class="de">Code für Amylase</div>
	 * <div class="fr">Code de amylase</div>
	 * <div class="it">Code per Amilasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AMYLASE_CODE = "387031005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Anagrelide</div>
	 * <div class="de">Code für Anagrelid</div>
	 * <div class="fr">Code de anagrélide</div>
	 * <div class="it">Code per Anagrelide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANAGRELIDE_CODE = "372561005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Anakinra</div>
	 * <div class="de">Code für Anakinra</div>
	 * <div class="fr">Code de anakinra</div>
	 * <div class="it">Code per Anakinra</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANAKINRA_CODE = "385549000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Anastrozole</div>
	 * <div class="de">Code für Anastrozol</div>
	 * <div class="fr">Code de anastrozole</div>
	 * <div class="it">Code per Anastrozolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANASTROZOLE_CODE = "386910003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Anetholtrithion</div>
	 * <div class="de">Code für Anetholtrithion</div>
	 * <div class="fr">Code de anétholtrithione</div>
	 * <div class="it">Code per Anetoltritione</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANETHOLTRITHION_CODE = "703112006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Anidulafungin</div>
	 * <div class="de">Code für Anidulafungin</div>
	 * <div class="fr">Code de anidulafungine</div>
	 * <div class="it">Code per Anidulafungina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANIDULAFUNGIN_CODE = "422157006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Antazoline</div>
	 * <div class="de">Code für Antazolin</div>
	 * <div class="fr">Code de antazoline</div>
	 * <div class="it">Code per Antazolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANTAZOLINE_CODE = "373544004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Antilymphocyte immunoglobulin</div>
	 * <div class="de">Code für Immunglobuline anti-Lymphozyten human</div>
	 * <div class="fr">Code de immunoglobuline anti-lymphocytes humains</div>
	 * <div class="it">Code per Immunoglobulina anti-linfociti T umani</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANTILYMPHOCYTE_IMMUNOGLOBULIN_CODE = "391784006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Antithymocyte immunoglobulin</div>
	 * <div class="de">Code für Immunglobulin anti-T-Lymphozyten human</div>
	 * <div class="fr">Code de immunoglobuline anti lymphocytes T humains</div>
	 * <div class="it">Code per Immunoglobulina antitimociti umani</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANTITHYMOCYTE_IMMUNOGLOBULIN_CODE = "768651008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Apixaban</div>
	 * <div class="de">Code für Apixaban</div>
	 * <div class="fr">Code de apixaban</div>
	 * <div class="it">Code per Apixaban</div>
	 * <!-- @formatter:on -->
	 */
	public static final String APIXABAN_CODE = "698090000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Apomorphine</div>
	 * <div class="de">Code für Apomorphin</div>
	 * <div class="fr">Code de apomorphine</div>
	 * <div class="it">Code per Apomorfina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String APOMORPHINE_CODE = "387375001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aprepitant</div>
	 * <div class="de">Code für Aprepitant</div>
	 * <div class="fr">Code de aprépitant</div>
	 * <div class="it">Code per Aprepitant</div>
	 * <!-- @formatter:on -->
	 */
	public static final String APREPITANT_CODE = "409205009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aprotinin</div>
	 * <div class="de">Code für Aprotinin</div>
	 * <div class="fr">Code de aprotinine</div>
	 * <div class="it">Code per Aprotinina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String APROTININ_CODE = "386961008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Argatroban</div>
	 * <div class="de">Code für Argatroban</div>
	 * <div class="fr">Code de argatroban</div>
	 * <div class="it">Code per Argatroban</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARGATROBAN_CODE = "116508003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Arginine</div>
	 * <div class="de">Code für Arginin</div>
	 * <div class="fr">Code de arginine</div>
	 * <div class="it">Code per Arginina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARGININE_CODE = "52625008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Argipressin</div>
	 * <div class="de">Code für Argipressin (Vasopressin)</div>
	 * <div class="fr">Code de argipressine (Vasopressine)</div>
	 * <div class="it">Code per Argipressina (Vasopressina)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARGIPRESSIN_CODE = "421078009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aripiprazole</div>
	 * <div class="de">Code für Aripiprazol</div>
	 * <div class="fr">Code de aripiprazole</div>
	 * <div class="it">Code per Aripirazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARIPIPRAZOLE_CODE = "406784005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Arsenic trioxide</div>
	 * <div class="de">Code für Arsentrioxid</div>
	 * <div class="fr">Code de trioxyde d'arsenic</div>
	 * <div class="it">Code per Arsenico triossido</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARSENIC_TRIOXIDE_CODE = "72251000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Artemether</div>
	 * <div class="de">Code für Artemether</div>
	 * <div class="fr">Code de artéméther</div>
	 * <div class="it">Code per Artemetere</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARTEMETHER_CODE = "420578008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Artesunate</div>
	 * <div class="de">Code für Artesunat</div>
	 * <div class="fr">Code de artésunate</div>
	 * <div class="it">Code per Artesunato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARTESUNATE_CODE = "432410005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Articaine</div>
	 * <div class="de">Code für Articain</div>
	 * <div class="fr">Code de articaïne</div>
	 * <div class="it">Code per Articaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ARTICAINE_CODE = "703107005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ascorbic acid</div>
	 * <div class="de">Code für Ascorbinsäure (Vitamin C, E300)</div>
	 * <div class="fr">Code de acide ascorbique (Vitamine C, E300)</div>
	 * <div class="it">Code per Acido ascorbico (Vitamina C, E300)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ASCORBIC_ACID_CODE = "43706004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Asparaginase</div>
	 * <div class="de">Code für Asparaginase</div>
	 * <div class="fr">Code de asparaginase</div>
	 * <div class="it">Code per Asparaginasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ASPARAGINASE_CODE = "371014004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aspartic acid</div>
	 * <div class="de">Code für Aspartinsäure</div>
	 * <div class="fr">Code de acide aspartique</div>
	 * <div class="it">Code per Acido aspartico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ASPARTIC_ACID_CODE = "44970006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aspirin</div>
	 * <div class="de">Code für Acetylsalicylsäure</div>
	 * <div class="fr">Code de acide acétylsalicylique</div>
	 * <div class="it">Code per Acido acetilsalicilico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ASPIRIN_CODE = "387458008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atazanavir</div>
	 * <div class="de">Code für Atazanavir</div>
	 * <div class="fr">Code de atazanavir</div>
	 * <div class="it">Code per Atazanavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATAZANAVIR_CODE = "413592000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atenolol</div>
	 * <div class="de">Code für Atenolol</div>
	 * <div class="fr">Code de aténolol</div>
	 * <div class="it">Code per Atenololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATENOLOL_CODE = "387506000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atezolizumab</div>
	 * <div class="de">Code für Atezolizumab</div>
	 * <div class="fr">Code de atézolizumab</div>
	 * <div class="it">Code per Atezolizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATEZOLIZUMAB_CODE = "719371003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atomoxetine</div>
	 * <div class="de">Code für Atomoxetin</div>
	 * <div class="fr">Code de atomoxétine</div>
	 * <div class="it">Code per Atomoxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATOMOXETINE_CODE = "407037005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atorvastatin</div>
	 * <div class="de">Code für Atorvastatin</div>
	 * <div class="fr">Code de atorvastatine</div>
	 * <div class="it">Code per Atorvastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATORVASTATIN_CODE = "373444002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atosiban</div>
	 * <div class="de">Code für Atosiban</div>
	 * <div class="fr">Code de atosiban</div>
	 * <div class="it">Code per Atosiban</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATOSIBAN_CODE = "391792002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atovaquone</div>
	 * <div class="de">Code für Atovaquon</div>
	 * <div class="fr">Code de atovaquone</div>
	 * <div class="it">Code per Atovaquone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATOVAQUONE_CODE = "386899002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atracurium</div>
	 * <div class="de">Code für Atracurium</div>
	 * <div class="fr">Code de atracurium</div>
	 * <div class="it">Code per Atracurio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATRACURIUM_CODE = "372835000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Atropine</div>
	 * <div class="de">Code für Atropin</div>
	 * <div class="fr">Code de atropine</div>
	 * <div class="it">Code per Atropina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ATROPINE_CODE = "372832002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Avanafil</div>
	 * <div class="de">Code für Avanafil</div>
	 * <div class="fr">Code de avanafil</div>
	 * <div class="it">Code per Avafanil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AVANAFIL_CODE = "703956007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Avelumab</div>
	 * <div class="de">Code für Avelumab</div>
	 * <div class="fr">Code de avélumab</div>
	 * <div class="it">Code per Avelumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AVELUMAB_CODE = "733055009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Azacitidine</div>
	 * <div class="de">Code für Azacitidin</div>
	 * <div class="fr">Code de azacitidine</div>
	 * <div class="it">Code per Azacitidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AZACITIDINE_CODE = "412328000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Azathioprine</div>
	 * <div class="de">Code für Azathioprin</div>
	 * <div class="fr">Code de azathioprine</div>
	 * <div class="it">Code per Azatioprina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AZATHIOPRINE_CODE = "372574004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Azelaic acid</div>
	 * <div class="de">Code für Azelainsäure</div>
	 * <div class="fr">Code de acide azélaïque</div>
	 * <div class="it">Code per Acido azelaico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AZELAIC_ACID_CODE = "386936005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Azelastine</div>
	 * <div class="de">Code für Azelastin</div>
	 * <div class="fr">Code de azélastine</div>
	 * <div class="it">Code per Azelastina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AZELASTINE_CODE = "372520005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Azithromycin</div>
	 * <div class="de">Code für Azithromycin</div>
	 * <div class="fr">Code de azithromycine</div>
	 * <div class="it">Code per Azitromicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AZITHROMYCIN_CODE = "387531004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Aztreonam</div>
	 * <div class="de">Code für Aztreonam</div>
	 * <div class="fr">Code de aztréonam</div>
	 * <div class="it">Code per Aztreonam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AZTREONAM_CODE = "387386004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bacitracin</div>
	 * <div class="de">Code für Bacitracin</div>
	 * <div class="fr">Code de bacitracine</div>
	 * <div class="it">Code per Bacitracina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BACITRACIN_CODE = "5220000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Baclofen</div>
	 * <div class="de">Code für Baclofen</div>
	 * <div class="fr">Code de baclofène</div>
	 * <div class="it">Code per Baclofene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BACLOFEN_CODE = "387342009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Basiliximab</div>
	 * <div class="de">Code für Basiliximab</div>
	 * <div class="fr">Code de basiliximab</div>
	 * <div class="it">Code per Basiliximab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BASILIXIMAB_CODE = "386978004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Beclometasone</div>
	 * <div class="de">Code für Beclometason</div>
	 * <div class="fr">Code de béclométasone</div>
	 * <div class="it">Code per Beclometasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BECLOMETASONE_CODE = "116574000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Beclometasone dipropionate</div>
	 * <div class="de">Code für Beclometason dipropionat</div>
	 * <div class="fr">Code de béclométasone dipropionate</div>
	 * <div class="it">Code per Beclometasone dipropionato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BECLOMETASONE_DIPROPIONATE_CODE = "116575004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Belatacept</div>
	 * <div class="de">Code für Belatacept</div>
	 * <div class="fr">Code de bélatacept</div>
	 * <div class="it">Code per Belatacept</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BELATACEPT_CODE = "713475001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Belimumab</div>
	 * <div class="de">Code für Belimumab</div>
	 * <div class="fr">Code de bélimumab</div>
	 * <div class="it">Code per Belimumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BELIMUMAB_CODE = "449043000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Benazepril</div>
	 * <div class="de">Code für Benazepril</div>
	 * <div class="fr">Code de bénazépril</div>
	 * <div class="it">Code per Benazepril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BENAZEPRIL_CODE = "372511001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bendamustine</div>
	 * <div class="de">Code für Bendamustin</div>
	 * <div class="fr">Code de bendamustine</div>
	 * <div class="it">Code per Bendamustina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BENDAMUSTINE_CODE = "428012008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Benserazide</div>
	 * <div class="de">Code für Benserazid</div>
	 * <div class="fr">Code de bensérazide</div>
	 * <div class="it">Code per Benserazide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BENSERAZIDE_CODE = "391821005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Benzocaine</div>
	 * <div class="de">Code für Benzocain</div>
	 * <div class="fr">Code de benzocaïne</div>
	 * <div class="it">Code per Benzocaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BENZOCAINE_CODE = "387357002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Benzydamine</div>
	 * <div class="de">Code für Benzydamin</div>
	 * <div class="fr">Code de benzydamine</div>
	 * <div class="it">Code per Benzidamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BENZYDAMINE_CODE = "421319000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Benzylpenicillin</div>
	 * <div class="de">Code für Benzylpenicillin</div>
	 * <div class="fr">Code de benzylpénicilline</div>
	 * <div class="it">Code per Benzilpenicillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BENZYLPENICILLIN_CODE = "323389000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Betahistine</div>
	 * <div class="de">Code für Betahistin</div>
	 * <div class="fr">Code de bétahistine</div>
	 * <div class="it">Code per Betaistina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BETAHISTINE_CODE = "418067008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Betaine</div>
	 * <div class="de">Code für Betain</div>
	 * <div class="fr">Code de bétaine</div>
	 * <div class="it">Code per Betaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BETAINE_CODE = "43356007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Betamethasone</div>
	 * <div class="de">Code für Betamethason</div>
	 * <div class="fr">Code de bétaméthasone</div>
	 * <div class="it">Code per Betametasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BETAMETHASONE_CODE = "116571008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Betaxolol</div>
	 * <div class="de">Code für Betaxolol</div>
	 * <div class="fr">Code de bétaxolol</div>
	 * <div class="it">Code per Betaxololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BETAXOLOL_CODE = "409276006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Beta-galactosidase</div>
	 * <div class="de">Code für Tilactase</div>
	 * <div class="fr">Code de tilactase</div>
	 * <div class="it">Code per Tilattasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BETA_GALACTOSIDASE_CODE = "28530008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bevacizumab</div>
	 * <div class="de">Code für Bevacizumab</div>
	 * <div class="fr">Code de bévacizumab</div>
	 * <div class="it">Code per Bevacizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BEVACIZUMAB_CODE = "409406007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bezafibrate</div>
	 * <div class="de">Code für Bezafibrat</div>
	 * <div class="fr">Code de bézafibrate</div>
	 * <div class="it">Code per Bezafibrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BEZAFIBRATE_CODE = "396025003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bicalutamide</div>
	 * <div class="de">Code für Bicalutamid</div>
	 * <div class="fr">Code de bicalutamide</div>
	 * <div class="it">Code per Bicalutamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BICALUTAMIDE_CODE = "386908000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bictegravir</div>
	 * <div class="de">Code für Bictegravir</div>
	 * <div class="fr">Code de bictégravir</div>
	 * <div class="it">Code per Bictegravir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BICTEGRAVIR_CODE = "772193003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bilastine</div>
	 * <div class="de">Code für Bilastin</div>
	 * <div class="fr">Code de bilastine</div>
	 * <div class="it">Code per Bilastina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BILASTINE_CODE = "697973006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bimatoprost</div>
	 * <div class="de">Code für Bimatoprost</div>
	 * <div class="fr">Code de bimatoprost</div>
	 * <div class="it">Code per Bimatoprost</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BIMATOPROST_CODE = "129492005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Binimetinib</div>
	 * <div class="de">Code für Binimetinib</div>
	 * <div class="fr">Code de binimétinib</div>
	 * <div class="it">Code per Binimetinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BINIMETINIB_CODE = "772195005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Biotin</div>
	 * <div class="de">Code für Biotin</div>
	 * <div class="fr">Code de biotine</div>
	 * <div class="it">Code per Biotina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BIOTIN_CODE = "8919000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Biperiden</div>
	 * <div class="de">Code für Biperiden</div>
	 * <div class="fr">Code de bipéridène</div>
	 * <div class="it">Code per Biperidene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BIPERIDEN_CODE = "387359004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bisacodyl</div>
	 * <div class="de">Code für Bisacodyl</div>
	 * <div class="fr">Code de bisacodyl</div>
	 * <div class="it">Code per Bisacodile</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BISACODYL_CODE = "387075009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bisoprolol</div>
	 * <div class="de">Code für Bisoprolol</div>
	 * <div class="fr">Code de bisoprolol</div>
	 * <div class="it">Code per Bisoprololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BISOPROLOL_CODE = "386868003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bivalirudin</div>
	 * <div class="de">Code für Bivalirudin</div>
	 * <div class="fr">Code de bivalirudine</div>
	 * <div class="it">Code per Bivalirudina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BIVALIRUDIN_CODE = "129498009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bleomycin</div>
	 * <div class="de">Code für Bleomycin</div>
	 * <div class="fr">Code de bléomycine</div>
	 * <div class="it">Code per Bleomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BLEOMYCIN_CODE = "372843005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bortezomib</div>
	 * <div class="de">Code für Bortezomib</div>
	 * <div class="fr">Code de bortézomib</div>
	 * <div class="it">Code per Bortezomib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BORTEZOMIB_CODE = "407097007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bosentan</div>
	 * <div class="de">Code für Bosentan</div>
	 * <div class="fr">Code de bosentan</div>
	 * <div class="it">Code per Bosentan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BOSENTAN_CODE = "385559004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bosutinib</div>
	 * <div class="de">Code für Bosutinib</div>
	 * <div class="fr">Code de bosutinib</div>
	 * <div class="it">Code per Bosutinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BOSUTINIB_CODE = "703128001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Botulinum toxin type A</div>
	 * <div class="de">Code für Botulinumtoxin Typ A</div>
	 * <div class="fr">Code de toxine botulique de type A</div>
	 * <div class="it">Code per Tossina botulinica tipo A</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BOTULINUM_TOXIN_TYPE_A_CODE = "108890005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Brentuximab vedotin</div>
	 * <div class="de">Code für Brentuximab vedotin</div>
	 * <div class="fr">Code de brentuximab védotine</div>
	 * <div class="it">Code per Brentuximab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BRENTUXIMAB_VEDOTIN_CODE = "713395006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Brexpiprazole</div>
	 * <div class="de">Code für Brexpiprazol</div>
	 * <div class="fr">Code de brexpiprazole</div>
	 * <div class="it">Code per Brexpiprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BREXPIPRAZOLE_CODE = "716069007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Brimonidine</div>
	 * <div class="de">Code für Brimonidin</div>
	 * <div class="fr">Code de brimonidine</div>
	 * <div class="it">Code per Brimonidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BRIMONIDINE_CODE = "372547000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Brinzolamide</div>
	 * <div class="de">Code für Brinzolamid</div>
	 * <div class="fr">Code de brinzolamide</div>
	 * <div class="it">Code per Brinzolamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BRINZOLAMIDE_CODE = "386925003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Brivaracetam</div>
	 * <div class="de">Code für Brivaracetam</div>
	 * <div class="fr">Code de brivaracétam</div>
	 * <div class="it">Code per Brivaracetam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BRIVARACETAM_CODE = "420813001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Brivudine</div>
	 * <div class="de">Code für Brivudin</div>
	 * <div class="fr">Code de brivudine</div>
	 * <div class="it">Code per Brivudina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BRIVUDINE_CODE = "698049003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bromazepam</div>
	 * <div class="de">Code für Bromazepam</div>
	 * <div class="fr">Code de bromazépam</div>
	 * <div class="it">Code per Bromazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BROMAZEPAM_CODE = "387571009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bromfenac</div>
	 * <div class="de">Code für Bromfenac</div>
	 * <div class="fr">Code de bromfénac</div>
	 * <div class="it">Code per Bromfenac</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BROMFENAC_CODE = "108520008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bromocriptine</div>
	 * <div class="de">Code für Bromocriptin</div>
	 * <div class="fr">Code de bromocriptine</div>
	 * <div class="it">Code per Bromocriptina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BROMOCRIPTINE_CODE = "387039007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Budesonide</div>
	 * <div class="de">Code für Budesonid</div>
	 * <div class="fr">Code de budésonide</div>
	 * <div class="it">Code per Budesonide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BUDESONIDE_CODE = "395726003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bufexamac</div>
	 * <div class="de">Code für Bufexamac</div>
	 * <div class="fr">Code de bufexamac</div>
	 * <div class="it">Code per Bufexamac</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BUFEXAMAC_CODE = "273952005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bupivacaine</div>
	 * <div class="de">Code für Bupivacain</div>
	 * <div class="fr">Code de bupivacaïne</div>
	 * <div class="it">Code per Bupivacaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BUPIVACAINE_CODE = "387150008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Buprenorphine</div>
	 * <div class="de">Code für Buprenorphin</div>
	 * <div class="fr">Code de buprénorphine</div>
	 * <div class="it">Code per Buprenorfina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BUPRENORPHINE_CODE = "387173000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Bupropion</div>
	 * <div class="de">Code für Bupropion</div>
	 * <div class="fr">Code de bupropion</div>
	 * <div class="it">Code per Buproprione</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BUPROPION_CODE = "387564004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Buserelin</div>
	 * <div class="de">Code für Buserelin</div>
	 * <div class="fr">Code de buséréline</div>
	 * <div class="it">Code per Buserelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BUSERELIN_CODE = "395744006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cabazitaxel</div>
	 * <div class="de">Code für Cabazitaxel</div>
	 * <div class="fr">Code de cabazitaxel</div>
	 * <div class="it">Code per Cabazitaxel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CABAZITAXEL_CODE = "446706007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cabergoline</div>
	 * <div class="de">Code für Cabergolin</div>
	 * <div class="fr">Code de cabergoline</div>
	 * <div class="it">Code per Cabergolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CABERGOLINE_CODE = "386979007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Caffeine</div>
	 * <div class="de">Code für Coffein</div>
	 * <div class="fr">Code de caféine</div>
	 * <div class="it">Code per Caffeina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CAFFEINE_CODE = "255641001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcipotriol</div>
	 * <div class="de">Code für Calcipotriol</div>
	 * <div class="fr">Code de calcipotriol</div>
	 * <div class="it">Code per Calcipotriolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIPOTRIOL_CODE = "395766004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcitriol</div>
	 * <div class="de">Code für Calcitriol</div>
	 * <div class="fr">Code de calcitriol</div>
	 * <div class="it">Code per Calcitriolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCITRIOL_CODE = "259333003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium acetate</div>
	 * <div class="de">Code für Calcium acetat</div>
	 * <div class="fr">Code de calcium acétate</div>
	 * <div class="it">Code per Calcio acetato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_ACETATE_CODE = "387019008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium alginate solution</div>
	 * <div class="de">Code für Calcium alginat</div>
	 * <div class="fr">Code de alginate calcique</div>
	 * <div class="it">Code per Calcio alginato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_ALGINATE_SOLUTION_CODE = "256620003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium carbaspirin</div>
	 * <div class="de">Code für Carbasalat calcium</div>
	 * <div class="fr">Code de carbasalate calcique</div>
	 * <div class="it">Code per Calcio carbasalato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_CARBASPIRIN_CODE = "111122008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium carbonate</div>
	 * <div class="de">Code für Calcium carbonat</div>
	 * <div class="fr">Code de calcium carbonate</div>
	 * <div class="it">Code per Calcio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_CARBONATE_CODE = "387307005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium chloride</div>
	 * <div class="de">Code für Calciumchlorid</div>
	 * <div class="fr">Code de calcium chlorure</div>
	 * <div class="it">Code per Calcio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_CHLORIDE_CODE = "387377009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium glubionate</div>
	 * <div class="de">Code für Calcium glubionat</div>
	 * <div class="fr">Code de calcium glubionate</div>
	 * <div class="it">Code per Calcio glubionato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_GLUBIONATE_CODE = "32445001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium gluconate</div>
	 * <div class="de">Code für Calcium gluconat</div>
	 * <div class="fr">Code de calcium gluconate</div>
	 * <div class="it">Code per Calcio gluconato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_GLUCONATE_CODE = "387292008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Calcium leucovorin</div>
	 * <div class="de">Code für Calcium folinat</div>
	 * <div class="fr">Code de acide folinique calcique</div>
	 * <div class="it">Code per Calcio folinato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CALCIUM_LEUCOVORIN_CODE = "126223008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Canagliflozin</div>
	 * <div class="de">Code für Canagliflozin</div>
	 * <div class="fr">Code de canagliflozine</div>
	 * <div class="it">Code per Canaglifozin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CANAGLIFLOZIN_CODE = "703676004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Canakinumab</div>
	 * <div class="de">Code für Canakinumab</div>
	 * <div class="fr">Code de canakinumab</div>
	 * <div class="it">Code per Canakinumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CANAKINUMAB_CODE = "698091001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Candesartan</div>
	 * <div class="de">Code für Candesartan</div>
	 * <div class="fr">Code de candésartan</div>
	 * <div class="it">Code per Candesartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CANDESARTAN_CODE = "372512008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cangrelor</div>
	 * <div class="de">Code für Cangrelor</div>
	 * <div class="fr">Code de cangrélor</div>
	 * <div class="it">Code per Cangrelor</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CANGRELOR_CODE = "716118009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cannabidiol</div>
	 * <div class="de">Code für Cannabidiol (CBD)</div>
	 * <div class="fr">Code de cannabidiol (CBD)</div>
	 * <div class="it">Code per Cannabidiolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CANNABIDIOL_CODE = "96223000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Capecitabine</div>
	 * <div class="de">Code für Capecitabin</div>
	 * <div class="fr">Code de capécitabine</div>
	 * <div class="it">Code per Capecitabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CAPECITABINE_CODE = "386906001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Capsaicin</div>
	 * <div class="de">Code für Capsaicin</div>
	 * <div class="fr">Code de capsaïcine</div>
	 * <div class="it">Code per Capsaicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CAPSAICIN_CODE = "95995002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Captopril</div>
	 * <div class="de">Code für Captopril</div>
	 * <div class="fr">Code de captopril</div>
	 * <div class="it">Code per Captopril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CAPTOPRIL_CODE = "387160004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carbachol</div>
	 * <div class="de">Code für Carbachol</div>
	 * <div class="fr">Code de carbachol</div>
	 * <div class="it">Code per Carbacolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBACHOL_CODE = "387183001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carbamazepine</div>
	 * <div class="de">Code für Carbamazepin</div>
	 * <div class="fr">Code de carbamazépine</div>
	 * <div class="it">Code per Carbamazepina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBAMAZEPINE_CODE = "387222003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carbetocin</div>
	 * <div class="de">Code für Carbetocin</div>
	 * <div class="fr">Code de carbétocine</div>
	 * <div class="it">Code per Carbetocina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBETOCIN_CODE = "425003007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carbidopa</div>
	 * <div class="de">Code für Carbidopa</div>
	 * <div class="fr">Code de carbidopa</div>
	 * <div class="it">Code per Carbidopa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBIDOPA_CODE = "73579000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carbimazole</div>
	 * <div class="de">Code für Carbimazol</div>
	 * <div class="fr">Code de carbimazole</div>
	 * <div class="it">Code per Carbimazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBIMAZOLE_CODE = "395831005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carbocisteine</div>
	 * <div class="de">Code für Carbocistein</div>
	 * <div class="fr">Code de carbocistéine</div>
	 * <div class="it">Code per Carbocisteina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBOCISTEINE_CODE = "395842001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carboplatin</div>
	 * <div class="de">Code für Carboplatin</div>
	 * <div class="fr">Code de carboplatine</div>
	 * <div class="it">Code per Carboplatino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARBOPLATIN_CODE = "386905002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carfilzomib</div>
	 * <div class="de">Code für Carfilzomib</div>
	 * <div class="fr">Code de carfilzomib</div>
	 * <div class="it">Code per Carfilzomib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARFILZOMIB_CODE = "713463006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cariprazine</div>
	 * <div class="de">Code für Cariprazin</div>
	 * <div class="fr">Code de cariprazine</div>
	 * <div class="it">Code per Cariprazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARIPRAZINE_CODE = "715295006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carmustine</div>
	 * <div class="de">Code für Carmustin</div>
	 * <div class="fr">Code de carmustine</div>
	 * <div class="it">Code per Carmustina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARMUSTINE_CODE = "387281007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carteolol</div>
	 * <div class="de">Code für Carteolol</div>
	 * <div class="fr">Code de cartéolol</div>
	 * <div class="it">Code per Carteololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARTEOLOL_CODE = "386866004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Carvedilol</div>
	 * <div class="de">Code für Carvedilol</div>
	 * <div class="fr">Code de carvédilol</div>
	 * <div class="it">Code per Carvedilolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CARVEDILOL_CODE = "386870007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Caspofungin</div>
	 * <div class="de">Code für Caspofungin</div>
	 * <div class="fr">Code de caspofungine</div>
	 * <div class="it">Code per Caspofungin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CASPOFUNGIN_CODE = "413770001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cefaclor</div>
	 * <div class="de">Code für Cefaclor</div>
	 * <div class="fr">Code de céfaclor</div>
	 * <div class="it">Code per Cefaclor</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFACLOR_CODE = "387270009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cefazolin</div>
	 * <div class="de">Code für Cefazolin</div>
	 * <div class="fr">Code de céfazoline</div>
	 * <div class="it">Code per Cefazolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFAZOLIN_CODE = "387470007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cefepime</div>
	 * <div class="de">Code für Cefepim</div>
	 * <div class="fr">Code de céfépime</div>
	 * <div class="it">Code per Cefepime</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFEPIME_CODE = "96048006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cefixime anhydrous</div>
	 * <div class="de">Code für Cefixim</div>
	 * <div class="fr">Code de céfixime</div>
	 * <div class="it">Code per Cefixima</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFIXIME_ANHYDROUS_CODE = "785697003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cefpodoxime</div>
	 * <div class="de">Code für Cefpodoxim</div>
	 * <div class="fr">Code de cefpodoxime</div>
	 * <div class="it">Code per Cefpodoxima</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFPODOXIME_CODE = "387534007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ceftazidime</div>
	 * <div class="de">Code für Ceftazidim</div>
	 * <div class="fr">Code de ceftazidime</div>
	 * <div class="it">Code per Ceftazidime</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFTAZIDIME_CODE = "387200005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ceftriaxone</div>
	 * <div class="de">Code für Ceftriaxon</div>
	 * <div class="fr">Code de ceftriaxone</div>
	 * <div class="it">Code per Ceftriaxone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFTRIAXONE_CODE = "372670001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cefuroxime</div>
	 * <div class="de">Code für Cefuroxim</div>
	 * <div class="fr">Code de céfuroxime</div>
	 * <div class="it">Code per Cefuroxime</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CEFUROXIME_CODE = "372833007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Celecoxib</div>
	 * <div class="de">Code für Celecoxib</div>
	 * <div class="fr">Code de célécoxib</div>
	 * <div class="it">Code per Celecoxib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CELECOXIB_CODE = "116081000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cetirizine</div>
	 * <div class="de">Code für Cetirizin</div>
	 * <div class="fr">Code de cétirizine</div>
	 * <div class="it">Code per Cetirizina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CETIRIZINE_CODE = "372523007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cetylpyridinium</div>
	 * <div class="de">Code für Cetylpyridinium</div>
	 * <div class="fr">Code de cétylpyridinium</div>
	 * <div class="it">Code per Cetilpiridinio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CETYLPYRIDINIUM_CODE = "387043006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chloramphenicol</div>
	 * <div class="de">Code für Chloramphenicol</div>
	 * <div class="fr">Code de chloramphénicol</div>
	 * <div class="it">Code per Cloramfenicolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORAMPHENICOL_CODE = "372777009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlordiazepoxide</div>
	 * <div class="de">Code für Chlordiazepoxid</div>
	 * <div class="fr">Code de chlordiazépoxide</div>
	 * <div class="it">Code per Clordiazepossido</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORDIAZEPOXIDE_CODE = "372866006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlorhexidine</div>
	 * <div class="de">Code für Chlorhexidin</div>
	 * <div class="fr">Code de chlorhexidine</div>
	 * <div class="it">Code per Clorexidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORHEXIDINE_CODE = "373568007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlormadinone</div>
	 * <div class="de">Code für Chlormadinon</div>
	 * <div class="fr">Code de chlormadinone</div>
	 * <div class="it">Code per Clormadinone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORMADINONE_CODE = "734645001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chloroquine</div>
	 * <div class="de">Code für Chloroquin</div>
	 * <div class="fr">Code de chloroquine</div>
	 * <div class="it">Code per Clorochina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLOROQUINE_CODE = "373468005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlorphenamine</div>
	 * <div class="de">Code für Chlorphenamin</div>
	 * <div class="fr">Code de chlorphénamine</div>
	 * <div class="it">Code per Clorfenamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORPHENAMINE_CODE = "372914003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlorpromazine</div>
	 * <div class="de">Code für Chlorpromazin</div>
	 * <div class="fr">Code de chlorpromazine</div>
	 * <div class="it">Code per Clorpromazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORPROMAZINE_CODE = "387258005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlorprothixene</div>
	 * <div class="de">Code für Chlorprothixen</div>
	 * <div class="fr">Code de chlorprothixène</div>
	 * <div class="it">Code per Clorprotixene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORPROTHIXENE_CODE = "387317000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chlortalidone</div>
	 * <div class="de">Code für Chlortalidon</div>
	 * <div class="fr">Code de chlortalidone</div>
	 * <div class="it">Code per Clortalidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHLORTALIDONE_CODE = "387324004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Chondroitin sulfate</div>
	 * <div class="de">Code für Chondroitinsulfate-Gemisch</div>
	 * <div class="fr">Code de chondroïtine sulfate</div>
	 * <div class="it">Code per Condroitinsolfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHONDROITIN_SULFATE_CODE = "4104007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Choriogonadotropin alfa</div>
	 * <div class="de">Code für Choriogonadotropin alfa</div>
	 * <div class="fr">Code de choriogonadotropine alfa</div>
	 * <div class="it">Code per Coriogonadotropina alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHORIOGONADOTROPIN_ALFA_CODE = "129494006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ciclesonide</div>
	 * <div class="de">Code für Ciclesonid</div>
	 * <div class="fr">Code de ciclésonide</div>
	 * <div class="it">Code per Ciclesonide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CICLESONIDE_CODE = "417420004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ciclopirox</div>
	 * <div class="de">Code für Ciclopirox</div>
	 * <div class="fr">Code de ciclopirox</div>
	 * <div class="it">Code per Ciclopirox</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CICLOPIROX_CODE = "372854000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ciclosporin</div>
	 * <div class="de">Code für Ciclosporin</div>
	 * <div class="fr">Code de ciclosporine</div>
	 * <div class="it">Code per Ciclosporina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CICLOSPORIN_CODE = "387467008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cilastatin</div>
	 * <div class="de">Code für Cilastatin</div>
	 * <div class="fr">Code de cilastatine</div>
	 * <div class="it">Code per Cilastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CILASTATIN_CODE = "96058005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cilazapril</div>
	 * <div class="de">Code für Cilazapril</div>
	 * <div class="fr">Code de cilazapril</div>
	 * <div class="it">Code per Cilazapril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CILAZAPRIL_CODE = "395947008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cimetidine</div>
	 * <div class="de">Code für Cimetidin</div>
	 * <div class="fr">Code de cimétidine</div>
	 * <div class="it">Code per Cimetidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CIMETIDINE_CODE = "373541007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cinacalcet</div>
	 * <div class="de">Code für Cinacalcet</div>
	 * <div class="fr">Code de cinacalcet</div>
	 * <div class="it">Code per Cinacalcet</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CINACALCET_CODE = "409392004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cinchocaine</div>
	 * <div class="de">Code für Cinchocain</div>
	 * <div class="fr">Code de cinchocaïne</div>
	 * <div class="it">Code per Cincocaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CINCHOCAINE_CODE = "395953008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cinnarizine</div>
	 * <div class="de">Code für Cinnarizin</div>
	 * <div class="fr">Code de cinnarizine</div>
	 * <div class="it">Code per Cinnarizina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CINNARIZINE_CODE = "395955001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ciprofloxacin</div>
	 * <div class="de">Code für Ciprofloxacin</div>
	 * <div class="fr">Code de ciprofloxacine</div>
	 * <div class="it">Code per Ciprofloxacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CIPROFLOXACIN_CODE = "372840008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cisatracurium</div>
	 * <div class="de">Code für Cisatracurium</div>
	 * <div class="fr">Code de cisatracurium</div>
	 * <div class="it">Code per Cisatracurio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CISATRACURIUM_CODE = "372495006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cisplatin</div>
	 * <div class="de">Code für Cisplatin</div>
	 * <div class="fr">Code de cisplatine</div>
	 * <div class="it">Code per Cisplatino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CISPLATIN_CODE = "387318005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Citalopram</div>
	 * <div class="de">Code für Citalopram</div>
	 * <div class="fr">Code de citalopram</div>
	 * <div class="it">Code per Citalopram</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CITALOPRAM_CODE = "372596005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Citric acid monohydrate</div>
	 * <div class="de">Code für Citronensäure-Monohydrat</div>
	 * <div class="fr">Code de acide citrique monohydrate</div>
	 * <div class="it">Code per Acido citrico monoidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CITRIC_ACID_MONOHYDRATE_CODE = "725962006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cladribine</div>
	 * <div class="de">Code für Cladribin</div>
	 * <div class="fr">Code de cladribine</div>
	 * <div class="it">Code per Cladribina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLADRIBINE_CODE = "386916009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clarithromycin</div>
	 * <div class="de">Code für Clarithromycin</div>
	 * <div class="fr">Code de clarithromycine</div>
	 * <div class="it">Code per Claritromicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLARITHROMYCIN_CODE = "387487009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clavulanic acid</div>
	 * <div class="de">Code für Clavulansäure</div>
	 * <div class="fr">Code de acide clavulanique</div>
	 * <div class="it">Code per Acido clavulanico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLAVULANIC_ACID_CODE = "395939008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clemastine</div>
	 * <div class="de">Code für Clemastin</div>
	 * <div class="fr">Code de clémastine</div>
	 * <div class="it">Code per Clemastina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLEMASTINE_CODE = "372744005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clevidipine</div>
	 * <div class="de">Code für Clevidipin</div>
	 * <div class="fr">Code de clévidipine</div>
	 * <div class="it">Code per Clevidipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLEVIDIPINE_CODE = "439471002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clindamycin</div>
	 * <div class="de">Code für Clindamycin</div>
	 * <div class="fr">Code de clindamycine</div>
	 * <div class="it">Code per Clindamicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLINDAMYCIN_CODE = "372786004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clioquinol</div>
	 * <div class="de">Code für Clioquinol</div>
	 * <div class="fr">Code de clioquinol</div>
	 * <div class="it">Code per Cliochinolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLIOQUINOL_CODE = "387291001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clobazam</div>
	 * <div class="de">Code für Clobazam</div>
	 * <div class="fr">Code de clobazam</div>
	 * <div class="it">Code per Clobazam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOBAZAM_CODE = "387572002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clobetasol</div>
	 * <div class="de">Code für Clobetasol</div>
	 * <div class="fr">Code de clobétasol</div>
	 * <div class="it">Code per Clobetasolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOBETASOL_CODE = "419129004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clobetasone</div>
	 * <div class="de">Code für Clobetason</div>
	 * <div class="fr">Code de clobétasone</div>
	 * <div class="it">Code per Clobetasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOBETASONE_CODE = "395963000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clofarabine</div>
	 * <div class="de">Code für Clofarabin</div>
	 * <div class="fr">Code de clofarabine</div>
	 * <div class="it">Code per Clofarabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOFARABINE_CODE = "413873006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clofazimine</div>
	 * <div class="de">Code für Clofazimin</div>
	 * <div class="fr">Code de clofazimine</div>
	 * <div class="it">Code per Clofazimina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOFAZIMINE_CODE = "387410004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clomethiazole</div>
	 * <div class="de">Code für Clomethiazol</div>
	 * <div class="fr">Code de clométhiazole</div>
	 * <div class="it">Code per Clometiazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOMETHIAZOLE_CODE = "395978007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clomipramine</div>
	 * <div class="de">Code für Clomipramin</div>
	 * <div class="fr">Code de clomipramine</div>
	 * <div class="it">Code per Clomipramina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOMIPRAMINE_CODE = "372903001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clonazepam</div>
	 * <div class="de">Code für Clonazepam</div>
	 * <div class="fr">Code de clonazépam</div>
	 * <div class="it">Code per Clonazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLONAZEPAM_CODE = "387383007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clonidine</div>
	 * <div class="de">Code für Clonidin</div>
	 * <div class="fr">Code de clonidine</div>
	 * <div class="it">Code per Clonidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLONIDINE_CODE = "372805007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clopidogrel</div>
	 * <div class="de">Code für Clopidogrel</div>
	 * <div class="fr">Code de clopidogrel</div>
	 * <div class="it">Code per Clopidogrel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOPIDOGREL_CODE = "386952008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clotiapine</div>
	 * <div class="de">Code für Clotiapin</div>
	 * <div class="fr">Code de clotiapine</div>
	 * <div class="it">Code per Clotiapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOTIAPINE_CODE = "698028004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clotrimazole</div>
	 * <div class="de">Code für Clotrimazol</div>
	 * <div class="fr">Code de clotrimazole</div>
	 * <div class="it">Code per Clotrimazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOTRIMAZOLE_CODE = "387325003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Clozapine</div>
	 * <div class="de">Code für Clozapin</div>
	 * <div class="fr">Code de clozapine</div>
	 * <div class="it">Code per Clozapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CLOZAPINE_CODE = "387568001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Coagulation factor II</div>
	 * <div class="de">Code für Blutgerinnungsfaktor II human (Prothrombin)</div>
	 * <div class="fr">Code de facteur II de coagulation humain (prothrombine)</div>
	 * <div class="it">Code per Fattore II di coagulazione umano (protrombina)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COAGULATION_FACTOR_II_CODE = "7348004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Coagulation factor IX</div>
	 * <div class="de">Code für Blutgerinnungsfaktor IX human</div>
	 * <div class="fr">Code de facteur IX de coagulation humain</div>
	 * <div class="it">Code per Fattore IX di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COAGULATION_FACTOR_IX_CODE = "54378000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Coagulation factor VII</div>
	 * <div class="de">Code für Blutgerinnungsfaktor VII human</div>
	 * <div class="fr">Code de facteur VII de coagulation humain</div>
	 * <div class="it">Code per Fattore VII di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COAGULATION_FACTOR_VII_CODE = "30804005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Coagulation factor X</div>
	 * <div class="de">Code für Blutgerinnungsfaktor X human</div>
	 * <div class="fr">Code de facteur X de coagulation humain</div>
	 * <div class="it">Code per Fattore X di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COAGULATION_FACTOR_X_CODE = "81444003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Coagulation factor XIII</div>
	 * <div class="de">Code für Blutgerinnungsfaktor XIII human</div>
	 * <div class="fr">Code de facteur XIII de coagulation humain</div>
	 * <div class="it">Code per Fattore XIII di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COAGULATION_FACTOR_XIII_CODE = "51161000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cobicistat</div>
	 * <div class="de">Code für Cobicistat</div>
	 * <div class="fr">Code de cobicistat</div>
	 * <div class="it">Code per Cobicistat</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COBICISTAT_CODE = "710109003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cocaine</div>
	 * <div class="de">Code für Cocain</div>
	 * <div class="fr">Code de cocaïne</div>
	 * <div class="it">Code per Cocaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COCAINE_CODE = "387085005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Codeine</div>
	 * <div class="de">Code für Codein</div>
	 * <div class="fr">Code de codéine</div>
	 * <div class="it">Code per Codeina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CODEINE_CODE = "387494007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Codeine phosphate hemihydrate</div>
	 * <div class="de">Code für Codein phosphat hemihydrat</div>
	 * <div class="fr">Code de codéine phosphate hémihydrate</div>
	 * <div class="it">Code per Codeina fosfato emiidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CODEINE_PHOSPHATE_HEMIHYDRATE_CODE = "725666006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Colchicine</div>
	 * <div class="de">Code für Colchicin</div>
	 * <div class="fr">Code de colchicine</div>
	 * <div class="it">Code per Colchicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COLCHICINE_CODE = "387413002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Colecalciferol</div>
	 * <div class="de">Code für Colecalciferol (Vitamin D3)</div>
	 * <div class="fr">Code de colécalciférol (Vitamine D3)</div>
	 * <div class="it">Code per Colecalciferolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COLECALCIFEROL_CODE = "18414002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Colestyramine</div>
	 * <div class="de">Code für Colestyramin</div>
	 * <div class="fr">Code de colestyramine</div>
	 * <div class="it">Code per Colestiramina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COLESTYRAMINE_CODE = "387408001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Colistin</div>
	 * <div class="de">Code für Colistin</div>
	 * <div class="fr">Code de colistine</div>
	 * <div class="it">Code per Colistina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COLISTIN_CODE = "387412007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Copper sulfate</div>
	 * <div class="de">Code für Kupfer(II)-sulfat, wasserfreies</div>
	 * <div class="fr">Code de cuivre sulfate</div>
	 * <div class="it">Code per Rame solfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COPPER_SULFATE_CODE = "70168001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cromoglicate sodium</div>
	 * <div class="de">Code für Cromoglicinsäure, Dinatriumsalz</div>
	 * <div class="fr">Code de cromoglicate sodique</div>
	 * <div class="it">Code per Sodio cromoglicato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CROMOGLICATE_SODIUM_CODE = "387221005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cromoglicic acid</div>
	 * <div class="de">Code für Cromoglicinsäure</div>
	 * <div class="fr">Code de acide cromoglicique</div>
	 * <div class="it">Code per Acido cromoglicico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CROMOGLICIC_ACID_CODE = "372672009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cyanocobalamin</div>
	 * <div class="de">Code für Cyanocobalamin (Vitamin B12)</div>
	 * <div class="fr">Code de cyanocobalamine (Vitamine B12)</div>
	 * <div class="it">Code per Cianocobalamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CYANOCOBALAMIN_CODE = "419382002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cyclophosphamide</div>
	 * <div class="de">Code für Cyclophosphamid</div>
	 * <div class="fr">Code de cyclophosphamide</div>
	 * <div class="it">Code per Ciclofosfamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CYCLOPHOSPHAMIDE_CODE = "387420009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cycloserine</div>
	 * <div class="de">Code für Cycloserin</div>
	 * <div class="fr">Code de cycloserine</div>
	 * <div class="it">Code per Cicloserina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CYCLOSERINE_CODE = "387282000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cyproterone</div>
	 * <div class="de">Code für Cyproteron</div>
	 * <div class="fr">Code de cyprotérone</div>
	 * <div class="it">Code per Ciproterone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CYPROTERONE_CODE = "126119006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cytarabine</div>
	 * <div class="de">Code für Cytarabin</div>
	 * <div class="fr">Code de cytarabine</div>
	 * <div class="it">Code per Citarabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CYTARABINE_CODE = "387511003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Cytomegalovirus antibody</div>
	 * <div class="de">Code für Cytomegalie-Immunglobulin human</div>
	 * <div class="fr">Code de immunoglobuline humaine anti cytomégalovirus</div>
	 * <div class="it">Code per Immunoglobulina umana anti-citomegalovirus</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CYTOMEGALOVIRUS_ANTIBODY_CODE = "120941004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dabigatran etexilate</div>
	 * <div class="de">Code für Dabigatran etexilat</div>
	 * <div class="fr">Code de dabigatran étexilate</div>
	 * <div class="it">Code per Dabigratan etexilato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DABIGATRAN_ETEXILATE_CODE = "700029008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dabrafenib</div>
	 * <div class="de">Code für Dabrafenib</div>
	 * <div class="fr">Code de dabrafénib</div>
	 * <div class="it">Code per Dabrafenib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DABRAFENIB_CODE = "703641001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dacarbazine</div>
	 * <div class="de">Code für Dacarbazin</div>
	 * <div class="fr">Code de dacarbazine</div>
	 * <div class="it">Code per Dacarbazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DACARBAZINE_CODE = "387441003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Daclatasvir</div>
	 * <div class="de">Code für Daclatasvir</div>
	 * <div class="fr">Code de daclatasvir</div>
	 * <div class="it">Code per Daclatasvir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DACLATASVIR_CODE = "712519008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dactinomycin</div>
	 * <div class="de">Code für Dactinomycin</div>
	 * <div class="fr">Code de dactinomycine</div>
	 * <div class="it">Code per Dactinomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DACTINOMYCIN_CODE = "387353003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dalteparin sodium</div>
	 * <div class="de">Code für Dalteparin natrium</div>
	 * <div class="fr">Code de daltéparine sodique</div>
	 * <div class="it">Code per Dalteparina sodica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DALTEPARIN_SODIUM_CODE = "108987000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Danaparoid</div>
	 * <div class="de">Code für Danaparoid</div>
	 * <div class="fr">Code de danaparoïde</div>
	 * <div class="it">Code per Danaparoid</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DANAPAROID_CODE = "372564002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dantrolene</div>
	 * <div class="de">Code für Dantrolen</div>
	 * <div class="fr">Code de dantrolène</div>
	 * <div class="it">Code per Dantrolene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DANTROLENE_CODE = "372819007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dapagliflozin</div>
	 * <div class="de">Code für Dapagliflozin</div>
	 * <div class="fr">Code de dapagliflozine</div>
	 * <div class="it">Code per Dapaglifozin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DAPAGLIFLOZIN_CODE = "703674001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dapoxetine</div>
	 * <div class="de">Code für Dapoxetin</div>
	 * <div class="fr">Code de dapoxétine</div>
	 * <div class="it">Code per Dapoxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DAPOXETINE_CODE = "702794006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Daptomycin</div>
	 * <div class="de">Code für Daptomycin</div>
	 * <div class="fr">Code de daptomycine</div>
	 * <div class="it">Code per Daptomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DAPTOMYCIN_CODE = "406439009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Daratumumab</div>
	 * <div class="de">Code für Daratumumab</div>
	 * <div class="fr">Code de daratumumab</div>
	 * <div class="it">Code per Daratumumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DARATUMUMAB_CODE = "716016006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Darbepoetin alfa</div>
	 * <div class="de">Code für Darbepoetin alfa</div>
	 * <div class="fr">Code de darbépoétine alfa</div>
	 * <div class="it">Code per Darbeaoetina alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DARBEPOETIN_ALFA_CODE = "385608005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Darifenacin</div>
	 * <div class="de">Code für Darifenacin</div>
	 * <div class="fr">Code de darifénacine</div>
	 * <div class="it">Code per Darifenacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DARIFENACIN_CODE = "416140008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Darunavir</div>
	 * <div class="de">Code für Darunavir</div>
	 * <div class="fr">Code de darunavir</div>
	 * <div class="it">Code per Darunavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DARUNAVIR_CODE = "423888002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dasatinib</div>
	 * <div class="de">Code für Dasatinib</div>
	 * <div class="fr">Code de dasatinib</div>
	 * <div class="it">Code per Dasatinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DASATINIB_CODE = "423658008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Daunorubicin</div>
	 * <div class="de">Code für Daunorubicin</div>
	 * <div class="fr">Code de daunorubicine</div>
	 * <div class="it">Code per Daunorubicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DAUNORUBICIN_CODE = "372715008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Decitabine</div>
	 * <div class="de">Code für Decitabin</div>
	 * <div class="fr">Code de décitabine</div>
	 * <div class="it">Code per Decitabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DECITABINE_CODE = "420759005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Deferasirox</div>
	 * <div class="de">Code für Deferasirox</div>
	 * <div class="fr">Code de déférasirox</div>
	 * <div class="it">Code per Deferasirox</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEFERASIROX_CODE = "419985007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Deferiprone</div>
	 * <div class="de">Code für Deferipron</div>
	 * <div class="fr">Code de défériprone</div>
	 * <div class="it">Code per Deferiprone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEFERIPRONE_CODE = "396011004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Deferoxamine</div>
	 * <div class="de">Code für Deferoxamin</div>
	 * <div class="fr">Code de déféroxamine</div>
	 * <div class="it">Code per Deferoxamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEFEROXAMINE_CODE = "372825006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Defibrotide</div>
	 * <div class="de">Code für Defibrotid</div>
	 * <div class="fr">Code de défibrotide</div>
	 * <div class="it">Code per Defibrotide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEFIBROTIDE_CODE = "442263003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Deflazacort</div>
	 * <div class="de">Code für Deflazacort</div>
	 * <div class="fr">Code de déflazacort</div>
	 * <div class="it">Code per Deflazacort</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEFLAZACORT_CODE = "396012006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Degarelix</div>
	 * <div class="de">Code für Degarelix</div>
	 * <div class="fr">Code de dégarélix</div>
	 * <div class="it">Code per Degarelix</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEGARELIX_CODE = "441864003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Delta-9-tetrahydrocannabinol</div>
	 * <div class="de">Code für Delta-9-Tetrahydrocannabinol (THC)</div>
	 * <div class="fr">Code de delta-9-tétrahydrocannabinol (THC)</div>
	 * <div class="it">Code per Delta-9-tetracannabinolo (THC)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DELTA_9_TETRAHYDROCANNABINOL_CODE = "96225007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Denosumab</div>
	 * <div class="de">Code für Denosumab</div>
	 * <div class="fr">Code de dénosumab</div>
	 * <div class="it">Code per Denosumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DENOSUMAB_CODE = "446321003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Desflurane</div>
	 * <div class="de">Code für Desfluran</div>
	 * <div class="fr">Code de desflurane</div>
	 * <div class="it">Code per Desflurano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DESFLURANE_CODE = "386841003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Desloratadine</div>
	 * <div class="de">Code für Desloratadin</div>
	 * <div class="fr">Code de desloratadine</div>
	 * <div class="it">Code per Desloratadina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DESLORATADINE_CODE = "396015008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Desmopressin</div>
	 * <div class="de">Code für Desmopressin</div>
	 * <div class="fr">Code de desmopressine</div>
	 * <div class="it">Code per Desmopressina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DESMOPRESSIN_CODE = "126189002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Desogestrel</div>
	 * <div class="de">Code für Desogestrel</div>
	 * <div class="fr">Code de désogestrel</div>
	 * <div class="it">Code per Desogestrel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DESOGESTREL_CODE = "126108008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexamethasone</div>
	 * <div class="de">Code für Dexamethason</div>
	 * <div class="fr">Code de dexaméthasone</div>
	 * <div class="it">Code per Desametasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXAMETHASONE_CODE = "372584003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexamfetamine</div>
	 * <div class="de">Code für Dexamfetamin</div>
	 * <div class="fr">Code de dexamfétamine</div>
	 * <div class="it">Code per Dexamfetamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXAMFETAMINE_CODE = "387278002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexibuprofen</div>
	 * <div class="de">Code für Dexibuprofen</div>
	 * <div class="fr">Code de dexibuprofène</div>
	 * <div class="it">Code per Dexibuprofene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXIBUPROFEN_CODE = "418868002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexketoprofen</div>
	 * <div class="de">Code für Dexketoprofen</div>
	 * <div class="fr">Code de dexkétoprofène</div>
	 * <div class="it">Code per Desketoprofene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXKETOPROFEN_CODE = "396018005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexlansoprazole</div>
	 * <div class="de">Code für Dexlansoprazol</div>
	 * <div class="fr">Code de dexlansoprazole</div>
	 * <div class="it">Code per Dexlansoprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXLANSOPRAZOLE_CODE = "441863009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexmedetomidine</div>
	 * <div class="de">Code für Dexmedetomidin</div>
	 * <div class="fr">Code de dexmédétomidine</div>
	 * <div class="it">Code per Dexmedetomidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXMEDETOMIDINE_CODE = "437750002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexmethylphenidate</div>
	 * <div class="de">Code für Dexmethylphenidat</div>
	 * <div class="fr">Code de dexméthylphénidate</div>
	 * <div class="it">Code per Dexmetilfenidato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXMETHYLPHENIDATE_CODE = "767715008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexpanthenol</div>
	 * <div class="de">Code für Dexpanthenol</div>
	 * <div class="fr">Code de dexpanthénol</div>
	 * <div class="it">Code per Despantenolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXPANTHENOL_CODE = "126226000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dexrazoxane</div>
	 * <div class="de">Code für Dexrazoxan</div>
	 * <div class="fr">Code de dexrazoxane</div>
	 * <div class="it">Code per Dexrazoxano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXRAZOXANE_CODE = "108825009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dextromethorphan</div>
	 * <div class="de">Code für Dextromethorphan</div>
	 * <div class="fr">Code de dextrométhorphane</div>
	 * <div class="it">Code per Destrometorfano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DEXTROMETHORPHAN_CODE = "387114001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diamorphine</div>
	 * <div class="de">Code für Heroin</div>
	 * <div class="fr">Code de héroïne</div>
	 * <div class="it">Code per Eroina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIAMORPHINE_CODE = "387341002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diazepam</div>
	 * <div class="de">Code für Diazepam</div>
	 * <div class="fr">Code de diazépam</div>
	 * <div class="it">Code per Diazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIAZEPAM_CODE = "387264003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diclofenac</div>
	 * <div class="de">Code für Diclofenac</div>
	 * <div class="fr">Code de diclofénac</div>
	 * <div class="it">Code per Diclofenac</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DICLOFENAC_CODE = "7034005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dienogest</div>
	 * <div class="de">Code für Dienogest</div>
	 * <div class="fr">Code de diénogest</div>
	 * <div class="it">Code per Dienogest</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIENOGEST_CODE = "703097002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diflucortolone</div>
	 * <div class="de">Code für Diflucortolon</div>
	 * <div class="fr">Code de diflucortolone</div>
	 * <div class="it">Code per Diflucortolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIFLUCORTOLONE_CODE = "395965007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Digitoxin</div>
	 * <div class="de">Code für Digitoxin</div>
	 * <div class="fr">Code de digitoxine</div>
	 * <div class="it">Code per Digitossina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIGITOXIN_CODE = "373534001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Digoxin</div>
	 * <div class="de">Code für Digoxin</div>
	 * <div class="fr">Code de digoxine</div>
	 * <div class="it">Code per Digossina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIGOXIN_CODE = "387461009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dihydralazine</div>
	 * <div class="de">Code für Dihydralazin</div>
	 * <div class="fr">Code de dihydralazine</div>
	 * <div class="it">Code per Diidralazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIHYDRALAZINE_CODE = "703113001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dihydrocodeine</div>
	 * <div class="de">Code für Dihydrocodein</div>
	 * <div class="fr">Code de dihydrocodéine</div>
	 * <div class="it">Code per Diidrocodeina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIHYDROCODEINE_CODE = "387322000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diltiazem</div>
	 * <div class="de">Code für Diltiazem</div>
	 * <div class="fr">Code de diltiazem</div>
	 * <div class="it">Code per Diltiazem</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DILTIAZEM_CODE = "372793000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dimenhydrinate</div>
	 * <div class="de">Code für Dimenhydrinat</div>
	 * <div class="fr">Code de diménhydrinate</div>
	 * <div class="it">Code per Dimenidrinato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIMENHYDRINATE_CODE = "387469006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dimethyl sulfoxide</div>
	 * <div class="de">Code für Dimethylsulfoxid</div>
	 * <div class="fr">Code de diméthylsulfoxyde</div>
	 * <div class="it">Code per Dimetilsolfossido (DMSO)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIMETHYL_SULFOXIDE_CODE = "115535002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dimeticone</div>
	 * <div class="de">Code für Dimeticon</div>
	 * <div class="fr">Code de diméticone</div>
	 * <div class="it">Code per Dimeticone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIMETICONE_CODE = "396031000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dimetindene</div>
	 * <div class="de">Code für Dimetinden</div>
	 * <div class="fr">Code de dimétindène</div>
	 * <div class="it">Code per Dimetindene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIMETINDENE_CODE = "387142004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dinoprostone</div>
	 * <div class="de">Code für Dinoproston</div>
	 * <div class="fr">Code de dinoprostone</div>
	 * <div class="it">Code per Dinoprostone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DINOPROSTONE_CODE = "387245009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diosmin</div>
	 * <div class="de">Code für Diosmin</div>
	 * <div class="fr">Code de diosmine</div>
	 * <div class="it">Code per Diosmina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIOSMIN_CODE = "8143001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diphenhydramine</div>
	 * <div class="de">Code für Diphenhydramin</div>
	 * <div class="fr">Code de diphénhydramine</div>
	 * <div class="it">Code per Difenidramina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIPHENHYDRAMINE_CODE = "372682005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Diphtheria vaccine</div>
	 * <div class="de">Code für Diphtherie-Impfstoff</div>
	 * <div class="fr">Code de diphtérie vaccin</div>
	 * <div class="it">Code per Difterite vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIPHTHERIA_VACCINE_CODE = "428126001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dipotassium clorazepate</div>
	 * <div class="de">Code für Dikalium clorazepat</div>
	 * <div class="fr">Code de clorazépate dipotassique</div>
	 * <div class="it">Code per Clorazepato potassico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIPOTASSIUM_CLORAZEPATE_CODE = "387453004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Disulfiram</div>
	 * <div class="de">Code für Disulfiram</div>
	 * <div class="fr">Code de disulfirame</div>
	 * <div class="it">Code per Disulfiram</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DISULFIRAM_CODE = "387212009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dobesilate calcium</div>
	 * <div class="de">Code für Calcium dobesilat</div>
	 * <div class="fr">Code de dobésilate de calcium</div>
	 * <div class="it">Code per Calcio dobesilato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOBESILATE_CALCIUM_CODE = "83438009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dobutamine</div>
	 * <div class="de">Code für Dobutamin</div>
	 * <div class="fr">Code de dobutamine</div>
	 * <div class="it">Code per Dobutamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOBUTAMINE_CODE = "387145002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Docetaxel</div>
	 * <div class="de">Code für Docetaxel</div>
	 * <div class="fr">Code de docétaxel</div>
	 * <div class="it">Code per Docetaxel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOCETAXEL_CODE = "386918005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Docosahexaenoic acid</div>
	 * <div class="de">Code für Docosahexaensäure DHA</div>
	 * <div class="fr">Code de acide docosahexaénoïque DHA</div>
	 * <div class="it">Code per Acido docosaesaenoico (DHA)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOCOSAHEXAENOIC_ACID_CODE = "226368001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dolutegravir</div>
	 * <div class="de">Code für Dolutegravir</div>
	 * <div class="fr">Code de dolutégravir</div>
	 * <div class="it">Code per Dolutegravir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOLUTEGRAVIR_CODE = "713464000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Domperidone</div>
	 * <div class="de">Code für Domperidon</div>
	 * <div class="fr">Code de dompéridone</div>
	 * <div class="it">Code per Domperidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOMPERIDONE_CODE = "387181004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Donepezil</div>
	 * <div class="de">Code für Donepezil</div>
	 * <div class="fr">Code de donépézil</div>
	 * <div class="it">Code per Donepezil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DONEPEZIL_CODE = "386855006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dopamine</div>
	 * <div class="de">Code für Dopamin</div>
	 * <div class="fr">Code de dopamine</div>
	 * <div class="it">Code per Dopamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOPAMINE_CODE = "412383006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dornase alfa</div>
	 * <div class="de">Code für Dornase alfa</div>
	 * <div class="fr">Code de dornase alfa</div>
	 * <div class="it">Code per Dornase alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DORNASE_ALFA_CODE = "386882003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dorzolamide</div>
	 * <div class="de">Code für Dorzolamid</div>
	 * <div class="fr">Code de dorzolamide</div>
	 * <div class="it">Code per Dorzolamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DORZOLAMIDE_CODE = "373447009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxapram</div>
	 * <div class="de">Code für Doxapram</div>
	 * <div class="fr">Code de doxapram</div>
	 * <div class="it">Code per Doxapram</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXAPRAM_CODE = "373339005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxazosin</div>
	 * <div class="de">Code für Doxazosin</div>
	 * <div class="fr">Code de doxazosine</div>
	 * <div class="it">Code per Doxazosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXAZOSIN_CODE = "372508002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxepin</div>
	 * <div class="de">Code für Doxepin</div>
	 * <div class="fr">Code de doxépine</div>
	 * <div class="it">Code per Doxepina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXEPIN_CODE = "372587005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxorubicin</div>
	 * <div class="de">Code für Doxorubicin</div>
	 * <div class="fr">Code de doxorubicine</div>
	 * <div class="it">Code per Doxorubicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXORUBICIN_CODE = "372817009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxycycline</div>
	 * <div class="de">Code für Doxycyclin</div>
	 * <div class="fr">Code de doxycycline</div>
	 * <div class="it">Code per Doxiciclina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXYCYCLINE_CODE = "372478003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxycycline hyclate</div>
	 * <div class="de">Code für Doxycyclin hyclat</div>
	 * <div class="fr">Code de doxycycline hyclate</div>
	 * <div class="it">Code per Doxiciclina iclato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXYCYCLINE_HYCLATE_CODE = "71417000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Doxylamine</div>
	 * <div class="de">Code für Doxylamin</div>
	 * <div class="fr">Code de doxylamine</div>
	 * <div class="it">Code per Doxilamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DOXYLAMINE_CODE = "44068004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dronedarone</div>
	 * <div class="de">Code für Dronedaron</div>
	 * <div class="fr">Code de dronédarone</div>
	 * <div class="it">Code per Dronedarone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DRONEDARONE_CODE = "443195003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Droperidol</div>
	 * <div class="de">Code für Droperidol</div>
	 * <div class="fr">Code de dropéridol</div>
	 * <div class="it">Code per Droperidolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DROPERIDOL_CODE = "387146001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Drospirenone</div>
	 * <div class="de">Code für Drospirenon</div>
	 * <div class="fr">Code de drospirénone</div>
	 * <div class="it">Code per Drospirenone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DROSPIRENONE_CODE = "410919000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dulaglutide</div>
	 * <div class="de">Code für Dulaglutid</div>
	 * <div class="fr">Code de dulaglutide</div>
	 * <div class="it">Code per Dulaglutide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DULAGLUTIDE_CODE = "714080005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Duloxetine</div>
	 * <div class="de">Code für Duloxetin</div>
	 * <div class="fr">Code de duloxétine</div>
	 * <div class="it">Code per Duloxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DULOXETINE_CODE = "407032004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dupilumab</div>
	 * <div class="de">Code für Dupilumab</div>
	 * <div class="fr">Code de dupilumab</div>
	 * <div class="it">Code per Dupilumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DUPILUMAB_CODE = "733487000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Durvalumab</div>
	 * <div class="de">Code für Durvalumab</div>
	 * <div class="fr">Code de durvalumab</div>
	 * <div class="it">Code per Durvalumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DURVALUMAB_CODE = "735230005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dutasteride</div>
	 * <div class="de">Code für Dutasterid</div>
	 * <div class="fr">Code de dutastéride</div>
	 * <div class="it">Code per Dutasteride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DUTASTERIDE_CODE = "385572003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Dydrogesterone</div>
	 * <div class="de">Code für Dydrogesteron</div>
	 * <div class="fr">Code de dydrogestérone</div>
	 * <div class="it">Code per Didrogesterone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DYDROGESTERONE_CODE = "126093005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for D-alpha-tocopherol</div>
	 * <div class="de">Code für Tocopherol D-alpha (Vitamin E)</div>
	 * <div class="fr">Code de tocophérol D-alfa (Vitamine E)</div>
	 * <div class="it">Code per D-alfa-tocoferolo (vitamina E)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String D_ALPHA_TOCOPHEROL_CODE = "116776001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Econazole</div>
	 * <div class="de">Code für Econazol</div>
	 * <div class="fr">Code de éconazole</div>
	 * <div class="it">Code per Econazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ECONAZOLE_CODE = "373471002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eculizumab</div>
	 * <div class="de">Code für Eculizumab</div>
	 * <div class="fr">Code de éculizumab</div>
	 * <div class="it">Code per Eculizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ECULIZUMAB_CODE = "427429004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Edoxaban</div>
	 * <div class="de">Code für Edoxaban</div>
	 * <div class="fr">Code de édoxaban</div>
	 * <div class="it">Code per Edoxaban</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EDOXABAN_CODE = "712778008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Efavirenz</div>
	 * <div class="de">Code für Efavirenz</div>
	 * <div class="fr">Code de éfavirenz</div>
	 * <div class="it">Code per Efavirenz</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EFAVIRENZ_CODE = "387001004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eicosapentaenoic acid</div>
	 * <div class="de">Code für Eicosapentaensäure EPA</div>
	 * <div class="fr">Code de acide eicosapentaénoïque EPA</div>
	 * <div class="it">Code per Acido eicosapentaenoico EPA</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EICOSAPENTAENOIC_ACID_CODE = "226367006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eletriptan</div>
	 * <div class="de">Code für Eletriptan</div>
	 * <div class="fr">Code de élétriptan</div>
	 * <div class="it">Code per Eletriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ELETRIPTAN_CODE = "410843003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Elotuzumab</div>
	 * <div class="de">Code für Elotuzumab</div>
	 * <div class="fr">Code de élotuzumab</div>
	 * <div class="it">Code per Elotuzumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ELOTUZUMAB_CODE = "715660001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eltrombopag</div>
	 * <div class="de">Code für Eltrombopag</div>
	 * <div class="fr">Code de eltrombopag</div>
	 * <div class="it">Code per Eltrombopag</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ELTROMBOPAG_CODE = "432005001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Elvitegravir</div>
	 * <div class="de">Code für Elvitegravir</div>
	 * <div class="fr">Code de elvitégravir</div>
	 * <div class="it">Code per Elvitegravir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ELVITEGRAVIR_CODE = "708828000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Emedastine</div>
	 * <div class="de">Code für Emedastin</div>
	 * <div class="fr">Code de émédastine</div>
	 * <div class="it">Code per Emedastina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EMEDASTINE_CODE = "372551003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Emicizumab</div>
	 * <div class="de">Code für Emicizumab</div>
	 * <div class="fr">Code de emicizumab</div>
	 * <div class="it">Code per Emicizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EMICIZUMAB_CODE = "763611007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Empagliflozin</div>
	 * <div class="de">Code für Empagliflozin</div>
	 * <div class="fr">Code de empagliflozine</div>
	 * <div class="it">Code per Empagliflozin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EMPAGLIFLOZIN_CODE = "703894008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Emtricitabine</div>
	 * <div class="de">Code für Emtricitabin</div>
	 * <div class="fr">Code de emtricitabine</div>
	 * <div class="it">Code per Emtricitabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EMTRICITABINE_CODE = "404856006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Enalapril</div>
	 * <div class="de">Code für Enalapril</div>
	 * <div class="fr">Code de énalapril</div>
	 * <div class="it">Code per Enalapril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ENALAPRIL_CODE = "372658000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Encorafenib</div>
	 * <div class="de">Code für Encorafenib</div>
	 * <div class="fr">Code de encorafénib</div>
	 * <div class="it">Code per Encorafenib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ENCORAFENIB_CODE = "772201002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Enoxaparin sodium</div>
	 * <div class="de">Code für Enoxaparin natrium</div>
	 * <div class="fr">Code de énoxaparine sodique</div>
	 * <div class="it">Code per Enoxaparina sodica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ENOXAPARIN_SODIUM_CODE = "108983001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Entacapone</div>
	 * <div class="de">Code für Entacapon</div>
	 * <div class="fr">Code de entacapone</div>
	 * <div class="it">Code per Entacapone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ENTACAPONE_CODE = "387018000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Entecavir</div>
	 * <div class="de">Code für Entecavir</div>
	 * <div class="fr">Code de entécavir</div>
	 * <div class="it">Code per Entacavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ENTECAVIR_CODE = "416644000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eosine</div>
	 * <div class="de">Code für Eosin</div>
	 * <div class="fr">Code de éosine</div>
	 * <div class="it">Code per Eosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EOSINE_CODE = "256012001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ephedrine</div>
	 * <div class="de">Code für Ephedrin</div>
	 * <div class="fr">Code de éphédrine</div>
	 * <div class="it">Code per Efedrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPHEDRINE_CODE = "387358007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ephedrine sulfate</div>
	 * <div class="de">Code für Ephedrin sulfat</div>
	 * <div class="fr">Code de éphédrine sulfate</div>
	 * <div class="it">Code per Efedrina solfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPHEDRINE_SULFATE_CODE = "76525000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epinastine</div>
	 * <div class="de">Code für Epinastin</div>
	 * <div class="fr">Code de épinastine</div>
	 * <div class="it">Code per Epinastine</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPINASTINE_CODE = "407068009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epinephrine</div>
	 * <div class="de">Code für Adrenalin (Epinephrin)</div>
	 * <div class="fr">Code de adrénaline (épinéphrine)</div>
	 * <div class="it">Code per Adrenalina (epinefrina)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPINEPHRINE_CODE = "387362001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epirubicin</div>
	 * <div class="de">Code für Epirubicin</div>
	 * <div class="fr">Code de épirubicine</div>
	 * <div class="it">Code per Epirubicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPIRUBICIN_CODE = "417916005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eplerenone</div>
	 * <div class="de">Code für Eplerenon</div>
	 * <div class="fr">Code de éplérénone</div>
	 * <div class="it">Code per Eplerenone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPLERENONE_CODE = "407010008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epoetin alfa</div>
	 * <div class="de">Code für Epoetin alfa rekombiniert</div>
	 * <div class="fr">Code de époétine alfa recombinante</div>
	 * <div class="it">Code per Epoetina alfa ricombinante</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPOETIN_ALFA_CODE = "386947003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epoetin beta</div>
	 * <div class="de">Code für Epoetin beta rekombiniert</div>
	 * <div class="fr">Code de époétine bêta recombinante</div>
	 * <div class="it">Code per Epoetina beta ricombinante</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPOETIN_BETA_CODE = "396043004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epoetin theta</div>
	 * <div class="de">Code für Epoetin theta</div>
	 * <div class="fr">Code de époétine thêta</div>
	 * <div class="it">Code per Epoetina teta ricombinante</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPOETIN_THETA_CODE = "708829008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Epoprostenol</div>
	 * <div class="de">Code für Epoprostenol</div>
	 * <div class="fr">Code de époprosténol</div>
	 * <div class="it">Code per Epoprostenolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPOPROSTENOL_CODE = "372513003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eprosartan</div>
	 * <div class="de">Code für Eprosartan</div>
	 * <div class="fr">Code de éprosartan</div>
	 * <div class="it">Code per Eprosartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPROSARTAN_CODE = "396044005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eptacog alfa</div>
	 * <div class="de">Code für Eptacog alfa (aktiviert)</div>
	 * <div class="fr">Code de eptacogum alfa (activatum)</div>
	 * <div class="it">Code per Eptacog alfa (attivato)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPTACOG_ALFA_CODE = "116066006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eptifibatide</div>
	 * <div class="de">Code für Eptifibatid</div>
	 * <div class="fr">Code de eptifibatide</div>
	 * <div class="it">Code per Eptifibatide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EPTIFIBATIDE_CODE = "386998009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Erdosteine</div>
	 * <div class="de">Code für Erdostein</div>
	 * <div class="fr">Code de erdostéine</div>
	 * <div class="it">Code per Erdosteina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERDOSTEINE_CODE = "426292005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Erenumab</div>
	 * <div class="de">Code für Erenumab</div>
	 * <div class="fr">Code de erénumab</div>
	 * <div class="it">Code per Erenumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERENUMAB_CODE = "771590007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Eribulin</div>
	 * <div class="de">Code für Eribulin</div>
	 * <div class="fr">Code de éribuline</div>
	 * <div class="it">Code per Eribulina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERIBULIN_CODE = "708166000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Erlotinib</div>
	 * <div class="de">Code für Erlotinib</div>
	 * <div class="fr">Code de erlotinib</div>
	 * <div class="it">Code per Erlotinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERLOTINIB_CODE = "414123001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ertapenem</div>
	 * <div class="de">Code für Ertapenem</div>
	 * <div class="fr">Code de ertapénem</div>
	 * <div class="it">Code per Ertapenem</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERTAPENEM_CODE = "396346003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ertugliflozin</div>
	 * <div class="de">Code für Ertugliflozin</div>
	 * <div class="fr">Code de ertugliflozine</div>
	 * <div class="it">Code per Ertugliflozin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERTUGLIFLOZIN_CODE = "764274008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Erythromycin</div>
	 * <div class="de">Code für Erythromycin</div>
	 * <div class="fr">Code de érythromycine</div>
	 * <div class="it">Code per Eritromicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ERYTHROMYCIN_CODE = "372694001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Escitalopram</div>
	 * <div class="de">Code für Escitalopram</div>
	 * <div class="fr">Code de escitalopram</div>
	 * <div class="it">Code per Escitalopram</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESCITALOPRAM_CODE = "400447003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Esmolol</div>
	 * <div class="de">Code für Esmolol</div>
	 * <div class="fr">Code de esmolol</div>
	 * <div class="it">Code per Esmololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESMOLOL_CODE = "372847006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Esomeprazole</div>
	 * <div class="de">Code für Esomeprazol</div>
	 * <div class="fr">Code de ésoméprazole</div>
	 * <div class="it">Code per Esomeprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESOMEPRAZOLE_CODE = "396047003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Estradiol</div>
	 * <div class="de">Code für Estradiol</div>
	 * <div class="fr">Code de estradiol</div>
	 * <div class="it">Code per Estradiolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESTRADIOL_CODE = "126172005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Estradiol hemihydrate</div>
	 * <div class="de">Code für Estradiol hemihydrat</div>
	 * <div class="fr">Code de estradiol hémihydrate</div>
	 * <div class="it">Code per Estradiolo emiidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESTRADIOL_HEMIHYDRATE_CODE = "116070003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Estradiol valerate</div>
	 * <div class="de">Code für Estradiol valerat</div>
	 * <div class="fr">Code de estradiol valérate</div>
	 * <div class="it">Code per Estradiolo valerato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESTRADIOL_VALERATE_CODE = "96350008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Estriol</div>
	 * <div class="de">Code für Estriol</div>
	 * <div class="fr">Code de estriol</div>
	 * <div class="it">Code per Estriolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ESTRIOL_CODE = "73723004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etanercept</div>
	 * <div class="de">Code für Etanercept</div>
	 * <div class="fr">Code de étanercept</div>
	 * <div class="it">Code per Etanercept</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETANERCEPT_CODE = "387045004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etelcalcetide</div>
	 * <div class="de">Code für Etelcalcetid</div>
	 * <div class="fr">Code de ételcalcétide</div>
	 * <div class="it">Code per Etelcalcetide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETELCALCETIDE_CODE = "723539000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ethacridine</div>
	 * <div class="de">Code für Ethacridin</div>
	 * <div class="fr">Code de éthacridine</div>
	 * <div class="it">Code per Etacridina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETHACRIDINE_CODE = "711320003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ethambutol</div>
	 * <div class="de">Code für Ethambutol</div>
	 * <div class="fr">Code de éthambutol</div>
	 * <div class="it">Code per Etambutolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETHAMBUTOL_CODE = "387129004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ethinylestradiol</div>
	 * <div class="de">Code für Ethinylestradiol</div>
	 * <div class="fr">Code de éthinylestradiol</div>
	 * <div class="it">Code per Etinilestradiolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETHINYLESTRADIOL_CODE = "126097006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ethionamide</div>
	 * <div class="de">Code für Ethionamid</div>
	 * <div class="fr">Code de ethionamide</div>
	 * <div class="it">Code per Etionamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETHIONAMIDE_CODE = "32800009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ethosuximide</div>
	 * <div class="de">Code für Ethosuximid</div>
	 * <div class="fr">Code de éthosuximide</div>
	 * <div class="it">Code per Etosuccimide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETHOSUXIMIDE_CODE = "387244008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ethyl chloride</div>
	 * <div class="de">Code für Chlorethan</div>
	 * <div class="fr">Code de éthyle chlorure</div>
	 * <div class="it">Code per Cloruro di etile</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETHYL_CHLORIDE_CODE = "22005007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etilefrine</div>
	 * <div class="de">Code für Etilefrin</div>
	 * <div class="fr">Code de étiléfrine</div>
	 * <div class="it">Code per Etilefrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETILEFRINE_CODE = "96255000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etodolac</div>
	 * <div class="de">Code für Etodolac</div>
	 * <div class="fr">Code de étodolac</div>
	 * <div class="it">Code per Etodolac</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETODOLAC_CODE = "386860005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etomidate</div>
	 * <div class="de">Code für Etomidat</div>
	 * <div class="fr">Code de étomidate</div>
	 * <div class="it">Code per Etomidato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETOMIDATE_CODE = "387218008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etonogestrel</div>
	 * <div class="de">Code für Etonogestrel</div>
	 * <div class="fr">Code de étonogestrel</div>
	 * <div class="it">Code per Etonogestrel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETONOGESTREL_CODE = "396050000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etoposide</div>
	 * <div class="de">Code für Etoposid</div>
	 * <div class="fr">Code de étoposide</div>
	 * <div class="it">Code per Etoposide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETOPOSIDE_CODE = "387316009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etoricoxib</div>
	 * <div class="de">Code für Etoricoxib</div>
	 * <div class="fr">Code de étoricoxib</div>
	 * <div class="it">Code per Etoricoxib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETORICOXIB_CODE = "409134009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Etravirine</div>
	 * <div class="de">Code für Etravirin</div>
	 * <div class="fr">Code de étravirine</div>
	 * <div class="it">Code per Etravirina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ETRAVIRINE_CODE = "432121008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Everolimus</div>
	 * <div class="de">Code für Everolimus</div>
	 * <div class="fr">Code de évérolimus</div>
	 * <div class="it">Code per Everolimus</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EVEROLIMUS_CODE = "428698007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Exemestane</div>
	 * <div class="de">Code für Exemestan</div>
	 * <div class="fr">Code de exémestane</div>
	 * <div class="it">Code per Exemestane</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EXEMESTANE_CODE = "387017005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Exenatide</div>
	 * <div class="de">Code für Exenatid</div>
	 * <div class="fr">Code de exénatide</div>
	 * <div class="it">Code per Exenatide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EXENATIDE_CODE = "416859008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ezetimibe</div>
	 * <div class="de">Code für Ezetimib</div>
	 * <div class="fr">Code de ézétimibe</div>
	 * <div class="it">Code per Ezetimibe</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EZETIMIBE_CODE = "409149001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Factor VIII</div>
	 * <div class="de">Code für Blutgerinnungsfaktor VIII human</div>
	 * <div class="fr">Code de facteur VIII de coagulation humain</div>
	 * <div class="it">Code per Fattore VIII di coagulazione umano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FACTOR_VIII_CODE = "278910002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Famciclovir</div>
	 * <div class="de">Code für Famciclovir</div>
	 * <div class="fr">Code de famciclovir</div>
	 * <div class="it">Code per Famciclovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FAMCICLOVIR_CODE = "387557001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Febuxostat</div>
	 * <div class="de">Code für Febuxostat</div>
	 * <div class="fr">Code de fébuxostat</div>
	 * <div class="it">Code per Febuxostat</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FEBUXOSTAT_CODE = "441743008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Felbamate</div>
	 * <div class="de">Code für Felbamat</div>
	 * <div class="fr">Code de felbamate</div>
	 * <div class="it">Code per Felbamato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FELBAMATE_CODE = "96194006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Felodipine</div>
	 * <div class="de">Code für Felodipin</div>
	 * <div class="fr">Code de félodipine</div>
	 * <div class="it">Code per Felodipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FELODIPINE_CODE = "386863007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fenofibrate</div>
	 * <div class="de">Code für Fenofibrat</div>
	 * <div class="fr">Code de fénofibrate</div>
	 * <div class="it">Code per Fenofibrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FENOFIBRATE_CODE = "386879008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fenoterol</div>
	 * <div class="de">Code für Fenoterol</div>
	 * <div class="fr">Code de fénotérol</div>
	 * <div class="it">Code per Fenoterolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FENOTEROL_CODE = "395976006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fentanyl</div>
	 * <div class="de">Code für Fentanyl</div>
	 * <div class="fr">Code de fentanyl</div>
	 * <div class="it">Code per Fentanil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FENTANYL_CODE = "373492002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ferric hexacyanoferrate-II</div>
	 * <div class="de">Code für Eisen(III)-hexacyanoferrat(II)</div>
	 * <div class="fr">Code de hexacyanoferrate II ferrique III</div>
	 * <div class="it">Code per Esacianoferrato (II) di ferro (III)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FERRIC_HEXACYANOFERRATE_II_CODE = "406452004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ferrous fumarate</div>
	 * <div class="de">Code für Eisen(II) fumarat</div>
	 * <div class="fr">Code de fer II fumarate</div>
	 * <div class="it">Code per Ferro (II) fumarato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FERROUS_FUMARATE_CODE = "387289009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ferrous sulfate</div>
	 * <div class="de">Code für Eisen(II)-sulfat</div>
	 * <div class="fr">Code de fer sulfate</div>
	 * <div class="it">Code per Solfato ferroso</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FERROUS_SULFATE_CODE = "387402000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fesoterodine fumarate</div>
	 * <div class="de">Code für Fesoterodin fumarat</div>
	 * <div class="fr">Code de fésotérodine fumarate</div>
	 * <div class="it">Code per Fesoterodine fumarato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FESOTERODINE_FUMARATE_CODE = "441469003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fexofenadine</div>
	 * <div class="de">Code für Fexofenadin</div>
	 * <div class="fr">Code de fexofénadine</div>
	 * <div class="it">Code per Fexofenadina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FEXOFENADINE_CODE = "372522002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fidaxomicin</div>
	 * <div class="de">Code für Fidaxomicin</div>
	 * <div class="fr">Code de fidaxomicine</div>
	 * <div class="it">Code per Fidaxomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FIDAXOMICIN_CODE = "703664004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Filgrastim</div>
	 * <div class="de">Code für Filgrastim rekombiniert</div>
	 * <div class="fr">Code de filgrastim recombinant</div>
	 * <div class="it">Code per Filgrastim</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FILGRASTIM_CODE = "386948008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Finasteride</div>
	 * <div class="de">Code für Finasterid</div>
	 * <div class="fr">Code de finastéride</div>
	 * <div class="it">Code per Finasteride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FINASTERIDE_CODE = "386963006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fingolimod</div>
	 * <div class="de">Code für Fingolimod</div>
	 * <div class="fr">Code de fingolimod</div>
	 * <div class="it">Code per Fingolimod</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FINGOLIMOD_CODE = "449000008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fish oil</div>
	 * <div class="de">Code für Fischkörperöl</div>
	 * <div class="fr">Code de poisson huile</div>
	 * <div class="it">Code per Pesce olio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FISH_OIL_CODE = "735341005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for 5-aminolevulinic acid</div>
	 * <div class="de">Code für 5-Aminolevulinsäure</div>
	 * <div class="fr">Code de acide 5-aminolévulinique</div>
	 * <div class="it">Code per Acido 5-aminolevulinico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FIVE_AMINOLEVULINIC_ACID_CODE = "259496005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flavoxate</div>
	 * <div class="de">Code für Flavoxat</div>
	 * <div class="fr">Code de flavoxate</div>
	 * <div class="it">Code per Flavossato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLAVOXATE_CODE = "372768002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flecainide</div>
	 * <div class="de">Code für Flecainid</div>
	 * <div class="fr">Code de flécaïnide</div>
	 * <div class="it">Code per Flecainide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLECAINIDE_CODE = "372751001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flucloxacillin</div>
	 * <div class="de">Code für Flucloxacillin</div>
	 * <div class="fr">Code de flucloxacilline</div>
	 * <div class="it">Code per Flucloxacillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUCLOXACILLIN_CODE = "387544009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluconazole</div>
	 * <div class="de">Code für Fluconazol</div>
	 * <div class="fr">Code de fluconazole</div>
	 * <div class="it">Code per Fluconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUCONAZOLE_CODE = "387174006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fludarabine</div>
	 * <div class="de">Code für Fludarabin</div>
	 * <div class="fr">Code de fludarabine</div>
	 * <div class="it">Code per Fludarabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUDARABINE_CODE = "386907005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fludrocortisone</div>
	 * <div class="de">Code für Fludrocortison</div>
	 * <div class="fr">Code de fludrocortisone</div>
	 * <div class="it">Code per Fludrocortisone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUDROCORTISONE_CODE = "116586002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flumazenil</div>
	 * <div class="de">Code für Flumazenil</div>
	 * <div class="fr">Code de flumazénil</div>
	 * <div class="it">Code per Flumazenil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUMAZENIL_CODE = "387575000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flumetasone</div>
	 * <div class="de">Code für Flumetason</div>
	 * <div class="fr">Code de flumétasone</div>
	 * <div class="it">Code per Flumetasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUMETASONE_CODE = "116598007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flunarizine</div>
	 * <div class="de">Code für Flunarizin</div>
	 * <div class="fr">Code de flunarizine</div>
	 * <div class="it">Code per Flunarizina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUNARIZINE_CODE = "418221001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flunitrazepam</div>
	 * <div class="de">Code für Flunitrazepam</div>
	 * <div class="fr">Code de flunitrazépam</div>
	 * <div class="it">Code per Flunitrazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUNITRAZEPAM_CODE = "387573007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluocinonide</div>
	 * <div class="de">Code für Fluocinonid</div>
	 * <div class="fr">Code de fluocinonide</div>
	 * <div class="it">Code per Fluocinonide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUOCINONIDE_CODE = "396060009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluorometholone</div>
	 * <div class="de">Code für Fluorometholon</div>
	 * <div class="fr">Code de fluorométholone</div>
	 * <div class="it">Code per Fluorometolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUOROMETHOLONE_CODE = "2925007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluorouracil</div>
	 * <div class="de">Code für Fluorouracil</div>
	 * <div class="fr">Code de fluorouracil</div>
	 * <div class="it">Code per Fluorouracile</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUOROURACIL_CODE = "387172005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluoxetine</div>
	 * <div class="de">Code für Fluoxetin</div>
	 * <div class="fr">Code de fluoxétine</div>
	 * <div class="it">Code per Fluoxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUOXETINE_CODE = "372767007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flupentixol</div>
	 * <div class="de">Code für Flupentixol</div>
	 * <div class="fr">Code de flupentixol</div>
	 * <div class="it">Code per Flupentixolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUPENTIXOL_CODE = "387567006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flupentixol decanoate</div>
	 * <div class="de">Code für Flupentixol decanoat</div>
	 * <div class="fr">Code de flupentixol décanoate</div>
	 * <div class="it">Code per Flupentixolo decanoato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUPENTIXOL_DECANOATE_CODE = "396062001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flurazepam</div>
	 * <div class="de">Code für Flurazepam</div>
	 * <div class="fr">Code de flurazépam</div>
	 * <div class="it">Code per Flurazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLURAZEPAM_CODE = "387109000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Flurbiprofen</div>
	 * <div class="de">Code für Flurbiprofen</div>
	 * <div class="fr">Code de flurbiprofène</div>
	 * <div class="it">Code per Flurbiprofene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLURBIPROFEN_CODE = "373506008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluticasone</div>
	 * <div class="de">Code für Fluticason</div>
	 * <div class="fr">Code de fluticasone</div>
	 * <div class="it">Code per Fluticasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUTICASONE_CODE = "397192001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluvastatin</div>
	 * <div class="de">Code für Fluvastatin</div>
	 * <div class="fr">Code de fluvastatine</div>
	 * <div class="it">Code per Fluvastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUVASTATIN_CODE = "387585004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fluvoxamine</div>
	 * <div class="de">Code für Fluvoxamin</div>
	 * <div class="fr">Code de fluvoxamine</div>
	 * <div class="it">Code per Fluvoxamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FLUVOXAMINE_CODE = "372905008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Folic acid</div>
	 * <div class="de">Code für Folsäure</div>
	 * <div class="fr">Code de acide folique</div>
	 * <div class="it">Code per Acido folico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOLIC_ACID_CODE = "63718003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Folinic acid</div>
	 * <div class="de">Code für Folinsäure</div>
	 * <div class="fr">Code de acide folinique</div>
	 * <div class="it">Code per Acido folinico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOLINIC_ACID_CODE = "396065004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Follitropin alfa</div>
	 * <div class="de">Code für Follitropin alfa</div>
	 * <div class="fr">Code de follitropine alfa</div>
	 * <div class="it">Code per Follitropina alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOLLITROPIN_ALFA_CODE = "395862009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Follitropin beta</div>
	 * <div class="de">Code für Follitropin beta</div>
	 * <div class="fr">Code de follitropine bêta</div>
	 * <div class="it">Code per Follitropina beta</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOLLITROPIN_BETA_CODE = "103028007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fomepizole</div>
	 * <div class="de">Code für Fomepizol</div>
	 * <div class="fr">Code de fomépizole</div>
	 * <div class="it">Code per Fomepizolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOMEPIZOLE_CODE = "386970006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fondaparinux sodium</div>
	 * <div class="de">Code für Fondaparinux natrium</div>
	 * <div class="fr">Code de fondaparinux sodique</div>
	 * <div class="it">Code per Fondaparinux sodico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FONDAPARINUX_SODIUM_CODE = "385517000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Formoterol</div>
	 * <div class="de">Code für Formoterol</div>
	 * <div class="fr">Code de formotérol</div>
	 * <div class="it">Code per Formoterolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FORMOTEROL_CODE = "414289007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fosamprenavir</div>
	 * <div class="de">Code für Fosamprenavir</div>
	 * <div class="fr">Code de fosamprénavir</div>
	 * <div class="it">Code per Fosamprenavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOSAMPRENAVIR_CODE = "407017006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Foscarnet</div>
	 * <div class="de">Code für Foscarnet</div>
	 * <div class="fr">Code de foscarnet</div>
	 * <div class="it">Code per Foscarnet</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOSCARNET_CODE = "372902006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fosfomycin</div>
	 * <div class="de">Code für Fosfomycin</div>
	 * <div class="fr">Code de fosfomycine</div>
	 * <div class="it">Code per Fosfomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOSFOMYCIN_CODE = "372534005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fosinopril</div>
	 * <div class="de">Code für Fosinopril</div>
	 * <div class="fr">Code de fosinopril</div>
	 * <div class="it">Code per Fosinopril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FOSINOPRIL_CODE = "372510000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Frovatriptan</div>
	 * <div class="de">Code für Frovatriptan</div>
	 * <div class="fr">Code de frovatriptan</div>
	 * <div class="it">Code per Frovatriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FROVATRIPTAN_CODE = "411990007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fruit bromelain</div>
	 * <div class="de">Code für Bromelain</div>
	 * <div class="fr">Code de bromélaïnes</div>
	 * <div class="it">Code per Bromelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FRUIT_BROMELAIN_CODE = "130663004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fulvestrant</div>
	 * <div class="de">Code für Fulvestrant</div>
	 * <div class="fr">Code de fulvestrant</div>
	 * <div class="it">Code per Fulvestrant</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FULVESTRANT_CODE = "385519002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Furosemide</div>
	 * <div class="de">Code für Furosemid</div>
	 * <div class="fr">Code de furosémide</div>
	 * <div class="it">Code per Furosemide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FUROSEMIDE_CODE = "387475002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Fusidic acid</div>
	 * <div class="de">Code für Fusidinsäure</div>
	 * <div class="fr">Code de acide fusidique</div>
	 * <div class="it">Code per Acido fusidico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String FUSIDIC_ACID_CODE = "387530003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gabapentin</div>
	 * <div class="de">Code für Gabapentin</div>
	 * <div class="fr">Code de gabapentine</div>
	 * <div class="it">Code per Gabapentin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GABAPENTIN_CODE = "386845007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gadobutrol</div>
	 * <div class="de">Code für Gadobutrol</div>
	 * <div class="fr">Code de gadobutrol</div>
	 * <div class="it">Code per Gadobutrolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GADOBUTROL_CODE = "418351005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gadoteric acid</div>
	 * <div class="de">Code für Gadotersäure</div>
	 * <div class="fr">Code de acide gadotérique</div>
	 * <div class="it">Code per Acido gadoterico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GADOTERIC_ACID_CODE = "710812003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Galactose</div>
	 * <div class="de">Code für Galactose</div>
	 * <div class="fr">Code de galactose</div>
	 * <div class="it">Code per Galattosio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GALACTOSE_CODE = "38182007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Galantamine</div>
	 * <div class="de">Code für Galantamin</div>
	 * <div class="fr">Code de galantamine</div>
	 * <div class="it">Code per Galantamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GALANTAMINE_CODE = "395727007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ganciclovir</div>
	 * <div class="de">Code für Ganciclovir</div>
	 * <div class="fr">Code de ganciclovir</div>
	 * <div class="it">Code per Ganciclovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GANCICLOVIR_CODE = "372848001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ganirelix</div>
	 * <div class="de">Code für Ganirelix</div>
	 * <div class="fr">Code de ganirélix</div>
	 * <div class="it">Code per Ganirelix</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GANIRELIX_CODE = "395728002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gemcitabine</div>
	 * <div class="de">Code für Gemcitabin</div>
	 * <div class="fr">Code de gemcitabine</div>
	 * <div class="it">Code per Gemcitabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GEMCITABINE_CODE = "386920008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gemfibrozil</div>
	 * <div class="de">Code für Gemfibrozil</div>
	 * <div class="fr">Code de gemfibrozil</div>
	 * <div class="it">Code per Gemfibrozil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GEMFIBROZIL_CODE = "387189002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gentamicin</div>
	 * <div class="de">Code für Gentamicin</div>
	 * <div class="fr">Code de gentamicine</div>
	 * <div class="it">Code per Gentamicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GENTAMICIN_CODE = "387321007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gestodene</div>
	 * <div class="de">Code für Gestoden</div>
	 * <div class="fr">Code de gestodène</div>
	 * <div class="it">Code per Gestodene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GESTODENE_CODE = "395945000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ginkgo biloba</div>
	 * <div class="de">Code für Ginkgo (Ginkgo biloba L.)</div>
	 * <div class="fr">Code de ginkgo (Ginkgo biloba L.)</div>
	 * <div class="it">Code per Ginko (Ginko biloba L.)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GINKGO_BILOBA_CODE = "420733007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glatiramer</div>
	 * <div class="de">Code für Glatiramer</div>
	 * <div class="fr">Code de glatiramère</div>
	 * <div class="it">Code per Glatiramer</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLATIRAMER_CODE = "372535006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glibenclamide</div>
	 * <div class="de">Code für Glibenclamid</div>
	 * <div class="fr">Code de glibenclamide</div>
	 * <div class="it">Code per Glibenclamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLIBENCLAMIDE_CODE = "384978002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gliclazide</div>
	 * <div class="de">Code für Gliclazid</div>
	 * <div class="fr">Code de gliclazide</div>
	 * <div class="it">Code per Gliclazide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLICLAZIDE_CODE = "395731001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glimepiride</div>
	 * <div class="de">Code für Glimepirid</div>
	 * <div class="fr">Code de glimépiride</div>
	 * <div class="it">Code per Glimepiride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLIMEPIRIDE_CODE = "386966003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glucagon</div>
	 * <div class="de">Code für Glucagon</div>
	 * <div class="fr">Code de glucagon</div>
	 * <div class="it">Code per Glucagone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLUCAGON_CODE = "66603002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glucose</div>
	 * <div class="de">Code für Glucose</div>
	 * <div class="fr">Code de glucose</div>
	 * <div class="it">Code per Glucosio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLUCOSE_CODE = "67079006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glyceryl trinitrate</div>
	 * <div class="de">Code für Nitroglycerin</div>
	 * <div class="fr">Code de nitroglycérine</div>
	 * <div class="it">Code per Nitroglicerina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLYCERYL_TRINITRATE_CODE = "387404004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glycine</div>
	 * <div class="de">Code für Glycin</div>
	 * <div class="fr">Code de glycine</div>
	 * <div class="it">Code per Glicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLYCINE_CODE = "15331006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glycocholic acid</div>
	 * <div class="de">Code für Glycocholsäure</div>
	 * <div class="fr">Code de acide glycocholique</div>
	 * <div class="it">Code per Acido glicocolico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLYCOCHOLIC_ACID_CODE = "96314001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Glycopyrronium</div>
	 * <div class="de">Code für Glycopyrronium-Kation</div>
	 * <div class="fr">Code de glycopyrronium</div>
	 * <div class="it">Code per Glicopirronio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GLYCOPYRRONIUM_CODE = "769097000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Golimumab</div>
	 * <div class="de">Code für Golimumab</div>
	 * <div class="fr">Code de golimumab</div>
	 * <div class="it">Code per Golimumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GOLIMUMAB_CODE = "442435002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gonadorelin</div>
	 * <div class="de">Code für Gonadorelin</div>
	 * <div class="fr">Code de gonadoréline</div>
	 * <div class="it">Code per Gonadorelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GONADORELIN_CODE = "397197007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Goserelin</div>
	 * <div class="de">Code für Goserelin</div>
	 * <div class="fr">Code de goséréline</div>
	 * <div class="it">Code per Goserelin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GOSERELIN_CODE = "108771008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Gramicidin</div>
	 * <div class="de">Code für Gramicidin</div>
	 * <div class="fr">Code de gramicidine</div>
	 * <div class="it">Code per Gramicidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GRAMICIDIN_CODE = "387524003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Granisetron</div>
	 * <div class="de">Code für Granisetron</div>
	 * <div class="fr">Code de granisétron</div>
	 * <div class="it">Code per Granisetron</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GRANISETRON_CODE = "372489005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Guaifenesin</div>
	 * <div class="de">Code für Guaifenesin</div>
	 * <div class="fr">Code de guaïfénésine</div>
	 * <div class="it">Code per Guaifenesina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GUAIFENESIN_CODE = "87174009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Guanfacine</div>
	 * <div class="de">Code für Guanfacin</div>
	 * <div class="fr">Code de guanfacine</div>
	 * <div class="it">Code per Guanfacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GUANFACINE_CODE = "372507007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Haemophilus influenzae type b vaccine</div>
	 * <div class="de">Code für Haemophilus-influenza-b-Impfstoff</div>
	 * <div class="fr">Code de vaccin Haemophilus influenza type B</div>
	 * <div class="it">Code per Anti-Hemophilus influenzae tipo B (Hib) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HAEMOPHILUS_INFLUENZAE_TYPE_B_VACCINE_CODE = "412374001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Halcinonide</div>
	 * <div class="de">Code für Halcinonid</div>
	 * <div class="fr">Code de halcinonide</div>
	 * <div class="it">Code per Alcinonide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HALCINONIDE_CODE = "395735005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Halometasone</div>
	 * <div class="de">Code für Halometason</div>
	 * <div class="fr">Code de halométasone</div>
	 * <div class="it">Code per Alometasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HALOMETASONE_CODE = "704673003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Haloperidol</div>
	 * <div class="de">Code für Haloperidol</div>
	 * <div class="fr">Code de halopéridol</div>
	 * <div class="it">Code per Aloperidolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HALOPERIDOL_CODE = "386837002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Heparin</div>
	 * <div class="de">Code für Heparin</div>
	 * <div class="fr">Code de héparine</div>
	 * <div class="it">Code per Eparina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEPARIN_CODE = "372877000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hepatitis A virus vaccine</div>
	 * <div class="de">Code für Hepatitis-A-Impfstoff</div>
	 * <div class="fr">Code de hépatite A vaccin</div>
	 * <div class="it">Code per Epatite A (HAV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEPATITIS_A_VIRUS_VACCINE_CODE = "396423004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hepatitis B antigen</div>
	 * <div class="de">Code für Hepatitis B Antigen</div>
	 * <div class="fr">Code de hepatitis B antigène</div>
	 * <div class="it">Code per Epatite B antigene purificato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEPATITIS_B_ANTIGEN_CODE = "303233001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hepatitis B virus recombinant vaccine</div>
	 * <div class="de">Code für Hepatitis-B-Impfstoff, rekombiniert, monovalent</div>
	 * <div class="fr">Code de hépatite B vaccin recombiné, monovalent</div>
	 * <div class="it">Code per Epatite B vaccino ricombinato (ADNr)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEPATITIS_B_VIRUS_RECOMBINANT_VACCINE_CODE = "412402004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hepatitis B virus vaccine</div>
	 * <div class="de">Code für Hepatitis-B-Impfstoff</div>
	 * <div class="fr">Code de vaccin hépatite B</div>
	 * <div class="it">Code per Epatite B (HBV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEPATITIS_B_VIRUS_VACCINE_CODE = "396424005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hexamidine</div>
	 * <div class="de">Code für Hexamidin</div>
	 * <div class="fr">Code de hexamidine</div>
	 * <div class="it">Code per Esamidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEXAMIDINE_CODE = "703831002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hexetidine</div>
	 * <div class="de">Code für Hexetidin</div>
	 * <div class="fr">Code de hexétidine</div>
	 * <div class="it">Code per Esetidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEXETIDINE_CODE = "387132001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hexoprenaline</div>
	 * <div class="de">Code für Hexoprenalin</div>
	 * <div class="fr">Code de hexoprénaline</div>
	 * <div class="it">Code per Esoprenalina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HEXOPRENALINE_CODE = "704987001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Histidine</div>
	 * <div class="de">Code für Histidin</div>
	 * <div class="fr">Code de histidine</div>
	 * <div class="it">Code per Istidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HISTIDINE_CODE = "60260004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human antithrombin III</div>
	 * <div class="de">Code für Antithrombin III human</div>
	 * <div class="fr">Code de antithrombine III humaine</div>
	 * <div class="it">Code per Antitrombina III umana</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_ANTITHROMBIN_III_CODE = "412564003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human anti-D immunoglobulin</div>
	 * <div class="de">Code für Anti-D-Immunglobulin vom Menschen</div>
	 * <div class="fr">Code de immunoglobuline humaine anti-D</div>
	 * <div class="it">Code per Immunoglobulina umana anti-D</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_ANTI_D_IMMUNOGLOBULIN_CODE = "769102002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human chorionic gonadotropin</div>
	 * <div class="de">Code für Choriongonadotropin</div>
	 * <div class="fr">Code de gonadotrophine chorionique</div>
	 * <div class="it">Code per Gonadotropina corionica umana (HCG)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_CHORIONIC_GONADOTROPIN_CODE = "59433001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human fibrinogen</div>
	 * <div class="de">Code für Fibrinogen (human)</div>
	 * <div class="fr">Code de fibrinogène humain</div>
	 * <div class="it">Code per Fibrinogeno umano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_FIBRINOGEN_CODE = "418326009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human immunoglobulin</div>
	 * <div class="de">Code für Immunglobulin vom Menschen</div>
	 * <div class="fr">Code de immunoglobuline humaine</div>
	 * <div class="it">Code per Immunoglobulina umana</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_IMMUNOGLOBULIN_CODE = "420084002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human immunoglobulin G</div>
	 * <div class="de">Code für Immunglobulin G human (IgG)</div>
	 * <div class="fr">Code de iImmunoglobulinum gamma humanum (IgG)</div>
	 * <div class="it">Code per Immunoglobulina G umana (IgG)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_IMMUNOGLOBULIN_G_CODE = "722197004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human insulin</div>
	 * <div class="de">Code für Insulin human</div>
	 * <div class="fr">Code de insulines humaines</div>
	 * <div class="it">Code per Insulina umana</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_INSULIN_CODE = "96367001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human menopausal gonadotropin</div>
	 * <div class="de">Code für Menotropin</div>
	 * <div class="fr">Code de ménotropine</div>
	 * <div class="it">Code per Menotropina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_MENOPAUSAL_GONADOTROPIN_CODE = "8203003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human papilloma virus type 16 and type 18 vaccine</div>
	 * <div class="de">Code für Papillomavirus (human)-Impfstoff, Typ 16 und Typ 18</div>
	 * <div class="fr">Code de papillomavirus vaccin humain, type 16 et type 18</div>
	 * <div class="it">Code per Papillomavirus umano tipo 16 e tipo 18 (HPV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_PAPILLOMA_VIRUS_TYPE_16_AND_TYPE_18_VACCINE_CODE = "722219008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Human papilloma virus type 9 vaccine</div>
	 * <div class="de">Code für Papillomavirus (human)-Impfstoff, Typ 9</div>
	 * <div class="fr">Code de papillomavirus vaccin humain, type 9</div>
	 * <div class="it">Code per Papillomavirus umano tipo 9 (HPV) vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HUMAN_PAPILLOMA_VIRUS_TYPE_9_VACCINE_CODE = "724330005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hyaluronic acid</div>
	 * <div class="de">Code für Hyaluronsäure</div>
	 * <div class="fr">Code de acide hyaluronique</div>
	 * <div class="it">Code per Acido ialuronico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYALURONIC_ACID_CODE = "38218009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydrochlorothiazide</div>
	 * <div class="de">Code für Hydrochlorothiazid</div>
	 * <div class="fr">Code de hydrochlorothiazide</div>
	 * <div class="it">Code per Idrocloratiazide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROCHLOROTHIAZIDE_CODE = "387525002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydrocodone</div>
	 * <div class="de">Code für Hydrocodon</div>
	 * <div class="fr">Code de hydrocodone</div>
	 * <div class="it">Code per Idrocodone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROCODONE_CODE = "372671002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydrocortisone</div>
	 * <div class="de">Code für Hydrocortison</div>
	 * <div class="fr">Code de hydrocortisone</div>
	 * <div class="it">Code per Idrocortisone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROCORTISONE_CODE = "396458002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydrocortisone acetate</div>
	 * <div class="de">Code für Hydrocortison acetat</div>
	 * <div class="fr">Code de hydrocortisone acétate</div>
	 * <div class="it">Code per Idrocortisone acetato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROCORTISONE_ACETATE_CODE = "79380007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydrogen peroxide</div>
	 * <div class="de">Code für Wasserstoffperoxid</div>
	 * <div class="fr">Code de peroxyde d´hydrogène</div>
	 * <div class="it">Code per Idrogeno perossido</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROGEN_PEROXIDE_CODE = "387171003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydromorphone</div>
	 * <div class="de">Code für Hydromorphon</div>
	 * <div class="fr">Code de hydromorphone</div>
	 * <div class="it">Code per Idromorfone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROMORPHONE_CODE = "44508008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydroquinone</div>
	 * <div class="de">Code für Hydrochinon</div>
	 * <div class="fr">Code de hydroquinone</div>
	 * <div class="it">Code per Idrochinone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROQUINONE_CODE = "387422001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydrotalcite</div>
	 * <div class="de">Code für Hydrotalcit</div>
	 * <div class="fr">Code de hydrotalcite</div>
	 * <div class="it">Code per Idrotalcite</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROTALCITE_CODE = "395738007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydroxocobalamin</div>
	 * <div class="de">Code für Hydroxocobalamin</div>
	 * <div class="fr">Code de hydroxocobalamine</div>
	 * <div class="it">Code per Idrossocobalamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROXOCOBALAMIN_CODE = "409258004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydroxycarbamide</div>
	 * <div class="de">Code für Hydroxycarbamid</div>
	 * <div class="fr">Code de hydroxycarbamide</div>
	 * <div class="it">Code per Idrossicarbamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROXYCARBAMIDE_CODE = "387314007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydroxychloroquine</div>
	 * <div class="de">Code für Hydroxychloroquin</div>
	 * <div class="fr">Code de hydroxychloroquine</div>
	 * <div class="it">Code per Idrossiclorochina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROXYCHLOROQUINE_CODE = "373540008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hydroxyzine</div>
	 * <div class="de">Code für Hydroxyzin</div>
	 * <div class="fr">Code de hydroxyzine</div>
	 * <div class="it">Code per Idrossizina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYDROXYZINE_CODE = "372856003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hypericin</div>
	 * <div class="de">Code für Hypericin</div>
	 * <div class="fr">Code de hypéricine</div>
	 * <div class="it">Code per Ipericina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYPERICIN_CODE = "123681008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Hypericum perforatum extract</div>
	 * <div class="de">Code für Johanniskraut (Hypericum perforatum L.)</div>
	 * <div class="fr">Code de millepertuis (Hypericum perforatum L.)</div>
	 * <div class="it">Code per Iperico (Hypericum perforatum L.)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HYPERICUM_PERFORATUM_EXTRACT_CODE = "412515006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ibandronic acid</div>
	 * <div class="de">Code für Ibandronsäure</div>
	 * <div class="fr">Code de acide ibandronique (ibandronate)</div>
	 * <div class="it">Code per Acido ibandronico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IBANDRONIC_ACID_CODE = "420936009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ibuprofen</div>
	 * <div class="de">Code für Ibuprofen</div>
	 * <div class="fr">Code de ibuprofène</div>
	 * <div class="it">Code per Ibuprofene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IBUPROFEN_CODE = "387207008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ibuprofen lysine</div>
	 * <div class="de">Code für Ibuprofen lysin</div>
	 * <div class="fr">Code de ibuprofène lysine</div>
	 * <div class="it">Code per Ibuprofene lisina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IBUPROFEN_LYSINE_CODE = "425516000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Icatibant</div>
	 * <div class="de">Code für Icatibant</div>
	 * <div class="fr">Code de icatibant</div>
	 * <div class="it">Code per Icatibant</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ICATIBANT_CODE = "703834005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Idarubicin</div>
	 * <div class="de">Code für Idarubicin</div>
	 * <div class="fr">Code de idarubicine</div>
	 * <div class="it">Code per Idarubicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IDARUBICIN_CODE = "372539000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Idarucizumab</div>
	 * <div class="de">Code für Idarucizumab</div>
	 * <div class="fr">Code de idarucizumab</div>
	 * <div class="it">Code per Idarucizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IDARUCIZUMAB_CODE = "716017002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Idebenone</div>
	 * <div class="de">Code für Idebenon</div>
	 * <div class="fr">Code de idébénone</div>
	 * <div class="it">Code per Idebenone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IDEBENONE_CODE = "429666007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Idelalisib</div>
	 * <div class="de">Code für Idelalisib</div>
	 * <div class="fr">Code de idélalisib</div>
	 * <div class="it">Code per Idelalisib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IDELALISIB_CODE = "710278000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ifosfamide</div>
	 * <div class="de">Code für Ifosfamid</div>
	 * <div class="fr">Code de ifosfamide</div>
	 * <div class="it">Code per Ifosfamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IFOSFAMIDE_CODE = "386904003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iloprost</div>
	 * <div class="de">Code für Iloprost</div>
	 * <div class="fr">Code de iloprost</div>
	 * <div class="it">Code per Iloprost</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ILOPROST_CODE = "395740002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Imatinib</div>
	 * <div class="de">Code für Imatinib</div>
	 * <div class="fr">Code de imatinib</div>
	 * <div class="it">Code per Imatinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMATINIB_CODE = "414460008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Imiglucerase</div>
	 * <div class="de">Code für Imiglucerase</div>
	 * <div class="fr">Code de imiglucérase</div>
	 * <div class="it">Code per Imiglucerasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMIGLUCERASE_CODE = "386968002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Imipenem</div>
	 * <div class="de">Code für Imipenem</div>
	 * <div class="fr">Code de imipénem</div>
	 * <div class="it">Code per Imipenem</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMIPENEM_CODE = "46558003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Imipramine</div>
	 * <div class="de">Code für Imipramin</div>
	 * <div class="fr">Code de imipramine</div>
	 * <div class="it">Code per Imipramina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMIPRAMINE_CODE = "372718005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Imiquimod</div>
	 * <div class="de">Code für Imiquimod</div>
	 * <div class="fr">Code de imiquimod</div>
	 * <div class="it">Code per Imiquimod</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMIQUIMOD_CODE = "386941002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Immunoglobulin A</div>
	 * <div class="de">Code für Immunglobulin A human (IgA)</div>
	 * <div class="fr">Code de immunglobulin A human (IgA)</div>
	 * <div class="it">Code per Immunoglobulina A umana (IgA)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMMUNOGLOBULIN_A_CODE = "46046006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Immunoglobulin M</div>
	 * <div class="de">Code für Immunglobulin M human (IgM)</div>
	 * <div class="fr">Code de immunoglobuline M humaine (IgM)</div>
	 * <div class="it">Code per Immunoglobulina M umana (IgM)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IMMUNOGLOBULIN_M_CODE = "74889000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Inactivated Poliovirus vaccine</div>
	 * <div class="de">Code für Poliomyelitis-Impfstoff inaktiviert</div>
	 * <div class="fr">Code de poliomyélite vaccin inactivé</div>
	 * <div class="it">Code per Poliomielite vaccino inattivato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INACTIVATED_POLIOVIRUS_VACCINE_CODE = "396435000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Indacaterol</div>
	 * <div class="de">Code für Indacaterol</div>
	 * <div class="fr">Code de indacatérol</div>
	 * <div class="it">Code per Indacaterol</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INDACATEROL_CODE = "702801003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Indapamide</div>
	 * <div class="de">Code für Indapamid</div>
	 * <div class="fr">Code de indapamide</div>
	 * <div class="it">Code per Indapamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INDAPAMIDE_CODE = "387419003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Indometacin</div>
	 * <div class="de">Code für Indometacin</div>
	 * <div class="fr">Code de indométacine</div>
	 * <div class="it">Code per Indometacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INDOMETACIN_CODE = "373513008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Infliximab</div>
	 * <div class="de">Code für Infliximab</div>
	 * <div class="fr">Code de infliximab</div>
	 * <div class="it">Code per Infliximab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INFLIXIMAB_CODE = "386891004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Influenza split virion vaccine</div>
	 * <div class="de">Code für Influenza-Spalt-Impfstoff, inaktiviert</div>
	 * <div class="fr">Code de vaccin grippal inactivé, virion fragmenté</div>
	 * <div class="it">Code per Influenza vaccino inattivato, virus frammentati</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INFLUENZA_SPLIT_VIRION_VACCINE_CODE = "419826009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Inositol</div>
	 * <div class="de">Code für Inositol</div>
	 * <div class="fr">Code de inositol</div>
	 * <div class="it">Code per Inositolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INOSITOL_CODE = "72164009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Insulin aspart</div>
	 * <div class="de">Code für Insulin aspart</div>
	 * <div class="fr">Code de insuline asparte</div>
	 * <div class="it">Code per Insulina aspartat</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INSULIN_ASPART_CODE = "325072002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Insulin degludec</div>
	 * <div class="de">Code für Insulin degludec</div>
	 * <div class="fr">Code de insuline dégludec</div>
	 * <div class="it">Code per Insulina degludec</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INSULIN_DEGLUDEC_CODE = "710281005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Insulin detemir</div>
	 * <div class="de">Code für Insulin detemir</div>
	 * <div class="fr">Code de insuline détémir</div>
	 * <div class="it">Code per Insulina detemir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INSULIN_DETEMIR_CODE = "414515005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Insulin glargine</div>
	 * <div class="de">Code für Insulin glargin</div>
	 * <div class="fr">Code de insuline glargine</div>
	 * <div class="it">Code per Insulina glargine</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INSULIN_GLARGINE_CODE = "411529005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Insulin glulisine</div>
	 * <div class="de">Code für Insulin glulisin</div>
	 * <div class="fr">Code de insuline glulisine</div>
	 * <div class="it">Code per Insulina glulisina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INSULIN_GLULISINE_CODE = "411530000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Insulin lispro</div>
	 * <div class="de">Code für Insulin lispro</div>
	 * <div class="fr">Code de insuline lispro</div>
	 * <div class="it">Code per Insulina lispro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INSULIN_LISPRO_CODE = "412210000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Interferon alfa-2a</div>
	 * <div class="de">Code für Interferon alfa-2a</div>
	 * <div class="fr">Code de interféron alfa 2a</div>
	 * <div class="it">Code per Interferone alfa 2a</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INTERFERON_ALFA_2A_CODE = "386914007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Interferon alfa-2b</div>
	 * <div class="de">Code für Interferon alfa-2b</div>
	 * <div class="fr">Code de interféron alfa 2b</div>
	 * <div class="it">Code per Interferone alfa 2b</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INTERFERON_ALFA_2B_CODE = "386915008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Interferon beta-1a</div>
	 * <div class="de">Code für Interferon beta-1a</div>
	 * <div class="fr">Code de interféron bêta-1a</div>
	 * <div class="it">Code per Interferone beta 1a</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INTERFERON_BETA_1A_CODE = "386902004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Interferon beta-1b</div>
	 * <div class="de">Code für Interferon beta-1b</div>
	 * <div class="fr">Code de interféron bêta 1b</div>
	 * <div class="it">Code per Interferone beta 1b</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INTERFERON_BETA_1B_CODE = "386903009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Interferon gamma-1b</div>
	 * <div class="de">Code für Interferon gamma-1b</div>
	 * <div class="fr">Code de interféron gamma 1b</div>
	 * <div class="it">Code per Interferone gamma 1b</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INTERFERON_GAMMA_1B_CODE = "386901006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Inulin</div>
	 * <div class="de">Code für Inulin</div>
	 * <div class="fr">Code de inuline</div>
	 * <div class="it">Code per Inulina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String INULIN_CODE = "32154009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iodixanol</div>
	 * <div class="de">Code für Iodixanol</div>
	 * <div class="fr">Code de iodixanol</div>
	 * <div class="it">Code per Iodixanolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IODIXANOL_CODE = "395750001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iohexol</div>
	 * <div class="de">Code für Iohexol</div>
	 * <div class="fr">Code de iohexol</div>
	 * <div class="it">Code per Ioexolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IOHEXOL_CODE = "395751002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iopamidol</div>
	 * <div class="de">Code für Iopamidol</div>
	 * <div class="fr">Code de iopamidol</div>
	 * <div class="it">Code per Iopamidolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IOPAMIDOL_CODE = "395754005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iopromide</div>
	 * <div class="de">Code für Iopromid</div>
	 * <div class="fr">Code de iopromide</div>
	 * <div class="it">Code per Iopromide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IOPROMIDE_CODE = "395756007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ipilimumab</div>
	 * <div class="de">Code für Ipilimumab</div>
	 * <div class="fr">Code de ipilimumab</div>
	 * <div class="it">Code per Ipilimumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IPILIMUMAB_CODE = "697995005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ipratropium</div>
	 * <div class="de">Code für Ipratropium</div>
	 * <div class="fr">Code de ipratropium</div>
	 * <div class="it">Code per Ipratropio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IPRATROPIUM_CODE = "372518007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Irbesartan</div>
	 * <div class="de">Code für Irbesartan</div>
	 * <div class="fr">Code de irbésartan</div>
	 * <div class="it">Code per Irbesartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IRBESARTAN_CODE = "386877005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Irinotecan</div>
	 * <div class="de">Code für Irinotecan</div>
	 * <div class="fr">Code de irinotécan</div>
	 * <div class="it">Code per Irinotecan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IRINOTECAN_CODE = "372538008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iron</div>
	 * <div class="de">Code für Eisen</div>
	 * <div class="fr">Code de fer</div>
	 * <div class="it">Code per Ferro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IRON_CODE = "3829006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Iron polymaltose</div>
	 * <div class="de">Code für Eisen(III)-hydroxid-Polymaltose-Komplex</div>
	 * <div class="fr">Code de fer hydroxyde polymalté</div>
	 * <div class="it">Code per Ferro (III) idrossido polimaltosato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IRON_POLYMALTOSE_CODE = "708805001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isavuconazole</div>
	 * <div class="de">Code für Isavuconazol</div>
	 * <div class="fr">Code de isavuconazole</div>
	 * <div class="it">Code per Isavuconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISAVUCONAZOLE_CODE = "765386003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isoconazole</div>
	 * <div class="de">Code für Isoconazol</div>
	 * <div class="fr">Code de isoconazole</div>
	 * <div class="it">Code per Isoconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISOCONAZOLE_CODE = "418371001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isoleucine</div>
	 * <div class="de">Code für Isoleucin</div>
	 * <div class="fr">Code de isoleucine</div>
	 * <div class="it">Code per Isoleucina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISOLEUCINE_CODE = "14971004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isoniazid</div>
	 * <div class="de">Code für Isoniazid</div>
	 * <div class="fr">Code de isoniazide</div>
	 * <div class="it">Code per Isoniazide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISONIAZID_CODE = "387472004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isoprenaline</div>
	 * <div class="de">Code für Isoprenalin</div>
	 * <div class="fr">Code de isoprénaline</div>
	 * <div class="it">Code per Isoprenalina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISOPRENALINE_CODE = "372781009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isopropyl alcohol</div>
	 * <div class="de">Code für Isopropylalkohol</div>
	 * <div class="fr">Code de isopropanol</div>
	 * <div class="it">Code per Alcol isopropilico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISOPROPYL_ALCOHOL_CODE = "259268001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isosorbide dinitrate</div>
	 * <div class="de">Code für Isosorbid dinitrat</div>
	 * <div class="fr">Code de isosorbide dinitrate</div>
	 * <div class="it">Code per Isosorbide dinitrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISOSORBIDE_DINITRATE_CODE = "387332007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Isotretinoin</div>
	 * <div class="de">Code für Isotretinoin</div>
	 * <div class="fr">Code de isotrétinoïne</div>
	 * <div class="it">Code per Isotretinoina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ISOTRETINOIN_CODE = "387208003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Itraconazole</div>
	 * <div class="de">Code für Itraconazol</div>
	 * <div class="fr">Code de itraconazole</div>
	 * <div class="it">Code per Itraconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ITRACONAZOLE_CODE = "387532006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ivabradine</div>
	 * <div class="de">Code für Ivabradin</div>
	 * <div class="fr">Code de ivabradine</div>
	 * <div class="it">Code per Ivabradina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IVABRADINE_CODE = "421228002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ivacaftor</div>
	 * <div class="de">Code für Ivacaftor</div>
	 * <div class="fr">Code de ivacaftor</div>
	 * <div class="it">Code per Ivacaftor</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IVACAFTOR_CODE = "703823007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ivermectin</div>
	 * <div class="de">Code für Ivermectin</div>
	 * <div class="fr">Code de ivermectine</div>
	 * <div class="it">Code per Ivermectina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IVERMECTIN_CODE = "387559003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ixekizumab</div>
	 * <div class="de">Code für Ixekizumab</div>
	 * <div class="fr">Code de ixékizumab</div>
	 * <div class="it">Code per Ixekizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String IXEKIZUMAB_CODE = "724037000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ketamine</div>
	 * <div class="de">Code für Ketamin</div>
	 * <div class="fr">Code de kétamine</div>
	 * <div class="it">Code per Ketamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KETAMINE_CODE = "373464007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ketoconazole</div>
	 * <div class="de">Code für Ketoconazol</div>
	 * <div class="fr">Code de kétoconazole</div>
	 * <div class="it">Code per Ketoconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KETOCONAZOLE_CODE = "387216007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ketoprofen</div>
	 * <div class="de">Code für Ketoprofen</div>
	 * <div class="fr">Code de kétoprofène</div>
	 * <div class="it">Code per Ketoprofene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KETOPROFEN_CODE = "386832008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ketorolac</div>
	 * <div class="de">Code für Ketorolac</div>
	 * <div class="fr">Code de kétorolac</div>
	 * <div class="it">Code per Ketorolac</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KETOROLAC_CODE = "372501008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ketotifen</div>
	 * <div class="de">Code für Ketotifen</div>
	 * <div class="fr">Code de kétotifène</div>
	 * <div class="it">Code per Ketotifene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KETOTIFEN_CODE = "372642003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Labetalol</div>
	 * <div class="de">Code für Labetalol</div>
	 * <div class="fr">Code de labétalol</div>
	 * <div class="it">Code per Labetalolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LABETALOL_CODE = "372750000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lacosamide</div>
	 * <div class="de">Code für Lacosamid</div>
	 * <div class="fr">Code de lacosamide</div>
	 * <div class="it">Code per Lacosamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LACOSAMIDE_CODE = "441647003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lactitol</div>
	 * <div class="de">Code für Lactitol</div>
	 * <div class="fr">Code de lactitol</div>
	 * <div class="it">Code per Lattilolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LACTITOL_CODE = "418929008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lactose</div>
	 * <div class="de">Code für Lactose</div>
	 * <div class="fr">Code de lactose</div>
	 * <div class="it">Code per Lattosio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LACTOSE_CODE = "47703008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lactulose</div>
	 * <div class="de">Code für Lactulose</div>
	 * <div class="fr">Code de lactulose</div>
	 * <div class="it">Code per Lattulosio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LACTULOSE_CODE = "273945008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lamivudine</div>
	 * <div class="de">Code für Lamivudin</div>
	 * <div class="fr">Code de lamivudine</div>
	 * <div class="it">Code per Lamivudina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LAMIVUDINE_CODE = "386897000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lamotrigine</div>
	 * <div class="de">Code für Lamotrigin</div>
	 * <div class="fr">Code de lamotrigine</div>
	 * <div class="it">Code per Lamotrigina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LAMOTRIGINE_CODE = "387562000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lanreotide</div>
	 * <div class="de">Code für Lanreotid</div>
	 * <div class="fr">Code de lanréotide</div>
	 * <div class="it">Code per Lanreotide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LANREOTIDE_CODE = "395765000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lansoprazole</div>
	 * <div class="de">Code für Lansoprazol</div>
	 * <div class="fr">Code de lansoprazole</div>
	 * <div class="it">Code per Lansoprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LANSOPRAZOLE_CODE = "386888004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lanthanum carbonate</div>
	 * <div class="de">Code für Lanthan(III) carbonat</div>
	 * <div class="fr">Code de lanthane(III) carbonate</div>
	 * <div class="it">Code per Lantanio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LANTHANUM_CARBONATE_CODE = "414571007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Latanoprost</div>
	 * <div class="de">Code für Latanoprost</div>
	 * <div class="fr">Code de latanoprost</div>
	 * <div class="it">Code per Latanoprost</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LATANOPROST_CODE = "386926002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lauromacrogol 400</div>
	 * <div class="de">Code für Lauromacrogol 400</div>
	 * <div class="fr">Code de lauromacrogol 400</div>
	 * <div class="it">Code per Lauromacrogol</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LAUROMACROGOL_400_CODE = "427905004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Leflunomide</div>
	 * <div class="de">Code für Leflunomid</div>
	 * <div class="fr">Code de léflunomide</div>
	 * <div class="it">Code per Leflunomide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEFLUNOMIDE_CODE = "386981009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lenalidomide</div>
	 * <div class="de">Code für Lenalidomid</div>
	 * <div class="fr">Code de lénalidomide</div>
	 * <div class="it">Code per Lenalidomide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LENALIDOMIDE_CODE = "421471009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lenograstime</div>
	 * <div class="de">Code für Lenograstim</div>
	 * <div class="fr">Code de lénograstim</div>
	 * <div class="it">Code per Lenograstim</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LENOGRASTIME_CODE = "395767008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lercanidipine</div>
	 * <div class="de">Code für Lercanidipin</div>
	 * <div class="fr">Code de lercanidipine</div>
	 * <div class="it">Code per Lercanidipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LERCANIDIPINE_CODE = "395986007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Letrozole</div>
	 * <div class="de">Code für Letrozol</div>
	 * <div class="fr">Code de létrozole</div>
	 * <div class="it">Code per Letrozolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LETROZOLE_CODE = "386911004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Leucine</div>
	 * <div class="de">Code für Leucin</div>
	 * <div class="fr">Code de leucine</div>
	 * <div class="it">Code per Leucina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEUCINE_CODE = "83797003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Leuprorelin</div>
	 * <div class="de">Code für Leuprorelin</div>
	 * <div class="fr">Code de leuproréline</div>
	 * <div class="it">Code per Leuprorelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEUPRORELIN_CODE = "397198002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levetiracetam</div>
	 * <div class="de">Code für Levetiracetam</div>
	 * <div class="fr">Code de lévétiracétam</div>
	 * <div class="it">Code per Levetiracetam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVETIRACETAM_CODE = "387000003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levobupivacaine</div>
	 * <div class="de">Code für Levobupivacain</div>
	 * <div class="fr">Code de lévobupivacaïne</div>
	 * <div class="it">Code per Levobupivacaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOBUPIVACAINE_CODE = "387011006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levocabastine</div>
	 * <div class="de">Code für Levocabastin</div>
	 * <div class="fr">Code de lévocabastine</div>
	 * <div class="it">Code per Levocabastina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOCABASTINE_CODE = "372554006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levocarnitine</div>
	 * <div class="de">Code für Levocarnitin</div>
	 * <div class="fr">Code de lévocarnitine</div>
	 * <div class="it">Code per Levocarnitina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOCARNITINE_CODE = "372601001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levocetirizine</div>
	 * <div class="de">Code für Levocetirizin</div>
	 * <div class="fr">Code de lévocétirizine</div>
	 * <div class="it">Code per Levocetirizina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOCETIRIZINE_CODE = "421889003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levodopa</div>
	 * <div class="de">Code für Levodopa</div>
	 * <div class="fr">Code de lévodopa</div>
	 * <div class="it">Code per Levodopa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVODOPA_CODE = "387086006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levofloxacin</div>
	 * <div class="de">Code für Levofloxacin</div>
	 * <div class="fr">Code de lévofloxacine</div>
	 * <div class="it">Code per Levofloxacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOFLOXACIN_CODE = "387552007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levomepromazine</div>
	 * <div class="de">Code für Levomepromazin</div>
	 * <div class="fr">Code de lévomépromazine</div>
	 * <div class="it">Code per Levomepromazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOMEPROMAZINE_CODE = "387509007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levonorgestrel</div>
	 * <div class="de">Code für Levonorgestrel</div>
	 * <div class="fr">Code de lévonorgestrel</div>
	 * <div class="it">Code per Levonorgestrel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVONORGESTREL_CODE = "126109000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levosimendan</div>
	 * <div class="de">Code für Levosimendan</div>
	 * <div class="fr">Code de lévosimendan</div>
	 * <div class="it">Code per Levosimendan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOSIMENDAN_CODE = "442795003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Levothyroxine</div>
	 * <div class="de">Code für Levothyroxin</div>
	 * <div class="fr">Code de lévothyroxine</div>
	 * <div class="it">Code per Levotiroxina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEVOTHYROXINE_CODE = "710809001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lidocaine</div>
	 * <div class="de">Code für Lidocain</div>
	 * <div class="fr">Code de lidocaïne</div>
	 * <div class="it">Code per Lidocaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LIDOCAINE_CODE = "387480006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Linagliptin</div>
	 * <div class="de">Code für Linagliptin</div>
	 * <div class="fr">Code de linagliptine</div>
	 * <div class="it">Code per Linagliptin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LINAGLIPTIN_CODE = "702798009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Linezolid</div>
	 * <div class="de">Code für Linezolid</div>
	 * <div class="fr">Code de linézolide</div>
	 * <div class="it">Code per Linezolid</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LINEZOLID_CODE = "387056004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Liothyronine</div>
	 * <div class="de">Code für Liothyronin</div>
	 * <div class="fr">Code de liothyronine</div>
	 * <div class="it">Code per Liotironina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LIOTHYRONINE_CODE = "61275002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Liraglutide</div>
	 * <div class="de">Code für Liraglutid</div>
	 * <div class="fr">Code de liraglutide</div>
	 * <div class="it">Code per Liraglutide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LIRAGLUTIDE_CODE = "444828003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lisdexamfetamine</div>
	 * <div class="de">Code für Lisdexamfetamin</div>
	 * <div class="fr">Code de lisdexamfétamine</div>
	 * <div class="it">Code per Lisdexamfetamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LISDEXAMFETAMINE_CODE = "425597005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lisinopril</div>
	 * <div class="de">Code für Lisinopril</div>
	 * <div class="fr">Code de lisinopril</div>
	 * <div class="it">Code per Lisinopril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LISINOPRIL_CODE = "386873009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lithium acetate</div>
	 * <div class="de">Code für Lithium acetat</div>
	 * <div class="fr">Code de lithium acétate</div>
	 * <div class="it">Code per Litio acetato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LITHIUM_ACETATE_CODE = "111080000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lithium carbonate</div>
	 * <div class="de">Code für Lithium carbonat</div>
	 * <div class="fr">Code de lithium carbonate</div>
	 * <div class="it">Code per Litio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LITHIUM_CARBONATE_CODE = "387095003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lithium sulfate</div>
	 * <div class="de">Code für Lithiumsulfat</div>
	 * <div class="fr">Code de lithium sulfate</div>
	 * <div class="it">Code per Litio solfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LITHIUM_SULFATE_CODE = "708197001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Live typhoid vaccine</div>
	 * <div class="de">Code für Typhus-Lebend-Impfstoff</div>
	 * <div class="fr">Code de typhus vaccin vivant</div>
	 * <div class="it">Code per Tifo vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LIVE_TYPHOID_VACCINE_CODE = "764303004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lixisenatide</div>
	 * <div class="de">Code für Lixisenatid</div>
	 * <div class="fr">Code de lixisénatide</div>
	 * <div class="it">Code per Lixisenatide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LIXISENATIDE_CODE = "708808004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lomustine</div>
	 * <div class="de">Code für Lomustin</div>
	 * <div class="fr">Code de lomustine</div>
	 * <div class="it">Code per Lomustina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LOMUSTINE_CODE = "387227009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lonoctocog alfa</div>
	 * <div class="de">Code für Lonoctocog alfa</div>
	 * <div class="fr">Code de lonoctocog alfa</div>
	 * <div class="it">Code per Lonoctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LONOCTOCOG_ALFA_CODE = "1012961000168107";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Loperamide</div>
	 * <div class="de">Code für Loperamid</div>
	 * <div class="fr">Code de lopéramide</div>
	 * <div class="it">Code per Loperamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LOPERAMIDE_CODE = "387040009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lopinavir</div>
	 * <div class="de">Code für Lopinavir</div>
	 * <div class="fr">Code de lopinavir</div>
	 * <div class="it">Code per Lopinavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LOPINAVIR_CODE = "387067003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Loratadine</div>
	 * <div class="de">Code für Loratadin</div>
	 * <div class="fr">Code de loratadine</div>
	 * <div class="it">Code per Loratadina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LORATADINE_CODE = "386884002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lorazepam</div>
	 * <div class="de">Code für Lorazepam</div>
	 * <div class="fr">Code de lorazépam</div>
	 * <div class="it">Code per Lorazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LORAZEPAM_CODE = "387106007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lormetazepam</div>
	 * <div class="de">Code für Lormetazepam</div>
	 * <div class="fr">Code de lormétazépam</div>
	 * <div class="it">Code per Lormetazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LORMETAZEPAM_CODE = "387570005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Losartan</div>
	 * <div class="de">Code für Losartan</div>
	 * <div class="fr">Code de losartan</div>
	 * <div class="it">Code per Losartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LOSARTAN_CODE = "373567002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lumefantrine</div>
	 * <div class="de">Code für Lumefantrin</div>
	 * <div class="fr">Code de luméfantrine</div>
	 * <div class="it">Code per Lumefantrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LUMEFANTRINE_CODE = "420307001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lurasidone</div>
	 * <div class="de">Code für Lurasidon</div>
	 * <div class="fr">Code de lurasidone</div>
	 * <div class="it">Code per Lurasidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LURASIDONE_CODE = "703115008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lutropin alfa</div>
	 * <div class="de">Code für Lutropin alfa</div>
	 * <div class="fr">Code de lutropine alfa</div>
	 * <div class="it">Code per Lutropina alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LUTROPIN_ALFA_CODE = "415248001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Lysine</div>
	 * <div class="de">Code für Lysin</div>
	 * <div class="fr">Code de lysine</div>
	 * <div class="it">Code per Lisina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LYSINE_CODE = "75799006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Macitentan</div>
	 * <div class="de">Code für Macitentan</div>
	 * <div class="fr">Code de macitentan</div>
	 * <div class="it">Code per Macitentan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MACITENTAN_CODE = "710283008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Macrogol</div>
	 * <div class="de">Code für Macrogol</div>
	 * <div class="fr">Code de macrogol</div>
	 * <div class="it">Code per Macrogol</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MACROGOL_CODE = "8030004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Macrogol 3350</div>
	 * <div class="de">Code für Macrogol 3350</div>
	 * <div class="fr">Code de macrogol 3350</div>
	 * <div class="it">Code per Macrogol 3350</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MACROGOL_3350_CODE = "712566007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Macrogol 4000</div>
	 * <div class="de">Code für Macrogol 4000</div>
	 * <div class="fr">Code de macrogol 4000</div>
	 * <div class="it">Code per Macrogol 4000</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MACROGOL_4000_CODE = "712567003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magaldrate</div>
	 * <div class="de">Code für Magaldrat</div>
	 * <div class="fr">Code de magaldrate</div>
	 * <div class="it">Code per Magaldrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGALDRATE_CODE = "387240004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium</div>
	 * <div class="de">Code für Magnesium</div>
	 * <div class="fr">Code de magnésium</div>
	 * <div class="it">Code per Magnesio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_CODE = "72717003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium acetate tetrahydrate</div>
	 * <div class="de">Code für Magnesium diacetat-4-Wasser</div>
	 * <div class="fr">Code de magnésium acétate tétrahydrique</div>
	 * <div class="it">Code per Magnesio acetato tetraidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_ACETATE_TETRAHYDRATE_CODE = "723586001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium carbonate</div>
	 * <div class="de">Code für Magnesium carbonat</div>
	 * <div class="fr">Code de magnésium carbonate</div>
	 * <div class="it">Code per Magnesio carbonato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_CARBONATE_CODE = "387401007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium chloride</div>
	 * <div class="de">Code für Magnesiumchlorid</div>
	 * <div class="fr">Code de magnésium chlorure</div>
	 * <div class="it">Code per Magnesio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_CHLORIDE_CODE = "45733002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium gluconate</div>
	 * <div class="de">Code für Magnesium digluconat wasserfrei</div>
	 * <div class="fr">Code de magnésium digluconate anhydre</div>
	 * <div class="it">Code per Magnesio gluconato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_GLUCONATE_CODE = "116126005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium hydroxide</div>
	 * <div class="de">Code für Magnesiumhydroxid</div>
	 * <div class="fr">Code de magnésium hydroxyde</div>
	 * <div class="it">Code per Magnesio idrossido</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_HYDROXIDE_CODE = "387337001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Magnesium sulfate</div>
	 * <div class="de">Code für Magnesiumsulfat</div>
	 * <div class="fr">Code de magnésium sulfate</div>
	 * <div class="it">Code per Magnesio solfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MAGNESIUM_SULFATE_CODE = "387202002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mannitol</div>
	 * <div class="de">Code für Mannitol</div>
	 * <div class="fr">Code de mannitol</div>
	 * <div class="it">Code per Mannitolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MANNITOL_CODE = "387168006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Maraviroc</div>
	 * <div class="de">Code für Maraviroc</div>
	 * <div class="fr">Code de maraviroc</div>
	 * <div class="it">Code per Maraviroc</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MARAVIROC_CODE = "429603001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Measles, mumps and rubella vaccine</div>
	 * <div class="de">Code für Masern-Mumps-Röteln-Lebendimpfstoff</div>
	 * <div class="fr">Code de rougeole-oreillons-rubéole vaccin vivant</div>
	 * <div class="it">Code per Morbillo, parotite, rosolia vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEASLES_MUMPS_AND_RUBELLA_VACCINE_CODE = "396429000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Measles vaccine</div>
	 * <div class="de">Code für Masern-Lebend-Impfstoff</div>
	 * <div class="fr">Code de rougeole vaccin vivant</div>
	 * <div class="it">Code per Morbillo vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEASLES_VACCINE_CODE = "396427003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mebendazole</div>
	 * <div class="de">Code für Mebendazol</div>
	 * <div class="fr">Code de mébendazole</div>
	 * <div class="it">Code per Mebendazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEBENDAZOLE_CODE = "387311004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mebeverine</div>
	 * <div class="de">Code für Mebeverin</div>
	 * <div class="fr">Code de mébévérine</div>
	 * <div class="it">Code per Mebeverina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEBEVERINE_CODE = "419830007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Meclozine</div>
	 * <div class="de">Code für Meclozin</div>
	 * <div class="fr">Code de méclozine</div>
	 * <div class="it">Code per Meclozina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MECLOZINE_CODE = "372879002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Medium chain triglyceride</div>
	 * <div class="de">Code für Triglyceride mittelkettige</div>
	 * <div class="fr">Code de triglycérides à chaîne moyenne</div>
	 * <div class="it">Code per Trigliceridi a catena media</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEDIUM_CHAIN_TRIGLYCERIDE_CODE = "395781005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Medroxyprogesterone</div>
	 * <div class="de">Code für Medroxyprogesteron</div>
	 * <div class="fr">Code de médroxyprogestérone</div>
	 * <div class="it">Code per Medrossiprogesterone acetato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEDROXYPROGESTERONE_CODE = "126113007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mefenamic acid</div>
	 * <div class="de">Code für Mefenaminsäure</div>
	 * <div class="fr">Code de acide méfénamique</div>
	 * <div class="it">Code per Acido mefenamico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEFENAMIC_ACID_CODE = "387185008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mefloquine</div>
	 * <div class="de">Code für Mefloquin</div>
	 * <div class="fr">Code de méfloquine</div>
	 * <div class="it">Code per Meflochina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEFLOQUINE_CODE = "387505001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Meglumine</div>
	 * <div class="de">Code für Meglumin</div>
	 * <div class="fr">Code de méglumine</div>
	 * <div class="it">Code per Meglumina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEGLUMINE_CODE = "769091004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Melatonin</div>
	 * <div class="de">Code für Melatonin</div>
	 * <div class="fr">Code de mélatonine</div>
	 * <div class="it">Code per Melatonina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MELATONIN_CODE = "41199001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Melitracen</div>
	 * <div class="de">Code für Melitracen</div>
	 * <div class="fr">Code de mélitracène</div>
	 * <div class="it">Code per Melitracene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MELITRACEN_CODE = "712683007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Melperone</div>
	 * <div class="de">Code für Melperon</div>
	 * <div class="fr">Code de melpérone</div>
	 * <div class="it">Code per Melperone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MELPERONE_CODE = "442519006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Melphalan</div>
	 * <div class="de">Code für Melphalan</div>
	 * <div class="fr">Code de melphalan</div>
	 * <div class="it">Code per Melphalan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MELPHALAN_CODE = "387297002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Memantine</div>
	 * <div class="de">Code für Memantin</div>
	 * <div class="fr">Code de mémantine</div>
	 * <div class="it">Code per Memantina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEMANTINE_CODE = "406458000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Meningococcus group C vaccine</div>
	 * <div class="de">Code für Meningokokken C-Saccharid-T-Konjugat-Impfstoff</div>
	 * <div class="fr">Code de méningite vaccin polysaccharidique C, conjugué T</div>
	 * <div class="it">Code per Meningite vaccino polisaccaridico C, coniugato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MENINGOCOCCUS_GROUP_C_VACCINE_CODE = "768366003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Meningococcus vaccine</div>
	 * <div class="de">Code für Meningokokken-Impfstoff</div>
	 * <div class="fr">Code de méningite vaccin</div>
	 * <div class="it">Code per Meningite vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MENINGOCOCCUS_VACCINE_CODE = "424891007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mepivacaine</div>
	 * <div class="de">Code für Mepivacain</div>
	 * <div class="fr">Code de mépivacaïne</div>
	 * <div class="it">Code per Mepicacaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEPIVACAINE_CODE = "59560006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mercaptamine</div>
	 * <div class="de">Code für Mercaptamin</div>
	 * <div class="fr">Code de mercaptamine</div>
	 * <div class="it">Code per Mercaptamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MERCAPTAMINE_CODE = "373457005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Meropenem</div>
	 * <div class="de">Code für Meropenem</div>
	 * <div class="fr">Code de méropénem</div>
	 * <div class="it">Code per Meropenem</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEROPENEM_CODE = "387540000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mesalazine</div>
	 * <div class="de">Code für Mesalazin</div>
	 * <div class="fr">Code de mésalazine</div>
	 * <div class="it">Code per Mesalazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MESALAZINE_CODE = "387501005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mesna</div>
	 * <div class="de">Code für Mesna</div>
	 * <div class="fr">Code de mesna</div>
	 * <div class="it">Code per Mesna</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MESNA_CODE = "386922000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Metamizole</div>
	 * <div class="de">Code für Metamizol</div>
	 * <div class="fr">Code de métamizole</div>
	 * <div class="it">Code per Metamizolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METAMIZOLE_CODE = "780831000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Metformin</div>
	 * <div class="de">Code für Metformin</div>
	 * <div class="fr">Code de metformine</div>
	 * <div class="it">Code per Metformina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METFORMIN_CODE = "372567009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methadone</div>
	 * <div class="de">Code für Methadon</div>
	 * <div class="fr">Code de méthadone</div>
	 * <div class="it">Code per Metadone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHADONE_CODE = "387286002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methionine</div>
	 * <div class="de">Code für Methionin</div>
	 * <div class="fr">Code de méthionine</div>
	 * <div class="it">Code per Metionina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHIONINE_CODE = "70288006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methotrexate</div>
	 * <div class="de">Code für Methotrexat</div>
	 * <div class="fr">Code de méthotrexate</div>
	 * <div class="it">Code per Metotrexato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHOTREXATE_CODE = "387381009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methoxsalen</div>
	 * <div class="de">Code für Methoxsalen</div>
	 * <div class="fr">Code de méthoxsalène</div>
	 * <div class="it">Code per Metoxsalene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHOXSALEN_CODE = "41062004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methoxy polyethylene glycol-epoetin beta</div>
	 * <div class="de">Code für PEG-Epoetin beta</div>
	 * <div class="fr">Code de époétine bêta pégylée</div>
	 * <div class="it">Code per Metossipolietilenglicole-epoetina beta</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHOXY_POLYETHYLENE_GLYCOL_EPOETIN_BETA_CODE = "425913002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methyldopa anhydrous</div>
	 * <div class="de">Code für Methyldopa</div>
	 * <div class="fr">Code de méthyldopa</div>
	 * <div class="it">Code per Metildopa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHYLDOPA_ANHYDROUS_CODE = "768043006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methylene blue stain</div>
	 * <div class="de">Code für Methylthioninium chlorid</div>
	 * <div class="fr">Code de méthylthionine chlorure</div>
	 * <div class="it">Code per Metiltioninio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHYLENE_BLUE_STAIN_CODE = "6725000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methylergometrine</div>
	 * <div class="de">Code für Methylergometrin</div>
	 * <div class="fr">Code de méthylergométrine</div>
	 * <div class="it">Code per Metilergometrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHYLERGOMETRINE_CODE = "126074008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methylphenidate</div>
	 * <div class="de">Code für Methylphenidat</div>
	 * <div class="fr">Code de méthylphénidate</div>
	 * <div class="it">Code per Metilfenidato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHYLPHENIDATE_CODE = "373337007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Methylprednisolone</div>
	 * <div class="de">Code für Methylprednisolon</div>
	 * <div class="fr">Code de méthylprednisolone</div>
	 * <div class="it">Code per Metilprednisolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METHYLPREDNISOLONE_CODE = "116593003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Metoclopramide</div>
	 * <div class="de">Code für Metoclopramid</div>
	 * <div class="fr">Code de métoclopramide</div>
	 * <div class="it">Code per Metoclopramide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METOCLOPRAMIDE_CODE = "372776000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Metolazone</div>
	 * <div class="de">Code für Metolazon</div>
	 * <div class="fr">Code de métolazone</div>
	 * <div class="it">Code per Metolazone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METOLAZONE_CODE = "387123003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Metoprolol</div>
	 * <div class="de">Code für Metoprolol</div>
	 * <div class="fr">Code de métoprolol</div>
	 * <div class="it">Code per Metoprololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METOPROLOL_CODE = "372826007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Metronidazole</div>
	 * <div class="de">Code für Metronidazol</div>
	 * <div class="fr">Code de métronidazole</div>
	 * <div class="it">Code per Metronidazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String METRONIDAZOLE_CODE = "372602008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mianserin hydrochloride</div>
	 * <div class="de">Code für Mianserin hydrochlorid</div>
	 * <div class="fr">Code de miansérine chlorhydrate</div>
	 * <div class="it">Code per Mianserina HCL</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIANSERIN_HYDROCHLORIDE_CODE = "395795008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Miconazole</div>
	 * <div class="de">Code für Miconazol</div>
	 * <div class="fr">Code de miconazole</div>
	 * <div class="it">Code per Miconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MICONAZOLE_CODE = "372738006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Midazolam</div>
	 * <div class="de">Code für Midazolam</div>
	 * <div class="fr">Code de midazolam</div>
	 * <div class="it">Code per Midazolam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIDAZOLAM_CODE = "373476007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Midodrine</div>
	 * <div class="de">Code für Midodrin</div>
	 * <div class="fr">Code de midodrine</div>
	 * <div class="it">Code per Midodrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIDODRINE_CODE = "372504000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mifepristone</div>
	 * <div class="de">Code für Mifepriston</div>
	 * <div class="fr">Code de mifépristone</div>
	 * <div class="it">Code per Miferpristone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIFEPRISTONE_CODE = "395796009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Milrinone</div>
	 * <div class="de">Code für Milrinon</div>
	 * <div class="fr">Code de milrinone</div>
	 * <div class="it">Code per Milrinone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MILRINONE_CODE = "373441005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Minocycline</div>
	 * <div class="de">Code für Minocyclin</div>
	 * <div class="fr">Code de minocycline</div>
	 * <div class="it">Code per Minociclina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MINOCYCLINE_CODE = "372653009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Minoxidil</div>
	 * <div class="de">Code für Minoxidil</div>
	 * <div class="fr">Code de minoxidil</div>
	 * <div class="it">Code per Minoxidil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MINOXIDIL_CODE = "387272001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mirabegron</div>
	 * <div class="de">Code für Mirabegron</div>
	 * <div class="fr">Code de mirabégron</div>
	 * <div class="it">Code per Mirabegron</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIRABEGRON_CODE = "703803006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mirtazapine</div>
	 * <div class="de">Code für Mirtazapin</div>
	 * <div class="fr">Code de mirtazapine</div>
	 * <div class="it">Code per Mirtazapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIRTAZAPINE_CODE = "386847004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Misoprostol</div>
	 * <div class="de">Code für Misoprostol</div>
	 * <div class="fr">Code de misoprostol</div>
	 * <div class="it">Code per Misoprostolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MISOPROSTOL_CODE = "387242007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mitomycin</div>
	 * <div class="de">Code für Mitomycin</div>
	 * <div class="fr">Code de mitomycine</div>
	 * <div class="it">Code per Mitomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MITOMYCIN_CODE = "387331000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mitoxantrone</div>
	 * <div class="de">Code für Mitoxantron</div>
	 * <div class="fr">Code de mitoxantrone</div>
	 * <div class="it">Code per Mitoxantrone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MITOXANTRONE_CODE = "386913001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mivacurium chloride</div>
	 * <div class="de">Code für Mivacurium chlorid</div>
	 * <div class="fr">Code de mivacurium chlorure</div>
	 * <div class="it">Code per Mivacurio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MIVACURIUM_CHLORIDE_CODE = "108447000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Moclobemide</div>
	 * <div class="de">Code für Moclobemid</div>
	 * <div class="fr">Code de moclobémide</div>
	 * <div class="it">Code per Moclobemide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MOCLOBEMIDE_CODE = "395800003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Modafinil</div>
	 * <div class="de">Code für Modafinil</div>
	 * <div class="fr">Code de modafinil</div>
	 * <div class="it">Code per Modafinil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MODAFINIL_CODE = "387004007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Molsidomine</div>
	 * <div class="de">Code für Molsidomin</div>
	 * <div class="fr">Code de molsidomine</div>
	 * <div class="it">Code per Molsidomina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MOLSIDOMINE_CODE = "698196008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mometasone</div>
	 * <div class="de">Code für Mometason</div>
	 * <div class="fr">Code de mométasone</div>
	 * <div class="it">Code per Mometasone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MOMETASONE_CODE = "395990009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Montelukast</div>
	 * <div class="de">Code für Montelukast</div>
	 * <div class="fr">Code de montélukast</div>
	 * <div class="it">Code per Montelukast</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MONTELUKAST_CODE = "373728005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Moroctocog alfa</div>
	 * <div class="de">Code für Moroctocog alfa</div>
	 * <div class="fr">Code de moroctocog alfa</div>
	 * <div class="it">Code per Moroctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MOROCTOCOG_ALFA_CODE = "441764007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Morphine</div>
	 * <div class="de">Code für Morphin</div>
	 * <div class="fr">Code de morphine</div>
	 * <div class="it">Code per Morfina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MORPHINE_CODE = "373529000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Moxifloxacin</div>
	 * <div class="de">Code für Moxifloxacin</div>
	 * <div class="fr">Code de moxifloxacine</div>
	 * <div class="it">Code per Moxifloxacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MOXIFLOXACIN_CODE = "412439003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Moxonidine</div>
	 * <div class="de">Code für Moxonidin</div>
	 * <div class="fr">Code de moxonidine</div>
	 * <div class="it">Code per Moxonidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MOXONIDINE_CODE = "395805008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mumps vaccine</div>
	 * <div class="de">Code für Mumps-Lebend-Impfstoff</div>
	 * <div class="fr">Code de virus des oreillons vaccin vivant</div>
	 * <div class="it">Code per Parotite vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MUMPS_VACCINE_CODE = "396431009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mupirocin</div>
	 * <div class="de">Code für Mupirocin</div>
	 * <div class="fr">Code de mupirocine</div>
	 * <div class="it">Code per Mupirocina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MUPIROCIN_CODE = "387397004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mycophenolate mofetil</div>
	 * <div class="de">Code für Mycophenolat mofetil</div>
	 * <div class="fr">Code de mycophénolate mofétil</div>
	 * <div class="it">Code per Micofenolato mofetile</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MYCOPHENOLATE_MOFETIL_CODE = "386976000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Mycophenolic acid</div>
	 * <div class="de">Code für Mycophenolsäure</div>
	 * <div class="fr">Code de acide mycophénolique</div>
	 * <div class="it">Code per Acido micofenolico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MYCOPHENOLIC_ACID_CODE = "409330005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nadolol</div>
	 * <div class="de">Code für Nadolol</div>
	 * <div class="fr">Code de nadolol</div>
	 * <div class="it">Code per Nadololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NADOLOL_CODE = "387482003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nadroparine</div>
	 * <div class="de">Code für Nadroparin</div>
	 * <div class="fr">Code de nadroparine</div>
	 * <div class="it">Code per Nadroparina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NADROPARINE_CODE = "699946002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nadroparin calcium</div>
	 * <div class="de">Code für Nadroparin calcium</div>
	 * <div class="fr">Code de nadroparine calcique</div>
	 * <div class="it">Code per Nadroparina calcica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NADROPARIN_CALCIUM_CODE = "698278006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naftazone</div>
	 * <div class="de">Code für Naftazon</div>
	 * <div class="fr">Code de naftazone</div>
	 * <div class="it">Code per Naftazone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NAFTAZONE_CODE = "713428001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naftidrofuryl</div>
	 * <div class="de">Code für Naftidrofuryl</div>
	 * <div class="fr">Code de naftidrofuryl</div>
	 * <div class="it">Code per Naftidrofurile</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NAFTIDROFURYL_CODE = "395992001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nalmefene</div>
	 * <div class="de">Code für Nalmefen</div>
	 * <div class="fr">Code de nalméfène</div>
	 * <div class="it">Code per Nalmefene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NALMEFENE_CODE = "109098006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naloxone</div>
	 * <div class="de">Code für Naloxon</div>
	 * <div class="fr">Code de naloxone</div>
	 * <div class="it">Code per Naloxone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NALOXONE_CODE = "372890007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naltrexone</div>
	 * <div class="de">Code für Naltrexon</div>
	 * <div class="fr">Code de naltrexone</div>
	 * <div class="it">Code per Naltrexone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NALTREXONE_CODE = "373546002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naphazoline</div>
	 * <div class="de">Code für Naphazolin</div>
	 * <div class="fr">Code de naphazoline</div>
	 * <div class="it">Code per Nafazolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NAPHAZOLINE_CODE = "372803000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naproxen</div>
	 * <div class="de">Code für Naproxen</div>
	 * <div class="fr">Code de naproxène</div>
	 * <div class="it">Code per Naprossene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NAPROXEN_CODE = "372588000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Naratriptan</div>
	 * <div class="de">Code für Naratriptan</div>
	 * <div class="fr">Code de naratriptan</div>
	 * <div class="it">Code per Naratriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NARATRIPTAN_CODE = "363571003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Natalizumab</div>
	 * <div class="de">Code für Natalizumab</div>
	 * <div class="fr">Code de natalizumab</div>
	 * <div class="it">Code per Natalizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NATALIZUMAB_CODE = "414805007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nateglinide</div>
	 * <div class="de">Code für Nateglinid</div>
	 * <div class="fr">Code de natéglinide</div>
	 * <div class="it">Code per Nateglinide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NATEGLINIDE_CODE = "387070004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nebivolol</div>
	 * <div class="de">Code für Nebivolol</div>
	 * <div class="fr">Code de nébivolol</div>
	 * <div class="it">Code per Nebivololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NEBIVOLOL_CODE = "395808005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nelfinavir</div>
	 * <div class="de">Code für Nelfinavir</div>
	 * <div class="fr">Code de nelfinavir</div>
	 * <div class="it">Code per Nelfinavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NELFINAVIR_CODE = "373445001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Neomycin</div>
	 * <div class="de">Code für Neomycin</div>
	 * <div class="fr">Code de néomycine</div>
	 * <div class="it">Code per Neomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NEOMYCIN_CODE = "373528008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Neostigmine</div>
	 * <div class="de">Code für Neostigmin</div>
	 * <div class="fr">Code de néostigmine</div>
	 * <div class="it">Code per Neostigmina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NEOSTIGMINE_CODE = "373346001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nevirapine</div>
	 * <div class="de">Code für Nevirapin</div>
	 * <div class="fr">Code de névirapine</div>
	 * <div class="it">Code per Nevirapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NEVIRAPINE_CODE = "386898005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nicardipine</div>
	 * <div class="de">Code für Nicardipin</div>
	 * <div class="fr">Code de nicardipine</div>
	 * <div class="it">Code per Nicardipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NICARDIPINE_CODE = "372502001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nicorandil</div>
	 * <div class="de">Code für Nicorandil</div>
	 * <div class="fr">Code de nicorandil</div>
	 * <div class="it">Code per Nicorandil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NICORANDIL_CODE = "395809002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nicotinamide</div>
	 * <div class="de">Code für Nicotinamid</div>
	 * <div class="fr">Code de nicotinamide</div>
	 * <div class="it">Code per Nicotinamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NICOTINAMIDE_CODE = "173196005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nifedipine</div>
	 * <div class="de">Code für Nifedipin</div>
	 * <div class="fr">Code de nifédipine</div>
	 * <div class="it">Code per Nifedipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NIFEDIPINE_CODE = "387490003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nimesulide</div>
	 * <div class="de">Code für Nimesulid</div>
	 * <div class="fr">Code de nimésulide</div>
	 * <div class="it">Code per Nimesulide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NIMESULIDE_CODE = "703479000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nimodipine</div>
	 * <div class="de">Code für Nimodipin</div>
	 * <div class="fr">Code de nimodipine</div>
	 * <div class="it">Code per Nimodipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NIMODIPINE_CODE = "387502003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for nimésulide</div>
	 * <div class="de">Code für Nicotin</div>
	 * <div class="fr">Code de nicotine</div>
	 * <div class="it">Code per Nicotina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NIM_SULIDE_CODE = "68540007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nintedanib</div>
	 * <div class="de">Code für Nintedanib</div>
	 * <div class="fr">Code de nintédanib</div>
	 * <div class="it">Code per Nintedanib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NINTEDANIB_CODE = "712494002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nitazoxanide</div>
	 * <div class="de">Code für Nitazoxanid</div>
	 * <div class="fr">Code de nitazoxanide</div>
	 * <div class="it">Code per Nitazoxanide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NITAZOXANIDE_CODE = "407148001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nitisinone</div>
	 * <div class="de">Code für Nitisinon</div>
	 * <div class="fr">Code de nitisinone</div>
	 * <div class="it">Code per Nitisinone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NITISINONE_CODE = "385996000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nitrazepam</div>
	 * <div class="de">Code für Nitrazepam</div>
	 * <div class="fr">Code de nitrazépam</div>
	 * <div class="it">Code per Nitrazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NITRAZEPAM_CODE = "387449001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nitrendipine</div>
	 * <div class="de">Code für Nitrendipin</div>
	 * <div class="fr">Code de nitrendipine</div>
	 * <div class="it">Code per Nitrendipina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NITRENDIPINE_CODE = "444757009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nitrofurantoin</div>
	 * <div class="de">Code für Nitrofurantoin</div>
	 * <div class="fr">Code de nitrofurantoïne</div>
	 * <div class="it">Code per Nitrofurantoina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NITROFURANTOIN_CODE = "373543005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nivolumab</div>
	 * <div class="de">Code für Nivolumab</div>
	 * <div class="fr">Code de nivolumab</div>
	 * <div class="it">Code per Nivolumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NIVOLUMAB_CODE = "704191007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nomegestrol acetate</div>
	 * <div class="de">Code für Nomegestrol acetat</div>
	 * <div class="fr">Code de nomégestrol acétate</div>
	 * <div class="it">Code per Nomegestrolo acetato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NOMEGESTROL_ACETATE_CODE = "698277001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Norepinephrine</div>
	 * <div class="de">Code für Noradrenalin (Norepinephrin)</div>
	 * <div class="fr">Code de noradrénaline (norépinéphrine)</div>
	 * <div class="it">Code per Noradrenalina (norepinefrina)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NOREPINEPHRINE_CODE = "45555007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Norethisterone</div>
	 * <div class="de">Code für Norethisteron</div>
	 * <div class="fr">Code de noréthistérone</div>
	 * <div class="it">Code per Noretisterone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NORETHISTERONE_CODE = "126102009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Norfloxacin</div>
	 * <div class="de">Code für Norfloxacin</div>
	 * <div class="fr">Code de norfloxacine</div>
	 * <div class="it">Code per Norfloxacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NORFLOXACIN_CODE = "387271008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Norgestimate</div>
	 * <div class="de">Code für Norgestimat</div>
	 * <div class="fr">Code de norgestimate</div>
	 * <div class="it">Code per Norgestimato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NORGESTIMATE_CODE = "126115000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Norgestrel</div>
	 * <div class="de">Code für Norgestrel</div>
	 * <div class="fr">Code de norgestrel</div>
	 * <div class="it">Code per Norgestrel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NORGESTREL_CODE = "126106007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Normal immunoglobulin human</div>
	 * <div class="de">Code für Immunglobulin vom Menschen</div>
	 * <div class="fr">Code de immunoglobuline humaine normale</div>
	 * <div class="it">Code per Immunoglobulina umana normale</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NORMAL_IMMUNOGLOBULIN_HUMAN_CODE = "713355009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nortriptyline</div>
	 * <div class="de">Code für Nortriptylin</div>
	 * <div class="fr">Code de nortriptyline</div>
	 * <div class="it">Code per Nortriptilina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NORTRIPTYLINE_CODE = "372652004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Noscapine</div>
	 * <div class="de">Code für Noscapin</div>
	 * <div class="fr">Code de noscapine</div>
	 * <div class="it">Code per Noscapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NOSCAPINE_CODE = "387437002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Nystatin</div>
	 * <div class="de">Code für Nystatin</div>
	 * <div class="fr">Code de nystatine</div>
	 * <div class="it">Code per Nistatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NYSTATIN_CODE = "387048002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Obeticholic acid</div>
	 * <div class="de">Code für Obeticholsäure</div>
	 * <div class="fr">Code de acide obéticholique</div>
	 * <div class="it">Code per Acido obeticolico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OBETICHOLIC_ACID_CODE = "720257002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Obinutuzumab</div>
	 * <div class="de">Code für Obinutuzumab</div>
	 * <div class="fr">Code de obinutuzumab</div>
	 * <div class="it">Code per Obinutuzumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OBINUTUZUMAB_CODE = "710287009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ocrelizumab</div>
	 * <div class="de">Code für Ocrelizumab</div>
	 * <div class="fr">Code de ocrélizumab</div>
	 * <div class="it">Code per Ocrelizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OCRELIZUMAB_CODE = "733464008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Octenidine</div>
	 * <div class="de">Code für Octenidin</div>
	 * <div class="fr">Code de octénidine</div>
	 * <div class="it">Code per Octenidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OCTENIDINE_CODE = "430477008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Octocog alfa</div>
	 * <div class="de">Code für Octocog alfa</div>
	 * <div class="fr">Code de octocog alfa</div>
	 * <div class="it">Code per Octocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OCTOCOG_ALFA_CODE = "418888003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Octreotide</div>
	 * <div class="de">Code für Octreotid</div>
	 * <div class="fr">Code de octréotide</div>
	 * <div class="it">Code per Octreotide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OCTREOTIDE_CODE = "109053000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ofloxacin</div>
	 * <div class="de">Code für Ofloxacin</div>
	 * <div class="fr">Code de ofloxacine</div>
	 * <div class="it">Code per Ofloxacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OFLOXACIN_CODE = "387551000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Olanzapine</div>
	 * <div class="de">Code für Olanzapin</div>
	 * <div class="fr">Code de olanzapine</div>
	 * <div class="it">Code per Olanzapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OLANZAPINE_CODE = "386849001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Olanzapine embonate</div>
	 * <div class="de">Code für Olanzapin embonat-1-Wasser</div>
	 * <div class="fr">Code de olanzapine embonate</div>
	 * <div class="it">Code per Olanzapina embonato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OLANZAPINE_EMBONATE_CODE = "725800004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Olive oil</div>
	 * <div class="de">Code für Olivenöl</div>
	 * <div class="fr">Code de olive huile</div>
	 * <div class="it">Code per Oliva olio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OLIVE_OIL_CODE = "41834005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Olmesartan</div>
	 * <div class="de">Code für Olmesartan</div>
	 * <div class="fr">Code de olmésartan</div>
	 * <div class="it">Code per Olmesartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OLMESARTAN_CODE = "412259001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Olodaterol</div>
	 * <div class="de">Code für Olodaterol</div>
	 * <div class="fr">Code de olodatérol</div>
	 * <div class="it">Code per Olodaterolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OLODATEROL_CODE = "704459002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Omalizumab</div>
	 * <div class="de">Code für Omalizumab</div>
	 * <div class="fr">Code de omalizumab</div>
	 * <div class="it">Code per Omalizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OMALIZUMAB_CODE = "406443008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Omega 3 fatty acid</div>
	 * <div class="de">Code für Omega-3-Fettsäuren</div>
	 * <div class="fr">Code de acides gras oméga-3</div>
	 * <div class="it">Code per Acidi grassi omega 3</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OMEGA_3_FATTY_ACID_CODE = "226365003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Omeprazole</div>
	 * <div class="de">Code für Omeprazol</div>
	 * <div class="fr">Code de oméprazole</div>
	 * <div class="it">Code per Omeprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OMEPRAZOLE_CODE = "387137007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ondansetron</div>
	 * <div class="de">Code für Ondansetron</div>
	 * <div class="fr">Code de ondansétron</div>
	 * <div class="it">Code per Ondansetron</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ONDANSETRON_CODE = "372487007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Orlistat</div>
	 * <div class="de">Code für Orlistat</div>
	 * <div class="fr">Code de orlistat</div>
	 * <div class="it">Code per Orlistat</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ORLISTAT_CODE = "387007000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ornidazole</div>
	 * <div class="de">Code für Ornidazol</div>
	 * <div class="fr">Code de ornidazole</div>
	 * <div class="it">Code per Ornidazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ORNIDAZOLE_CODE = "442924004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oseltamivir</div>
	 * <div class="de">Code für Oseltamivir</div>
	 * <div class="fr">Code de oseltamivir</div>
	 * <div class="it">Code per Oseltamivir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OSELTAMIVIR_CODE = "412261005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxaliplatin</div>
	 * <div class="de">Code für Oxaliplatin</div>
	 * <div class="fr">Code de oxaliplatine</div>
	 * <div class="it">Code per Oxaliplatino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXALIPLATIN_CODE = "395814003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxandrolone</div>
	 * <div class="de">Code für Oxandrolon</div>
	 * <div class="fr">Code de oxandrolone</div>
	 * <div class="it">Code per Oxandrolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXANDROLONE_CODE = "126128007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxazepam</div>
	 * <div class="de">Code für Oxazepam</div>
	 * <div class="fr">Code de oxazépam</div>
	 * <div class="it">Code per Oxazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXAZEPAM_CODE = "387455006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxcarbazepine</div>
	 * <div class="de">Code für Oxcarbazepin</div>
	 * <div class="fr">Code de oxcarbazépine</div>
	 * <div class="it">Code per Oxcarbazepina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXCARBAZEPINE_CODE = "387025007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxomemazine</div>
	 * <div class="de">Code für Oxomemazin</div>
	 * <div class="fr">Code de oxomémazine</div>
	 * <div class="it">Code per Oxomemazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXOMEMAZINE_CODE = "772837001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxybuprocaine</div>
	 * <div class="de">Code für Oxybuprocain</div>
	 * <div class="fr">Code de oxybuprocaïne</div>
	 * <div class="it">Code per Oxibuprocaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXYBUPROCAINE_CODE = "52140009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxybutynin</div>
	 * <div class="de">Code für Oxybutynin</div>
	 * <div class="fr">Code de oxybutynine</div>
	 * <div class="it">Code per Ossibutinina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXYBUTYNIN_CODE = "372717000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxycodone</div>
	 * <div class="de">Code für Oxycodon</div>
	 * <div class="fr">Code de oxycodone</div>
	 * <div class="it">Code per Ossicodone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXYCODONE_CODE = "55452001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxymetazoline</div>
	 * <div class="de">Code für Oxymetazolin</div>
	 * <div class="fr">Code de oxymétazoline</div>
	 * <div class="it">Code per Ossimetazolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXYMETAZOLINE_CODE = "387158001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxytetracycline</div>
	 * <div class="de">Code für Oxytetracyclin</div>
	 * <div class="fr">Code de oxytétracycline</div>
	 * <div class="it">Code per Ossitetraciclina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXYTETRACYCLINE_CODE = "372675006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Oxytocin</div>
	 * <div class="de">Code für Oxytocin</div>
	 * <div class="fr">Code de oxytocine</div>
	 * <div class="it">Code per Ossitocina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String OXYTOCIN_CODE = "112115002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Paclitaxel</div>
	 * <div class="de">Code für Paclitaxel</div>
	 * <div class="fr">Code de paclitaxel</div>
	 * <div class="it">Code per Paclitaxel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PACLITAXEL_CODE = "387374002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Paliperidone</div>
	 * <div class="de">Code für Paliperidon</div>
	 * <div class="fr">Code de palipéridone</div>
	 * <div class="it">Code per Paliperidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PALIPERIDONE_CODE = "426276000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Palivizumab</div>
	 * <div class="de">Code für Palivizumab</div>
	 * <div class="fr">Code de palivizumab</div>
	 * <div class="it">Code per Palivizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PALIVIZUMAB_CODE = "386900007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Palonosetron</div>
	 * <div class="de">Code für Palonosetron</div>
	 * <div class="fr">Code de palonosétron</div>
	 * <div class="it">Code per Palonosetron</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PALONOSETRON_CODE = "404852008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pancuronium</div>
	 * <div class="de">Code für Pancuronium</div>
	 * <div class="fr">Code de pancuronium</div>
	 * <div class="it">Code per Pancuronio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PANCURONIUM_CODE = "373738000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pantoprazole</div>
	 * <div class="de">Code für Pantoprazol</div>
	 * <div class="fr">Code de pantoprazole</div>
	 * <div class="it">Code per Pantoprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PANTOPRAZOLE_CODE = "395821003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pantothenic acid</div>
	 * <div class="de">Code für Pantothensäure</div>
	 * <div class="fr">Code de acide pantothénique</div>
	 * <div class="it">Code per Acido pantotenico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PANTOTHENIC_ACID_CODE = "86431009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Papaverine</div>
	 * <div class="de">Code für Papaverin</div>
	 * <div class="fr">Code de papavérine</div>
	 * <div class="it">Code per Papaverina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PAPAVERINE_CODE = "372784001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Paracetamol</div>
	 * <div class="de">Code für Paracetamol</div>
	 * <div class="fr">Code de paracétamol</div>
	 * <div class="it">Code per Paracetamolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PARACETAMOL_CODE = "387517004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Paraffin</div>
	 * <div class="de">Code für Paraffin</div>
	 * <div class="fr">Code de paraffine</div>
	 * <div class="it">Code per Paraffina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PARAFFIN_CODE = "255667006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Paricalcitol</div>
	 * <div class="de">Code für Paricalcitol</div>
	 * <div class="fr">Code de paricalcitol</div>
	 * <div class="it">Code per Paracalcitolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PARICALCITOL_CODE = "108946001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Paroxetine</div>
	 * <div class="de">Code für Paroxetin</div>
	 * <div class="fr">Code de paroxétine</div>
	 * <div class="it">Code per Paroxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PAROXETINE_CODE = "372595009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pegaspargase</div>
	 * <div class="de">Code für Pegaspargase</div>
	 * <div class="fr">Code de pégaspargase</div>
	 * <div class="it">Code per Pegaspargase</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PEGASPARGASE_CODE = "108814000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pegfilgrastim</div>
	 * <div class="de">Code für Pegfilgrastim</div>
	 * <div class="fr">Code de pegfilgrastim</div>
	 * <div class="it">Code per Pegfilgrastim</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PEGFILGRASTIM_CODE = "385544005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Peginterferon alfa-2a</div>
	 * <div class="de">Code für Peginterferon alfa-2a</div>
	 * <div class="fr">Code de peginterféron alfa-2a</div>
	 * <div class="it">Code per Peginterferone alfa-2a</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PEGINTERFERON_ALFA_2A_CODE = "421559001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pembrolizumab</div>
	 * <div class="de">Code für Pembrolizumab</div>
	 * <div class="fr">Code de pembrolizumab</div>
	 * <div class="it">Code per Pembrolizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PEMBROLIZUMAB_CODE = "716125002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pemetrexed</div>
	 * <div class="de">Code für Pemetrexed</div>
	 * <div class="fr">Code de pémétrexed</div>
	 * <div class="it">Code per Pemetrexed</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PEMETREXED_CODE = "409159000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Penciclovir</div>
	 * <div class="de">Code für Penciclovir</div>
	 * <div class="fr">Code de penciclovir</div>
	 * <div class="it">Code per Penciclovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PENCICLOVIR_CODE = "386939003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pentamidine isethionate</div>
	 * <div class="de">Code für Pentamidin diisetionat</div>
	 * <div class="fr">Code de pentamidine diiséthionate</div>
	 * <div class="it">Code per Pentamidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PENTAMIDINE_ISETHIONATE_CODE = "16826009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pentoxifylline</div>
	 * <div class="de">Code für Pentoxifyllin</div>
	 * <div class="fr">Code de pentoxyfylline</div>
	 * <div class="it">Code per Pentossifillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PENTOXIFYLLINE_CODE = "387522004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Perampanel</div>
	 * <div class="de">Code für Perampanel</div>
	 * <div class="fr">Code de pérampanel</div>
	 * <div class="it">Code per Perampanel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PERAMPANEL_CODE = "703127006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Perindopril</div>
	 * <div class="de">Code für Perindopril</div>
	 * <div class="fr">Code de périndopril</div>
	 * <div class="it">Code per Perindopril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PERINDOPRIL_CODE = "372916001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Permethrin</div>
	 * <div class="de">Code für Permethrin</div>
	 * <div class="fr">Code de perméthrine</div>
	 * <div class="it">Code per Permetrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PERMETHRIN_CODE = "410457007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pertussis vaccine</div>
	 * <div class="de">Code für Pertussis-Impfstoff</div>
	 * <div class="fr">Code de vaccin anticoquelucheux</div>
	 * <div class="it">Code per Pertosse vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PERTUSSIS_VACCINE_CODE = "396433007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pertuzumab</div>
	 * <div class="de">Code für Pertuzumab</div>
	 * <div class="fr">Code de pertuzumab</div>
	 * <div class="it">Code per Pertuzumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PERTUZUMAB_CODE = "704226002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pethidine</div>
	 * <div class="de">Code für Pethidin</div>
	 * <div class="fr">Code de péthidine</div>
	 * <div class="it">Code per Petidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PETHIDINE_CODE = "387298007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenazone</div>
	 * <div class="de">Code für Phenazon</div>
	 * <div class="fr">Code de phénazone</div>
	 * <div class="it">Code per Fenazone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENAZONE_CODE = "55486005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pheniramine</div>
	 * <div class="de">Code für Pheniramin</div>
	 * <div class="fr">Code de phéniramine</div>
	 * <div class="it">Code per Feniramina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENIRAMINE_CODE = "373500002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenobarbital</div>
	 * <div class="de">Code für Phenobarbital</div>
	 * <div class="fr">Code de phénobarbital</div>
	 * <div class="it">Code per Fenobarbital</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENOBARBITAL_CODE = "373505007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenoxybenzamine</div>
	 * <div class="de">Code für Phenoxybenzamin</div>
	 * <div class="fr">Code de phénoxybenzamine</div>
	 * <div class="it">Code per Fenossibenzamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENOXYBENZAMINE_CODE = "372838003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenoxymethylpenicillin potassium</div>
	 * <div class="de">Code für Phenoxymethylpenicillin kalium</div>
	 * <div class="fr">Code de phénoxyméthylpénicilline potassique</div>
	 * <div class="it">Code per Fenossimetilpenicillina potassica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENOXYMETHYLPENICILLIN_POTASSIUM_CODE = "56723006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenprocoumon</div>
	 * <div class="de">Code für Phenprocoumon</div>
	 * <div class="fr">Code de phenprocoumone</div>
	 * <div class="it">Code per Fenprocumone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENPROCOUMON_CODE = "59488002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phentolamine</div>
	 * <div class="de">Code für Phentolamin</div>
	 * <div class="fr">Code de phentolamine</div>
	 * <div class="it">Code per Fentolamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENTOLAMINE_CODE = "372863003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenylalanine</div>
	 * <div class="de">Code für Phenylalanin</div>
	 * <div class="fr">Code de phénylalanine</div>
	 * <div class="it">Code per Fenilalanina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENYLALANINE_CODE = "63004003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenylephrine</div>
	 * <div class="de">Code für Phenylephrin</div>
	 * <div class="fr">Code de phényléphrine</div>
	 * <div class="it">Code per Fenilefrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENYLEPHRINE_CODE = "372771005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phenytoin</div>
	 * <div class="de">Code für Phenytoin</div>
	 * <div class="fr">Code de phénytoïne</div>
	 * <div class="it">Code per Fenitoina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHENYTOIN_CODE = "387220006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pholcodine</div>
	 * <div class="de">Code für Pholcodin</div>
	 * <div class="fr">Code de pholcodine</div>
	 * <div class="it">Code per Folcodina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHOLCODINE_CODE = "396486005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phospholipid</div>
	 * <div class="de">Code für Phospholipide</div>
	 * <div class="fr">Code de phospholipides</div>
	 * <div class="it">Code per Fosfolipidi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHOSPHOLIPID_CODE = "78447009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Physostigmine</div>
	 * <div class="de">Code für Physostigmin</div>
	 * <div class="fr">Code de physostigmine</div>
	 * <div class="it">Code per Fisostigmina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHYSOSTIGMINE_CODE = "373347005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Phytomenadione</div>
	 * <div class="de">Code für Phytomenadion (Vitamin K1)</div>
	 * <div class="fr">Code de phytoménadione (Vitamine K1)</div>
	 * <div class="it">Code per Fitomenadione (vitamina K1)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PHYTOMENADIONE_CODE = "66656000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pilocarpine</div>
	 * <div class="de">Code für Pilocarpin</div>
	 * <div class="fr">Code de pilocarpine</div>
	 * <div class="it">Code per Pilocarpina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PILOCARPINE_CODE = "372895002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pimecrolimus</div>
	 * <div class="de">Code für Pimecrolimus</div>
	 * <div class="fr">Code de pimécrolimus</div>
	 * <div class="it">Code per Pimecrolimus</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIMECROLIMUS_CODE = "385580005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pioglitazone</div>
	 * <div class="de">Code für Pioglitazon</div>
	 * <div class="fr">Code de pioglitazone</div>
	 * <div class="it">Code per Pioglitazone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIOGLITAZONE_CODE = "395828009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pipamperone</div>
	 * <div class="de">Code für Pipamperon</div>
	 * <div class="fr">Code de pipampérone</div>
	 * <div class="it">Code per Pipamperone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIPAMPERONE_CODE = "703362007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Piperacillin</div>
	 * <div class="de">Code für Piperacillin</div>
	 * <div class="fr">Code de pipéracilline</div>
	 * <div class="it">Code per Piperacillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIPERACILLIN_CODE = "372836004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Piracetam</div>
	 * <div class="de">Code für Piracetam</div>
	 * <div class="fr">Code de piracétam</div>
	 * <div class="it">Code per Piracetam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIRACETAM_CODE = "395833008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Piretanide</div>
	 * <div class="de">Code für Piretanid</div>
	 * <div class="fr">Code de pirétanide</div>
	 * <div class="it">Code per Piretanide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIRETANIDE_CODE = "419451002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Piroxicam</div>
	 * <div class="de">Code für Piroxicam</div>
	 * <div class="fr">Code de piroxicam</div>
	 * <div class="it">Code per Piroxicam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PIROXICAM_CODE = "387153005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pitavastatin</div>
	 * <div class="de">Code für Pitavastatin</div>
	 * <div class="fr">Code de pitavastatine</div>
	 * <div class="it">Code per Pitavastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PITAVASTATIN_CODE = "443586000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pituitary luteinizing hormone</div>
	 * <div class="de">Code für Lutropin (hLH)</div>
	 * <div class="fr">Code de lutropine (hLH)</div>
	 * <div class="it">Code per Ormone luteinizzante umano, hLH</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PITUITARY_LUTEINIZING_HORMONE_CODE = "64182005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Plerixafor</div>
	 * <div class="de">Code für Plerixafor</div>
	 * <div class="fr">Code de plérixafor</div>
	 * <div class="it">Code per Plerixafor</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PLERIXAFOR_CODE = "442264009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pneumococcal polysaccharide vaccine</div>
	 * <div class="de">Code für Pneumokokken-Polysaccharid-Impfstoff</div>
	 * <div class="fr">Code de pneumocoque vaccin polysaccharidique</div>
	 * <div class="it">Code per Pneumococchi vaccino polisaccaridico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PNEUMOCOCCAL_POLYSACCHARIDE_VACCINE_CODE = "417011002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Polihexanide</div>
	 * <div class="de">Code für Polihexanid</div>
	 * <div class="fr">Code de polyhexanide</div>
	 * <div class="it">Code per Poliesanide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POLIHEXANIDE_CODE = "421952002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Polymyxin</div>
	 * <div class="de">Code für Polymyxin B</div>
	 * <div class="fr">Code de polymyxine B</div>
	 * <div class="it">Code per Polimixina B</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POLYMYXIN_CODE = "373224006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Posaconazole</div>
	 * <div class="de">Code für Posaconazol</div>
	 * <div class="fr">Code de posaconazole</div>
	 * <div class="it">Code per Posaconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POSACONAZOLE_CODE = "421747003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Potassium acetate</div>
	 * <div class="de">Code für Kaliumacetat</div>
	 * <div class="fr">Code de potassium acétate</div>
	 * <div class="it">Code per Potassio acetato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POTASSIUM_ACETATE_CODE = "52394008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Potassium chloride</div>
	 * <div class="de">Code für Kaliumchlorid</div>
	 * <div class="fr">Code de potassium chlorure</div>
	 * <div class="it">Code per Potassio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POTASSIUM_CHLORIDE_CODE = "8631001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Potassium citrate</div>
	 * <div class="de">Code für Kalium citrat (E332)</div>
	 * <div class="fr">Code de potassium citrate (E332)</div>
	 * <div class="it">Code per Potassio citrato (E332)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POTASSIUM_CITRATE_CODE = "387450001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Potassium gluconate</div>
	 * <div class="de">Code für Kalium D-gluconat wasserfrei</div>
	 * <div class="fr">Code de potassium D-gluconate anhydre</div>
	 * <div class="it">Code per Potassio gluconato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POTASSIUM_GLUCONATE_CODE = "89219006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Potassium permanganate</div>
	 * <div class="de">Code für Kaliumpermanganat</div>
	 * <div class="fr">Code de permanganate de potassium</div>
	 * <div class="it">Code per Potassio permanganato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POTASSIUM_PERMANGANATE_CODE = "4681002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Potassium phosphate</div>
	 * <div class="de">Code für Kalium phosphat</div>
	 * <div class="fr">Code de potassium phosphate</div>
	 * <div class="it">Code per Potassio fosfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POTASSIUM_PHOSPHATE_CODE = "80916004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Povidone iodine</div>
	 * <div class="de">Code für Povidon iod</div>
	 * <div class="fr">Code de povidone iodée</div>
	 * <div class="it">Code per Iodopovidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String POVIDONE_IODINE_CODE = "386989006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pramipexole</div>
	 * <div class="de">Code für Pramipexol</div>
	 * <div class="fr">Code de pramipexole</div>
	 * <div class="it">Code per Pramipexolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRAMIPEXOLE_CODE = "386852009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prasugrel</div>
	 * <div class="de">Code für Prasugrel</div>
	 * <div class="fr">Code de prasugrel</div>
	 * <div class="it">Code per Prasugrel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRASUGREL_CODE = "443129001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pravastatin</div>
	 * <div class="de">Code für Pravastatin</div>
	 * <div class="fr">Code de pravastatine</div>
	 * <div class="it">Code per Pravastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRAVASTATIN_CODE = "373566006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prazepam</div>
	 * <div class="de">Code für Prazepam</div>
	 * <div class="fr">Code de prazépam</div>
	 * <div class="it">Code per Prazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRAZEPAM_CODE = "387417001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Praziquantel</div>
	 * <div class="de">Code für Praziquantel</div>
	 * <div class="fr">Code de praziquantel</div>
	 * <div class="it">Code per Praziquantel</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRAZIQUANTEL_CODE = "387310003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prednicarbate</div>
	 * <div class="de">Code für Prednicarbat</div>
	 * <div class="fr">Code de prednicarbate</div>
	 * <div class="it">Code per Prednicarbato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PREDNICARBATE_CODE = "126086006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prednisolone</div>
	 * <div class="de">Code für Prednisolon</div>
	 * <div class="fr">Code de prednisolone</div>
	 * <div class="it">Code per Prednisolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PREDNISOLONE_CODE = "116601002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prednisone</div>
	 * <div class="de">Code für Prednison</div>
	 * <div class="fr">Code de prednisone</div>
	 * <div class="it">Code per Prednisone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PREDNISONE_CODE = "116602009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pregabalin</div>
	 * <div class="de">Code für Pregabalin</div>
	 * <div class="fr">Code de prégabaline</div>
	 * <div class="it">Code per Pregabalin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PREGABALIN_CODE = "415160008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prilocaine</div>
	 * <div class="de">Code für Prilocain</div>
	 * <div class="fr">Code de prilocaïne</div>
	 * <div class="it">Code per Prilocaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRILOCAINE_CODE = "387107003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Primaquine</div>
	 * <div class="de">Code für Primaquin</div>
	 * <div class="fr">Code de primaquine</div>
	 * <div class="it">Code per Primachina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRIMAQUINE_CODE = "429663004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Primidone</div>
	 * <div class="de">Code für Primidon</div>
	 * <div class="fr">Code de primidone</div>
	 * <div class="it">Code per Primidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRIMIDONE_CODE = "387256009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Probenecid</div>
	 * <div class="de">Code für Probenecid</div>
	 * <div class="fr">Code de probénécide</div>
	 * <div class="it">Code per Probenecid</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROBENECID_CODE = "387365004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Procainamide</div>
	 * <div class="de">Code für Procainamid</div>
	 * <div class="fr">Code de procaïnamide</div>
	 * <div class="it">Code per Procainamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROCAINAMIDE_CODE = "372589008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Procaine</div>
	 * <div class="de">Code für Procain</div>
	 * <div class="fr">Code de procaïne</div>
	 * <div class="it">Code per Procaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROCAINE_CODE = "387238009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Procyclidine</div>
	 * <div class="de">Code für Procyclidin</div>
	 * <div class="fr">Code de procyclidine</div>
	 * <div class="it">Code per Prociclidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROCYCLIDINE_CODE = "387247001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Progesterone</div>
	 * <div class="de">Code für Progesteron</div>
	 * <div class="fr">Code de progestérone</div>
	 * <div class="it">Code per Progesterone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROGESTERONE_CODE = "16683002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Proguanil</div>
	 * <div class="de">Code für Proguanil</div>
	 * <div class="fr">Code de proguanil</div>
	 * <div class="it">Code per Proguanil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROGUANIL_CODE = "387094004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Proline</div>
	 * <div class="de">Code für Prolin</div>
	 * <div class="fr">Code de proline</div>
	 * <div class="it">Code per Prolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROLINE_CODE = "52541003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Promazine hydrochloride</div>
	 * <div class="de">Code für Promazin hydrochlorid</div>
	 * <div class="fr">Code de promazine chlorhydrate</div>
	 * <div class="it">Code per Promazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROMAZINE_HYDROCHLORIDE_CODE = "79135001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Promethazine</div>
	 * <div class="de">Code für Promethazin</div>
	 * <div class="fr">Code de prométhazine</div>
	 * <div class="it">Code per Prometazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROMETHAZINE_CODE = "372871004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Propafenone</div>
	 * <div class="de">Code für Propafenon</div>
	 * <div class="fr">Code de propafénone</div>
	 * <div class="it">Code per Propafenone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROPAFENONE_CODE = "372910007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Propofol</div>
	 * <div class="de">Code für Propofol</div>
	 * <div class="fr">Code de propofol</div>
	 * <div class="it">Code per Propofol</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROPOFOL_CODE = "387423006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Propranolol</div>
	 * <div class="de">Code für Propranolol</div>
	 * <div class="fr">Code de propranolol</div>
	 * <div class="it">Code per Propranololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROPRANOLOL_CODE = "372772003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Propyphenazone</div>
	 * <div class="de">Code für Propyphenazon</div>
	 * <div class="fr">Code de propyphénazone</div>
	 * <div class="it">Code per Propifenazone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROPYPHENAZONE_CODE = "699188007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Protamine</div>
	 * <div class="de">Code für Protamin</div>
	 * <div class="fr">Code de protamine</div>
	 * <div class="it">Code per Protamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROTAMINE_CODE = "372630008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Protein C</div>
	 * <div class="de">Code für Protein C (human)</div>
	 * <div class="fr">Code de protéine C humaine</div>
	 * <div class="it">Code per Proteina C (umana)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROTEIN_C_CODE = "25525005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Protein S</div>
	 * <div class="de">Code für Protein S (human)</div>
	 * <div class="fr">Code de protéine S humaine</div>
	 * <div class="it">Code per Proteina S (umana)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROTEIN_S_CODE = "56898001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Protionamide</div>
	 * <div class="de">Code für Protionamid</div>
	 * <div class="fr">Code de protionamide</div>
	 * <div class="it">Code per Protionamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROTIONAMIDE_CODE = "703589003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Protireline</div>
	 * <div class="de">Code für Protirelin</div>
	 * <div class="fr">Code de protiréline</div>
	 * <div class="it">Code per Protirelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROTIRELINE_CODE = "412495007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Prucalopride</div>
	 * <div class="de">Code für Prucaloprid</div>
	 * <div class="fr">Code de prucalopride</div>
	 * <div class="it">Code per Prucalopride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PRUCALOPRIDE_CODE = "699273008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pseudoephedrine</div>
	 * <div class="de">Code für Pseudoephedrin</div>
	 * <div class="fr">Code de pseudoéphédrine</div>
	 * <div class="it">Code per Pseudoefedrina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PSEUDOEPHEDRINE_CODE = "372900003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pyrazinamide</div>
	 * <div class="de">Code für Pyrazinamid</div>
	 * <div class="fr">Code de pyrazinamide</div>
	 * <div class="it">Code per Pirazinamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PYRAZINAMIDE_CODE = "387076005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pyridostigmine bromide</div>
	 * <div class="de">Code für Pyridostigmin bromid</div>
	 * <div class="fr">Code de pyridostigmine bromure</div>
	 * <div class="it">Code per Piridostigmina bromuro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PYRIDOSTIGMINE_BROMIDE_CODE = "34915005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pyridoxal phosphate</div>
	 * <div class="de">Code für Pyridoxal-5-phosphat</div>
	 * <div class="fr">Code de pyridoxal-5-phosphate</div>
	 * <div class="it">Code per Piridossal fosfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PYRIDOXAL_PHOSPHATE_CODE = "259663004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pyridoxine</div>
	 * <div class="de">Code für Pyridoxin (Vitamin B6)</div>
	 * <div class="fr">Code de pyridoxine (Vitamine B6)</div>
	 * <div class="it">Code per Piridossina (vitamina B6)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PYRIDOXINE_CODE = "430469009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Pyrimethamine</div>
	 * <div class="de">Code für Pyrimethamin</div>
	 * <div class="fr">Code de pyriméthamine</div>
	 * <div class="it">Code per Pirimetamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PYRIMETHAMINE_CODE = "373769001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Quetiapine</div>
	 * <div class="de">Code für Quetiapin</div>
	 * <div class="fr">Code de quétiapine</div>
	 * <div class="it">Code per Quetiapina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String QUETIAPINE_CODE = "386850001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Quinapril</div>
	 * <div class="de">Code für Quinapril</div>
	 * <div class="fr">Code de quinapril</div>
	 * <div class="it">Code per Quinapril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String QUINAPRIL_CODE = "386874003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Quinine</div>
	 * <div class="de">Code für Chinin</div>
	 * <div class="fr">Code de quinine</div>
	 * <div class="it">Code per Chinina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String QUININE_CODE = "373497008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rabeprazole</div>
	 * <div class="de">Code für Rabeprazol</div>
	 * <div class="fr">Code de rabéprazole</div>
	 * <div class="it">Code per Rabeprazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RABEPRAZOLE_CODE = "422225001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rabies human immune globulin</div>
	 * <div class="de">Code für Tollwut-Immunglobulin vom Menschen</div>
	 * <div class="fr">Code de immunoglobuline humaine anti-rabique</div>
	 * <div class="it">Code per Immunoglobulina umana antirabbica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RABIES_HUMAN_IMMUNE_GLOBULIN_CODE = "422303009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Raloxifene</div>
	 * <div class="de">Code für Raloxifen</div>
	 * <div class="fr">Code de raloxifène</div>
	 * <div class="it">Code per Raloxifene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RALOXIFENE_CODE = "109029006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Raltegravir</div>
	 * <div class="de">Code für Raltegravir</div>
	 * <div class="fr">Code de raltégravir</div>
	 * <div class="it">Code per Raltegravir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RALTEGRAVIR_CODE = "429707008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Raltitrexed</div>
	 * <div class="de">Code für Raltitrexed</div>
	 * <div class="fr">Code de raltitrexed</div>
	 * <div class="it">Code per Raltitrexed</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RALTITREXED_CODE = "395857008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ramipril</div>
	 * <div class="de">Code für Ramipril</div>
	 * <div class="fr">Code de ramipril</div>
	 * <div class="it">Code per Ramipril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RAMIPRIL_CODE = "386872004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ramucirumab</div>
	 * <div class="de">Code für Ramucirumab</div>
	 * <div class="fr">Code de ramucirumab</div>
	 * <div class="it">Code per Ramucirumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RAMUCIRUMAB_CODE = "704259004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ranitidine</div>
	 * <div class="de">Code für Ranitidin</div>
	 * <div class="fr">Code de ranitidine</div>
	 * <div class="it">Code per Ranitidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RANITIDINE_CODE = "372755005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ranolazine</div>
	 * <div class="de">Code für Ranolazin</div>
	 * <div class="fr">Code de ranolazine</div>
	 * <div class="it">Code per Ranolazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RANOLAZINE_CODE = "420365007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rasagiline</div>
	 * <div class="de">Code für Rasagilin</div>
	 * <div class="fr">Code de rasagiline</div>
	 * <div class="it">Code per Rasagilina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RASAGILINE_CODE = "418734001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rasburicase</div>
	 * <div class="de">Code für Rasburicase</div>
	 * <div class="fr">Code de rasburicase</div>
	 * <div class="it">Code per Rasburicase</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RASBURICASE_CODE = "395858003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Reboxetine</div>
	 * <div class="de">Code für Reboxetin</div>
	 * <div class="fr">Code de réboxétine</div>
	 * <div class="it">Code per Reboxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String REBOXETINE_CODE = "395859006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Remifentanil</div>
	 * <div class="de">Code für Remifentanil</div>
	 * <div class="fr">Code de rémifentanil</div>
	 * <div class="it">Code per Remifentanil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String REMIFENTANIL_CODE = "386839004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Repaglinide</div>
	 * <div class="de">Code für Repaglinid</div>
	 * <div class="fr">Code de répaglinide</div>
	 * <div class="it">Code per Repaglinide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String REPAGLINIDE_CODE = "386964000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Retigabine</div>
	 * <div class="de">Code für Retigabin</div>
	 * <div class="fr">Code de rétigabine</div>
	 * <div class="it">Code per Retigabina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RETIGABINE_CODE = "699271005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Retinol</div>
	 * <div class="de">Code für Retinol (Vitamin A)</div>
	 * <div class="fr">Code de rétinol (Vitamine a)</div>
	 * <div class="it">Code per Retinolo (vitamina A)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RETINOL_CODE = "82622003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ribavirin</div>
	 * <div class="de">Code für Ribavirin</div>
	 * <div class="fr">Code de ribavirine</div>
	 * <div class="it">Code per Ribavirina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIBAVIRIN_CODE = "387188005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Riboflavin</div>
	 * <div class="de">Code für Riboflavin (Vitamin B2, E101)</div>
	 * <div class="fr">Code de riboflavine (Vitamine B2, E101)</div>
	 * <div class="it">Code per Riboflavina (vitamina B2, E 101)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIBOFLAVIN_CODE = "13235001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rifabutin</div>
	 * <div class="de">Code für Rifabutin</div>
	 * <div class="fr">Code de rifabutine</div>
	 * <div class="it">Code per Rifabutina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIFABUTIN_CODE = "386893001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rifampicin</div>
	 * <div class="de">Code für Rifampicin</div>
	 * <div class="fr">Code de rifampicine</div>
	 * <div class="it">Code per Rifampicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIFAMPICIN_CODE = "387159009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rifaximin</div>
	 * <div class="de">Code für Rifaximin</div>
	 * <div class="fr">Code de rifaximine</div>
	 * <div class="it">Code per Rifaximina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIFAXIMIN_CODE = "412553001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rilpivirine</div>
	 * <div class="de">Code für Rilpivirin</div>
	 * <div class="fr">Code de rilpivirine</div>
	 * <div class="it">Code per Rilpivirina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RILPIVIRINE_CODE = "703123005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Riluzole</div>
	 * <div class="de">Code für Riluzol</div>
	 * <div class="fr">Code de riluzole</div>
	 * <div class="it">Code per Riluzolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RILUZOLE_CODE = "386980005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rimexolone</div>
	 * <div class="de">Code für Rimexolon</div>
	 * <div class="fr">Code de rimexolone</div>
	 * <div class="it">Code per Rimexolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIMEXOLONE_CODE = "387046003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Riociguat</div>
	 * <div class="de">Code für Riociguat</div>
	 * <div class="fr">Code de riociguat</div>
	 * <div class="it">Code per Riociguat</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIOCIGUAT_CODE = "713333001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Risedronate sodium</div>
	 * <div class="de">Code für Risedronat natrium</div>
	 * <div class="fr">Code de risédronate sodique</div>
	 * <div class="it">Code per Sodio risedronato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RISEDRONATE_SODIUM_CODE = "387064005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Risedronic acid</div>
	 * <div class="de">Code für Risedronsäure</div>
	 * <div class="fr">Code de acide risédronique</div>
	 * <div class="it">Code per Acido risedronico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RISEDRONIC_ACID_CODE = "768539002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Risperidone</div>
	 * <div class="de">Code für Risperidon</div>
	 * <div class="fr">Code de rispéridone</div>
	 * <div class="it">Code per Risperidone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RISPERIDONE_CODE = "386840002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ritonavir</div>
	 * <div class="de">Code für Ritonavir</div>
	 * <div class="fr">Code de ritonavir</div>
	 * <div class="it">Code per Ritonavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RITONAVIR_CODE = "386896009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rituximab</div>
	 * <div class="de">Code für Rituximab</div>
	 * <div class="fr">Code de rituximab</div>
	 * <div class="it">Code per Rituximab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RITUXIMAB_CODE = "386919002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rivaroxaban</div>
	 * <div class="de">Code für Rivaroxaban</div>
	 * <div class="fr">Code de rivaroxaban</div>
	 * <div class="it">Code per Rivaroxaban</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIVAROXABAN_CODE = "442031002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rivastigmine</div>
	 * <div class="de">Code für Rivastigmin</div>
	 * <div class="fr">Code de rivastigmine</div>
	 * <div class="it">Code per Rivastigmina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIVASTIGMINE_CODE = "395868008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rizatriptan</div>
	 * <div class="de">Code für Rizatriptan</div>
	 * <div class="fr">Code de rizatriptan</div>
	 * <div class="it">Code per Rizatriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RIZATRIPTAN_CODE = "363573000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rocuronium bromide</div>
	 * <div class="de">Code für Rocuronium bromid</div>
	 * <div class="fr">Code de rocuronium bromure</div>
	 * <div class="it">Code per Rocuronio bromuro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROCURONIUM_BROMIDE_CODE = "108450002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Roflumilast</div>
	 * <div class="de">Code für Roflumilast</div>
	 * <div class="fr">Code de roflumilast</div>
	 * <div class="it">Code per Roflumilast</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROFLUMILAST_CODE = "448971002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Romiplostim</div>
	 * <div class="de">Code für Romiplostim</div>
	 * <div class="fr">Code de romiplostim</div>
	 * <div class="it">Code per Romiplostim</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROMIPLOSTIM_CODE = "439122000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ropinirole</div>
	 * <div class="de">Code für Ropinirol</div>
	 * <div class="fr">Code de ropinirole</div>
	 * <div class="it">Code per Ropinirolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROPINIROLE_CODE = "372499000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ropivacaine</div>
	 * <div class="de">Code für Ropivacain</div>
	 * <div class="fr">Code de ropivacaïne</div>
	 * <div class="it">Code per Ropivacaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROPIVACAINE_CODE = "386969005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rosuvastatin</div>
	 * <div class="de">Code für Rosuvastatin</div>
	 * <div class="fr">Code de rosuvastatine</div>
	 * <div class="it">Code per Rosuvastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROSUVASTATIN_CODE = "700067006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rotigotine</div>
	 * <div class="de">Code für Rotigotin</div>
	 * <div class="fr">Code de rotigotine</div>
	 * <div class="it">Code per Rotigotina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ROTIGOTINE_CODE = "421924006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rubella vaccine</div>
	 * <div class="de">Code für Röteln-Lebend-Impfstoff</div>
	 * <div class="fr">Code de rubéole vaccin vivant</div>
	 * <div class="it">Code per Rosolia vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RUBELLA_VACCINE_CODE = "396438003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Rufinamide</div>
	 * <div class="de">Code für Rufinamid</div>
	 * <div class="fr">Code de rufinamide</div>
	 * <div class="it">Code per Rufinamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RUFINAMIDE_CODE = "429835003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Saccharomyces boulardii</div>
	 * <div class="de">Code für Saccharomyces boulardii</div>
	 * <div class="fr">Code de saccharomyces boulardii</div>
	 * <div class="it">Code per Saccharomyces boulardii</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SACCHAROMYCES_BOULARDII_CODE = "700441006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sacubitril</div>
	 * <div class="de">Code für Sacubitril</div>
	 * <div class="fr">Code de sacubitril</div>
	 * <div class="it">Code per Sacubitril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SACUBITRIL_CODE = "716072000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Safinamide</div>
	 * <div class="de">Code für Safinamid</div>
	 * <div class="fr">Code de safinamide</div>
	 * <div class="it">Code per Safinamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SAFINAMIDE_CODE = "718852000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Salbutamol</div>
	 * <div class="de">Code für Salbutamol</div>
	 * <div class="fr">Code de salbutamol</div>
	 * <div class="it">Code per Salbutamolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SALBUTAMOL_CODE = "372897005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Salicylamide</div>
	 * <div class="de">Code für Salicylamid</div>
	 * <div class="fr">Code de salicylamide</div>
	 * <div class="it">Code per Salicilamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SALICYLAMIDE_CODE = "22192002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Salicylic acid</div>
	 * <div class="de">Code für Salicylsäure</div>
	 * <div class="fr">Code de acide salicylique</div>
	 * <div class="it">Code per Acido salicilico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SALICYLIC_ACID_CODE = "387253001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Salmeterol</div>
	 * <div class="de">Code für Salmeterol</div>
	 * <div class="fr">Code de salmétérol</div>
	 * <div class="it">Code per Salmeterolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SALMETEROL_CODE = "372515005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sapropterin</div>
	 * <div class="de">Code für Sapropterin</div>
	 * <div class="fr">Code de saproptérine</div>
	 * <div class="it">Code per Sapropterina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SAPROPTERIN_CODE = "432859002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Saquinavir</div>
	 * <div class="de">Code für Saquinavir</div>
	 * <div class="fr">Code de saquinavir</div>
	 * <div class="it">Code per Saquinavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SAQUINAVIR_CODE = "372530001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sarilumab</div>
	 * <div class="de">Code für Sarilumab</div>
	 * <div class="fr">Code de sarilumab</div>
	 * <div class="it">Code per Sarilumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SARILUMAB_CODE = "735231009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Saxagliptin</div>
	 * <div class="de">Code für Saxagliptin</div>
	 * <div class="fr">Code de saxagliptine</div>
	 * <div class="it">Code per Saxagliptin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SAXAGLIPTIN_CODE = "443087004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Scopolamine</div>
	 * <div class="de">Code für Scopolamin</div>
	 * <div class="fr">Code de scopolamine</div>
	 * <div class="it">Code per Scopolamina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SCOPOLAMINE_CODE = "387409009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Scopolamine butylbromide</div>
	 * <div class="de">Code für Scopolamin butylbromid</div>
	 * <div class="fr">Code de scopolamine butylbromure</div>
	 * <div class="it">Code per Scopolamina butilbromuro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SCOPOLAMINE_BUTYLBROMIDE_CODE = "395739004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Secretin</div>
	 * <div class="de">Code für Secretin</div>
	 * <div class="fr">Code de sécrétine</div>
	 * <div class="it">Code per Secretina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SECRETIN_CODE = "19205004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Secukinumab</div>
	 * <div class="de">Code für Secukinumab</div>
	 * <div class="fr">Code de sécukinumab</div>
	 * <div class="it">Code per Secukinumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SECUKINUMAB_CODE = "708822004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Serine</div>
	 * <div class="de">Code für L-Serin</div>
	 * <div class="fr">Code de l-sérine</div>
	 * <div class="it">Code per Serina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SERINE_CODE = "14125007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sertraline</div>
	 * <div class="de">Code für Sertralin</div>
	 * <div class="fr">Code de sertraline</div>
	 * <div class="it">Code per Sertralina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SERTRALINE_CODE = "372594008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sevelamer</div>
	 * <div class="de">Code für Sevelamer</div>
	 * <div class="fr">Code de sévélamer</div>
	 * <div class="it">Code per Sevelamer</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SEVELAMER_CODE = "395871000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sevoflurane</div>
	 * <div class="de">Code für Sevofluran</div>
	 * <div class="fr">Code de sévoflurane</div>
	 * <div class="it">Code per Sevoflurano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SEVOFLURANE_CODE = "386842005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sildenafil</div>
	 * <div class="de">Code für Sildenafil</div>
	 * <div class="fr">Code de sildénafil</div>
	 * <div class="it">Code per Sildenafil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SILDENAFIL_CODE = "372572000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Silibinin</div>
	 * <div class="de">Code für Silibinin</div>
	 * <div class="fr">Code de silibinine</div>
	 * <div class="it">Code per Silibina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SILIBININ_CODE = "720527007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Silodosin</div>
	 * <div class="de">Code für Silodosin</div>
	 * <div class="fr">Code de silodosine</div>
	 * <div class="it">Code per Silodosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SILODOSIN_CODE = "442042006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Simeticone</div>
	 * <div class="de">Code für Simeticon</div>
	 * <div class="fr">Code de siméticone</div>
	 * <div class="it">Code per Simeticone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SIMETICONE_CODE = "387442005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Simoctocog alfa</div>
	 * <div class="de">Code für Simoctocog alfa</div>
	 * <div class="fr">Code de simoctocog alfa</div>
	 * <div class="it">Code per Simoctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SIMOCTOCOG_ALFA_CODE = "718853005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Simvastatin</div>
	 * <div class="de">Code für Simvastatin</div>
	 * <div class="fr">Code de simvastatine</div>
	 * <div class="it">Code per Simvastatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SIMVASTATIN_CODE = "387584000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sirolimus</div>
	 * <div class="de">Code für Sirolimus</div>
	 * <div class="fr">Code de sirolimus</div>
	 * <div class="it">Code per Sirolimus</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SIROLIMUS_CODE = "387014003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sitagliptin</div>
	 * <div class="de">Code für Sitagliptin</div>
	 * <div class="fr">Code de sitagliptine</div>
	 * <div class="it">Code per Sitagliptin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SITAGLIPTIN_CODE = "423307000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium acetate trihydrat</div>
	 * <div class="de">Code für Natrium acetat-3-Wasser</div>
	 * <div class="fr">Code de sodium acétate trihydrate</div>
	 * <div class="it">Code per Sodio acetato triidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_ACETATE_TRIHYDRAT_CODE = "726006002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium bicarbonate</div>
	 * <div class="de">Code für Natriumhydrogencarbonat</div>
	 * <div class="fr">Code de sodium bicarbonate</div>
	 * <div class="it">Code per Bicarbonato di sodio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_BICARBONATE_CODE = "387319002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium chloride</div>
	 * <div class="de">Code für Natriumchlorid</div>
	 * <div class="fr">Code de sodium chlorure</div>
	 * <div class="it">Code per Sodio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_CHLORIDE_CODE = "387390002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium citrate</div>
	 * <div class="de">Code für Natriumcitrat</div>
	 * <div class="fr">Code de sodium citrate</div>
	 * <div class="it">Code per Sodio citrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_CITRATE_CODE = "412546005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium dihydrogen phosphate dihydrate</div>
	 * <div class="de">Code für Natrium dihydrogenphosphat-2-Wasser</div>
	 * <div class="fr">Code de sodium dihydrogénophosphate dihydrate</div>
	 * <div class="it">Code per Sodio fosfato monobasico diidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_DIHYDROGEN_PHOSPHATE_DIHYDRATE_CODE = "726716000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium hydroxide</div>
	 * <div class="de">Code für Natriumhydroxid</div>
	 * <div class="fr">Code de sodium hydroxyde</div>
	 * <div class="it">Code per Sodio idrossido</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_HYDROXIDE_CODE = "23423003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium nitroprusside</div>
	 * <div class="de">Code für Nitroprussidnatrium, wasserfrei</div>
	 * <div class="fr">Code de nitroprussiate de sodium anhydre</div>
	 * <div class="it">Code per Sodio nitroprussiato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_NITROPRUSSIDE_CODE = "387139005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium picosulfate</div>
	 * <div class="de">Code für Natrium picosulfat</div>
	 * <div class="fr">Code de picosulfate sodique</div>
	 * <div class="it">Code per Sodio picosolfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_PICOSULFATE_CODE = "395881001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium selenite</div>
	 * <div class="de">Code für Dinatrium-selenit</div>
	 * <div class="fr">Code de sélénite disodique</div>
	 * <div class="it">Code per Sodio selenito</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_SELENITE_CODE = "96277001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sodium thiosulfate</div>
	 * <div class="de">Code für Dinatrium-thiosulfat</div>
	 * <div class="fr">Code de sodium thiosulfate</div>
	 * <div class="it">Code per Sodio tiosolfato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SODIUM_THIOSULFATE_CODE = "387209006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sofosbuvir</div>
	 * <div class="de">Code für Sofosbuvir</div>
	 * <div class="fr">Code de sofosbuvir</div>
	 * <div class="it">Code per Sofosbuvir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOFOSBUVIR_CODE = "710806008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Solifenacin</div>
	 * <div class="de">Code für Solifenacin</div>
	 * <div class="fr">Code de solifénacine</div>
	 * <div class="it">Code per Solifenacina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOLIFENACIN_CODE = "407030007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Somatostatin</div>
	 * <div class="de">Code für Somatostatin</div>
	 * <div class="fr">Code de somatostatine</div>
	 * <div class="it">Code per Somatostatina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOMATOSTATIN_CODE = "49722008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Somatotropin releasing factor</div>
	 * <div class="de">Code für Somatorelin</div>
	 * <div class="fr">Code de somatoréline</div>
	 * <div class="it">Code per Somatorelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOMATOTROPIN_RELEASING_FACTOR_CODE = "16628008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Somatropin</div>
	 * <div class="de">Code für Somatropin</div>
	 * <div class="fr">Code de somatropine</div>
	 * <div class="it">Code per Somatropina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOMATROPIN_CODE = "395883003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sorafenib</div>
	 * <div class="de">Code für Sorafenib</div>
	 * <div class="fr">Code de sorafénib</div>
	 * <div class="it">Code per Sorafenib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SORAFENIB_CODE = "422042001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sotalol</div>
	 * <div class="de">Code für Sotalol</div>
	 * <div class="fr">Code de sotalol</div>
	 * <div class="it">Code per Sotalolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOTALOL_CODE = "372911006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Soy bean oil</div>
	 * <div class="de">Code für Sojabohnenöl</div>
	 * <div class="fr">Code de soja fèves huile</div>
	 * <div class="it">Code per Soia olio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SOY_BEAN_OIL_CODE = "226911007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Spironolactone</div>
	 * <div class="de">Code für Spironolacton</div>
	 * <div class="fr">Code de spironolactone</div>
	 * <div class="it">Code per Spironolattone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SPIRONOLACTONE_CODE = "387078006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Stiripentol</div>
	 * <div class="de">Code für Stiripentol</div>
	 * <div class="fr">Code de stiripentol</div>
	 * <div class="it">Code per Stiripentolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String STIRIPENTOL_CODE = "428221002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Substance with protease mechanism of action</div>
	 * <div class="de">Code für Protease</div>
	 * <div class="fr">Code de protéase</div>
	 * <div class="it">Code per Proteasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SUBSTANCE_WITH_PROTEASE_MECHANISM_OF_ACTION_CODE = "387033008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Succinylcholine chloride</div>
	 * <div class="de">Code für Suxamethonium chlorid</div>
	 * <div class="fr">Code de suxaméthonium chlorure</div>
	 * <div class="it">Code per Succinilcolina cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SUCCINYLCHOLINE_CHLORIDE_CODE = "58907007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sufentanil</div>
	 * <div class="de">Code für Sufentanil</div>
	 * <div class="fr">Code de sufentanil</div>
	 * <div class="it">Code per Sufentanil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SUFENTANIL_CODE = "49998007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sugammadex</div>
	 * <div class="de">Code für Sugammadex</div>
	 * <div class="fr">Code de sugammadex</div>
	 * <div class="it">Code per Sugammadex</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SUGAMMADEX_CODE = "442340006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulfadiazine</div>
	 * <div class="de">Code für Sulfadiazin</div>
	 * <div class="fr">Code de sulfadiazine</div>
	 * <div class="it">Code per Sulfadiazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULFADIAZINE_CODE = "74523009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulfamethoxazole</div>
	 * <div class="de">Code für Sulfamethoxazol</div>
	 * <div class="fr">Code de sulfaméthoxazole</div>
	 * <div class="it">Code per Sulfametossazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULFAMETHOXAZOLE_CODE = "363528007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulfamethoxazole and trimethoprim</div>
	 * <div class="de">Code für Sulfamethoxazol und Trimethoprim</div>
	 * <div class="fr">Code de sulfaméthoxazole et triméthoprime</div>
	 * <div class="it">Code per Sulfametossazolo e trimetoprim</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULFAMETHOXAZOLE_AND_TRIMETHOPRIM_CODE = "703745000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulfasalazine</div>
	 * <div class="de">Code für Sulfasalazin</div>
	 * <div class="fr">Code de sulfasalazine</div>
	 * <div class="it">Code per Sulfasalazina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULFASALAZINE_CODE = "387248006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulfur hexafluoride</div>
	 * <div class="de">Code für Schwefelhexafluorid</div>
	 * <div class="fr">Code de soufre hexafluorure</div>
	 * <div class="it">Code per Zolfo esafluoruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULFUR_HEXAFLUORIDE_CODE = "259276004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulpiride</div>
	 * <div class="de">Code für Sulpirid</div>
	 * <div class="fr">Code de sulpiride</div>
	 * <div class="it">Code per Sulpiride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULPIRIDE_CODE = "395891007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulprostone</div>
	 * <div class="de">Code für Sulproston</div>
	 * <div class="fr">Code de sulprostone</div>
	 * <div class="it">Code per Sulprostone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULPROSTONE_CODE = "713461008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sulthiamine</div>
	 * <div class="de">Code für Sultiam</div>
	 * <div class="fr">Code de sultiame</div>
	 * <div class="it">Code per Sultiame</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SULTHIAMINE_CODE = "50580004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Sumatriptan</div>
	 * <div class="de">Code für Sumatriptan</div>
	 * <div class="fr">Code de sumatriptan</div>
	 * <div class="it">Code per Sumatriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SUMATRIPTAN_CODE = "395892000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tacrolimus</div>
	 * <div class="de">Code für Tacrolimus</div>
	 * <div class="fr">Code de tacrolimus</div>
	 * <div class="it">Code per Tacrolimus</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TACROLIMUS_CODE = "386975001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tadalafil</div>
	 * <div class="de">Code für Tadalafil</div>
	 * <div class="fr">Code de tadalafil</div>
	 * <div class="it">Code per Tadalafil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TADALAFIL_CODE = "407111005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tafluprost</div>
	 * <div class="de">Code für Tafluprost</div>
	 * <div class="fr">Code de tafluprost</div>
	 * <div class="it">Code per Tafluprost</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TAFLUPROST_CODE = "699181001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tamoxifen</div>
	 * <div class="de">Code für Tamoxifen</div>
	 * <div class="fr">Code de tamoxifène</div>
	 * <div class="it">Code per Tamoxifene</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TAMOXIFEN_CODE = "373345002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tamsulosin</div>
	 * <div class="de">Code für Tamsulosin</div>
	 * <div class="fr">Code de tamsulosine</div>
	 * <div class="it">Code per Tamsulosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TAMSULOSIN_CODE = "372509005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tapentadol</div>
	 * <div class="de">Code für Tapentadol</div>
	 * <div class="fr">Code de tapentadol</div>
	 * <div class="it">Code per Tapentadolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TAPENTADOL_CODE = "441757005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Taurine</div>
	 * <div class="de">Code für Taurin</div>
	 * <div class="fr">Code de taurine</div>
	 * <div class="it">Code per Taurina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TAURINE_CODE = "10944007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tazobactam</div>
	 * <div class="de">Code für Tazobactam</div>
	 * <div class="fr">Code de tazobactam</div>
	 * <div class="it">Code per Tazobactam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TAZOBACTAM_CODE = "96007008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Teicoplanin</div>
	 * <div class="de">Code für Teicoplanin</div>
	 * <div class="fr">Code de téicoplanine</div>
	 * <div class="it">Code per Teicoplanina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TEICOPLANIN_CODE = "387529008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Telmisartan</div>
	 * <div class="de">Code für Telmisartan</div>
	 * <div class="fr">Code de telmisartan</div>
	 * <div class="it">Code per Telmisartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TELMISARTAN_CODE = "387069000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Temazepam</div>
	 * <div class="de">Code für Temazepam</div>
	 * <div class="fr">Code de témazépam</div>
	 * <div class="it">Code per Temazepam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TEMAZEPAM_CODE = "387300007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Temozolomide</div>
	 * <div class="de">Code für Temozolomid</div>
	 * <div class="fr">Code de témozolomide</div>
	 * <div class="it">Code per Temozolomide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TEMOZOLOMIDE_CODE = "387009002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tenofovir</div>
	 * <div class="de">Code für Tenofovir</div>
	 * <div class="fr">Code de ténofovir</div>
	 * <div class="it">Code per Tenofovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TENOFOVIR_CODE = "422091007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Terbinafine</div>
	 * <div class="de">Code für Terbinafin</div>
	 * <div class="fr">Code de terbinafine</div>
	 * <div class="it">Code per Terbinafina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TERBINAFINE_CODE = "373450007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Terbutaline sulfate</div>
	 * <div class="de">Code für Terbutalin sulfat</div>
	 * <div class="fr">Code de terbutaline sulfate</div>
	 * <div class="it">Code per Terbutalina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TERBUTALINE_SULFATE_CODE = "24583009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Teriparatide</div>
	 * <div class="de">Code für Teriparatid</div>
	 * <div class="fr">Code de tériparatide</div>
	 * <div class="it">Code per Teriparatide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TERIPARATIDE_CODE = "425438001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Terlipressin</div>
	 * <div class="de">Code für Terlipressin</div>
	 * <div class="fr">Code de terlipressine</div>
	 * <div class="it">Code per Terlipressina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TERLIPRESSIN_CODE = "395899009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Testosterone</div>
	 * <div class="de">Code für Testosteron</div>
	 * <div class="fr">Code de testostérone</div>
	 * <div class="it">Code per Testosterone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TESTOSTERONE_CODE = "43688007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tetanus immunoglobulin of human origin</div>
	 * <div class="de">Code für Tetanus-Immunglobulin vom Menschen</div>
	 * <div class="fr">Code de immunoglobuline humaine anti-tétanique</div>
	 * <div class="it">Code per Immunoglobulina umana antitetanica</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TETANUS_IMMUNOGLOBULIN_OF_HUMAN_ORIGIN_CODE = "428527002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tetanus vaccine</div>
	 * <div class="de">Code für Tetanus-Adsorbat-Impfstoff</div>
	 * <div class="fr">Code de tétanos vaccin adsorbé</div>
	 * <div class="it">Code per Tetano vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TETANUS_VACCINE_CODE = "412375000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tetracaine</div>
	 * <div class="de">Code für Tetracain</div>
	 * <div class="fr">Code de tétracaïne</div>
	 * <div class="it">Code per Tetracaina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TETRACAINE_CODE = "387309008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tetracosactide</div>
	 * <div class="de">Code für Tetracosactid</div>
	 * <div class="fr">Code de tétracosactide</div>
	 * <div class="it">Code per Tetracosactide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TETRACOSACTIDE_CODE = "96363002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tetracycline</div>
	 * <div class="de">Code für Tetracyclin</div>
	 * <div class="fr">Code de tétracycline</div>
	 * <div class="it">Code per Tetraciclina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TETRACYCLINE_CODE = "372809001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tetryzoline</div>
	 * <div class="de">Code für Tetryzolin</div>
	 * <div class="fr">Code de tétryzoline</div>
	 * <div class="it">Code per Tetrizolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TETRYZOLINE_CODE = "372673004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Theophylline</div>
	 * <div class="de">Code für Theophyllin</div>
	 * <div class="fr">Code de théophylline</div>
	 * <div class="it">Code per Teofillina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String THEOPHYLLINE_CODE = "372810006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Thiamine</div>
	 * <div class="de">Code für Thiamin (Vitamin B1)</div>
	 * <div class="fr">Code de thiamine (Vitamine B1)</div>
	 * <div class="it">Code per Tiamina (vitamina B1)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String THIAMINE_CODE = "259659006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Thiotepa</div>
	 * <div class="de">Code für Thiotepa</div>
	 * <div class="fr">Code de thiotépa</div>
	 * <div class="it">Code per Tiotepa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String THIOTEPA_CODE = "387508004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Threonine</div>
	 * <div class="de">Code für Threonin</div>
	 * <div class="fr">Code de thréonine</div>
	 * <div class="it">Code per Treonina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String THREONINE_CODE = "52736009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tiapride</div>
	 * <div class="de">Code für Tiaprid</div>
	 * <div class="fr">Code de tiapride</div>
	 * <div class="it">Code per Tiapride</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIAPRIDE_CODE = "699180000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tibolone</div>
	 * <div class="de">Code für Tibolon</div>
	 * <div class="fr">Code de tibolone</div>
	 * <div class="it">Code per Tibolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIBOLONE_CODE = "395903002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ticagrelor</div>
	 * <div class="de">Code für Ticagrelor</div>
	 * <div class="fr">Code de ticagrélor</div>
	 * <div class="it">Code per Ticagrelor</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TICAGRELOR_CODE = "698805004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tick-borne encephalitis vaccine</div>
	 * <div class="de">Code für FSME-Impfstoff, inaktiviert</div>
	 * <div class="fr">Code de encéphalite verno-estivale FSME inactivé vaccin</div>
	 * <div class="it">Code per Meningoencefalite verno-estiva (FSME) vaccino inattivato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TICK_BORNE_ENCEPHALITIS_VACCINE_CODE = "398783009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tigecycline</div>
	 * <div class="de">Code für Tigecyclin</div>
	 * <div class="fr">Code de tigécycline</div>
	 * <div class="it">Code per Tigeciclina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIGECYCLINE_CODE = "418313005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tilidine hydrochloride</div>
	 * <div class="de">Code für Tilidin hydrochlorid</div>
	 * <div class="fr">Code de tilidine chlorhydrate</div>
	 * <div class="it">Code per Tilidina cloridrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TILIDINE_HYDROCHLORIDE_CODE = "96186004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Timolol</div>
	 * <div class="de">Code für Timolol</div>
	 * <div class="fr">Code de timolol</div>
	 * <div class="it">Code per Timololo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIMOLOL_CODE = "372880004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tioguanine</div>
	 * <div class="de">Code für Tioguanin</div>
	 * <div class="fr">Code de tioguanine</div>
	 * <div class="it">Code per Tioguanina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIOGUANINE_CODE = "387407006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tiotropium</div>
	 * <div class="de">Code für Tiotropium</div>
	 * <div class="fr">Code de tiotropium</div>
	 * <div class="it">Code per Tiotropio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIOTROPIUM_CODE = "409169006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tipranavir</div>
	 * <div class="de">Code für Tipranavir</div>
	 * <div class="fr">Code de tipranavir</div>
	 * <div class="it">Code per Tipranavir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIPRANAVIR_CODE = "419409009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tizanidine</div>
	 * <div class="de">Code für Tizanidin</div>
	 * <div class="fr">Code de tizanidine</div>
	 * <div class="it">Code per Tizanidina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TIZANIDINE_CODE = "373440006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tobramycine</div>
	 * <div class="de">Code für Tobramycin</div>
	 * <div class="fr">Code de tobramycine</div>
	 * <div class="it">Code per Tobramicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOBRAMYCINE_CODE = "373548001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tocilizumab</div>
	 * <div class="de">Code für Tocilizumab</div>
	 * <div class="fr">Code de tocilizumab</div>
	 * <div class="it">Code per Tocilizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOCILIZUMAB_CODE = "444648007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tolcapone</div>
	 * <div class="de">Code für Tolcapon</div>
	 * <div class="fr">Code de tolcapone</div>
	 * <div class="it">Code per Tolcapone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOLCAPONE_CODE = "386851002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tolperisone</div>
	 * <div class="de">Code für Tolperison</div>
	 * <div class="fr">Code de tolpérisone</div>
	 * <div class="it">Code per Tolperisone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOLPERISONE_CODE = "703717006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tolterodine</div>
	 * <div class="de">Code für Tolterodin</div>
	 * <div class="fr">Code de toltérodine</div>
	 * <div class="it">Code per Tolterodina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOLTERODINE_CODE = "372570008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tolvaptan</div>
	 * <div class="de">Code für Tolvaptan</div>
	 * <div class="fr">Code de tolvaptan</div>
	 * <div class="it">Code per Tolvaptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOLVAPTAN_CODE = "443058000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Topiramate</div>
	 * <div class="de">Code für Topiramat</div>
	 * <div class="fr">Code de topiramate</div>
	 * <div class="it">Code per Topiramato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOPIRAMATE_CODE = "386844006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Topotecan</div>
	 * <div class="de">Code für Topotecan</div>
	 * <div class="fr">Code de topotécan</div>
	 * <div class="it">Code per Topotecan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TOPOTECAN_CODE = "372536007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Torasemide</div>
	 * <div class="de">Code für Torasemid</div>
	 * <div class="fr">Code de torasémide</div>
	 * <div class="it">Code per Torasemide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TORASEMIDE_CODE = "108476002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trabectedin</div>
	 * <div class="de">Code für Trabectedin</div>
	 * <div class="fr">Code de trabectédine</div>
	 * <div class="it">Code per Trabectedina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRABECTEDIN_CODE = "433127001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tramadol</div>
	 * <div class="de">Code für Tramadol</div>
	 * <div class="fr">Code de tramadol</div>
	 * <div class="it">Code per Tramadol</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRAMADOL_CODE = "386858008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trametinib</div>
	 * <div class="de">Code für Trametinib</div>
	 * <div class="fr">Code de tramétinib</div>
	 * <div class="it">Code per Trametinib</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRAMETINIB_CODE = "708711009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trandolapril</div>
	 * <div class="de">Code für Trandolapril</div>
	 * <div class="fr">Code de trandolapril</div>
	 * <div class="it">Code per Trandolapril</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRANDOLAPRIL_CODE = "386871006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tranexamic acid</div>
	 * <div class="de">Code für Tranexamsäure</div>
	 * <div class="fr">Code de acide tranexamique</div>
	 * <div class="it">Code per Acido tranexamico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRANEXAMIC_ACID_CODE = "386960009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tranylcypromine</div>
	 * <div class="de">Code für Tranylcypromin</div>
	 * <div class="fr">Code de tranylcypromine</div>
	 * <div class="it">Code per Tranilcipromina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRANYLCYPROMINE_CODE = "372891006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trastuzumab</div>
	 * <div class="de">Code für Trastuzumab</div>
	 * <div class="fr">Code de trastuzumab</div>
	 * <div class="it">Code per Trastuzumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRASTUZUMAB_CODE = "387003001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Travoprost</div>
	 * <div class="de">Code für Travoprost</div>
	 * <div class="fr">Code de travoprost</div>
	 * <div class="it">Code per Travoprost</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRAVOPROST_CODE = "129493000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trazodone</div>
	 * <div class="de">Code für Trazodon</div>
	 * <div class="fr">Code de trazodone</div>
	 * <div class="it">Code per Trazodone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRAZODONE_CODE = "372829000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Treprostinil</div>
	 * <div class="de">Code für Treprostinil</div>
	 * <div class="fr">Code de tréprostinil</div>
	 * <div class="it">Code per Treprostinil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TREPROSTINIL_CODE = "443570007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tretinoin</div>
	 * <div class="de">Code für Tretinoin</div>
	 * <div class="fr">Code de trétinoïne</div>
	 * <div class="it">Code per Tretinoina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRETINOIN_CODE = "387305002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Triacylglycerol lipase</div>
	 * <div class="de">Code für Lipase</div>
	 * <div class="fr">Code de lipase</div>
	 * <div class="it">Code per Lipasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIACYLGLYCEROL_LIPASE_CODE = "72993008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Triamcinolone</div>
	 * <div class="de">Code für Triamcinolon</div>
	 * <div class="fr">Code de triamcinolone</div>
	 * <div class="it">Code per Triamcinolone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIAMCINOLONE_CODE = "116594009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Triamcinolone acetonide</div>
	 * <div class="de">Code für Triamcinolon acetonid</div>
	 * <div class="fr">Code de triamcinolone acétonide</div>
	 * <div class="it">Code per Triamcinolone acetonide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIAMCINOLONE_ACETONIDE_CODE = "395913005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Triazolam</div>
	 * <div class="de">Code für Triazolam</div>
	 * <div class="fr">Code de triazolam</div>
	 * <div class="it">Code per Triazolam</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIAZOLAM_CODE = "386984001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Triclosan</div>
	 * <div class="de">Code für Triclosan</div>
	 * <div class="fr">Code de triclosan</div>
	 * <div class="it">Code per Triclosan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRICLOSAN_CODE = "387054001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trimethoprim</div>
	 * <div class="de">Code für Trimethoprim</div>
	 * <div class="fr">Code de triméthoprime</div>
	 * <div class="it">Code per Trimetoprim</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIMETHOPRIM_CODE = "387179001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trimipramine</div>
	 * <div class="de">Code für Trimipramin</div>
	 * <div class="fr">Code de trimipramine</div>
	 * <div class="it">Code per Trimipramina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIMIPRAMINE_CODE = "373550009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Triptorelin</div>
	 * <div class="de">Code für Triptorelin</div>
	 * <div class="fr">Code de triptoréline</div>
	 * <div class="it">Code per Triptorelina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRIPTORELIN_CODE = "395915003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tropicamide</div>
	 * <div class="de">Code für Tropicamid</div>
	 * <div class="fr">Code de tropicamide</div>
	 * <div class="it">Code per Tropicamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TROPICAMIDE_CODE = "387526001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Trospium chloride</div>
	 * <div class="de">Code für Trospium chlorid</div>
	 * <div class="fr">Code de trospium chlorure</div>
	 * <div class="it">Code per Trospio cloruro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TROSPIUM_CHLORIDE_CODE = "326557004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tryptophan</div>
	 * <div class="de">Code für Tryptophan</div>
	 * <div class="fr">Code de tryptophane</div>
	 * <div class="it">Code per Triptofano</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TRYPTOPHAN_CODE = "54821000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tuberculin purified protein derivative</div>
	 * <div class="de">Code für Tuberkulin, gereinigtes PPD</div>
	 * <div class="fr">Code de tuberculine dérivé protéinique purifié</div>
	 * <div class="it">Code per Tubercolina derivato proteico purificato (PPD)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TUBERCULIN_PURIFIED_PROTEIN_DERIVATIVE_CODE = "108731003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Turoctocog alfa</div>
	 * <div class="de">Code für Turoctocog alfa</div>
	 * <div class="fr">Code de turoctocog alfa</div>
	 * <div class="it">Code per Turoctocog alfa</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TUROCTOCOG_ALFA_CODE = "735055007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tyrosine</div>
	 * <div class="de">Code für Tyrosin</div>
	 * <div class="fr">Code de tyrosine</div>
	 * <div class="it">Code per Tirosina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TYROSINE_CODE = "27378009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Tyrothricin</div>
	 * <div class="de">Code für Tyrothricin</div>
	 * <div class="fr">Code de tyrothricine</div>
	 * <div class="it">Code per Tirotricina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String TYROTHRICIN_CODE = "36661005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ulipristal</div>
	 * <div class="de">Code für Ulipristal</div>
	 * <div class="fr">Code de ulipristal</div>
	 * <div class="it">Code per Ulipristal</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ULIPRISTAL_CODE = "703249005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Umeclidinium</div>
	 * <div class="de">Code für Umeclidinium</div>
	 * <div class="fr">Code de uméclidinium</div>
	 * <div class="it">Code per Umeclidinio</div>
	 * <!-- @formatter:on -->
	 */
	public static final String UMECLIDINIUM_CODE = "706898002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Urapidil</div>
	 * <div class="de">Code für Urapidil</div>
	 * <div class="fr">Code de urapidil</div>
	 * <div class="it">Code per Urapidil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String URAPIDIL_CODE = "698807007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Urokinase</div>
	 * <div class="de">Code für Urokinase</div>
	 * <div class="fr">Code de urokinase</div>
	 * <div class="it">Code per Urochinasi</div>
	 * <!-- @formatter:on -->
	 */
	public static final String UROKINASE_CODE = "59082006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ursodeoxycholic acid</div>
	 * <div class="de">Code für Ursodeoxycholsäure</div>
	 * <div class="fr">Code de acide ursodésoxycholique</div>
	 * <div class="it">Code per Acido ursodesossicolico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String URSODEOXYCHOLIC_ACID_CODE = "41143004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Ustekinumab</div>
	 * <div class="de">Code für Ustekinumab</div>
	 * <div class="fr">Code de ustékinumab</div>
	 * <div class="it">Code per Ustekinumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String USTEKINUMAB_CODE = "443465002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valaciclovir</div>
	 * <div class="de">Code für Valaciclovir</div>
	 * <div class="fr">Code de valaciclovir</div>
	 * <div class="it">Code per Valaciclovir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALACICLOVIR_CODE = "96098007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valerian extract</div>
	 * <div class="de">Code für Baldrianwurzel-Extrakt</div>
	 * <div class="fr">Code de valériane extrait</div>
	 * <div class="it">Code per Valeriana estratto</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALERIAN_EXTRACT_CODE = "412266000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valganciclovir</div>
	 * <div class="de">Code für Valganciclovir</div>
	 * <div class="fr">Code de valganciclovir</div>
	 * <div class="it">Code per Valganciclovit</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALGANCICLOVIR_CODE = "129476000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valine</div>
	 * <div class="de">Code für Valin</div>
	 * <div class="fr">Code de valine</div>
	 * <div class="it">Code per Valina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALINE_CODE = "72840006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valproate semisodium</div>
	 * <div class="de">Code für Valproat seminatrium</div>
	 * <div class="fr">Code de valproate semisodique</div>
	 * <div class="it">Code per Valproato semisodico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALPROATE_SEMISODIUM_CODE = "5641004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valproate sodium</div>
	 * <div class="de">Code für Valproat natrium</div>
	 * <div class="fr">Code de valproate sodique</div>
	 * <div class="it">Code per Valproato sodico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALPROATE_SODIUM_CODE = "387481005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valproic acid</div>
	 * <div class="de">Code für Valproinsäure</div>
	 * <div class="fr">Code de acide valproïque</div>
	 * <div class="it">Code per Acido valproico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALPROIC_ACID_CODE = "387080000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Valsartan</div>
	 * <div class="de">Code für Valsartan</div>
	 * <div class="fr">Code de valsartan</div>
	 * <div class="it">Code per Valsartan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALSARTAN_CODE = "386876001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vancomycin</div>
	 * <div class="de">Code für Vancomycin</div>
	 * <div class="fr">Code de vancomycine</div>
	 * <div class="it">Code per Vancomicina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VANCOMYCIN_CODE = "372735009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vardenafil</div>
	 * <div class="de">Code für Vardenafil</div>
	 * <div class="fr">Code de vardénafil</div>
	 * <div class="it">Code per Vardenafil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VARDENAFIL_CODE = "404858007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Varenicline</div>
	 * <div class="de">Code für Vareniclin</div>
	 * <div class="fr">Code de varénicline</div>
	 * <div class="it">Code per Vareniclina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VARENICLINE_CODE = "421772003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Varicella virus live vaccine</div>
	 * <div class="de">Code für Varizellen-Lebend-Impfstoff</div>
	 * <div class="fr">Code de vaccin varicelle vivant</div>
	 * <div class="it">Code per Varicella vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VARICELLA_VIRUS_LIVE_VACCINE_CODE = "412529007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Varicella virus vaccine</div>
	 * <div class="de">Code für Varizellen-Impfstoff</div>
	 * <div class="fr">Code de vaccin varicelle</div>
	 * <div class="it">Code per Varicella vaccino</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VARICELLA_VIRUS_VACCINE_CODE = "396442000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Varicella-zoster virus antibody</div>
	 * <div class="de">Code für Varizellen-Immunglobulin vom Menschen</div>
	 * <div class="fr">Code de immunoglobuline humaine antivaricelle</div>
	 * <div class="it">Code per Immunoglobulina umana antivaricella</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VARICELLA_ZOSTER_VIRUS_ANTIBODY_CODE = "259858000";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vecuronium bromide</div>
	 * <div class="de">Code für Vecuronium bromid</div>
	 * <div class="fr">Code de vécuronium bromure</div>
	 * <div class="it">Code per Vecuronio bromuro</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VECURONIUM_BROMIDE_CODE = "87472002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vedolizumab</div>
	 * <div class="de">Code für Vedolizumab</div>
	 * <div class="fr">Code de védolizumab</div>
	 * <div class="it">Code per Vedolizumab</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VEDOLIZUMAB_CODE = "704256006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Venlafaxine</div>
	 * <div class="de">Code für Venlafaxin</div>
	 * <div class="fr">Code de venlafaxine</div>
	 * <div class="it">Code per Venlafaxina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VENLAFAXINE_CODE = "372490001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Verapamil</div>
	 * <div class="de">Code für Verapamil</div>
	 * <div class="fr">Code de vérapamil</div>
	 * <div class="it">Code per Verapamil</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VERAPAMIL_CODE = "372754009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vigabatrin</div>
	 * <div class="de">Code für Vigabatrin</div>
	 * <div class="fr">Code de vigabatrine</div>
	 * <div class="it">Code per Vigabatrin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VIGABATRIN_CODE = "310283001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vilanterol</div>
	 * <div class="de">Code für Vilanterol</div>
	 * <div class="fr">Code de vilantérol</div>
	 * <div class="it">Code per Vilanterolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VILANTEROL_CODE = "702408004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vildagliptin</div>
	 * <div class="de">Code für Vildagliptin</div>
	 * <div class="fr">Code de vildagliptine</div>
	 * <div class="it">Code per Vildagliptin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VILDAGLIPTIN_CODE = "428611002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vinblastine</div>
	 * <div class="de">Code für Vinblastin</div>
	 * <div class="fr">Code de vinblastine</div>
	 * <div class="it">Code per Vinblastina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VINBLASTINE_CODE = "387051009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vincristine</div>
	 * <div class="de">Code für Vincristin</div>
	 * <div class="fr">Code de vincristine</div>
	 * <div class="it">Code per Vincristina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VINCRISTINE_CODE = "387126006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vindesine</div>
	 * <div class="de">Code für Vindesin</div>
	 * <div class="fr">Code de vindésine</div>
	 * <div class="it">Code per Vindesina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VINDESINE_CODE = "409198005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vinorelbine</div>
	 * <div class="de">Code für Vinorelbin</div>
	 * <div class="fr">Code de vinorelbine</div>
	 * <div class="it">Code per Vinorelbina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VINORELBINE_CODE = "372541004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vitamin E</div>
	 * <div class="de">Code für Tocopherol DL-alpha (E307)</div>
	 * <div class="fr">Code de tocophérol DL-alfa (E307)</div>
	 * <div class="it">Code per Alfa-Tocoferolo (vitamina E, E307)</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VITAMIN_E_CODE = "37237003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Voriconazole</div>
	 * <div class="de">Code für Voriconazol</div>
	 * <div class="fr">Code de voriconazole</div>
	 * <div class="it">Code per Voriconazolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VORICONAZOLE_CODE = "385469007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Vortioxetine</div>
	 * <div class="de">Code für Vortioxetin</div>
	 * <div class="fr">Code de vortioxétine</div>
	 * <div class="it">Code per Vortioxetina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VORTIOXETINE_CODE = "708717008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Warfarin</div>
	 * <div class="de">Code für Warfarin</div>
	 * <div class="fr">Code de warfarine</div>
	 * <div class="it">Code per Warfarin</div>
	 * <!-- @formatter:on -->
	 */
	public static final String WARFARIN_CODE = "372756006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Xylometazoline</div>
	 * <div class="de">Code für Xylometazolin</div>
	 * <div class="fr">Code de xylométazoline</div>
	 * <div class="it">Code per Xilometazolina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String XYLOMETAZOLINE_CODE = "372841007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Yellow fever vaccine</div>
	 * <div class="de">Code für Gelbfieber-Lebend-Impfstoff</div>
	 * <div class="fr">Code de fièvre jaune vaccin vivant</div>
	 * <div class="it">Code per Febbre gialla vaccino vivo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String YELLOW_FEVER_VACCINE_CODE = "396444004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zanamivir</div>
	 * <div class="de">Code für Zanamivir</div>
	 * <div class="fr">Code de zanamivir</div>
	 * <div class="it">Code per Zanamivir</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZANAMIVIR_CODE = "387010007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zidovudine</div>
	 * <div class="de">Code für Zidovudin</div>
	 * <div class="fr">Code de zidovudine</div>
	 * <div class="it">Code per Zidovudina</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZIDOVUDINE_CODE = "387151007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zinc acetate dihydrate</div>
	 * <div class="de">Code für Zinkdiacetat-2-Wasser</div>
	 * <div class="fr">Code de zinc acétate dihydrate</div>
	 * <div class="it">Code per Zinco acetato diidrato</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZINC_ACETATE_DIHYDRATE_CODE = "725761005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zinc pyrithione</div>
	 * <div class="de">Code für Pyrithion zink</div>
	 * <div class="fr">Code de pyrithione zinc</div>
	 * <div class="it">Code per Zinco piritione</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZINC_PYRITHIONE_CODE = "255954005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zoledronic acid</div>
	 * <div class="de">Code für Zoledronsäure</div>
	 * <div class="fr">Code de acide zolédronique (zolédronate)</div>
	 * <div class="it">Code per Acido zoledronico</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZOLEDRONIC_ACID_CODE = "395926009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zolmitriptan</div>
	 * <div class="de">Code für Zolmitriptan</div>
	 * <div class="fr">Code de zolmitriptan</div>
	 * <div class="it">Code per Zolmitriptan</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZOLMITRIPTAN_CODE = "363582006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zolpidem</div>
	 * <div class="de">Code für Zolpidem</div>
	 * <div class="fr">Code de zolpidem</div>
	 * <div class="it">Code per Zolpidem</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZOLPIDEM_CODE = "387569009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zonisamide</div>
	 * <div class="de">Code für Zonisamid</div>
	 * <div class="fr">Code de zonisamide</div>
	 * <div class="it">Code per Zonisamide</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZONISAMIDE_CODE = "125693002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zopiclone</div>
	 * <div class="de">Code für Zopiclon</div>
	 * <div class="fr">Code de zopiclone</div>
	 * <div class="it">Code per Zopiclone</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZOPICLONE_CODE = "395929002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for Zuclopenthixol</div>
	 * <div class="de">Code für Zuclopenthixol</div>
	 * <div class="fr">Code de zuclopenthixol</div>
	 * <div class="it">Code per Zuclopentixolo</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZUCLOPENTHIXOL_CODE = "428715002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Identifier of the value set</div>
	 * <div class="de">Identifikator für das Value Set</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALUE_SET_ID = "2.16.756.5.30.1.1.11.82";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Name of the value set</div>
	 * <div class="de">Name des Value Sets</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALUE_SET_NAME = "ActivePharmaceuticalIngredient";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the Enum with a given code</div>
	 * <div class="de">Liefert den Enum anhand eines gegebenen codes</div>
	 * <!-- @formatter:on -->
	 *
	 * @param code
	 *            <div class="de"> code</div>
	 * @return <div class="en">the enum</div>
	 */
	public static ActivePharmaceuticalIngredient getEnum(final String code) {
		for (final ActivePharmaceuticalIngredient x : values()) {
			if (x.getCodeValue().equals(code)) {
				return x;
			}
		}
		return null;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Checks if a given enum is part of this value set.</div>
	 * <div class="de">Prüft, ob der angegebene enum Teil dieses Value Sets ist.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param enumName
	 *            <div class="de"> enumName</div>
	 * @return true, if enum is in this value set
	 */
	public static boolean isEnumOfValueSet(final String enumName) {
		if (enumName == null) {
			return false;
		}
		try {
			Enum.valueOf(ActivePharmaceuticalIngredient.class, enumName);
			return true;
		} catch (final IllegalArgumentException ex) {
			return false;
		}
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Checks if a given code value is in this value set.</div>
	 * <div class="de">Prüft, ob der angegebene code in diesem Value Set vorhanden ist.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param codeValue
	 *            <div class="de"> code</div>
	 * @return true, if is in value set
	 */
	public static boolean isInValueSet(final String codeValue) {
		for (final ActivePharmaceuticalIngredient x : values()) {
			if (x.getCodeValue().equals(codeValue)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Machine interpretable and (inside this class) unique code</div>
	 * <div class="de">Maschinen interpretierbarer und (innerhalb dieser Klasse) eindeutiger Code</div>
	 * <!-- @formatter:on -->
	 */
	private String code;

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Identifier of the referencing code system.</div>
	 * <div class="de">Identifikator des referenzierende Codesystems.</div>
	 * <!-- @formatter:on -->
	 */
	private String codeSystem;

	/**
	 * The display names per language
	 */
	private Map<LanguageCode, String> displayNames;

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Instantiates this Enum Object with a given Code and Display Name</div>
	 * <div class="de">Instanziiert dieses Enum Object mittels eines Codes und einem Display Name</div>.
	 * <!-- @formatter:on -->
	 *
	 * @param code
	 *            code
	 * @param codeSystem
	 *            codeSystem
	 * @param displayName
	 *            the default display name
	 * @param displayNameEn
	 *            the display name en
	 * @param displayNameDe
	 *            the display name de
	 * @param displayNameFr
	 *            the display name fr
	 * @param displayNameIt
	 *            the display name it
	 */
	ActivePharmaceuticalIngredient(final String code, final String codeSystem,
			final String displayName, final String displayNameEn, final String displayNameDe,
			final String displayNameFr, final String displayNameIt) {
		this.code = code;
		this.codeSystem = codeSystem;
		this.displayNames = new HashMap<>();
		this.displayNames.put(null, displayName);
		this.displayNames.put(LanguageCode.ENGLISH, displayNameEn);
		this.displayNames.put(LanguageCode.GERMAN, displayNameDe);
		this.displayNames.put(LanguageCode.FRENCH, displayNameFr);
		this.displayNames.put(LanguageCode.ITALIAN, displayNameIt);
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the code system identifier.</div>
	 * <div class="de">Liefert den Code System Identifikator.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code system identifier</div>
	 */
	@Override
	public String getCodeSystemId() {
		return this.codeSystem;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the code system name.</div>
	 * <div class="de">Liefert den Code System Namen.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code system identifier</div>
	 */
	@Override
	public String getCodeSystemName() {
		String retVal = "";
		CodeSystems cs = CodeSystems.getEnum(this.codeSystem);
		if (cs != null)
			retVal = cs.getCodeSystemName();
		return retVal;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the actual Code as string</div>
	 * <div class="de">Liefert den eigentlichen Code als String</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code</div>
	 */
	@Override
	public String getCodeValue() {
		return this.code;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the display name defined by the language param. If
	 * there is no english translation, the default display name is returned.</div>
	 * <div class="de">Liefert display name gemäss Parameter, falls es keine
	 * Englische Übersetzung gibt, wird der default-Name zurückgegeben.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param languageCode
	 *            the language code to get the display name for
	 * @return returns the display name in the desired language. if language not
	 *         found, display name in german will returned
	 */
	@Override
	public String getDisplayName(LanguageCode languageCode) {
		String displayName = this.displayNames.get(languageCode);
		if (displayName == null && languageCode == LanguageCode.ENGLISH) {
			return this.displayNames.get(null);
		}
		return displayName;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the value set identifier.</div>
	 * <div class="de">Liefert den Value Set Identifikator.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the value set identifier</div>
	 */
	@Override
	public String getValueSetId() {
		return VALUE_SET_ID;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the value set name.</div>
	 * <div class="de">Liefert den Value Set Namen.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the value set name</div>
	 */
	@Override
	public String getValueSetName() {
		return VALUE_SET_NAME;
	}
}
