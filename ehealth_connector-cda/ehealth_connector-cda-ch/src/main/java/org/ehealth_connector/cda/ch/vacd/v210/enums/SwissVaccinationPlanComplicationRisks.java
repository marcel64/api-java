/*
 * The authorship of this project and accompanying materials is held by medshare GmbH, Switzerland.
 * All rights reserved. https://medshare.net
 *
 * Source code, documentation and other resources have been contributed by various people.
 * Project Team: https://gitlab.com/ehealth-connector/api/wikis/Team/
 * For exact developer information, please refer to the commit history of the forge.
 *
 * This code is made available under the terms of the Eclipse Public License v1.0.
 *
 * Accompanying materials are made available under the terms of the Creative Commons
 * Attribution-ShareAlike 4.0 License.
 *
 * This line is intended for UTF-8 encoding checks, do not modify/delete: äöüéè
 *
 */
package org.ehealth_connector.cda.ch.vacd.v210.enums;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import org.ehealth_connector.common.enums.CodeSystems;
import org.ehealth_connector.common.enums.LanguageCode;
import org.ehealth_connector.common.mdht.enums.ValueSetEnumInterface;

/**
 * <!-- @formatter:off -->
 * <div class="en">no designation found for language ENGLISH</div>
 * <div class="de">no designation found for language GERMAN</div>
 * <div class="fr">no designation found for language FRENCH</div>
 * <div class="it">no designation found for language ITALIAN</div>
 * <!-- @formatter:on -->
 */
@Generated(value = "org.ehealth_connector.codegenerator.ch.valuesets.UpdateValueSets")
public enum SwissVaccinationPlanComplicationRisks implements ValueSetEnumInterface {

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ANATOMISCHE_ODER_FUNKTIONELLE_ASPLENIE</div>
	 * <!-- @formatter:on -->
	 */
	ANATOMISCHE_ODER_FUNKTIONELLE_ASPLENIE("113001", "2.16.756.5.30.1.127.3.3.1",
			"ANATOMISCHE_ODER_FUNKTIONELLE_ASPLENIE", "ANATOMISCHE_ODER_FUNKTIONELLE_ASPLENIE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ANDERER_KREBS</div>
	 * <!-- @formatter:on -->
	 */
	ANDERER_KREBS("114034", "2.16.756.5.30.1.127.3.3.1", "ANDERER_KREBS", "ANDERER_KREBS",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ANDERE_LEBERERKRANKUNG</div>
	 * <!-- @formatter:on -->
	 */
	ANDERE_LEBERERKRANKUNG("114036", "2.16.756.5.30.1.127.3.3.1", "ANDERE_LEBERERKRANKUNG",
			"ANDERE_LEBERERKRANKUNG", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ANDERE_LUNGENERKRANKUNGEN_ZB_MUKOVISZIDOSE_ASTHMA_BRONCHIALE_ETC</div>
	 * <!-- @formatter:on -->
	 */
	ANDERE_LUNGENERKRANKUNGEN_ZB_MUKOVISZIDOSE_ASTHMA_BRONCHIALE_ETC("114006",
			"2.16.756.5.30.1.127.3.3.1",
			"ANDERE_LUNGENERKRANKUNGEN_ZB_MUKOVISZIDOSE_ASTHMA_BRONCHIALE_ETC",
			"ANDERE_LUNGENERKRANKUNGEN_ZB_MUKOVISZIDOSE_ASTHMA_BRONCHIALE_ETC", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ANDERE_NIERENERKRANKUNG</div>
	 * <!-- @formatter:on -->
	 */
	ANDERE_NIERENERKRANKUNG("114033", "2.16.756.5.30.1.127.3.3.1", "ANDERE_NIERENERKRANKUNG",
			"ANDERE_NIERENERKRANKUNG", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ANGEBORENE_IMMUNDEFIZIENZ_VARIABLES_IMMUNDEFEKTSYNDROM_DEFIZITAERE_ANTWORT_AUF_POLYSACCHARIDE</div>
	 * <!-- @formatter:on -->
	 */
	ANGEBORENE_IMMUNDEFIZIENZ_VARIABLES_IMMUNDEFEKTSYNDROM_DEFIZITAERE_ANTWORT_AUF_POLYSACCHARIDE(
			"114025", "2.16.756.5.30.1.127.3.3.1",
			"ANGEBORENE_IMMUNDEFIZIENZ_VARIABLES_IMMUNDEFEKTSYNDROM_DEFIZITAERE_ANTWORT_AUF_POLYSACCHARIDE",
			"ANGEBORENE_IMMUNDEFIZIENZ_VARIABLES_IMMUNDEFEKTSYNDROM_DEFIZITAERE_ANTWORT_AUF_POLYSACCHARIDE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_ERFORDERT</div>
	 * <!-- @formatter:on -->
	 */
	AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_ERFORDERT("114021", "2.16.756.5.30.1.127.3.3.1",
			"AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_ERFORDERT",
			"AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_ERFORDERT", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">BRONCHIEKTASEN_DURCH_ANTIKOERPERMANGEL</div>
	 * <!-- @formatter:on -->
	 */
	BRONCHIEKTASEN_DURCH_ANTIKOERPERMANGEL("114005", "2.16.756.5.30.1.127.3.3.1",
			"BRONCHIEKTASEN_DURCH_ANTIKOERPERMANGEL", "BRONCHIEKTASEN_DURCH_ANTIKOERPERMANGEL",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">CHRONISCHE_LEBERERKRANKUNG</div>
	 * <!-- @formatter:on -->
	 */
	CHRONISCHE_LEBERERKRANKUNG("114007", "2.16.756.5.30.1.127.3.3.1", "CHRONISCHE_LEBERERKRANKUNG",
			"CHRONISCHE_LEBERERKRANKUNG", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">CHRONISCH_OBSTRUKTIVE_PNEUMOPATHIE</div>
	 * <!-- @formatter:on -->
	 */
	CHRONISCH_OBSTRUKTIVE_PNEUMOPATHIE("114003", "2.16.756.5.30.1.127.3.3.1",
			"CHRONISCH_OBSTRUKTIVE_PNEUMOPATHIE", "CHRONISCH_OBSTRUKTIVE_PNEUMOPATHIE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">COCHLEAIMPLANTAT_IN_SITU_ODER_GEPLANT</div>
	 * <!-- @formatter:on -->
	 */
	COCHLEAIMPLANTAT_IN_SITU_ODER_GEPLANT("113002", "2.16.756.5.30.1.127.3.3.1",
			"COCHLEAIMPLANTAT_IN_SITU_ODER_GEPLANT", "COCHLEAIMPLANTAT_IN_SITU_ODER_GEPLANT",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">DIABETES_MIT_AUSWIRKUNG_AUF_DIE_FUNKTION_VON_HERZ_LUNGEN_ODER_NIEREN</div>
	 * <!-- @formatter:on -->
	 */
	DIABETES_MIT_AUSWIRKUNG_AUF_DIE_FUNKTION_VON_HERZ_LUNGEN_ODER_NIEREN("114015",
			"2.16.756.5.30.1.127.3.3.1",
			"DIABETES_MIT_AUSWIRKUNG_AUF_DIE_FUNKTION_VON_HERZ_LUNGEN_ODER_NIEREN",
			"DIABETES_MIT_AUSWIRKUNG_AUF_DIE_FUNKTION_VON_HERZ_LUNGEN_ODER_NIEREN", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">EMPFAENGER_EINER_SOLIDORGANTRANSPLANTATION</div>
	 * <!-- @formatter:on -->
	 */
	EMPFAENGER_EINER_SOLIDORGANTRANSPLANTATION("114019", "2.16.756.5.30.1.127.3.3.1",
			"EMPFAENGER_EINER_SOLIDORGANTRANSPLANTATION",
			"EMPFAENGER_EINER_SOLIDORGANTRANSPLANTATION", "TOTRANSLATE", "TOTRANSLATE",
			"TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">EMPFAENGER_EINER_STAMMZELLTRANSPLANTATION</div>
	 * <!-- @formatter:on -->
	 */
	EMPFAENGER_EINER_STAMMZELLTRANSPLANTATION("114020", "2.16.756.5.30.1.127.3.3.1",
			"EMPFAENGER_EINER_STAMMZELLTRANSPLANTATION",
			"EMPFAENGER_EINER_STAMMZELLTRANSPLANTATION", "TOTRANSLATE", "TOTRANSLATE",
			"TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">GEBURT_VOR_DER_33_SCHWANGERSCHAFTSWOCHE_ODER_GEBURTSGEWICHT_GROESSER_1500G</div>
	 * <!-- @formatter:on -->
	 */
	GEBURT_VOR_DER_33_SCHWANGERSCHAFTSWOCHE_ODER_GEBURTSGEWICHT_GROESSER_1500G("113004",
			"2.16.756.5.30.1.127.3.3.1",
			"GEBURT_VOR_DER_33_SCHWANGERSCHAFTSWOCHE_ODER_GEBURTSGEWICHT_GROESSER_1500G",
			"GEBURT_VOR_DER_33_SCHWANGERSCHAFTSWOCHE_ODER_GEBURTSGEWICHT_GROESSER_1500G",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">HAEMODIALYSEPATIENT</div>
	 * <!-- @formatter:on -->
	 */
	HAEMODIALYSEPATIENT("114037", "2.16.756.5.30.1.127.3.3.1", "HAEMODIALYSEPATIENT",
			"HAEMODIALYSEPATIENT", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">HAEMOPHILIE_PATIENT</div>
	 * <!-- @formatter:on -->
	 */
	HAEMOPHILIE_PATIENT("114038", "2.16.756.5.30.1.127.3.3.1", "HAEMOPHILIE_PATIENT",
			"HAEMOPHILIE_PATIENT", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">HERZINSUFFIZIENZ</div>
	 * <!-- @formatter:on -->
	 */
	HERZINSUFFIZIENZ("114001", "2.16.756.5.30.1.127.3.3.1", "HERZINSUFFIZIENZ", "HERZINSUFFIZIENZ",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">HERZKRANKHEITEN_CHRONISCHE_KONGENITALE_MISSBILDUNGEN</div>
	 * <!-- @formatter:on -->
	 */
	HERZKRANKHEITEN_CHRONISCHE_KONGENITALE_MISSBILDUNGEN("113007", "2.16.756.5.30.1.127.3.3.1",
			"HERZKRANKHEITEN_CHRONISCHE_KONGENITALE_MISSBILDUNGEN",
			"HERZKRANKHEITEN_CHRONISCHE_KONGENITALE_MISSBILDUNGEN", "TOTRANSLATE", "TOTRANSLATE",
			"TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">HIV_INFEKTION_MIT_CD4_ZELLEN_GROESSER_GLEICH_15_PROZENT_ERWACHSENE_GROESSER_GLEICH_200_PRO_UL</div>
	 * <!-- @formatter:on -->
	 */
	HIV_INFEKTION_MIT_CD4_ZELLEN_GROESSER_GLEICH_15_PROZENT_ERWACHSENE_GROESSER_GLEICH_200_PRO_UL(
			"113008", "2.16.756.5.30.1.127.3.3.1",
			"HIV_INFEKTION_MIT_CD4_ZELLEN_GROESSER_GLEICH_15_PROZENT_ERWACHSENE_GROESSER_GLEICH_200_PRO_UL",
			"HIV_INFEKTION_MIT_CD4_ZELLEN_GROESSER_GLEICH_15_PROZENT_ERWACHSENE_GROESSER_GLEICH_200_PRO_UL",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">HIV_INFEKTION_MIT_CD4_ZELLEN_KLEINER_15_PROZENT_ERWACHSENE_KLEINER_200_PRO_UL</div>
	 * <!-- @formatter:on -->
	 */
	HIV_INFEKTION_MIT_CD4_ZELLEN_KLEINER_15_PROZENT_ERWACHSENE_KLEINER_200_PRO_UL("113009",
			"2.16.756.5.30.1.127.3.3.1",
			"HIV_INFEKTION_MIT_CD4_ZELLEN_KLEINER_15_PROZENT_ERWACHSENE_KLEINER_200_PRO_UL",
			"HIV_INFEKTION_MIT_CD4_ZELLEN_KLEINER_15_PROZENT_ERWACHSENE_KLEINER_200_PRO_UL",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">KANDIDATEN_FUER_EINE_SOLIDORGANTRANSPLANTATION</div>
	 * <!-- @formatter:on -->
	 */
	KANDIDATEN_FUER_EINE_SOLIDORGANTRANSPLANTATION("114018", "2.16.756.5.30.1.127.3.3.1",
			"KANDIDATEN_FUER_EINE_SOLIDORGANTRANSPLANTATION",
			"KANDIDATEN_FUER_EINE_SOLIDORGANTRANSPLANTATION", "TOTRANSLATE", "TOTRANSLATE",
			"TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">KARDIOPATHIE</div>
	 * <!-- @formatter:on -->
	 */
	KARDIOPATHIE("114002", "2.16.756.5.30.1.127.3.3.1", "KARDIOPATHIE", "KARDIOPATHIE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">LEBERZIRRHOSE</div>
	 * <!-- @formatter:on -->
	 */
	LEBERZIRRHOSE("114008", "2.16.756.5.30.1.127.3.3.1", "LEBERZIRRHOSE", "LEBERZIRRHOSE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">LYMPHOM_LEUKAEMIE_MYELOM</div>
	 * <!-- @formatter:on -->
	 */
	LYMPHOM_LEUKAEMIE_MYELOM("113012", "2.16.756.5.30.1.127.3.3.1", "LYMPHOM_LEUKAEMIE_MYELOM",
			"LYMPHOM_LEUKAEMIE_MYELOM", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">LYMPHOM_LEUKAEMIE_MYELOM_WAEHREND_KLINISCHER_REMISSION</div>
	 * <!-- @formatter:on -->
	 */
	LYMPHOM_LEUKAEMIE_MYELOM_WAEHREND_KLINISCHER_REMISSION("118001", "2.16.756.5.30.1.127.3.3.1",
			"LYMPHOM_LEUKAEMIE_MYELOM_WAEHREND_KLINISCHER_REMISSION",
			"LYMPHOM_LEUKAEMIE_MYELOM_WAEHREND_KLINISCHER_REMISSION", "TOTRANSLATE", "TOTRANSLATE",
			"TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">MANGEL_AN_MANNOSE_BINDENDEM_LEKTIN</div>
	 * <!-- @formatter:on -->
	 */
	MANGEL_AN_MANNOSE_BINDENDEM_LEKTIN("113014", "2.16.756.5.30.1.127.3.3.1",
			"MANGEL_AN_MANNOSE_BINDENDEM_LEKTIN", "MANGEL_AN_MANNOSE_BINDENDEM_LEKTIN",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">MANGEL_IM_KLASSISCHEN_ODER_ALTERNATIVEN_WEG_DER_KOMPLEMENTAKTIVIERUNG</div>
	 * <!-- @formatter:on -->
	 */
	MANGEL_IM_KLASSISCHEN_ODER_ALTERNATIVEN_WEG_DER_KOMPLEMENTAKTIVIERUNG("114026",
			"2.16.756.5.30.1.127.3.3.1",
			"MANGEL_IM_KLASSISCHEN_ODER_ALTERNATIVEN_WEG_DER_KOMPLEMENTAKTIVIERUNG",
			"MANGEL_IM_KLASSISCHEN_ODER_ALTERNATIVEN_WEG_DER_KOMPLEMENTAKTIVIERUNG", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">MEDIKAMENTOESE_IMMUNOSUPPRESSION_INKL_SYSTEMISCHE_LANGZEITKORTIKOIDTHERAPIE_UND_RADIOTHERAPIE</div>
	 * <!-- @formatter:on -->
	 */
	MEDIKAMENTOESE_IMMUNOSUPPRESSION_INKL_SYSTEMISCHE_LANGZEITKORTIKOIDTHERAPIE_UND_RADIOTHERAPIE(
			"114022", "2.16.756.5.30.1.127.3.3.1",
			"MEDIKAMENTOESE_IMMUNOSUPPRESSION_INKL_SYSTEMISCHE_LANGZEITKORTIKOIDTHERAPIE_UND_RADIOTHERAPIE",
			"MEDIKAMENTOESE_IMMUNOSUPPRESSION_INKL_SYSTEMISCHE_LANGZEITKORTIKOIDTHERAPIE_UND_RADIOTHERAPIE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">MORBIDE_ADIPOSITAS_BMI_GROESSER_40</div>
	 * <!-- @formatter:on -->
	 */
	MORBIDE_ADIPOSITAS_BMI_GROESSER_40("114016", "2.16.756.5.30.1.127.3.3.1",
			"MORBIDE_ADIPOSITAS_BMI_GROESSER_40", "MORBIDE_ADIPOSITAS_BMI_GROESSER_40",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">NEPHROTISCHES_SYNDROM</div>
	 * <!-- @formatter:on -->
	 */
	NEPHROTISCHES_SYNDROM("113018", "2.16.756.5.30.1.127.3.3.1", "NEPHROTISCHES_SYNDROM",
			"NEPHROTISCHES_SYNDROM", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">NEUROMUSKULAERE_KRANKHEIT_FALLS_AUSWIRKUNGEN_AUF_HERZ__LUNGEN_ODER_NIERENFUNKTION</div>
	 * <!-- @formatter:on -->
	 */
	NEUROMUSKULAERE_KRANKHEIT_FALLS_AUSWIRKUNGEN_AUF_HERZ_LUNGEN_ODER_NIERENFUNKTION("114012",
			"2.16.756.5.30.1.127.3.3.1",
			"NEUROMUSKULAERE_KRANKHEIT_FALLS_AUSWIRKUNGEN_AUF_HERZ__LUNGEN_ODER_NIERENFUNKTION",
			"NEUROMUSKULAERE_KRANKHEIT_FALLS_AUSWIRKUNGEN_AUF_HERZ__LUNGEN_ODER_NIERENFUNKTION",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">NIERENINSUFFIZIENZ</div>
	 * <!-- @formatter:on -->
	 */
	NIERENINSUFFIZIENZ("113023", "2.16.756.5.30.1.127.3.3.1", "NIERENINSUFFIZIENZ",
			"NIERENINSUFFIZIENZ", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">PROTEIN_S_ODER_PROTEIN_C_DEFIZIT</div>
	 * <!-- @formatter:on -->
	 */
	PROTEIN_S_ODER_PROTEIN_C_DEFIZIT("114035", "2.16.756.5.30.1.127.3.3.1",
			"PROTEIN_S_ODER_PROTEIN_C_DEFIZIT", "PROTEIN_S_ODER_PROTEIN_C_DEFIZIT", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">RHEUMATISMUS_ODER_AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_BEDINGT</div>
	 * <div class="de">Rheumatismus oder Autoimmunkrankheit, welche eine Immunsuppression bedingt</div>
	 * <div class="fr">Maladie rhumatismale ou auto-immune allant nécessiter une immunosuppression</div>
	 * <div class="it">Malattia reumatica o autoimmune che presto richiederà l'immunosoppressione</div>
	 * <!-- @formatter:on -->
	 */
	RHEUMATISMUS_ODER_AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_BEDINGT("113035",
			"2.16.756.5.30.1.127.3.3.1",
			"RHEUMATISMUS_ODER_AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_BEDINGT",
			"RHEUMATISMUS_ODER_AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_BEDINGT",
			"Rheumatismus oder Autoimmunkrankheit, welche eine Immunsuppression bedingt",
			"Maladie rhumatismale ou auto-immune allant nécessiter une immunosuppression",
			"Malattia reumatica o autoimmune che presto richiederà l'immunosoppressione"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">SCHAEDELBASISFRAKTUR_MISSBILDUNG_ZEREBROSPINALE_LIQUORFISTEL</div>
	 * <!-- @formatter:on -->
	 */
	SCHAEDELBASISFRAKTUR_MISSBILDUNG_ZEREBROSPINALE_LIQUORFISTEL("114032",
			"2.16.756.5.30.1.127.3.3.1",
			"SCHAEDELBASISFRAKTUR_MISSBILDUNG_ZEREBROSPINALE_LIQUORFISTEL",
			"SCHAEDELBASISFRAKTUR_MISSBILDUNG_ZEREBROSPINALE_LIQUORFISTEL", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">SCHWANGERSCHAFT_UND_POST_PARTUM_PERIODE</div>
	 * <!-- @formatter:on -->
	 */
	SCHWANGERSCHAFT_UND_POST_PARTUM_PERIODE("114028", "2.16.756.5.30.1.127.3.3.1",
			"SCHWANGERSCHAFT_UND_POST_PARTUM_PERIODE", "SCHWANGERSCHAFT_UND_POST_PARTUM_PERIODE",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">SCHWERES_ASTHMA_BEI_VERLAENGERTER_ODER_HAEUFIGER_BEHANDLUNG_MIT_ORALEN_STEROIDEN</div>
	 * <!-- @formatter:on -->
	 */
	SCHWERES_ASTHMA_BEI_VERLAENGERTER_ODER_HAEUFIGER_BEHANDLUNG_MIT_ORALEN_STEROIDEN("114004",
			"2.16.756.5.30.1.127.3.3.1",
			"SCHWERES_ASTHMA_BEI_VERLAENGERTER_ODER_HAEUFIGER_BEHANDLUNG_MIT_ORALEN_STEROIDEN",
			"SCHWERES_ASTHMA_BEI_VERLAENGERTER_ODER_HAEUFIGER_BEHANDLUNG_MIT_ORALEN_STEROIDEN",
			"TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">SCHWERE_NEURODERMITIS_BEIM_KIND</div>
	 * <!-- @formatter:on -->
	 */
	SCHWERE_NEURODERMITIS_BEIM_KIND("113021", "2.16.756.5.30.1.127.3.3.1",
			"SCHWERE_NEURODERMITIS_BEIM_KIND", "SCHWERE_NEURODERMITIS_BEIM_KIND", "TOTRANSLATE",
			"TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">SICHELZELLANAEMIE</div>
	 * <!-- @formatter:on -->
	 */
	SICHELZELLANAEMIE("114014", "2.16.756.5.30.1.127.3.3.1", "SICHELZELLANAEMIE",
			"SICHELZELLANAEMIE", "TOTRANSLATE", "TOTRANSLATE", "TOTRANSLATE"),
	/**
	 * <!-- @formatter:off -->
	 * <div class="en">ZOELIAKIE</div>
	 * <div class="de">Zöliakie</div>
	 * <div class="fr">Coeliaquie</div>
	 * <div class="it">Celiachia</div>
	 * <!-- @formatter:on -->
	 */
	ZOELIAKIE("113036", "2.16.756.5.30.1.127.3.3.1", "ZOELIAKIE", "ZOELIAKIE", "Zöliakie",
			"Coeliaquie", "Celiachia");

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ANATOMISCHE_ODER_FUNKTIONELLE_ASPLENIE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANATOMISCHE_ODER_FUNKTIONELLE_ASPLENIE_CODE = "113001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ANDERER_KREBS</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANDERER_KREBS_CODE = "114034";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ANDERE_LEBERERKRANKUNG</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANDERE_LEBERERKRANKUNG_CODE = "114036";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ANDERE_LUNGENERKRANKUNGEN_ZB_MUKOVISZIDOSE_ASTHMA_BRONCHIALE_ETC</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANDERE_LUNGENERKRANKUNGEN_ZB_MUKOVISZIDOSE_ASTHMA_BRONCHIALE_ETC_CODE = "114006";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ANDERE_NIERENERKRANKUNG</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANDERE_NIERENERKRANKUNG_CODE = "114033";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ANGEBORENE_IMMUNDEFIZIENZ_VARIABLES_IMMUNDEFEKTSYNDROM_DEFIZITAERE_ANTWORT_AUF_POLYSACCHARIDE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ANGEBORENE_IMMUNDEFIZIENZ_VARIABLES_IMMUNDEFEKTSYNDROM_DEFIZITAERE_ANTWORT_AUF_POLYSACCHARIDE_CODE = "114025";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_ERFORDERT</div>
	 * <!-- @formatter:on -->
	 */
	public static final String AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_ERFORDERT_CODE = "114021";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for BRONCHIEKTASEN_DURCH_ANTIKOERPERMANGEL</div>
	 * <!-- @formatter:on -->
	 */
	public static final String BRONCHIEKTASEN_DURCH_ANTIKOERPERMANGEL_CODE = "114005";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for CHRONISCHE_LEBERERKRANKUNG</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHRONISCHE_LEBERERKRANKUNG_CODE = "114007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for CHRONISCH_OBSTRUKTIVE_PNEUMOPATHIE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String CHRONISCH_OBSTRUKTIVE_PNEUMOPATHIE_CODE = "114003";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for COCHLEAIMPLANTAT_IN_SITU_ODER_GEPLANT</div>
	 * <!-- @formatter:on -->
	 */
	public static final String COCHLEAIMPLANTAT_IN_SITU_ODER_GEPLANT_CODE = "113002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for DIABETES_MIT_AUSWIRKUNG_AUF_DIE_FUNKTION_VON_HERZ_LUNGEN_ODER_NIEREN</div>
	 * <!-- @formatter:on -->
	 */
	public static final String DIABETES_MIT_AUSWIRKUNG_AUF_DIE_FUNKTION_VON_HERZ_LUNGEN_ODER_NIEREN_CODE = "114015";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for EMPFAENGER_EINER_SOLIDORGANTRANSPLANTATION</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EMPFAENGER_EINER_SOLIDORGANTRANSPLANTATION_CODE = "114019";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for EMPFAENGER_EINER_STAMMZELLTRANSPLANTATION</div>
	 * <!-- @formatter:on -->
	 */
	public static final String EMPFAENGER_EINER_STAMMZELLTRANSPLANTATION_CODE = "114020";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for GEBURT_VOR_DER_33_SCHWANGERSCHAFTSWOCHE_ODER_GEBURTSGEWICHT_GROESSER_1500G</div>
	 * <!-- @formatter:on -->
	 */
	public static final String GEBURT_VOR_DER_33_SCHWANGERSCHAFTSWOCHE_ODER_GEBURTSGEWICHT_GROESSER_1500G_CODE = "113004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for HAEMODIALYSEPATIENT</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HAEMODIALYSEPATIENT_CODE = "114037";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for HAEMOPHILIE_PATIENT</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HAEMOPHILIE_PATIENT_CODE = "114038";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for HERZINSUFFIZIENZ</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HERZINSUFFIZIENZ_CODE = "114001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for HERZKRANKHEITEN_CHRONISCHE_KONGENITALE_MISSBILDUNGEN</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HERZKRANKHEITEN_CHRONISCHE_KONGENITALE_MISSBILDUNGEN_CODE = "113007";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for HIV_INFEKTION_MIT_CD4_ZELLEN_GROESSER_GLEICH_15_PROZENT_ERWACHSENE_GROESSER_GLEICH_200_PRO_UL</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HIV_INFEKTION_MIT_CD4_ZELLEN_GROESSER_GLEICH_15_PROZENT_ERWACHSENE_GROESSER_GLEICH_200_PRO_UL_CODE = "113008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for HIV_INFEKTION_MIT_CD4_ZELLEN_KLEINER_15_PROZENT_ERWACHSENE_KLEINER_200_PRO_UL</div>
	 * <!-- @formatter:on -->
	 */
	public static final String HIV_INFEKTION_MIT_CD4_ZELLEN_KLEINER_15_PROZENT_ERWACHSENE_KLEINER_200_PRO_UL_CODE = "113009";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for KANDIDATEN_FUER_EINE_SOLIDORGANTRANSPLANTATION</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KANDIDATEN_FUER_EINE_SOLIDORGANTRANSPLANTATION_CODE = "114018";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for KARDIOPATHIE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String KARDIOPATHIE_CODE = "114002";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for LEBERZIRRHOSE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LEBERZIRRHOSE_CODE = "114008";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for LYMPHOM_LEUKAEMIE_MYELOM</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LYMPHOM_LEUKAEMIE_MYELOM_CODE = "113012";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for LYMPHOM_LEUKAEMIE_MYELOM_WAEHREND_KLINISCHER_REMISSION</div>
	 * <!-- @formatter:on -->
	 */
	public static final String LYMPHOM_LEUKAEMIE_MYELOM_WAEHREND_KLINISCHER_REMISSION_CODE = "118001";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for MANGEL_AN_MANNOSE_BINDENDEM_LEKTIN</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MANGEL_AN_MANNOSE_BINDENDEM_LEKTIN_CODE = "113014";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for MANGEL_IM_KLASSISCHEN_ODER_ALTERNATIVEN_WEG_DER_KOMPLEMENTAKTIVIERUNG</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MANGEL_IM_KLASSISCHEN_ODER_ALTERNATIVEN_WEG_DER_KOMPLEMENTAKTIVIERUNG_CODE = "114026";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for MEDIKAMENTOESE_IMMUNOSUPPRESSION_INKL_SYSTEMISCHE_LANGZEITKORTIKOIDTHERAPIE_UND_RADIOTHERAPIE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MEDIKAMENTOESE_IMMUNOSUPPRESSION_INKL_SYSTEMISCHE_LANGZEITKORTIKOIDTHERAPIE_UND_RADIOTHERAPIE_CODE = "114022";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for MORBIDE_ADIPOSITAS_BMI_GROESSER_40</div>
	 * <!-- @formatter:on -->
	 */
	public static final String MORBIDE_ADIPOSITAS_BMI_GROESSER_40_CODE = "114016";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for NEPHROTISCHES_SYNDROM</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NEPHROTISCHES_SYNDROM_CODE = "113018";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for NEUROMUSKULAERE_KRANKHEIT_FALLS_AUSWIRKUNGEN_AUF_HERZ__LUNGEN_ODER_NIERENFUNKTION</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NEUROMUSKULAERE_KRANKHEIT_FALLS_AUSWIRKUNGEN_AUF_HERZ_LUNGEN_ODER_NIERENFUNKTION_CODE = "114012";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for NIERENINSUFFIZIENZ</div>
	 * <!-- @formatter:on -->
	 */
	public static final String NIERENINSUFFIZIENZ_CODE = "113023";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for PROTEIN_S_ODER_PROTEIN_C_DEFIZIT</div>
	 * <!-- @formatter:on -->
	 */
	public static final String PROTEIN_S_ODER_PROTEIN_C_DEFIZIT_CODE = "114035";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for RHEUMATISMUS_ODER_AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_BEDINGT</div>
	 * <div class="de">Code für Rheumatismus oder Autoimmunkrankheit, welche eine Immunsuppression bedingt</div>
	 * <div class="fr">Code de Maladie rhumatismale ou auto-immune allant nécessiter une immunosuppression</div>
	 * <div class="it">Code per Malattia reumatica o autoimmune che presto richiederà l'immunosoppressione</div>
	 * <!-- @formatter:on -->
	 */
	public static final String RHEUMATISMUS_ODER_AUTOIMMUNKRANKHEIT_WELCHE_EINE_IMMUNSUPPRESSION_BEDINGT_CODE = "113035";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for SCHAEDELBASISFRAKTUR_MISSBILDUNG_ZEREBROSPINALE_LIQUORFISTEL</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SCHAEDELBASISFRAKTUR_MISSBILDUNG_ZEREBROSPINALE_LIQUORFISTEL_CODE = "114032";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for SCHWANGERSCHAFT_UND_POST_PARTUM_PERIODE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SCHWANGERSCHAFT_UND_POST_PARTUM_PERIODE_CODE = "114028";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for SCHWERES_ASTHMA_BEI_VERLAENGERTER_ODER_HAEUFIGER_BEHANDLUNG_MIT_ORALEN_STEROIDEN</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SCHWERES_ASTHMA_BEI_VERLAENGERTER_ODER_HAEUFIGER_BEHANDLUNG_MIT_ORALEN_STEROIDEN_CODE = "114004";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for SCHWERE_NEURODERMITIS_BEIM_KIND</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SCHWERE_NEURODERMITIS_BEIM_KIND_CODE = "113021";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for SICHELZELLANAEMIE</div>
	 * <!-- @formatter:on -->
	 */
	public static final String SICHELZELLANAEMIE_CODE = "114014";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Code for ZOELIAKIE</div>
	 * <div class="de">Code für Zöliakie</div>
	 * <div class="fr">Code de Coeliaquie</div>
	 * <div class="it">Code per Celiachia</div>
	 * <!-- @formatter:on -->
	 */
	public static final String ZOELIAKIE_CODE = "113036";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Identifier of the value set</div>
	 * <div class="de">Identifikator für das Value Set</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALUE_SET_ID = "2.16.756.5.30.1.1.11.72";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Name of the value set</div>
	 * <div class="de">Name des Value Sets</div>
	 * <!-- @formatter:on -->
	 */
	public static final String VALUE_SET_NAME = "SwissVaccinationPlanComplicationRisks";

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the Enum with a given code</div>
	 * <div class="de">Liefert den Enum anhand eines gegebenen codes</div>
	 * <!-- @formatter:on -->
	 *
	 * @param code
	 *            <div class="de"> code</div>
	 * @return <div class="en">the enum</div>
	 */
	public static SwissVaccinationPlanComplicationRisks getEnum(String code) {
		for (final SwissVaccinationPlanComplicationRisks x : values()) {
			if (x.getCodeValue().equals(code)) {
				return x;
			}
		}
		return null;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Checks if a given enum is part of this value set.</div>
	 * <div class="de">Prüft, ob der angegebene enum Teil dieses Value Sets ist.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param enumName
	 *            <div class="de"> enumName</div>
	 * @return true, if enum is in this value set
	 */
	public static boolean isEnumOfValueSet(String enumName) {
		if (enumName == null) {
			return false;
		}
		try {
			Enum.valueOf(SwissVaccinationPlanComplicationRisks.class, enumName);
			return true;
		} catch (final IllegalArgumentException ex) {
			return false;
		}
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Checks if a given code value is in this value set.</div>
	 * <div class="de">Prüft, ob der angegebene code in diesem Value Set vorhanden ist.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param codeValue
	 *            <div class="de"> code</div>
	 * @return true, if is in value set
	 */
	public static boolean isInValueSet(String codeValue) {
		for (final SwissVaccinationPlanComplicationRisks x : values()) {
			if (x.getCodeValue().equals(codeValue)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Machine interpretable and (inside this class) unique code</div>
	 * <div class="de">Maschinen interpretierbarer und (innerhalb dieser Klasse) eindeutiger Code</div>
	 * <!-- @formatter:on -->
	 */
	private String code;

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Identifier of the referencing code system.</div>
	 * <div class="de">Identifikator des referenzierende Codesystems.</div>
	 * <!-- @formatter:on -->
	 */
	private String codeSystem;

	/**
	 * The display names per language
	 */
	private Map<LanguageCode, String> displayNames;

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Instantiates this Enum Object with a given Code and Display Name</div>
	 * <div class="de">Instanziiert dieses Enum Object mittels eines Codes und einem Display Name</div>.
	 * <!-- @formatter:on -->
	 *
	 * @param code
	 *            code
	 * @param codeSystem
	 *            codeSystem
	 * @param displayName
	 *            the default display name
	 * @param displayNameEn
	 *            the display name en
	 * @param displayNameDe
	 *            the display name de
	 * @param displayNameFr
	 *            the display name fr
	 * @param displayNameIt
	 *            the display name it
	 */
	SwissVaccinationPlanComplicationRisks(String code, String codeSystem, String displayName,
			String displayNameEn, String displayNameDe, String displayNameFr,
			String displayNameIt) {
		this.code = code;
		this.codeSystem = codeSystem;
		displayNames = new HashMap<>();
		displayNames.put(null, displayName);
		displayNames.put(LanguageCode.ENGLISH, displayNameEn);
		displayNames.put(LanguageCode.GERMAN, displayNameDe);
		displayNames.put(LanguageCode.FRENCH, displayNameFr);
		displayNames.put(LanguageCode.ITALIAN, displayNameIt);
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the code system identifier.</div>
	 * <div class="de">Liefert den Code System Identifikator.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code system identifier</div>
	 */
	@Override
	public String getCodeSystemId() {
		return this.codeSystem;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the code system name.</div>
	 * <div class="de">Liefert den Code System Namen.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code system identifier</div>
	 */
	@Override
	public String getCodeSystemName() {
		String retVal = "";
		CodeSystems cs = CodeSystems.getEnum(this.codeSystem);
		if (cs != null)
			retVal = cs.getCodeSystemName();
		return retVal;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the actual Code as string</div>
	 * <div class="de">Liefert den eigentlichen Code als String</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the code</div>
	 */
	@Override
	public String getCodeValue() {
		return this.code;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the display name defined by the language param. If
	 * there is no english translation, the default display name is returned.</div>
	 * <div class="de">Liefert display name gemäss Parameter, falls es keine
	 * Englische Übersetzung gibt, wird der default-Name zurückgegeben.</div>
	 * <!-- @formatter:on -->
	 *
	 * @param languageCode
	 *            the language code to get the display name for
	 * @return returns the display name in the desired language. if language not
	 *         found, display name in german will returned
	 */
	@Override
	public String getDisplayName(LanguageCode languageCode) {
		String displayName = displayNames.get(languageCode);
		if (displayName == null && languageCode == LanguageCode.ENGLISH) {
			return displayNames.get(null);
		}
		return displayName;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the value set identifier.</div>
	 * <div class="de">Liefert den Value Set Identifikator.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the value set identifier</div>
	 */
	@Override
	public String getValueSetId() {
		return VALUE_SET_ID;
	}

	/**
	 * <!-- @formatter:off -->
	 * <div class="en">Gets the value set name.</div>
	 * <div class="de">Liefert den Value Set Namen.</div>
	 * <!-- @formatter:on -->
	 *
	 * @return <div class="en">the value set name</div>
	 */
	@Override
	public String getValueSetName() {
		return VALUE_SET_NAME;
	}
}
